//
//  SiteMgr.h
//  TrinityCore
//
//  Created by e on 2/4/13.
//
//

#ifndef __TRINITY_SITEMGR_H
#define __TRINITY_SITEMGR_H

#include "Common.h"
#include "Player.h"
#include <ace/SOCK_Dgram.h>
#include <ace/Singleton.h>
#include "DatabaseEnv.h"
#include "Chat.h"

class SiteMgr {
private:
    ACE_SOCK_Dgram m_ACE_SOCK_Dgram;
    
public:
    SiteMgr();
    
    uint32 GetVotePoints(uint32 accountId) const;
    uint32 GetVotePoints(Player *player) const;
    
    uint32 GetDonationPoints(uint32 accountId);
    uint32 GetDonationPoints(Player *player);
    
    bool GetRewardCurrencies(Player *player, uint32 &votePoints, uint32 &donationPoints);
    bool GetRewardCurrencies(uint32 accountId, uint32 &votePoints, uint32 &donationPoints);

    bool CanAffordReward(Player *player, uint32 votePoints, uint32 donationPoints);
    bool CanAffordReward(uint32 accountId, uint32 votePoints, uint32 donationPoints);
    
    bool ModifyVotePoints(Player *player, int32 votePoints);
    bool ModifyVotePoints(uint32 accountId, int32 votePoints);
    
    bool ModifyDonationPoints(Player *player, int32 donationPoints);
    bool ModifyDonationPoints(uint32 accountId, int32 donationPoints);
    
    bool PerformTransaction(Player *player, uint32 votePoints, uint32 donationPoints);
    bool PerformTransaction(uint32 accountId, uint32 votePoints, uint32 donationPoints);
    
    void LogToIRC(std::string const& message);
    void LogToIRC(const char* str, ...);

    void LogGMCommand(const ChatCommand *table, WorldSession *session, const char* fullcmd);
    
private:
    
    void resolveFullCommandString(const ChatCommand *table, WorldSession *session,
                                  const char* cmd_in, std::string &expanded_cmd,
                                  std::string &args);
    
};

#define sSiteMgr ACE_Singleton<SiteMgr, ACE_Null_Mutex>::instance()

#endif