//
//  SiteMgr.cpp
//  TrinityCore
//
//  Created by e on 2/4/13.
//
//

#include "SiteMgr.h"

SiteMgr::SiteMgr() {
    ACE_INET_Addr local((u_short) 0);
    
    if(!m_ACE_SOCK_Dgram.open(local) == -1)
        ASSERT(false); // crash if we can't open a socket!
}

uint32 SiteMgr::GetVotePoints(uint32 accountId) const
{
    PreparedStatement* stmt = WebDatabase.GetPreparedStatement(WEB_GET_VOTE_POINTS);
    stmt->setUInt32(0, accountId);
    
    PreparedQueryResult result = WebDatabase.Query(stmt);
    
    if(!result)
        return 0;
    
    return result->Fetch()->GetUInt32();
}

uint32 SiteMgr::GetVotePoints(Player *player) const
{
    uint32 accountId = player->GetSession()->GetAccountId();
    return GetVotePoints(accountId);
}

uint32 SiteMgr::GetDonationPoints(uint32 accountId)
{
    PreparedStatement *stmt = WebDatabase.GetPreparedStatement(WEB_GET_DONATION_POINTS);
    stmt->setUInt32(0, accountId);
    
    PreparedQueryResult result = WebDatabase.Query(stmt);
    
    if(!result)
        return 0;
    
    return result->Fetch()->GetUInt32();
}

uint32 SiteMgr::GetDonationPoints(Player *player)
{
    uint32 accountId = player->GetSession()->GetAccountId();
    return GetDonationPoints(accountId);
}

bool SiteMgr::GetRewardCurrencies(uint32 accountId, uint32 &votePoints, uint32 &donationPoints)
{
    PreparedStatement *stmt = WebDatabase.GetPreparedStatement(WEB_GET_BOTH_POINTS);
    stmt->setUInt32(0, accountId);
    
    PreparedQueryResult result = WebDatabase.Query(stmt);
    
    if(!result)
        return false;
    
    Field *field = result->Fetch();
    votePoints = field[0].GetUInt32();
    donationPoints = field[1].GetUInt32();
    
    return true;
    
}

bool SiteMgr::GetRewardCurrencies(Player *player, uint32 &votePoints, uint32 &donationPoints)
{
    uint32 accountId = player->GetSession()->GetAccountId();
    return GetRewardCurrencies(accountId, votePoints, donationPoints);
}

bool SiteMgr::CanAffordReward(uint32 accountId, uint32 votePoints, uint32 donationPoints)
{
    uint32 myVotePoints, myDonationPoints;
    
    if(!GetRewardCurrencies(accountId, myVotePoints, myDonationPoints))
        return false;
    
    return myVotePoints >= votePoints && myDonationPoints >= donationPoints;
    
}

bool SiteMgr::CanAffordReward(Player *player, uint32 votePoints, uint32 donationPoints)
{
    uint32 accountId = player->GetSession()->GetAccountId();
    return CanAffordReward(accountId, votePoints, donationPoints);
}

bool SiteMgr::ModifyVotePoints(uint32 accountId, int32 votePoints)
{
    PreparedStatement *stmt = WebDatabase.GetPreparedStatement(WEB_UPD_VOTE_POINTS);
    stmt->setInt32(0, votePoints);
    stmt->setUInt32(1, accountId);
    stmt->setInt32(2, votePoints);
    
    uint64 affectedRows;
    if(!WebDatabase.DirectExecute(stmt, affectedRows))
        return false;
    
    return affectedRows >= 1;
}

bool SiteMgr::ModifyVotePoints(Player *player, int32 votePoints)
{
    uint32 accountId = player->GetSession()->GetAccountId();
    return ModifyVotePoints(accountId, votePoints);
}

bool SiteMgr::ModifyDonationPoints(uint32 accountId, int32 donationPoints)
{
    PreparedStatement *stmt = WebDatabase.GetPreparedStatement(WEB_UPD_DONATION_POINTS);
    stmt->setInt32(0, donationPoints);
    stmt->setUInt32(1, accountId);
    stmt->setInt32(2, donationPoints);
    
    uint64 affectedRows;
    if(!WebDatabase.DirectExecute(stmt, affectedRows))
        return false;
    
    return affectedRows >= 1;
}

bool SiteMgr::ModifyDonationPoints(Player *player, int32 donationPoints)
{
    uint32 accountId = player->GetSession()->GetAccountId();
    return ModifyDonationPoints(accountId, donationPoints);
}

bool SiteMgr::PerformTransaction(uint32 accountId, uint32 votePoints, uint32 donationPoints)
{
    PreparedStatement *stmt = WebDatabase.GetPreparedStatement(WEB_UPD_DO_TRANSACTION);
    uint8 i = 0;
    stmt->setUInt32(i++, votePoints);
    stmt->setUInt32(i++, donationPoints);
    stmt->setUInt32(i++, accountId);
    stmt->setUInt32(i++, votePoints);
    stmt->setUInt32(i++, donationPoints);
    
    uint64 affectedRows;
    if(!WebDatabase.DirectExecute(stmt, affectedRows))
        return false;
    
    return affectedRows >= 1;
}

bool SiteMgr::PerformTransaction(Player *player, uint32 votePoints, uint32 donationPoints)
{
    uint32 accountId = player->GetSession()->GetAccountId();
    return PerformTransaction(accountId, votePoints, donationPoints);
}

void SiteMgr::LogToIRC(std::string const& message) {
    if(!sWorld->getBoolConfig(CONFIG_UDP_LOG_ENABLED))
        return;
    
    ACE_INET_Addr remote (sWorld->getIntConfig(CONFIG_UDP_LOG_PORT), sWorld->GetUdpLogAddr().c_str());
    
    m_ACE_SOCK_Dgram.send(message.c_str(), message.size() + 1, remote);
}

void SiteMgr::LogToIRC(const char *str, ...) {
    char message[MAX_QUERY_LEN];
    
    va_list ap;
    va_start(ap, str);
    vsnprintf(message, MAX_QUERY_LEN, str, ap);
    va_end(ap);
    
    LogToIRC((std::string) message);
}

void SiteMgr::LogGMCommand(const ChatCommand *table, WorldSession *session, const char* fullcmd) {
    std::string expanded_command, arguments;
    // strip off beginning . or !
    if(*fullcmd == '!' || *fullcmd == '.') ++fullcmd;
    resolveFullCommandString(table, session, fullcmd, expanded_command, arguments);
    
    PreparedStatement *stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_GM_COMMAND_LOG);
    Player *gm = session->GetPlayer();
    uint64 sel_guid = gm->GetSelection();
    Unit* sel = gm->GetSelectedUnit();
    
    uint8 i = 0;
    stmt->setUInt32(i++, realmID);
    stmt->setString(i++, expanded_command);
    stmt->setString(i++, arguments);
    stmt->setUInt32(i++, session->GetAccountId());
    stmt->setUInt32(i++, gm->GetGUIDLow());
    stmt->setFloat(i++, gm->GetPositionX());
    stmt->setFloat(i++, gm->GetPositionY());
    stmt->setFloat(i++, gm->GetPositionZ());
    stmt->setUInt32(i++, gm->GetMapId());
    if(sel)
    {
        stmt->setString(i++, GetLogNameForGuid(sel_guid));
        stmt->setFloat(i++, sel->GetPositionX());
        stmt->setFloat(i++, sel->GetPositionY());
        stmt->setFloat(i++, sel->GetPositionZ()); // map is same as gm's map.
        stmt->setString(i++, sel->GetName());
        stmt->setUInt64(i++, sel_guid);
        stmt->setInt32(i++, GUID_LOPART(sel_guid));
        if(Player *p = sel->ToPlayer())
            stmt->setInt32(i++, p->GetSession()->GetAccountId());
        else
            stmt->setNull(i++);
        
    } else
    {
        for(uint8 j = 0; j < 8; j++)
            stmt->setNull(i++);
    }
    
    LoginDatabase.Execute(stmt);
}

bool hasStringAbbr(const char* name, const char* part)
{
    // non "" command
    if (*name)
    {
        // "" part from non-"" command
        if (!*part)
            return false;
        
        for (;;)
        {
            if (!*part)
                return true;
            else if (!*name)
                return false;
            else if (tolower(*name) != tolower(*part))
                return false;
            ++name; ++part;
        }
    }
    // allow with any for ""
    
    return true;
}


void SiteMgr::resolveFullCommandString(const ChatCommand *table, WorldSession *session,
                                       const char* cmd_in, std::string &expanded_cmd,
                                       std::string &args)
{
    char const* oldtext = cmd_in;
    std::string cmd = "";
    
    while (*cmd_in != ' ' && *cmd_in != '\0')
    {
        cmd += *cmd_in;
        ++cmd_in;
    }
    
    while (*cmd_in == ' ') ++cmd_in;
    
    for (uint32 i = 0; table[i].Name != NULL; ++i) {
        if (!hasStringAbbr(table[i].Name, cmd.c_str()))
            continue;
        
        bool match = false;
        if (strlen(table[i].Name) > cmd.length())
        {
            for (uint32 j = 0; table[j].Name != NULL; ++j)
            {
                if (!hasStringAbbr(table[j].Name, cmd.c_str()))
                    continue;
                
                if (strcmp(table[j].Name, cmd.c_str()) != 0)
                    continue;
                else
                {
                    match = true;
                    break;
                }
            }
        }
        if (match)
            continue;
        
        
        if (table[i].ChildCommands != NULL)
        {
            expanded_cmd += table[i].Name;
            expanded_cmd += " ";
            return resolveFullCommandString(table[i].ChildCommands, session, cmd_in, expanded_cmd, args);
        }

        if (!table[i].Handler || session->GetSecurity() < AccountTypes(table[i].SecurityLevel))
            continue;
        
        expanded_cmd += table[i].Name;
        args = table[i].Name[0] == '\0' ? oldtext : cmd_in;
        
        return;
    }
    
}


