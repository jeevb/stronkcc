/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlegroundRL.h"
#include "Language.h"
#include "Object.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "WorldPacket.h"
#include "Battleground.h"

BattlegroundRL::BattlegroundRL()
{
    BgObjects.resize(BG_RL_OBJECT_MAX);
    m_readyTeams = 0;
    StartDelayTimes[BG_STARTING_EVENT_FIRST]  = BG_START_DELAY_1M;
    StartDelayTimes[BG_STARTING_EVENT_SECOND] = BG_START_DELAY_30S;
    StartDelayTimes[BG_STARTING_EVENT_THIRD]  = BG_START_DELAY_15S;
    StartDelayTimes[BG_STARTING_EVENT_FOURTH] = BG_START_DELAY_NONE;
    //we must set messageIds
    StartMessageIds[BG_STARTING_EVENT_FIRST]  = LANG_ARENA_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_ARENA_THIRTY_SECONDS;
    StartMessageIds[BG_STARTING_EVENT_THIRD]  = LANG_ARENA_FIFTEEN_SECONDS;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_ARENA_HAS_BEGUN;
}

BattlegroundRL::~BattlegroundRL()
{

}

void BattlegroundRL::StartingEventCloseDoors()
{
    for (uint32 i = BG_RL_OBJECT_DOOR_1; i <= BG_RL_OBJECT_DOOR_2; ++i)
        SpawnBGObject(i, RESPAWN_IMMEDIATELY);
}

void BattlegroundRL::StartingEventOpenDoors()
{
    for (uint32 i = BG_RL_OBJECT_DOOR_1; i <= BG_RL_OBJECT_DOOR_2; ++i)
        DoorOpen(i);

    for (uint32 i = BG_RL_OBJECT_BUFF_1; i <= BG_RL_OBJECT_BUFF_2; ++i)
        SpawnBGObject(i, 60);
    
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            player->SetPhaseMask(1, true);
    
    DelObject(BG_RL_OBJECT_MAGE_TABLE_A);
    DelObject(BG_RL_OBJECT_MAGE_TABLE_H);
    DelObject(BG_RL_OBJECT_SOUL_WELL_A);
    DelObject(BG_RL_OBJECT_SOUL_WELL_H);
}

void BattlegroundRL::AddPlayer(Player* player)
{
    Battleground::AddPlayer(player);
    
    if (player->isSpectator())
        return;
    
    player->SetPhaseMask(3, true);
    
    switch (player->getClass()) {
        case CLASS_MAGE:
        {
            uint32 mageTableType = player->GetBGTeam() == ALLIANCE ? BG_RL_OBJECT_MAGE_TABLE_A : BG_RL_OBJECT_MAGE_TABLE_H;
            GameObject* mageTable = GetBgMap()->GetGameObject(BgObjects[mageTableType]);
            if (mageTable && mageTable->GetGOInfo()->entry == BG_RL_OBJECT_TYPE_MAGE_TABLE_R2)
                break;
            if (player->HasSpell(58659))    // Ritual of Refreshment (Rank 2)
            {
                if (mageTable)
                    DelObject(mageTableType);
                if (mageTableType == BG_RL_OBJECT_MAGE_TABLE_A) // Alliance spawn
                    mageTable = player->SummonGameObject(BG_RL_OBJECT_TYPE_MAGE_TABLE_R2, 1281.261841f, 1745.230103f, 31.605143f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (mageTableType == BG_RL_OBJECT_MAGE_TABLE_H) // Horde spawn
                    mageTable = player->SummonGameObject(BG_RL_OBJECT_TYPE_MAGE_TABLE_R2, 1291.316162f, 1585.938477f, 31.615685f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[mageTableType] = mageTable->GetGUID();
                break;
            }
            if (mageTable && mageTable->GetGOInfo()->entry == BG_RL_OBJECT_TYPE_MAGE_TABLE_R1)
                break;
            if (player->HasSpell(43987))    // Ritual of Refreshment (Rank 1)
            {
                if (mageTable) // Just in case...
                    DelObject(mageTableType);
                if (mageTableType == BG_RL_OBJECT_MAGE_TABLE_A) // Alliance spawn
                    mageTable = player->SummonGameObject(BG_RL_OBJECT_TYPE_MAGE_TABLE_R1, 1281.261841f, 1745.230103f, 31.605143f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (mageTableType == BG_RL_OBJECT_MAGE_TABLE_H) // Horde spawn
                    mageTable = player->SummonGameObject(BG_RL_OBJECT_TYPE_MAGE_TABLE_R1, 1291.316162f, 1585.938477f, 31.615685f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[mageTableType] = mageTable->GetGUID();
                break;
            }
            break;
        }
        case CLASS_WARLOCK:
        {
            uint32 soulWellType = player->GetBGTeam() == ALLIANCE ? BG_RL_OBJECT_SOUL_WELL_A : BG_RL_OBJECT_SOUL_WELL_H;
            GameObject* soulWell = GetBgMap()->GetGameObject(BgObjects[soulWellType]);
            if (soulWell && soulWell->GetGOInfo()->entry == BG_RL_OBJECT_TYPE_SOUL_WELL_R2)
                break;
            if (player->HasSpell(58887))    // Ritual of Souls (Rank 2)
            {
                if (soulWell)
                    DelObject(soulWellType);
                if (soulWellType == BG_RL_OBJECT_SOUL_WELL_A) // Alliance spawn
                    soulWell = player->SummonGameObject(BG_RL_OBJECT_TYPE_SOUL_WELL_R2, 1273.575317f, 1744.501831f, 31.605143f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (soulWellType == BG_RL_OBJECT_SOUL_WELL_H) // Horde spawn
                    soulWell = player->SummonGameObject(BG_RL_OBJECT_TYPE_SOUL_WELL_R2, 1298.720337f, 1587.021851f, 31.615685f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[soulWellType] = soulWell->GetGUID();
                break;
            }
            if (soulWell && soulWell->GetGOInfo()->entry == BG_RL_OBJECT_TYPE_SOUL_WELL_R1)
                break;
            if (player->HasSpell(29893))    // Ritual of Souls (Rank 1)
            {
                if (soulWell) // Just in case...
                    DelObject(soulWellType);
                if (soulWellType == BG_RL_OBJECT_SOUL_WELL_A) // Alliance spawn
                    soulWell = player->SummonGameObject(BG_RL_OBJECT_TYPE_SOUL_WELL_R1, 1273.575317f, 1744.501831f, 31.605143f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (soulWellType == BG_RL_OBJECT_SOUL_WELL_H) // Horde spawn
                    soulWell = player->SummonGameObject(BG_RL_OBJECT_TYPE_SOUL_WELL_R1, 1298.720337f, 1587.021851f, 31.615685f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[soulWellType] = soulWell->GetGUID();
                break;
            }
            break;
        }
        default:
            break;
    }
    
    PlayerScores[player->GetGUID()] = new BattlegroundScore;
    UpdateArenaWorldState();
}

void BattlegroundRL::RemovePlayer(Player* player, uint64 guid, uint32 /*team*/)
{
    for (uint8 i = 0; i < BG_RL_OBJECT_MAX; ++i)
        if (BattlegroundMap* bgMap = FindBgMap())
            if (GameObject* BgObject = bgMap->GetGameObject(BgObjects[i]))
                if (uint64 ownerGUID = BgObject->GetOwnerGUID())
                    if (ownerGUID == guid)
                        DelObject(i);
    
    if (player) {
        player->SetPhaseMask(1, true);
        player->SetArenaReadyState(); // Reset player's arena ready state upon leaving arena
    }
    
    if (GetStatus() == STATUS_WAIT_LEAVE)
        return;

    UpdateArenaWorldState();
    CheckArenaWinConditions();
}

void BattlegroundRL::HandleKillPlayer(Player* player, Player* killer)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    if (!killer)
    {
        TC_LOG_ERROR(LOG_FILTER_BATTLEGROUND, "Killer player not found");
        return;
    }

    Battleground::HandleKillPlayer(player, killer);

    UpdateArenaWorldState();
    CheckArenaWinConditions();
}

bool BattlegroundRL::HandlePlayerUnderMap(Player* player)
{
    player->TeleportTo(GetMapId(), 1285.810547f, 1667.896851f, 39.957642f, player->GetOrientation());
    return true;
}

void BattlegroundRL::HandleAreaTrigger(Player* player, uint32 trigger)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    switch (trigger)
    {
        case 4696:                                          // buff trigger?
        case 4697:                                          // buff trigger?
            break;
        default:
            Battleground::HandleAreaTrigger(player, trigger);
            break;
    }
}

void BattlegroundRL::FillInitialWorldStates(WorldPacket &data)
{
    data << uint32(0xbba) << uint32(1);           // 9
    UpdateArenaWorldState();
}

void BattlegroundRL::Reset()
{
    //call parent's reset
    Battleground::Reset();
}

bool BattlegroundRL::SetupBattleground()
{
        // gates
    if (!AddObject(BG_RL_OBJECT_DOOR_1, BG_RL_OBJECT_TYPE_DOOR_1, 1293.561f, 1601.938f, 31.60557f, -1.457349f, 0, 0, -0.6658813f, 0.7460576f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_RL_OBJECT_DOOR_2, BG_RL_OBJECT_TYPE_DOOR_2, 1278.648f, 1730.557f, 31.60557f, 1.684245f, 0, 0, 0.7460582f, 0.6658807f, RESPAWN_IMMEDIATELY)
        // buffs
        || !AddObject(BG_RL_OBJECT_BUFF_1, BG_RL_OBJECT_TYPE_BUFF_1, 1328.719971f, 1632.719971f, 36.730400f, -1.448624f, 0, 0, 0.6626201f, -0.7489557f, 120)
        || !AddObject(BG_RL_OBJECT_BUFF_2, BG_RL_OBJECT_TYPE_BUFF_2, 1243.300049f, 1699.170044f, 34.872601f, -0.06981307f, 0, 0, 0.03489945f, -0.9993908f, 120)
        // early start triggers
        || !AddObject(BG_RL_OBJECT_READY_A, BG_RL_OBJECT_TYPE_READY, 1275.343018f, 1735.316040f, 31.603460f, 1.429425f, 0, 0, 0, 0, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_RL_OBJECT_READY_H, BG_RL_OBJECT_TYPE_READY, 1296.893433f, 1596.386108f, 31.614275f, 4.536458f, 0, 0, 0, 0, RESPAWN_IMMEDIATELY))
    {
        TC_LOG_ERROR(LOG_FILTER_SQL, "BatteGroundRL: Failed to spawn some object!");
        return false;
    }
    
    if (GameObject* allianceEarlyStartTrigger = GetBgMap()->GetGameObject(BgObjects[BG_RL_OBJECT_READY_A]))
        allianceEarlyStartTrigger->SetPhaseMask(2, true);
    if (GameObject* hordeEarlyStartTrigger = GetBgMap()->GetGameObject(BgObjects[BG_RL_OBJECT_READY_H]))
        hordeEarlyStartTrigger->SetPhaseMask(2, true);

    return true;
}

void BattlegroundRL::CheckEarlyStartTrigger()
{
    if (GetBgMap()->GetGameObject(BgObjects[BG_RL_OBJECT_READY_A]))
    {
        uint8 allianceReadyPlayers = 0;
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            {
                uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                if ((team == ALLIANCE) && (player->GetArenaReadyState() == 1))
                    allianceReadyPlayers++;
            }
        if (allianceReadyPlayers == GetArenaType())
        {
            m_readyTeams++;
            DelObject(BG_RL_OBJECT_READY_A);
            for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
                if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                {
                    uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                    if (team == HORDE)
                        player->SendPvPMessage(5, "Your opponents are ready!");
                }
        }
    }
    
    if (GetBgMap()->GetGameObject(BgObjects[BG_RL_OBJECT_READY_H]))
    {
        uint8 hordeReadyPlayers = 0;
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            {
                uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                if ((team == HORDE) && (player->GetArenaReadyState() == 1))
                    hordeReadyPlayers++;
            }
        if (hordeReadyPlayers == GetArenaType())
        {
            m_readyTeams++;
            DelObject(BG_RL_OBJECT_READY_H);
            for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
                if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                {
                    uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                    if (team == ALLIANCE)
                        player->SendPvPMessage(5, "Your opponents are ready!");
                }
        }
    }
    
    if (m_readyTeams == 2)
    {
        SetStartDelayTime(BG_START_DELAY_15S);
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                player->SendPvPMessage(0, "Both teams are ready. The Arena battle will begin shortly.");
    }
}

void BattlegroundRL::EndEarlyStartWindow()
{
    DelObject(BG_RL_OBJECT_READY_A);
    DelObject(BG_RL_OBJECT_READY_H);
}
