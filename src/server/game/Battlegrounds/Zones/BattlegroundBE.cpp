/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlegroundBE.h"
#include "Language.h"
#include "Object.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "WorldPacket.h"

BattlegroundBE::BattlegroundBE()
{
    BgObjects.resize(BG_BE_OBJECT_MAX);
    m_readyTeams = 0;
    StartDelayTimes[BG_STARTING_EVENT_FIRST]  = BG_START_DELAY_1M;
    StartDelayTimes[BG_STARTING_EVENT_SECOND] = BG_START_DELAY_30S;
    StartDelayTimes[BG_STARTING_EVENT_THIRD]  = BG_START_DELAY_15S;
    StartDelayTimes[BG_STARTING_EVENT_FOURTH] = BG_START_DELAY_NONE;
    //we must set messageIds
    StartMessageIds[BG_STARTING_EVENT_FIRST]  = LANG_ARENA_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_ARENA_THIRTY_SECONDS;
    StartMessageIds[BG_STARTING_EVENT_THIRD]  = LANG_ARENA_FIFTEEN_SECONDS;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_ARENA_HAS_BEGUN;
}

BattlegroundBE::~BattlegroundBE()
{

}

void BattlegroundBE::StartingEventCloseDoors()
{
    for (uint32 i = BG_BE_OBJECT_DOOR_1; i <= BG_BE_OBJECT_DOOR_4; ++i)
        SpawnBGObject(i, RESPAWN_IMMEDIATELY);

    for (uint32 i = BG_BE_OBJECT_BUFF_1; i <= BG_BE_OBJECT_BUFF_2; ++i)
        SpawnBGObject(i, RESPAWN_ONE_DAY);
}

void BattlegroundBE::StartingEventOpenDoors()
{
    for (uint32 i = BG_BE_OBJECT_DOOR_1; i <= BG_BE_OBJECT_DOOR_2; ++i)
        DoorOpen(i);

    for (uint32 i = BG_BE_OBJECT_BUFF_1; i <= BG_BE_OBJECT_BUFF_2; ++i)
        SpawnBGObject(i, 60);
    
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            player->SetPhaseMask(1, true);
    
    DelObject(BG_BE_OBJECT_MAGE_TABLE_A);
    DelObject(BG_BE_OBJECT_MAGE_TABLE_H);
    DelObject(BG_BE_OBJECT_SOUL_WELL_A);
    DelObject(BG_BE_OBJECT_SOUL_WELL_H);
}

void BattlegroundBE::AddPlayer(Player* player)
{
    Battleground::AddPlayer(player);
    
    if (player->isSpectator())
        return;
    
    player->SetPhaseMask(3, true);
    
    switch (player->getClass()) {
        case CLASS_MAGE:
        {
            uint32 mageTableType = player->GetBGTeam() == ALLIANCE ? BG_BE_OBJECT_MAGE_TABLE_A : BG_BE_OBJECT_MAGE_TABLE_H;
            GameObject* mageTable = GetBgMap()->GetGameObject(BgObjects[mageTableType]);
            if (mageTable && mageTable->GetGOInfo()->entry == BG_BE_OBJECT_TYPE_MAGE_TABLE_R2)
                break;
            if (player->HasSpell(58659))    // Ritual of Refreshment (Rank 2)
            {
                if (mageTable)
                    DelObject(mageTableType);
                if (mageTableType == BG_BE_OBJECT_MAGE_TABLE_A) // Alliance spawn
                    mageTable = player->SummonGameObject(BG_BE_OBJECT_TYPE_MAGE_TABLE_R2, 6297.004883f, 289.513458f, 5.356233f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (mageTableType == BG_BE_OBJECT_MAGE_TABLE_H) // Horde spawn
                    mageTable = player->SummonGameObject(BG_BE_OBJECT_TYPE_MAGE_TABLE_R2, 6180.372070f, 234.574387f, 5.323598f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[mageTableType] = mageTable->GetGUID();
                break;
            }
            if (mageTable && mageTable->GetGOInfo()->entry == BG_BE_OBJECT_TYPE_MAGE_TABLE_R1)
                break;
            if (player->HasSpell(43987))    // Ritual of Refreshment (Rank 1)
            {
                if (mageTable) // Just in case...
                    DelObject(mageTableType);
                if (mageTableType == BG_BE_OBJECT_MAGE_TABLE_A) // Alliance spawn
                    mageTable = player->SummonGameObject(BG_BE_OBJECT_TYPE_MAGE_TABLE_R1, 6297.004883f, 289.513458f, 5.356233f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (mageTableType == BG_BE_OBJECT_MAGE_TABLE_H) // Horde spawn
                    mageTable = player->SummonGameObject(BG_BE_OBJECT_TYPE_MAGE_TABLE_R1, 6180.372070f, 234.574387f, 5.323598f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[mageTableType] = mageTable->GetGUID();
                break;
            }
            break;
        }
        case CLASS_WARLOCK:
        {
            uint32 soulWellType = player->GetBGTeam() == ALLIANCE ? BG_BE_OBJECT_SOUL_WELL_A : BG_BE_OBJECT_SOUL_WELL_H;
            GameObject* soulWell = GetBgMap()->GetGameObject(BgObjects[soulWellType]);
            if (soulWell && soulWell->GetGOInfo()->entry == BG_BE_OBJECT_TYPE_SOUL_WELL_R2)
                break;
            if (player->HasSpell(58887))    // Ritual of Souls (Rank 2)
            {
                if (soulWell)
                    DelObject(soulWellType);
                if (soulWellType == BG_BE_OBJECT_SOUL_WELL_A) // Alliance spawn
                    soulWell = player->SummonGameObject(BG_BE_OBJECT_TYPE_SOUL_WELL_R2, 6292.438965f, 292.988464f, 5.264946f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (soulWellType == BG_BE_OBJECT_SOUL_WELL_H) // Horde spawn
                    soulWell = player->SummonGameObject(BG_BE_OBJECT_TYPE_SOUL_WELL_R2, 6184.238770f, 231.542313f, 5.210634f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[soulWellType] = soulWell->GetGUID();
                break;
            }
            if (soulWell && soulWell->GetGOInfo()->entry == BG_BE_OBJECT_TYPE_SOUL_WELL_R1)
                break;
            if (player->HasSpell(29893))    // Ritual of Souls (Rank 1)
            {
                if (soulWell) // Just in case...
                    DelObject(soulWellType);
                if (soulWellType == BG_BE_OBJECT_SOUL_WELL_A) // Alliance spawn
                    soulWell = player->SummonGameObject(BG_BE_OBJECT_TYPE_SOUL_WELL_R1, 6292.438965f, 292.988464f, 5.264946f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (soulWellType == BG_BE_OBJECT_SOUL_WELL_H) // Horde spawn
                    soulWell = player->SummonGameObject(BG_BE_OBJECT_TYPE_SOUL_WELL_R1, 6184.238770f, 231.542313f, 5.210634f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[soulWellType] = soulWell->GetGUID();
                break;
            }
            break;
        }
        default:
            break;
    }
    
    PlayerScores[player->GetGUID()] = new BattlegroundScore;
    UpdateArenaWorldState();
}

void BattlegroundBE::RemovePlayer(Player* player, uint64 guid, uint32 /*team*/)
{
    for (uint8 i = 0; i < BG_BE_OBJECT_MAX; ++i)
        if (BattlegroundMap* bgMap = FindBgMap())
            if (GameObject* BgObject = bgMap->GetGameObject(BgObjects[i]))
                if (uint64 ownerGUID = BgObject->GetOwnerGUID())
                    if (ownerGUID == guid)
                        DelObject(i);
    
    if (player) {
        player->SetPhaseMask(1, true);
        player->SetArenaReadyState(); // Reset player's arena ready state upon leaving arena
    }
    
    if (GetStatus() == STATUS_WAIT_LEAVE)
        return;

    UpdateArenaWorldState();
    CheckArenaWinConditions();
}

void BattlegroundBE::HandleKillPlayer(Player* player, Player* killer)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    if (!killer)
    {
        TC_LOG_ERROR(LOG_FILTER_BATTLEGROUND, "Killer player not found");
        return;
    }

    Battleground::HandleKillPlayer(player, killer);

    UpdateArenaWorldState();
    CheckArenaWinConditions();
}

bool BattlegroundBE::HandlePlayerUnderMap(Player* player)
{
    player->TeleportTo(GetMapId(), 6238.930176f, 262.963470f, 0.889519f, player->GetOrientation());
    return true;
}

void BattlegroundBE::HandleAreaTrigger(Player* player, uint32 trigger)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    switch (trigger)
    {
        case 4538:                                          // buff trigger?
        case 4539:                                          // buff trigger?
            break;
        default:
            Battleground::HandleAreaTrigger(player, trigger);
            break;
    }
}

void BattlegroundBE::FillInitialWorldStates(WorldPacket &data)
{
    data << uint32(0x9f3) << uint32(1);           // 9
    UpdateArenaWorldState();
}

void BattlegroundBE::Reset()
{
    //call parent's class reset
    Battleground::Reset();
}

bool BattlegroundBE::SetupBattleground()
{
        // gates
    if (!AddObject(BG_BE_OBJECT_DOOR_1, BG_BE_OBJECT_TYPE_DOOR_1, 6287.277f, 282.1877f, 3.810925f, -2.260201f, 0, 0, 0.9044551f, -0.4265689f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_BE_OBJECT_DOOR_2, BG_BE_OBJECT_TYPE_DOOR_2, 6189.546f, 241.7099f, 3.101481f, 0.8813917f, 0, 0, 0.4265689f, 0.9044551f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_BE_OBJECT_DOOR_3, BG_BE_OBJECT_TYPE_DOOR_3, 6299.116f, 296.5494f, 3.308032f, 0.8813917f, 0, 0, 0.4265689f, 0.9044551f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_BE_OBJECT_DOOR_4, BG_BE_OBJECT_TYPE_DOOR_4, 6177.708f, 227.3481f, 3.604374f, -2.260201f, 0, 0, 0.9044551f, -0.4265689f, RESPAWN_IMMEDIATELY)
        // buffs
        || !AddObject(BG_BE_OBJECT_BUFF_1, BG_BE_OBJECT_TYPE_BUFF_1, 6249.042f, 275.3239f, 11.22033f, -1.448624f, 0, 0, 0.6626201f, -0.7489557f, 120)
        || !AddObject(BG_BE_OBJECT_BUFF_2, BG_BE_OBJECT_TYPE_BUFF_2, 6228.26f, 249.566f, 11.21812f, -0.06981307f, 0, 0, 0.03489945f, -0.9993908f, 120)
        // early start triggers
        || !AddObject(BG_BE_OBJECT_READY_A, BG_BE_OBJECT_TYPE_READY, 6288.062988f, 287.070465f, 5.199358f, 0.317301f, 0, 0, 0, 0, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_BE_OBJECT_READY_H, BG_BE_OBJECT_TYPE_READY, 6188.332031f, 236.777527f, 5.163993f, 3.458891f, 0, 0, 0, 0, RESPAWN_IMMEDIATELY))
    {
        TC_LOG_ERROR(LOG_FILTER_SQL, "BatteGroundBE: Failed to spawn some object!");
        return false;
    }
    
    if (GameObject* allianceEarlyStartTrigger = GetBgMap()->GetGameObject(BgObjects[BG_BE_OBJECT_READY_A]))
        allianceEarlyStartTrigger->SetPhaseMask(2, true);
    if (GameObject* hordeEarlyStartTrigger = GetBgMap()->GetGameObject(BgObjects[BG_BE_OBJECT_READY_H]))
        hordeEarlyStartTrigger->SetPhaseMask(2, true);

    return true;
}

void BattlegroundBE::UpdatePlayerScore(Player* Source, uint32 type, uint32 value, bool doAddHonor)
{

    BattlegroundScoreMap::iterator itr = PlayerScores.find(Source->GetGUID());
    if (itr == PlayerScores.end())                         // player not found...
        return;

    //there is nothing special in this score
    Battleground::UpdatePlayerScore(Source, type, value, doAddHonor);

}

void BattlegroundBE::CheckEarlyStartTrigger()
{
    if (GetBgMap()->GetGameObject(BgObjects[BG_BE_OBJECT_READY_A]))
    {
        uint8 allianceReadyPlayers = 0;
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            {
                uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                if ((team == ALLIANCE) && (player->GetArenaReadyState() == 1))
                    allianceReadyPlayers++;
            }
        if (allianceReadyPlayers == GetArenaType())
        {
            m_readyTeams++;
            DelObject(BG_BE_OBJECT_READY_A);
            for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
                if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                {
                    uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                    if (team == HORDE)
                        player->SendPvPMessage(5, "Your opponents are ready!");
                }
        }
    }
    
    if (GetBgMap()->GetGameObject(BgObjects[BG_BE_OBJECT_READY_H]))
    {
        uint8 hordeReadyPlayers = 0;
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            {
                uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                if ((team == HORDE) && (player->GetArenaReadyState() == 1))
                    hordeReadyPlayers++;
            }
        if (hordeReadyPlayers == GetArenaType())
        {
            m_readyTeams++;
            DelObject(BG_BE_OBJECT_READY_H);
            for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
                if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                {
                    uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                    if (team == ALLIANCE)
                        player->SendPvPMessage(5, "Your opponents are ready!");
                }
        }
    }
    
    if (m_readyTeams == 2)
    {
        SetStartDelayTime(BG_START_DELAY_15S);
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                player->SendPvPMessage(0, "Both teams are ready. The Arena battle will begin shortly.");
    }
}

void BattlegroundBE::EndEarlyStartWindow()
{
    DelObject(BG_BE_OBJECT_READY_A);
    DelObject(BG_BE_OBJECT_READY_H);
}
