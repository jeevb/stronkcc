/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlegroundNA.h"
#include "Language.h"
#include "Object.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "WorldPacket.h"

BattlegroundNA::BattlegroundNA()
{
    BgObjects.resize(BG_NA_OBJECT_MAX);
    m_readyTeams = 0;
    StartDelayTimes[BG_STARTING_EVENT_FIRST]  = BG_START_DELAY_1M;
    StartDelayTimes[BG_STARTING_EVENT_SECOND] = BG_START_DELAY_30S;
    StartDelayTimes[BG_STARTING_EVENT_THIRD]  = BG_START_DELAY_15S;
    StartDelayTimes[BG_STARTING_EVENT_FOURTH] = BG_START_DELAY_NONE;
    //we must set messageIds
    StartMessageIds[BG_STARTING_EVENT_FIRST]  = LANG_ARENA_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_ARENA_THIRTY_SECONDS;
    StartMessageIds[BG_STARTING_EVENT_THIRD]  = LANG_ARENA_FIFTEEN_SECONDS;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_ARENA_HAS_BEGUN;
}

BattlegroundNA::~BattlegroundNA()
{

}

void BattlegroundNA::StartingEventCloseDoors()
{
    for (uint32 i = BG_NA_OBJECT_DOOR_1; i <= BG_NA_OBJECT_DOOR_4; ++i)
        SpawnBGObject(i, RESPAWN_IMMEDIATELY);
}

void BattlegroundNA::StartingEventOpenDoors()
{
    for (uint32 i = BG_NA_OBJECT_DOOR_1; i <= BG_NA_OBJECT_DOOR_2; ++i)
        DoorOpen(i);

    for (uint32 i = BG_NA_OBJECT_BUFF_1; i <= BG_NA_OBJECT_BUFF_2; ++i)
        SpawnBGObject(i, 60);
    
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            player->SetPhaseMask(1, true);
    
    DelObject(BG_NA_OBJECT_MAGE_TABLE_A);
    DelObject(BG_NA_OBJECT_MAGE_TABLE_H);
    DelObject(BG_NA_OBJECT_SOUL_WELL_A);
    DelObject(BG_NA_OBJECT_SOUL_WELL_H);
}

void BattlegroundNA::AddPlayer(Player* player)
{
    Battleground::AddPlayer(player);
    
    if (player->isSpectator())
        return;
    
    player->SetPhaseMask(3, true);
    
    switch (player->getClass()) {
        case CLASS_MAGE:
        {
            uint32 mageTableType = player->GetBGTeam() == ALLIANCE ? BG_NA_OBJECT_MAGE_TABLE_A : BG_NA_OBJECT_MAGE_TABLE_H;
            GameObject* mageTable = GetBgMap()->GetGameObject(BgObjects[mageTableType]);
            if (mageTable && mageTable->GetGOInfo()->entry == BG_NA_OBJECT_TYPE_MAGE_TABLE_R2)
                break;
            if (player->HasSpell(58659))    // Ritual of Refreshment (Rank 2)
            {
                if (mageTable)
                    DelObject(mageTableType);
                if (mageTableType == BG_NA_OBJECT_MAGE_TABLE_A) // Alliance spawn
                    mageTable = player->SummonGameObject(BG_NA_OBJECT_TYPE_MAGE_TABLE_R2, 4031.214355f, 2979.177734f,11.818675f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (mageTableType == BG_NA_OBJECT_MAGE_TABLE_H) // Horde spawn
                    mageTable = player->SummonGameObject(BG_NA_OBJECT_TYPE_MAGE_TABLE_R2, 4081.721924f, 2862.231445f, 11.691152f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[mageTableType] = mageTable->GetGUID();
                break;
            }
            if (mageTable && mageTable->GetGOInfo()->entry == BG_NA_OBJECT_TYPE_MAGE_TABLE_R1)
                break;
            if (player->HasSpell(43987))    // Ritual of Refreshment (Rank 1)
            {
                if (mageTable) // Just in case...
                    DelObject(mageTableType);
                if (mageTableType == BG_NA_OBJECT_MAGE_TABLE_A) // Alliance spawn
                    mageTable = player->SummonGameObject(BG_NA_OBJECT_TYPE_MAGE_TABLE_R1, 4031.214355f, 2979.177734f,11.818675f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (mageTableType == BG_NA_OBJECT_MAGE_TABLE_H) // Horde spawn
                    mageTable = player->SummonGameObject(BG_NA_OBJECT_TYPE_MAGE_TABLE_R1, 4081.721924f, 2862.231445f, 11.691152f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[mageTableType] = mageTable->GetGUID();
                break;
            }
            break;
        }
        case CLASS_WARLOCK:
        {
            uint32 soulWellType = player->GetBGTeam() == ALLIANCE ? BG_NA_OBJECT_SOUL_WELL_A : BG_NA_OBJECT_SOUL_WELL_H;
            GameObject* soulWell = GetBgMap()->GetGameObject(BgObjects[soulWellType]);
            if (soulWell && soulWell->GetGOInfo()->entry == BG_NA_OBJECT_TYPE_SOUL_WELL_R2)
                break;
            if (player->HasSpell(58887))    // Ritual of Souls (Rank 2)
            {
                if (soulWell)
                    DelObject(soulWellType);
                if (soulWellType == BG_NA_OBJECT_SOUL_WELL_A) // Alliance spawn
                    soulWell = player->SummonGameObject(BG_NA_OBJECT_TYPE_SOUL_WELL_R2, 4022.143311f, 2972.960693f,11.991140f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (soulWellType == BG_NA_OBJECT_SOUL_WELL_H) // Horde spawn
                    soulWell = player->SummonGameObject(BG_NA_OBJECT_TYPE_SOUL_WELL_R2, 4090.883301f, 2867.872559f, 11.832984f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[soulWellType] = soulWell->GetGUID();
                break;
            }
            if (soulWell && soulWell->GetGOInfo()->entry == BG_NA_OBJECT_TYPE_SOUL_WELL_R1)
                break;
            if (player->HasSpell(29893))    // Ritual of Souls (Rank 1)
            {
                if (soulWell) // Just in case...
                    DelObject(soulWellType);
                if (soulWellType == BG_NA_OBJECT_SOUL_WELL_A) // Alliance spawn
                    soulWell = player->SummonGameObject(BG_NA_OBJECT_TYPE_SOUL_WELL_R1, 4022.143311f, 2972.960693f,11.991140f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                else if (soulWellType == BG_NA_OBJECT_SOUL_WELL_H) // Horde spawn
                    soulWell = player->SummonGameObject(BG_NA_OBJECT_TYPE_SOUL_WELL_R1, 4090.883301f, 2867.872559f, 11.832984f, 0, 0, 0, 0, 0, RESPAWN_IMMEDIATELY);
                BgObjects[soulWellType] = soulWell->GetGUID();
                break;
            }
            break;
        }
        default:
            break;
    }
    
    PlayerScores[player->GetGUID()] = new BattlegroundScore;
    UpdateArenaWorldState();
}

void BattlegroundNA::RemovePlayer(Player* player, uint64 guid, uint32 /*team*/)
{
    for (uint8 i = 0; i < BG_NA_OBJECT_MAX; ++i)
        if (BattlegroundMap* bgMap = FindBgMap())
            if (GameObject* BgObject = bgMap->GetGameObject(BgObjects[i]))
                if (uint64 ownerGUID = BgObject->GetOwnerGUID())
                    if (ownerGUID == guid)
                        DelObject(i);
    
    if (player) {
        player->SetPhaseMask(1, true);
        player->SetArenaReadyState(); // Reset player's arena ready state upon leaving arena
    }
    
    if (GetStatus() == STATUS_WAIT_LEAVE)
        return;

    UpdateArenaWorldState();
    CheckArenaWinConditions();
}

void BattlegroundNA::HandleKillPlayer(Player* player, Player* killer)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    if (!killer)
    {
        TC_LOG_ERROR(LOG_FILTER_BATTLEGROUND, "BattlegroundNA: Killer player not found");
        return;
    }

    Battleground::HandleKillPlayer(player, killer);

    UpdateArenaWorldState();
    CheckArenaWinConditions();
}

bool BattlegroundNA::HandlePlayerUnderMap(Player* player)
{
    player->TeleportTo(GetMapId(), 4055.504395f, 2919.660645f, 13.611241f, player->GetOrientation());
    return true;
}

void BattlegroundNA::HandleAreaTrigger(Player* player, uint32 trigger)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    switch (trigger)
    {
        case 4536:                                          // buff trigger?
        case 4537:                                          // buff trigger?
            break;
        default:
            Battleground::HandleAreaTrigger(player, trigger);
            break;
    }
}

void BattlegroundNA::FillInitialWorldStates(WorldPacket &data)
{
    data << uint32(0xa11) << uint32(1);           // 9
    UpdateArenaWorldState();
}

void BattlegroundNA::Reset()
{
    //call parent's class reset
    Battleground::Reset();
}

bool BattlegroundNA::SetupBattleground()
{
    // gates
    if (!AddObject(BG_NA_OBJECT_DOOR_1, BG_NA_OBJECT_TYPE_DOOR_1, 4031.854f, 2966.833f, 12.6462f, -2.648788f, 0, 0, 0.9697962f, -0.2439165f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_NA_OBJECT_DOOR_2, BG_NA_OBJECT_TYPE_DOOR_2, 4081.179f, 2874.97f, 12.39171f, 0.4928045f, 0, 0, 0.2439165f, 0.9697962f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_NA_OBJECT_DOOR_3, BG_NA_OBJECT_TYPE_DOOR_3, 4023.709f, 2981.777f, 10.70117f, -2.648788f, 0, 0, 0.9697962f, -0.2439165f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_NA_OBJECT_DOOR_4, BG_NA_OBJECT_TYPE_DOOR_4, 4090.064f, 2858.438f, 10.23631f, 0.4928045f, 0, 0, 0.2439165f, 0.9697962f, RESPAWN_IMMEDIATELY)
    // buffs
        || !AddObject(BG_NA_OBJECT_BUFF_1, BG_NA_OBJECT_TYPE_BUFF_1, 4009.189941f, 2895.250000f, 13.052700f, -1.448624f, 0, 0, 0.6626201f, -0.7489557f, 120)
        || !AddObject(BG_NA_OBJECT_BUFF_2, BG_NA_OBJECT_TYPE_BUFF_2, 4103.330078f, 2946.350098f, 13.051300f, -0.06981307f, 0, 0, 0.03489945f, -0.9993908f, 120)
    // early start triggers
        || !AddObject(BG_NA_OBJECT_READY_A, BG_NA_OBJECT_TYPE_READY, 4024.248779f, 2967.446777f, 12.165474f, 1.253485f, 0, 0, 0, 0, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_NA_OBJECT_READY_H, BG_NA_OBJECT_TYPE_READY, 4088.415283f, 2873.128906f, 12.129560f, 4.421790f, 0, 0, 0, 0, RESPAWN_IMMEDIATELY))
    {
        TC_LOG_ERROR(LOG_FILTER_SQL, "BatteGroundNA: Failed to spawn some object!");
        return false;
    }
    
    if (GameObject* allianceEarlyStartTrigger = GetBgMap()->GetGameObject(BgObjects[BG_NA_OBJECT_READY_A]))
        allianceEarlyStartTrigger->SetPhaseMask(2, true);
    if (GameObject* hordeEarlyStartTrigger = GetBgMap()->GetGameObject(BgObjects[BG_NA_OBJECT_READY_H]))
        hordeEarlyStartTrigger->SetPhaseMask(2, true);
    
    return true;
}

void BattlegroundNA::CheckEarlyStartTrigger()
{
    if (GetBgMap()->GetGameObject(BgObjects[BG_NA_OBJECT_READY_A]))
    {
        uint8 allianceReadyPlayers = 0;
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            {
                uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                if ((team == ALLIANCE) && (player->GetArenaReadyState() == 1))
                    allianceReadyPlayers++;
            }
        if (allianceReadyPlayers == GetArenaType())
        {
            m_readyTeams++;
            DelObject(BG_NA_OBJECT_READY_A);
            for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
                if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                {
                    uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                    if (team == HORDE)
                        player->SendPvPMessage(5, "Your opponents are ready!");
                }
        }
    }
        
    if (GetBgMap()->GetGameObject(BgObjects[BG_NA_OBJECT_READY_H]))
    {
        uint8 hordeReadyPlayers = 0;
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
            {
                uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                if ((team == HORDE) && (player->GetArenaReadyState() == 1))
                    hordeReadyPlayers++;
            }
        if (hordeReadyPlayers == GetArenaType())
        {
            m_readyTeams++;
            DelObject(BG_NA_OBJECT_READY_H);
            for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
                if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                {
                    uint32 team = (itr->second.Team ? itr->second.Team : player->GetTeam());
                    if (team == ALLIANCE)
                        player->SendPvPMessage(5, "Your opponents are ready!");
                }
        }
    }
    
    if (m_readyTeams == 2)
    {
        SetStartDelayTime(BG_START_DELAY_15S);
        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
            if (Player* player = sObjectAccessor->FindPlayer(itr->first))
                player->SendPvPMessage(0, "Both teams are ready. The Arena battle will begin shortly.");
    }
}

void BattlegroundNA::EndEarlyStartWindow()
{
    DelObject(BG_NA_OBJECT_READY_A);
    DelObject(BG_NA_OBJECT_READY_H);
}
