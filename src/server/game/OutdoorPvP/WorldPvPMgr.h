#ifndef WORLD_PVP_MGR_H_
#define WORLD_PVP_MGR_H_

#include <ace/Singleton.h>

enum WorldPvPMgrEnums {
    GUARD_RESPAWN_DELAY         = 3600,
    NPC_RESPAWN_DELAY           = 300,
    TIME_TO_TAKEOVER            = 10 * MINUTE,
    REWARD_INTERVAL             = 2 * HOUR,
    GO_WORLD_PVP_CHALLENGE_FLAG = 600001,
    GO_WORLD_PVP_FLAG_BEACON    = 190745,
    BROADCAST_INTERVAL          = 5 * IN_MILLISECONDS
};

struct WorldPvPArea {
    WorldPvPArea(): difficulty(0), gySafeLocId(0), flagMapId(0), flagPos_x(0.0f), flagPos_y(0.0f),
    flagOrientation(0.0f), flagRot_0(0.0f), flagRot_1(0.0f), flagRot_2(0.0f), flagRot_3(0.0f),
    guardianSpawn_x(0.0f), guardianSpawn_y(0.0f), guardianSpawn_z(0.0f), guardianSpawn_o(0.0f) { }
    
    uint8 difficulty;
    uint32 gySafeLocId;
    uint32 flagMapId;
    float flagPos_x;
    float flagPos_y;
    float flagPos_z;
    float flagOrientation;
    float flagRot_0;
    float flagRot_1;
    float flagRot_2;
    float flagRot_3;
    float guardianSpawn_x;
    float guardianSpawn_y;
    float guardianSpawn_z;
    float guardianSpawn_o;
};

struct NpcSpawnData {
    NpcSpawnData(): entry(0), mapId(0), x(0.0f), y(0.0f), z(0.0f), o(0.0f), movementType(0), pathId(0) { }
    
    uint32 entry;
    uint32 mapId;
    float x, y, z, o;
    uint8 movementType;
    uint32 pathId;
};

struct GuardSpawnData {
    GuardSpawnData(): mapId(0), x(0.0f), y(0.0f), z(0.0f), o(0.0f), movementType(0), pathId(0) { }
    
    uint32 mapId;
    float x, y, z, o;
    uint8 movementType;
    uint32 pathId;
};

struct AreaData {
    AreaData(): flagGUID(0), beaconGUID(0), guardianGUID(0), holdingGuildId(0), challengingGuildId(0), 
    guildIdAwaitingTakeOver(0), takeOverTime(0), rewardTime(0), broadcastInterval(0) { }
    
    uint64 flagGUID;
    uint64 beaconGUID;
    uint64 guardianGUID;
    uint32 holdingGuildId;
    uint32 challengingGuildId;
    uint32 guildIdAwaitingTakeOver;
    uint32 takeOverTime;
    uint32 rewardTime;
    TimeTracker broadcastInterval;
};

struct ProfSpell {
    ProfSpell(): spellId(0), difficulty(0) { }

    uint32 spellId;
    uint8 difficulty;
};

class WorldPvPMgr
{
    friend class ACE_Singleton<WorldPvPMgr, ACE_Null_Mutex>;

public:
    void InitWorldPvP();

    void Update(uint32 diff);

    typedef std::map<uint32, AreaData> AreaDataMap;
    AreaDataMap const* GetAreaDataMap() const { return &m_areaDataMap; }

    void RemoveAreaFromUpdateQueue(uint32 areaId);
    void RemoveAreaFromRewardQueue(uint32 areaId);
    bool ProcessAreaTakeOver(uint32 areaId, bool scheduleTakeOver);
    bool ProcessAreaRewards(uint32 areaId);
    void ResetAreaOwnership(uint32 areaId);
    void SaveAreaStateToDB(uint32 areaId);

    AreaData* GetAreaDataByAreaId(uint32 areaId);
    uint8 GetAreaDifficulty(uint32 areaId) const;
    uint32 GetAreaIdLinkedToGraveyard(uint32 safeLocId) const;
    uint32 GetHoldingGuildId(uint32 areaId);
    uint32 GetAreaGoldReward(uint32 areaId) const;
    uint32 GetAreaXPReward(uint32 areaId) const;

    bool HandleOpenGo(Player* player, GameObject* go);
    void HandleAreaFlagChallenged(Player* player, GameObject* go);

    void InitAreaChallenge(Player* player, GameObject* go, uint32 challengingGuildId);

    void AreaGuardianDespawned(uint32 areaId);

    void ActivateFlagBeacon(uint32 areaId, bool activate);
    void RespawnFlag(uint32 areaId);
    void SpawnNpcsAndGuards(uint32 areaId);
    void DespawnNpcsAndGuards(uint32 areaId);
    void RespawnNpcsAndGuards(uint32 areaId);
    uint32 GetRandomGuardForArea(uint32 areaId) const;

    uint64 AddGameObject(uint32 entry, uint32 mapId, float x, float y, float z, float o, float rotation0, float rotation1, float rotation2, float rotation3, uint32 phaseMask = PHASEMASK_NORMAL);
    void DeleteGameObject(uint64 guid);

    uint64 AddCreature(uint32 entry, uint32 mapId, uint32 areaId, float x, float y, float z, float o, uint32 respawnDelay = 0, uint8 movementType = 0, uint32 pathId = 0);
    void DeleteCreature(uint64 guid);

    bool IsWorldPvPArea(uint32 areaId) { return m_areas.find(areaId) != m_areas.end(); }
    bool IsAvailableGraveyardForGuild(uint32 safeLocId, uint32 guildId);
    bool IsAreaControlled(AreaData* aData) { return aData && aData->holdingGuildId; }
    bool IsAreaUnderAttack(AreaData* aData) { return aData && aData->challengingGuildId && aData->guardianGUID; }
    bool IsAreaSuccessfullyChallenged(AreaData* aData) { return aData && aData->guildIdAwaitingTakeOver && aData->takeOverTime; }

    void BroadcastAreaUnderAttack(uint32 areaId);
    void SendGlobalNotification(AreaData* aData, uint8 type, const char* format, ...);
    void SendNotificationToGuild(uint32 guildId, uint8 type, const char * format, ...);

    std::string GetTimeString(uint32 time);

    // For vendors
    void LoadItemDifficulties();
    void LoadProfessionSpellDifficulties();
    bool IsItemRewardAvailableInArea(uint32 itemId, uint32 areaId) const;
    bool IsItemAvailableInArea(uint32 itemId, uint32 areaId) const;
    bool IsLearnableProfSpell(Player* player, uint32 spellId, uint8 spellDifficulty, uint32 skillId, uint32 areaId) const;
    void LearnProfSpellsForArea(Player* player, uint32 skillId, uint32 areaId);
    bool CanUpgradeProfSpellsInArea(Player* player, uint32 skillId, uint32 areaId) const;
    bool CanInteract(Player* player, Creature* creature);
    uint8 GetRequiredGuildRepRankForVendor(uint32 areaId) const;

    float GetModifierValue(Creature* creature, float baseValue, float coeff);
    float GetModifierValue(uint32 areaId, float baseValue, float coeff);
    float GetModifierValue(uint8 areaDifficulty, float baseValue, float coeff);

private:
    WorldPvPMgr() { }

    typedef std::map<uint32, WorldPvPArea> Areas;
    Areas m_areas;

    typedef std::vector<NpcSpawnData> NpcSpawns;
    typedef std::map<uint32, NpcSpawns> NpcSpawnsMap;
    NpcSpawnsMap m_npcSpawnsMap;

    typedef std::vector<GuardSpawnData> GuardSpawns;
    typedef std::map<uint32, GuardSpawns> GuardSpawnsMap;
    GuardSpawnsMap m_guardSpawnsMap;

    typedef std::vector<uint32> GuardEntry;
    typedef std::map<uint32, GuardEntry> GuardEntryMap;
    GuardEntryMap m_guardEntryMap;

    typedef std::vector<uint64> AreaCreatures;
    typedef std::map<uint32, AreaCreatures> CreaturesMap;
    CreaturesMap m_creaturesMap;

    AreaDataMap m_areaDataMap;

    typedef std::list<uint32> AreaQueue;
    AreaQueue m_areaDataUpdateQueue;
    AreaQueue m_areaRewardQueue;

    // For vendors
    typedef std::map<uint32, uint8> ItemDifficulty;
    ItemDifficulty m_itemDifficulty;

    typedef std::vector<ProfSpell> ProfSpells;
    ProfSpells m_profSpells;
};

#define sWorldPvPMgr ACE_Singleton<WorldPvPMgr, ACE_Null_Mutex>::instance()

#endif /*WORLD_PVP_MGR_H_*/