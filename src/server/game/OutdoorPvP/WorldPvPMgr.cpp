#include "Chat.h"
#include "CreatureAI.h"
#include "Guild.h"
#include "GuildMgr.h"
#include "SpellInfo.h"
#include "World.h"
#include "WorldPvPMgr.h"

void WorldPvPMgr::InitWorldPvP()
{
    PreparedStatement* stmt = NULL;
    
    m_areas.clear();
    stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_PVP_AREAS);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do
        {
            Field* fields = result->Fetch();
            WorldPvPArea& worldPvPArea    = m_areas[fields[0].GetUInt32()];
            
            worldPvPArea.difficulty       = fields[1].GetUInt8();
            worldPvPArea.gySafeLocId      = fields[2].GetUInt32();
            worldPvPArea.flagMapId        = fields[3].GetUInt32();
            worldPvPArea.flagPos_x        = fields[4].GetFloat();
            worldPvPArea.flagPos_y        = fields[5].GetFloat();
            worldPvPArea.flagPos_z        = fields[6].GetFloat();
            worldPvPArea.flagOrientation  = fields[7].GetFloat();
            worldPvPArea.flagRot_0        = fields[8].GetFloat();
            worldPvPArea.flagRot_1        = fields[9].GetFloat();
            worldPvPArea.flagRot_2        = fields[10].GetFloat();
            worldPvPArea.flagRot_3        = fields[11].GetFloat();
            worldPvPArea.guardianSpawn_x  = fields[12].GetFloat();
            worldPvPArea.guardianSpawn_y  = fields[13].GetFloat();
            worldPvPArea.guardianSpawn_z  = fields[14].GetFloat();
            worldPvPArea.guardianSpawn_o  = fields[15].GetFloat();
            
        } while (result->NextRow());
    }
    
    m_areaDataMap.clear();
    m_areaDataUpdateQueue.clear();
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_WORLD_PVP_AREA_DATA);
    if (PreparedQueryResult result = CharacterDatabase.Query(stmt))
    {
        do
        {
            Field* fields = result->Fetch();
            
            bool resaveArea = false;
            uint32 areaId                           = fields[0].GetUInt32();
            
            AreaData& areaData = m_areaDataMap[areaId];
            
            if (sGuildMgr->GetGuildById(fields[1].GetUInt32()))
            {
                areaData.holdingGuildId           = fields[1].GetUInt32();
                areaData.rewardTime               = fields[4].GetUInt32();
                
            } else
                resaveArea = true;
            
            if (sGuildMgr->GetGuildById(fields[2].GetUInt32()))
            {
                areaData.guildIdAwaitingTakeOver  = fields[2].GetUInt32();
                areaData.takeOverTime             = fields[3].GetUInt32();
                
            } else
                resaveArea = true;

            
            if (areaData.takeOverTime)
                m_areaDataUpdateQueue.push_back(areaId);
            
            if (resaveArea)
                SaveAreaStateToDB(areaId);
        
        } while (result->NextRow());
    }
    
    m_areaRewardQueue.clear();
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_WORLD_PVP_AREA_REWARD);
    if (PreparedQueryResult result = CharacterDatabase.Query(stmt))
    {
        do
        {
            Field* fields = result->Fetch();
            uint32 holdingGuildId = fields[1].GetUInt32();
            
            if (!sGuildMgr->GetGuildById(holdingGuildId))
                continue;
            
            // Ensure that the area is still being held by the guild, before queueing it up.
            uint32 areaId = fields[0].GetUInt32();
            AreaData* areaData = GetAreaDataByAreaId(areaId);
            if (!areaData)
                continue;
            
            if (areaData->holdingGuildId != holdingGuildId)
                continue;
            
            m_areaRewardQueue.push_back(fields[0].GetUInt32());
            
            
        } while (result->NextRow());
    }
    
    m_npcSpawnsMap.clear();
    stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_NPC_SPAWN);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do
        {
            Field* fields = result->Fetch();
            NpcSpawnData m_npcSpawnData;
            m_npcSpawnData.entry        = fields[1].GetUInt32();
            m_npcSpawnData.mapId        = fields[2].GetUInt32();
            m_npcSpawnData.x            = fields[3].GetFloat();
            m_npcSpawnData.y            = fields[4].GetFloat();
            m_npcSpawnData.z            = fields[5].GetFloat();
            m_npcSpawnData.o            = fields[6].GetFloat();
            m_npcSpawnData.movementType = fields[7].GetUInt8();
            m_npcSpawnData.pathId       = fields[8].GetUInt32();
            m_npcSpawnsMap[fields[0].GetUInt32()].push_back(m_npcSpawnData);
            
        } while (result->NextRow());
    }
    
    m_guardSpawnsMap.clear();
    stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_GUARD_SPAWN);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do
        {
            Field* fields = result->Fetch();
            GuardSpawnData m_guardSpawnData;
            m_guardSpawnData.mapId          = fields[1].GetUInt32();
            m_guardSpawnData.x              = fields[2].GetFloat();
            m_guardSpawnData.y              = fields[3].GetFloat();
            m_guardSpawnData.z              = fields[4].GetFloat();
            m_guardSpawnData.o              = fields[5].GetFloat();
            m_guardSpawnData.movementType   = fields[6].GetUInt8();
            m_guardSpawnData.pathId         = fields[7].GetUInt32();
            m_guardSpawnsMap[fields[0].GetUInt32()].push_back(m_guardSpawnData);
            
        } while (result->NextRow());
    }
    
    m_guardEntryMap.clear();
    stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_GUARD_ENTRY);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do
        {
            Field* fields       = result->Fetch();
            uint32 areaId       = fields[0].GetUInt32();
            uint32 guardEntry   = fields[1].GetUInt32();
            
            if (!sObjectMgr->GetCreatureTemplate(guardEntry))
                continue; // TODO: Log this, our database contains an invalid guard entry ID.
            
            m_guardEntryMap[areaId].push_back(guardEntry);
            
        } while (result->NextRow());
    }
    
    for (Areas::const_iterator itr = m_areas.begin(); itr != m_areas.end(); ++itr)
    {
        uint64 flagGUID = AddGameObject(GO_WORLD_PVP_CHALLENGE_FLAG, itr->second.flagMapId,
                                        itr->second.flagPos_x, itr->second.flagPos_y,
                                        itr->second.flagPos_z, itr->second.flagOrientation,
                                        itr->second.flagRot_0, itr->second.flagRot_1,
                                        itr->second.flagRot_2, itr->second.flagRot_3);
        
        if (AreaData* aData = GetAreaDataByAreaId(itr->first))
        {
            if (flagGUID)
                aData->flagGUID = flagGUID;
        
            if (uint64 beaconGUID = AddGameObject(GO_WORLD_PVP_FLAG_BEACON, itr->second.flagMapId,
                                                  itr->second.flagPos_x, itr->second.flagPos_y,
                                                  itr->second.flagPos_z, itr->second.flagOrientation,
                                                  itr->second.flagRot_0, itr->second.flagRot_1,
                                                  itr->second.flagRot_2, itr->second.flagRot_3,
                                                  IsAreaSuccessfullyChallenged(aData) ? PHASEMASK_NORMAL : PHASEMASK_NONE))
                aData->beaconGUID = beaconGUID;
            
        } else
        { // This should add all remaining areas not already present in m_areaDataMap
            AreaData m_areaData;
            m_areaData.flagGUID = flagGUID;
            if (uint64 beaconGUID = AddGameObject(GO_WORLD_PVP_FLAG_BEACON, itr->second.flagMapId,
                                                  itr->second.flagPos_x, itr->second.flagPos_y,
                                                  itr->second.flagPos_z, itr->second.flagOrientation,
                                                  itr->second.flagRot_0, itr->second.flagRot_1,
                                                  itr->second.flagRot_2, itr->second.flagRot_3,
                                                  PHASEMASK_NONE))
                m_areaData.beaconGUID = beaconGUID;
            
            m_areaDataMap[itr->first] = m_areaData;
            
        }
        
        SpawnNpcsAndGuards(itr->first);
    
    }

    LoadItemDifficulties();
    LoadProfessionSpellDifficulties();
}

void WorldPvPMgr::Update(uint32 diff)
{
    for (AreaDataMap::iterator itr = m_areaDataMap.begin(); itr != m_areaDataMap.end(); ++itr)
        if (!itr->second.broadcastInterval.Passed())
            itr->second.broadcastInterval.Update(diff);
    
    if (!m_areaDataUpdateQueue.empty() && ProcessAreaTakeOver(*(m_areaDataUpdateQueue.begin()), false))
        m_areaDataUpdateQueue.pop_front();
    
    if (!m_areaRewardQueue.empty() && ProcessAreaRewards(*(m_areaRewardQueue.begin())))
        m_areaRewardQueue.pop_front();
}

void WorldPvPMgr::RemoveAreaFromUpdateQueue(uint32 areaId)
{
    for (AreaQueue::iterator itr = m_areaDataUpdateQueue.begin(); itr != m_areaDataUpdateQueue.end(); ++itr)
        if (*itr == areaId)
        {
            m_areaDataUpdateQueue.erase(itr);
            return;
        }
}

void WorldPvPMgr::RemoveAreaFromRewardQueue(uint32 areaId)
{
    for (AreaQueue::iterator itr = m_areaRewardQueue.begin(); itr != m_areaRewardQueue.end(); ++itr)
        if (*itr == areaId)
        {
            m_areaRewardQueue.erase(itr);
            return;
        }
}

bool WorldPvPMgr::ProcessAreaTakeOver(uint32 areaId, bool scheduleTakeOver)
{
    AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(areaId);
    std::string areaDesc = areaEntry ? areaEntry->area_name[sWorld->GetDefaultDbcLocale()] : "<Unknown>";
    AreaData* aData = GetAreaDataByAreaId(areaId);
    
    ASSERT(aData);
    
    if (scheduleTakeOver)
    {
        RemoveAreaFromUpdateQueue(areaId);
        ActivateFlagBeacon(areaId, true);
        
        aData->guildIdAwaitingTakeOver = aData->challengingGuildId;
        aData->takeOverTime = sWorld->GetGameTime() + TIME_TO_TAKEOVER;
        m_areaDataUpdateQueue.push_back(areaId);
        
        Guild* guildAwaitingTakeOver = sGuildMgr->GetGuildById(aData->guildIdAwaitingTakeOver);
        
        SendNotificationToGuild(aData->holdingGuildId, 5, "%s has successfully assaulted %s, and will assume control in %s, if left unchallenged!",
                                guildAwaitingTakeOver ? guildAwaitingTakeOver->GetName().c_str() : "An unknown guild", 
                                areaDesc.c_str(), GetTimeString(aData->takeOverTime - sWorld->GetGameTime()).c_str());
        
        SendNotificationToGuild(aData->guildIdAwaitingTakeOver, 5, "Your guild has successfully assaulted %s, and will assume control in %s, if left unchallenged!",
                                areaDesc.c_str(), GetTimeString(aData->takeOverTime - sWorld->GetGameTime()).c_str());
        
        SendGlobalNotification(aData, 0, "%s has successfully assaulted %s, and will assume control in %s, if left unchallenged!",
                               guildAwaitingTakeOver ? guildAwaitingTakeOver->GetName().c_str() : "An unknown guild",
                               areaDesc.c_str(), GetTimeString(aData->takeOverTime - sWorld->GetGameTime()).c_str());
        
    } else if (aData->takeOverTime <= sWorld->GetGameTime())
    {
        RemoveAreaFromRewardQueue(areaId);
        SendNotificationToGuild(aData->holdingGuildId, 5, "Your guild has lost control of %s!", areaDesc.c_str());
        
        aData->holdingGuildId = aData->guildIdAwaitingTakeOver;
        aData->guildIdAwaitingTakeOver = 0;
        aData->takeOverTime = 0;
        aData->rewardTime = sWorld->GetGameTime() + REWARD_INTERVAL;
        m_areaRewardQueue.push_back(areaId);
        
        ActivateFlagBeacon(areaId, false);
        RespawnFlag(areaId);
        RespawnNpcsAndGuards(areaId);
        
        SendNotificationToGuild(aData->holdingGuildId, 5, "%s is now under your guild's control!", areaDesc.c_str());
        
        Guild* holdingGuild = sGuildMgr->GetGuildById(aData->holdingGuildId);
        
        SendGlobalNotification(aData, 0, "%s is now under the control of %s!", areaDesc.c_str(),
                               holdingGuild ? holdingGuild->GetName().c_str() : "an unknown guild");
        
    } else
        return false;
    
    DeleteCreature(aData->guardianGUID);
    
    aData->guardianGUID = 0;
    aData->challengingGuildId = 0;
    SaveAreaStateToDB(areaId);
    
    return true;
}

bool WorldPvPMgr::ProcessAreaRewards(uint32 areaId)
{
    AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(areaId);
    std::string areaDesc = areaEntry ? areaEntry->area_name[sWorld->GetDefaultDbcLocale()] : "<Unknown>";
    AreaData* aData = GetAreaDataByAreaId(areaId);
    
    ASSERT(aData);
    
    if (aData->rewardTime > sWorld->GetGameTime())
        return false;
    
    aData->rewardTime = sWorld->GetGameTime() + REWARD_INTERVAL;
    if (Guild* rewardRecipient = sGuildMgr->GetGuildById(aData->holdingGuildId))
    {
        float mod = rewardRecipient->GetGuildRewardsMod();

        uint32 goldReward = GetAreaGoldReward(areaId);

        // Apply guild talent modifier
        AddPct(goldReward, rewardRecipient->GetTalentModifierValue(INCR_GOLD_RATE));

        // Apply guild membership penalty
        goldReward = float(goldReward) * mod;

        rewardRecipient->ModifyBankMoney(goldReward * GOLD, true);
        SendNotificationToGuild(aData->holdingGuildId, 0, "Your guild has received %u gold for maintaining "
                                "control of %s!", goldReward, areaDesc.c_str());
        
        if (rewardRecipient->GetGuildLevel() < sWorld->getIntConfig(CONFIG_GUILD_LEVEL_CAP))
        { // Max guild level
            uint32 xpReward = GetAreaXPReward(areaId);

            // Apply guild membership penalty
            xpReward = float(xpReward) * mod;

            rewardRecipient->GiveXP(xpReward);
            SendNotificationToGuild(aData->holdingGuildId, 0, "Your guild has received %u experience for maintaining "
                                    "control of %s!", xpReward, areaDesc.c_str());
        }
    }
    
    ResetAreaOwnership(areaId);
    return true;
}

void WorldPvPMgr::ResetAreaOwnership(uint32 areaId)
{
    AreaData* aData = GetAreaDataByAreaId(areaId);
    
    ASSERT(aData);
    
    aData->holdingGuildId = 0;
    aData->rewardTime = 0;
    
    AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(areaId);
    std::string areaDesc = areaEntry ? areaEntry->area_name[sWorld->GetDefaultDbcLocale()] : "<Unknown>";
    
    if (IsAreaUnderAttack(aData))
        AreaGuardianDespawned(areaId);
    
    RespawnNpcsAndGuards(areaId);
    
    SendGlobalNotification(aData, 0, "%s has been reset!", areaDesc.c_str());
    
    SaveAreaStateToDB(areaId);
}

void WorldPvPMgr::SaveAreaStateToDB(uint32 areaId)
{
    AreaData* aData = GetAreaDataByAreaId(areaId);
    
    ASSERT(aData);
    
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_WORLD_PVP_AREA_DATA);
    stmt->setUInt32(0, areaId);
    stmt->setUInt32(1, aData->holdingGuildId);
    stmt->setUInt32(2, aData->guildIdAwaitingTakeOver);
    stmt->setUInt32(3, aData->takeOverTime);
    stmt->setUInt32(4, aData->rewardTime);
    CharacterDatabase.Execute(stmt);
}

AreaData* WorldPvPMgr::GetAreaDataByAreaId(uint32 areaId)
{
    AreaDataMap::iterator itr = m_areaDataMap.find(areaId);
    return (itr != m_areaDataMap.end()) ? &itr->second : NULL;
}

uint8 WorldPvPMgr::GetAreaDifficulty(uint32 areaId) const
{
    Areas::const_iterator itr = m_areas.find(areaId);
    if (itr != m_areas.end())
        return itr->second.difficulty;
    
    return 0;
}

uint32 WorldPvPMgr::GetAreaIdLinkedToGraveyard(uint32 safeLocId) const
{
    for (Areas::const_iterator itr = m_areas.begin(); itr != m_areas.end(); ++itr)
        if (itr->second.gySafeLocId == safeLocId)
            return itr->first;
    
    return 0;
}

uint32 WorldPvPMgr::GetHoldingGuildId(uint32 areaId)
{
    if (AreaData* aData = GetAreaDataByAreaId(areaId))
        return aData->holdingGuildId;
    
    return 0;
}

uint32 WorldPvPMgr::GetAreaGoldReward(uint32 areaId) const
{
    if (float areaDifficulty = GetAreaDifficulty(areaId))
        return ceilf(5.4f * exp(0.6f * areaDifficulty));
    
    return 0;
}

uint32 WorldPvPMgr::GetAreaXPReward(uint32 areaId) const
{
    if (float areaDifficulty = GetAreaDifficulty(areaId))
        return ceilf(90.0f * exp(0.6f * areaDifficulty));
    
    return 0;
}

bool WorldPvPMgr::HandleOpenGo(Player* player, GameObject* go)
{
    if (go->GetEntry() != GO_WORLD_PVP_CHALLENGE_FLAG)
        return false;
    
    HandleAreaFlagChallenged(player, go);
    return true;
}

void WorldPvPMgr::HandleAreaFlagChallenged(Player* player, GameObject* go)
{
    uint32 challengingGuildId = player->GetGuildId();
    if (!challengingGuildId)
    {
        ChatHandler(player->GetSession()).PSendSysMessage("You are not in a guild!");
        return;
    }
    
    uint32 areaId = go->GetMap()->GetAreaId(go->GetPositionX(), go->GetPositionY(), go->GetPositionZ());
    
    Areas::const_iterator itr = m_areas.find(areaId);
    if (itr == m_areas.end())
        return;
    
    AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(areaId);
    std::string areaDesc = areaEntry ? areaEntry->area_name[sWorld->GetDefaultDbcLocale()] : "<Unknown>";
    
    AreaData* aData = GetAreaDataByAreaId(areaId);
    if (IsAreaControlled(aData) && aData->holdingGuildId == challengingGuildId)
    {
        if (IsAreaSuccessfullyChallenged(aData))
        {
            RemoveAreaFromUpdateQueue(areaId);
            ActivateFlagBeacon(areaId, false);
        
            Guild* holdingGuild = sGuildMgr->GetGuildById(aData->holdingGuildId);
            Guild* guildAwaitingTakeOver = sGuildMgr->GetGuildById(aData->guildIdAwaitingTakeOver);
            
            SendNotificationToGuild(aData->holdingGuildId, 0, "%s has successfully defended %s from an assault by %s!",
                                    player->GetName().c_str(), areaDesc.c_str(), 
                                    guildAwaitingTakeOver ? guildAwaitingTakeOver->GetName().c_str() : "an unknown guild");
            
            SendNotificationToGuild(aData->guildIdAwaitingTakeOver, 5, "%s has successfully defended %s from an assault by your guild!",
                                    holdingGuild ? holdingGuild->GetName().c_str() : "An unknown guild", 
                                    areaDesc.c_str());
            
            aData->guildIdAwaitingTakeOver = 0;
            aData->takeOverTime = 0;
        } else
        {
            RemoveAreaFromUpdateQueue(areaId);
            player->SendPvPMessage(0, "Your guild already controls %s!", areaDesc.c_str());
        }
        
        return;
    } else if (IsAreaSuccessfullyChallenged(aData))
    {
        if (aData->guildIdAwaitingTakeOver == challengingGuildId)
            player->SendPvPMessage(0, "Your guild has already successfully assaulted %s, and will assume control in %s, if left unchallenged.", 
                                   areaDesc.c_str(), GetTimeString(aData->takeOverTime - sWorld->GetGameTime()).c_str());
        
        else
        {
            RemoveAreaFromUpdateQueue(areaId);
            
            uint32 origGuildIdAwaitingTakeOver = aData->guildIdAwaitingTakeOver;
            aData->guildIdAwaitingTakeOver = challengingGuildId;
            aData->takeOverTime = sWorld->GetGameTime() + TIME_TO_TAKEOVER;
            
            Guild* guildAwaitingTakeOver = sGuildMgr->GetGuildById(aData->guildIdAwaitingTakeOver);
            
            SendNotificationToGuild(aData->holdingGuildId, 5, "%s has successfully assaulted %s, and will assume control in %s, if left unchallenged!",
                                    guildAwaitingTakeOver ? guildAwaitingTakeOver->GetName().c_str() : "An unknown guild", 
                                    areaDesc.c_str(), GetTimeString(aData->takeOverTime - sWorld->GetGameTime()).c_str());
            
            SendNotificationToGuild(origGuildIdAwaitingTakeOver, 5, "%s has challenged your assault on %s, and will assume control in %s, if left unchallenged!",
                                    guildAwaitingTakeOver ? guildAwaitingTakeOver->GetName().c_str() : "An unknown guild", 
                                    areaDesc.c_str(), GetTimeString(aData->takeOverTime - sWorld->GetGameTime()).c_str());
            
            SendNotificationToGuild(aData->guildIdAwaitingTakeOver, 5, "Your guild has successfully assaulted %s, and will assume control in %s, if left unchallenged!",
                                    areaDesc.c_str(), GetTimeString(aData->takeOverTime - sWorld->GetGameTime()).c_str());
            
            m_areaDataUpdateQueue.push_back(areaId);
        }
        
        return;
    }
    else if (IsAreaUnderAttack(aData))
    {
        Guild* challengingGuild = sGuildMgr->GetGuildById(aData->challengingGuildId);
        
        player->SendPvPMessage(0, "%s is already under attack by %s!", areaDesc.c_str(), 
                               challengingGuild ? challengingGuild->GetName().c_str() : "an unknown guild");
        return;
    }
    
    InitAreaChallenge(player, go, challengingGuildId);
}

void WorldPvPMgr::InitAreaChallenge(Player* player, GameObject* go, uint32 challengingGuildId)
{
    uint32 areaId = go->GetMap()->GetAreaId(go->GetPositionX(), go->GetPositionY(), go->GetPositionZ());
    Areas::const_iterator itr = m_areas.find(areaId);
    
    ASSERT(itr != m_areas.end());
    
    if (uint64 guardianGUID = AddCreature(700000, itr->second.flagMapId, areaId, itr->second.guardianSpawn_x,
                                          itr->second.guardianSpawn_y, itr->second.guardianSpawn_z,
                                          itr->second.guardianSpawn_o))
    {
        AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(areaId);
        std::string areaDesc = areaEntry ? areaEntry->area_name[sWorld->GetDefaultDbcLocale()] : "<Unknown>";
        
        SendNotificationToGuild(challengingGuildId, 5, "Your guild is assaulting %s!", areaDesc.c_str());
        if (AreaData* aData = GetAreaDataByAreaId(areaId))
        {
            aData->challengingGuildId = challengingGuildId;
            aData->guardianGUID = guardianGUID;
            
            Guild* challengingGuild = sGuildMgr->GetGuildById(aData->challengingGuildId);
            
            SendNotificationToGuild(aData->holdingGuildId, 5, "Your guild's control of %s is being challenged by %s!",
                                    areaDesc.c_str(), challengingGuild ? challengingGuild->GetName().c_str() : "an unknown guild");
            SendGlobalNotification(aData, 0, "%s is under assault!", areaDesc.c_str());
        }
        
        if (Creature* areaGuardian = HashMapHolder<Creature>::Find(guardianGUID))
            areaGuardian->AI()->AttackStart(player);
    }
}

void WorldPvPMgr::AreaGuardianDespawned(uint32 areaId)
{
    AreaData* aData = GetAreaDataByAreaId(areaId);
    
    ASSERT(aData);
    
    AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(areaId);
    std::string areaDesc = areaEntry ? areaEntry->area_name[sWorld->GetDefaultDbcLocale()] : "<Unknown>";
    
    Guild* challengingGuild = sGuildMgr->GetGuildById(aData->challengingGuildId);
    
    SendNotificationToGuild(aData->holdingGuildId, 0, "%s's assault on %s has failed!",
                            challengingGuild ? challengingGuild->GetName().c_str() : "An unknown guild", areaDesc.c_str());
    
    SendNotificationToGuild(aData->challengingGuildId, 5, "Your guild's assault on %s has failed!", areaDesc.c_str());
    
    DeleteCreature(aData->guardianGUID);
    
    aData->guardianGUID = 0;
    aData->challengingGuildId = 0;
}

void WorldPvPMgr::ActivateFlagBeacon(uint32 areaId, bool activate)
{
    AreaData* aData = GetAreaDataByAreaId(areaId);
    
    ASSERT(aData && aData->beaconGUID);
    
    if (!activate)
    {
        if (GameObject* beacon = HashMapHolder<GameObject>::Find(aData->beaconGUID))
            beacon->SetPhaseMask(PHASEMASK_NONE, true);
        else
            sObjectMgr->NewGOData(GUID_LOPART(aData->beaconGUID)).phaseMask = PHASEMASK_NONE;
    
    }
    else if (GameObject* beacon = HashMapHolder<GameObject>::Find(aData->beaconGUID))
        beacon->SetPhaseMask(PHASEMASK_NORMAL, true);
    
    else
        sObjectMgr->NewGOData(GUID_LOPART(aData->beaconGUID)).phaseMask = PHASEMASK_NORMAL;
}

void WorldPvPMgr::RespawnFlag(uint32 areaId)
{
    AreaData* aData = GetAreaDataByAreaId(areaId);
    
    ASSERT(aData && aData->flagGUID);
    
    // Remove existing flag
    DeleteGameObject(aData->flagGUID);
    
    aData->flagGUID = 0; // Re-initialize the flagGUID once the old flag is removed, just in case...
    
    Areas::const_iterator itr = m_areas.find(areaId);
    if (itr == m_areas.end())
        return;
    
    WorldPvPArea const& area = itr->second;
    
    // Spawn new flag
    uint64 flagGUID = AddGameObject(GO_WORLD_PVP_CHALLENGE_FLAG, area.flagMapId,
                                    area.flagPos_x, area.flagPos_y,
                                    area.flagPos_z, area.flagOrientation,
                                    area.flagRot_0, area.flagRot_1,
                                    area.flagRot_2, area.flagRot_3);

    if (flagGUID)
        aData->flagGUID = flagGUID;
    
}

void WorldPvPMgr::SpawnNpcsAndGuards(uint32 areaId)
{
    NpcSpawnsMap::const_iterator npcItr = m_npcSpawnsMap.find(areaId);
    if (npcItr != m_npcSpawnsMap.end())
    {
        const NpcSpawns &m_npcSpawns = npcItr->second;
        for (NpcSpawns::const_iterator npcItr1 = m_npcSpawns.begin(); npcItr1 != m_npcSpawns.end(); ++npcItr1)
        {
            uint64 creatureGuid = AddCreature(npcItr1->entry, npcItr1->mapId, areaId,
                                              npcItr1->x, npcItr1->y, npcItr1->z, npcItr1->o,
                                              NPC_RESPAWN_DELAY, npcItr1->movementType, npcItr1->pathId);
            
            m_creaturesMap[areaId].push_back(creatureGuid);
        }
    }
    
    GuardSpawnsMap::const_iterator guardItr = m_guardSpawnsMap.find(areaId);
    if (guardItr != m_guardSpawnsMap.end())
    {
        const GuardSpawns &m_guardSpawns = guardItr->second;
        for (GuardSpawns::const_iterator guardItr1 = m_guardSpawns.begin(); guardItr1 != m_guardSpawns.end(); ++guardItr1)
        {
            uint32 guardEntry = GetRandomGuardForArea(areaId);
            if (!guardEntry)
                break;
            
            uint64 creatureGuid = AddCreature(guardEntry, guardItr1->mapId, areaId,
                                              guardItr1->x, guardItr1->y, guardItr1->z, guardItr1->o,
                                              GUARD_RESPAWN_DELAY, guardItr1->movementType, guardItr1->pathId);
            
            m_creaturesMap[areaId].push_back(creatureGuid);
        }
    }
}

void WorldPvPMgr::DespawnNpcsAndGuards(uint32 areaId)
{
    CreaturesMap::iterator itr = m_creaturesMap.find(areaId);
    if (itr != m_creaturesMap.end())
    {
        AreaCreatures &m_areaCreatures = itr->second;
        for (AreaCreatures::iterator itr1 = m_areaCreatures.begin(); itr1 != m_areaCreatures.end(); ++itr1)
            DeleteCreature(*itr1);
    
        m_areaCreatures.clear();
    }
}

void WorldPvPMgr::RespawnNpcsAndGuards(uint32 areaId)
{
    DespawnNpcsAndGuards(areaId);
    SpawnNpcsAndGuards(areaId);
}

uint32 WorldPvPMgr::GetRandomGuardForArea(uint32 areaId) const
{
    GuardEntryMap::const_iterator itr = m_guardEntryMap.find(areaId);
    if (itr != m_guardEntryMap.end())
    {
        const GuardEntry &guardEntry = itr->second;
        if (guardEntry.empty())
            return 0;
        
        return guardEntry[urand(0, guardEntry.size() - 1)];
        
    }
    
    return 0;
}

uint64 WorldPvPMgr::AddGameObject(uint32 entry, uint32 mapId,
                                  float x, float y, float z, float o,
                                  float rotation0, float rotation1, float rotation2, float rotation3,
                                  uint32 phaseMask)
{
    uint32 lowGuid = sObjectMgr->AddGOData(entry, mapId, x, y, z, o, 0, rotation0, rotation1, rotation2, rotation3, phaseMask);
    ASSERT(lowGuid);
    return MAKE_NEW_GUID(lowGuid, entry, HIGHGUID_GAMEOBJECT);
}

void WorldPvPMgr::DeleteGameObject(uint64 guid)
{
    if (GameObject* gameObject = HashMapHolder<GameObject>::Find(guid))
        gameObject->Delete();
    
    sObjectMgr->DeleteGOData(GUID_LOPART(guid));
}

uint64 WorldPvPMgr::AddCreature(uint32 entry, uint32 mapId, uint32 areaId,
                                float x, float y, float z, float o,
                                uint32 respawnDelay, uint8 movementType, uint32 pathId)
{
    bool isVehicle = false;
    if (CreatureTemplate const* cInfo = sObjectMgr->GetCreatureTemplate(entry))
        isVehicle = cInfo->VehicleId;
    
    uint32 lowGuid = sObjectMgr->AddCreData(entry, 0, mapId, x, y, z, o, respawnDelay, movementType, pathId, areaId, GetHoldingGuildId(areaId));
    ASSERT(lowGuid);
    
    return MAKE_NEW_GUID(lowGuid, entry, isVehicle ? HIGHGUID_VEHICLE : HIGHGUID_UNIT);
}

void WorldPvPMgr::DeleteCreature(uint64 guid)
{
    if (Creature* creature = HashMapHolder<Creature>::Find(guid))
    {
        creature->DisappearAndDie();
        creature->AddObjectToRemoveList();
    }
    
    sObjectMgr->DeleteCreatureData(GUID_LOPART(guid));
    sObjectMgr->DeleteCreatureAddon(GUID_LOPART(guid));
}

bool WorldPvPMgr::IsAvailableGraveyardForGuild(uint32 safeLocId, uint32 guildId)
{
    if (uint32 linkedAreaId = GetAreaIdLinkedToGraveyard(safeLocId))
    {
        AreaData* aData = GetAreaDataByAreaId(linkedAreaId);
        ASSERT(aData);
        
        if (   !guildId
            || guildId != aData->holdingGuildId
            || IsAreaUnderAttack(aData)
            || IsAreaSuccessfullyChallenged(aData))
            
            return false;
    
    }
    return true;
}

void WorldPvPMgr::BroadcastAreaUnderAttack(uint32 areaId)
{
    AreaData* aData = GetAreaDataByAreaId(areaId);
    if (!aData || !aData->broadcastInterval.Passed())
        return;

    AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(areaId);
    std::string areaDesc = areaEntry ? areaEntry->area_name[sWorld->GetDefaultDbcLocale()] : "one of your guild-controlled areas";
    
    SendNotificationToGuild(aData->holdingGuildId, 0, "Aggressors have been spotted at %s.", areaDesc.c_str());
    aData->broadcastInterval.Reset(BROADCAST_INTERVAL);
}

void WorldPvPMgr::SendGlobalNotification(AreaData* aData, uint8 type, const char* format, ...)
{
    ASSERT(aData);
    
    va_list args;
    va_start(args,format);
    char* msg = new char[500];
    vsprintf(msg,format,args);

    for (SessionMap::const_iterator itr = sWorld->GetAllSessions().begin(); itr != sWorld->GetAllSessions().end(); ++itr) {
        if (!itr->second)
            continue;
        Player* player = itr->second->GetPlayer();
        if (   !player
            || !player->IsInWorld()
            || (aData->holdingGuildId && player->GetGuildId() == aData->holdingGuildId)
            || (aData->challengingGuildId && player->GetGuildId() == aData->challengingGuildId)
            || (aData->guildIdAwaitingTakeOver && player->GetGuildId() == aData->guildIdAwaitingTakeOver))
            continue;
        
        player->SendPvPMessage(type, msg);
    }
    
    delete[] msg;
}

void WorldPvPMgr::SendNotificationToGuild(uint32 guildId, uint8 type, const char * format, ...)
{
    Guild* guild = sGuildMgr->GetGuildById(guildId);
    if (!guild)
        return;
    
    va_list args;
    va_start(args,format);
    char* msg = new char[500];
    vsprintf(msg,format,args);
    
    const Guild::Members& guildMembers = guild->GetMembers();
    
    for (Guild::Members::const_iterator itr = guildMembers.begin(); itr != guildMembers.end(); ++itr)
        if (Player* member = itr->second->FindPlayer())
            member->SendPvPMessage(type, msg);
    
    
    delete[] msg;
}

std::string WorldPvPMgr::GetTimeString(uint32 time)
{
    uint32 days = time / DAY, hours = (time % DAY) / HOUR, minutes = (time % HOUR) / MINUTE, seconds = (time % MINUTE);
    std::ostringstream ss;
    
    if (days)
        ss << days << (days == 1 ? " day" : " days");
    else if (hours)
        ss << hours << (hours == 1 ? " hour" : " hours");
    else if (minutes)
        ss << minutes << (minutes == 1 ? " minute" : " minutes");
    else
        ss << seconds << (seconds == 1 ? " second" : " seconds");
    
    return ss.str();
}

void WorldPvPMgr::LoadItemDifficulties()
{
    m_itemDifficulty.clear();
    
    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_ITEM_DIFFICULTY);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do
        {
            Field* fields = result->Fetch();
            m_itemDifficulty[fields[0].GetUInt32()] = fields[1].GetUInt8();
        }
        while (result->NextRow());
    }
}

void WorldPvPMgr::LoadProfessionSpellDifficulties()
{
    m_profSpells.clear();
    
    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_PROFESSION_SPELL_DIFFICULTY);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();
            ProfSpell m_profSpell;
            m_profSpell.spellId = fields[0].GetUInt32();
            m_profSpell.difficulty = fields[1].GetUInt8();
            m_profSpells.push_back(m_profSpell);
        }
        while (result->NextRow());
    }
}

bool WorldPvPMgr::IsItemRewardAvailableInArea(uint32 itemId, uint32 areaId) const
{
    ItemDifficulty::const_iterator itr = m_itemDifficulty.find(itemId);
    if (itr != m_itemDifficulty.end())
        return itr->second == GetAreaDifficulty(areaId) || itr->second == GetAreaDifficulty(areaId) + 1;
    
    return false;
}

bool WorldPvPMgr::IsItemAvailableInArea(uint32 itemId, uint32 areaId) const
{
    ItemDifficulty::const_iterator itr = m_itemDifficulty.find(itemId);
    if (itr != m_itemDifficulty.end())
        return itr->second == GetAreaDifficulty(areaId);
    
    return !GetAreaDifficulty(areaId);
}

bool WorldPvPMgr::IsLearnableProfSpell(Player* player, uint32 spellId, uint8 spellDifficulty, uint32 skillId, uint32 areaId) const
{
    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId);

    return     spellDifficulty <= GetAreaDifficulty(areaId)
            && spellInfo
            && SpellMgr::IsSpellValid(spellInfo, player, false)
            && spellInfo->IsAbilityOfSkillType(skillId)
            && !player->HasSpell(spellId);
}

void WorldPvPMgr::LearnProfSpellsForArea(Player* player, uint32 skillId, uint32 areaId)
{
    for (ProfSpells::const_iterator itr = m_profSpells.begin(); itr != m_profSpells.end(); ++itr)
        if (IsLearnableProfSpell(player, (*itr).spellId, (*itr).difficulty, skillId, areaId))
            player->learnSpell((*itr).spellId, false);
}

bool WorldPvPMgr::CanUpgradeProfSpellsInArea(Player* player, uint32 skillId, uint32 areaId) const
{
    for (ProfSpells::const_iterator itr = m_profSpells.begin(); itr != m_profSpells.end(); ++itr)
        if (IsLearnableProfSpell(player, (*itr).spellId, (*itr).difficulty, skillId, areaId))
            return true;
    
    return false;
}

bool WorldPvPMgr::CanInteract(Player* player, Creature* creature)
{
    uint32 creatureAreaAffiliation = creature->GetAreaAffiliation();

    if (!player->IsGameMaster() && creatureAreaAffiliation)
    {
        if (!player->GetGuildId())
        {
            ChatHandler(player->GetSession()).SendSysMessage("You are not in a guild.");
            return false;
        }
        
        if (  !GetHoldingGuildId(creatureAreaAffiliation)
            || player->GetGuildId() != GetHoldingGuildId(creatureAreaAffiliation))
        {
            ChatHandler(player->GetSession()).SendSysMessage("Your guild does not control this area.");
            return false;
        }
        
        if (!player->HasGuildRepRank(GetRequiredGuildRepRankForVendor(creatureAreaAffiliation)))
        {
            std::ostringstream os;
            os  << "You need to be " << GetGuildRepRankDesc(GetRequiredGuildRepRankForVendor(creatureAreaAffiliation))
                << " with " << sGuildMgr->GetGuildById(GetHoldingGuildId(creatureAreaAffiliation))->GetName()
                << " to use this vendor. You are currently " << GetGuildRepRankDesc(player->GetGuildRepRank()) << ".";
        
            ChatHandler(player->GetSession()).SendSysMessage(os.str().c_str());
            return false;
        }
    }
    
    return true;
}

uint8 WorldPvPMgr::GetRequiredGuildRepRankForVendor(uint32 areaId) const
{
    switch (GetAreaDifficulty(areaId))
    {
        case 0:     return GUILD_REP_NEUTRAL;
        case 1:     return GUILD_REP_FRIENDLY;
        case 2:     return GUILD_REP_HONORED;
        case 3:     return GUILD_REP_REVERED;
        default:    return GUILD_REP_EXALTED;
    }
}

float WorldPvPMgr::GetModifierValue(Creature* creature, float baseValue, float coeff)
{
    return GetModifierValue(creature->GetAreaAffiliation(), baseValue, coeff);
}

float WorldPvPMgr::GetModifierValue(uint32 areaId, float baseValue, float coeff)
{
    if (uint32 areaDifficulty = GetAreaDifficulty(areaId))
        return baseValue * pow(coeff, areaDifficulty - 1);
    
    return 1.0f;
}

float WorldPvPMgr::GetModifierValue(uint8 areaDifficulty, float baseValue, float coeff)
{
    if (areaDifficulty)
        return baseValue * pow(coeff, areaDifficulty - 1);
    
    return 1.0f;
}