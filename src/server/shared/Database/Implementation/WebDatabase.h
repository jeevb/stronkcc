#ifndef __WEBDATABASE_H__
#define __WEBDATABASE_H__

#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"

class WebDatabaseConnection : public MySQLConnection
{
    public:
        //- Constructors for sync and async connections
        WebDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) {}
        WebDatabaseConnection(ACE_Activation_Queue* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) {}

        //- Loads database type specific prepared statements
		void DoPrepareStatements();
};

typedef DatabaseWorkerPool<WebDatabaseConnection> WebDatabaseWorkerPool;

enum WebDatabaseStatements
{
	WEB_GET_VOTE_POINTS,
    WEB_GET_DONATION_POINTS,
    WEB_GET_BOTH_POINTS,
    WEB_UPD_VOTE_POINTS,
    WEB_UPD_DONATION_POINTS,
    WEB_UPD_DO_TRANSACTION,

	MAX_WEBDATABASE_STATEMENTS
};

#endif