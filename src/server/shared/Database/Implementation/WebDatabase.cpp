#include "WebDatabase.h"

void WebDatabaseConnection::DoPrepareStatements()
{
	if (!m_reconnecting)
		m_stmts.resize(MAX_WEBDATABASE_STATEMENTS);
    
    PrepareStatement(WEB_GET_VOTE_POINTS, "SELECT `vote_points` FROM `website_siteaccount` WHERE `account_id` = ?", CONNECTION_SYNCH);
    PrepareStatement(WEB_GET_DONATION_POINTS, "SELECT `donation_points` FROM `website_siteaccount` WHERE `account_id` = ?", CONNECTION_SYNCH);
    PrepareStatement(WEB_GET_BOTH_POINTS, "SELECT `vote_points`, `donation_points` FROM `website_siteaccount` WHERE `account_id` = ?", CONNECTION_SYNCH);
    PrepareStatement(WEB_UPD_VOTE_POINTS, "UPDATE `website_siteaccount` SET `vote_points` = `vote_points` + ? WHERE `account_id` = ? AND `vote_points` + ? >= 0", CONNECTION_SYNCH);
    PrepareStatement(WEB_UPD_DONATION_POINTS, "UPDATE `website_siteaccount` SET `donation_points` = `donation_points` + ? WHERE `account_id` = ? AND `donation_points` + ? >= 0", CONNECTION_SYNCH);
    PrepareStatement(WEB_UPD_DO_TRANSACTION, "UPDATE `website_siteaccount` SET `vote_points` = `vote_points` - ?, `donation_points` = `donation_points` - ? "
                                             "WHERE `account_id` = ? AND `vote_points` >= ? AND `donation_points` >= ?", CONNECTION_SYNCH);
}
