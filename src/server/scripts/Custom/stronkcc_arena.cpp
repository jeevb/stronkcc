#include "ScriptPCH.h"
#include "Chat.h"
#include "ArenaTeamMgr.h"
#include "BattlegroundMgr.h"
#include "Player.h"

class arena_spectator_commands : public CommandScript {
public:
    arena_spectator_commands() : CommandScript("arena_spectator_commands") { }

    static bool HandleSpectateCommand(ChatHandler* handler, const char *args) {
        Player* target;
        uint64 target_guid;
        std::string target_name;
        if (!handler->extractPlayerTarget((char*) args, &target, &target_guid, &target_name))
            return false;

        Player* player = handler->GetSession()->GetPlayer();
        if (target == player || target_guid == player->GetGUID()) {
            handler->SendSysMessage(LANG_CANT_TELEPORT_SELF);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->IsInCombat()) {
            handler->SendSysMessage(LANG_YOU_IN_COMBAT);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!target) {
            handler->PSendSysMessage(LANG_PLAYER_NOT_EXIST_OR_OFFLINE, target_name.c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetMap()->IsBattlegroundOrArena() && !player->isSpectator()) {
            handler->PSendSysMessage("You are already in a battleground or arena.");
            handler->SetSentErrorMessage(true);
            return false;
        }
        
        if (player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_2v2) ||
            player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_3v3) ||
            player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5)) {
            handler->PSendSysMessage("You cannot do that while in an arena queue.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        Map* cMap = target->GetMap();
        if (!cMap->IsBattleArena()) {
            handler->PSendSysMessage("Player not found in an arena match.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetMap()->IsBattleground()) {
            handler->PSendSysMessage("You cannot do that while you are on battleground.");
            handler->SetSentErrorMessage(true);
            return false;
        }
        
        if (target->isSpectator()) {
            handler->PSendSysMessage("You cannot spectate other spectators");
            handler->SetSentErrorMessage(true);
            return false;
        }
        
        if (player->IsInFlight()) {
            handler->PSendSysMessage("You cannot spectate while in flight.");
            handler->SetSentErrorMessage(true);
            return false;
        }
       
        // search for two teams
        Battleground *bGround = target->GetBattleground();
        if (bGround && bGround->isRated()) {
            uint32 slot = bGround->GetArenaType() - 2;
            
            if (bGround->GetArenaType() > 3)
                slot = 2;
            
            uint32 firstTeamID = target->GetArenaTeamId(slot);
            uint32 secondTeamID = 0;
            Player *firstTeamMember = target;
            Player *secondTeamMember = NULL;
            
            for (Battleground::BattlegroundPlayerMap::const_iterator itr = bGround->GetPlayers().begin(); itr != bGround->GetPlayers().end(); ++itr) {
                if (Player * tmpPlayer = ObjectAccessor::FindPlayer(itr->first)) {
                    if (tmpPlayer->isSpectator())
                        continue;

                    uint32 tmpID = tmpPlayer->GetArenaTeamId(slot);
                    if (tmpID != firstTeamID && tmpID > 0) {
                        secondTeamID = tmpID;
                        secondTeamMember = tmpPlayer;
                        break;
                    }
                }
            }

            if (firstTeamID > 0 && secondTeamID > 0 && secondTeamMember) {
                ArenaTeam *firstTeam = sArenaTeamMgr->GetArenaTeamById(firstTeamID);
                ArenaTeam *secondTeam = sArenaTeamMgr->GetArenaTeamById(secondTeamID);
                
                if (firstTeam && secondTeam) {
                    handler->PSendSysMessage("You are spectating a rated arena match between %s (%u, mmr: %u) and %s (%u, mmr: %u).",
                            firstTeam->GetName().c_str(), firstTeam->GetRating(), firstTeam->GetAverageMMR(firstTeamMember->GetGroup()),
                            secondTeam->GetName().c_str(), secondTeam->GetRating(), secondTeam->GetAverageMMR(secondTeamMember->GetGroup())
                    );
                }
            }
        }

        if (player->IsMounted()) // Necessary to "expose" the pets. Pets may not be removed if player is mounted.
        {
            player->Dismount();
            player->RemoveAurasByType(SPELL_AURA_MOUNTED);
        }
        
        // Remove pets and totems, they show up beside you visible to the arena spectators.
        player->RemoveAllControlled();
        player->UnsummonAllTotems();

        // all's well, set bg id
        // when porting out from the bg, it will be reset to 0
        player->SetBattlegroundId(target->GetBattlegroundId(), target->GetBattlegroundTypeId());
        // remember current position as entry point for return at bg end teleportation
        if (!player->GetMap()->IsBattlegroundOrArena())
            player->SetBattlegroundEntryPoint();

        player->SaveRecallPosition();
        
        // to point to see at target with same orientation
        float x, y, z;
        target->GetContactPoint(player, x, y, z);

        player->TeleportTo(target->GetMapId(), x, y, z, player->GetAngle(target), TELE_TO_GM_MODE);
        player->SetPhaseMask(PHASEMASK_NORMAL, true);
        player->SetSpectate(true);
        target->GetBattleground()->AddSpectator(player->GetGUID());

        return true;
    }

    static bool HandleSpectateLeaveCommand(ChatHandler* handler, const char* /*args*/) {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player->isSpectator()) {
            handler->PSendSysMessage("You are not a spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        player->CancelSpectate();
        player->TeleportToBGEntryPoint();

        return true;
    }

    static bool HandleSpectateViewCommand(ChatHandler* handler, const char *args) {
        Player* target;
        uint64 target_guid;
        std::string target_name;
        if (!handler->extractPlayerTarget((char*) args, &target, &target_guid, &target_name))
            return false;

        Player* player = handler->GetSession()->GetPlayer();
        if(!player)
            return false;

        if (!player->isSpectator()) {
            handler->PSendSysMessage("You are not a spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }
        
        if (!target) {
            handler->PSendSysMessage("Player does not exist.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (target->isSpectator() && target != player) {
            handler->PSendSysMessage("You cannot spectate a spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetMap() != target->GetMap()) {
            handler->PSendSysMessage("You may only spectate players in this arena game.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // check for arena preperation
        // if exists than battle didn`t begin
        if (target->HasAura(32728) || target->HasAura(32727)) {
            handler->PSendSysMessage("The arena match has not started.");
            handler->SetSentErrorMessage(true);
            return false;
        }
        
        if (!player->CanSeeOrDetect(target, false)) {
            handler->PSendSysMessage("You cannot spectate a stealthed target.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if(target == player && player->getSpectateFrom())
            player->SetViewpoint(player->getSpectateFrom(), false);
        else
            player->SetViewpoint(target, true);
        
        return true;
    }

    static bool HandleSpectateResetCommand(ChatHandler* handler, const char * /*args */) {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player) {
            handler->PSendSysMessage("Player does not exist.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!player->isSpectator()) {
            handler->PSendSysMessage("You are not a spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        Battleground *bGround = player->GetBattleground();
        if (!bGround)
            return false;

        if (bGround->GetStatus() != STATUS_IN_PROGRESS)
            return true;

        for (Battleground::BattlegroundPlayerMap::const_iterator itr = bGround->GetPlayers().begin(); itr != bGround->GetPlayers().end(); ++itr)
            if (Player * tmpPlayer = ObjectAccessor::FindPlayer(itr->first)) {
                if (tmpPlayer->isSpectator())
                    continue;

                uint32 tmpID = bGround->GetPlayerTeam(tmpPlayer->GetGUID());

                // generate addon massage
                std::string pName = tmpPlayer->GetName();
                std::string tName = "";

                if (Player * target = tmpPlayer->GetSelectedPlayer())
                    tName = target->GetName();

                SpectatorAddonMsg msg;
                msg.SetPlayer(pName);
                if (tName != "")
                    msg.SetTarget(tName);
                msg.SetStatus(tmpPlayer->IsAlive());
                msg.SetClass(tmpPlayer->getClass());
                msg.SetCurrentHP(tmpPlayer->GetHealth());
                msg.SetMaxHP(tmpPlayer->GetMaxHealth());
                Powers powerType = tmpPlayer->getPowerType();
                msg.SetMaxPower(tmpPlayer->GetMaxPower(powerType));
                msg.SetCurrentPower(tmpPlayer->GetPower(powerType));
                msg.SetPowerType(powerType);
                msg.SetTeam(tmpID);
                msg.SendPacket(player->GetGUID());
            }

        return true;
    }

    ChatCommand* GetCommands() const {
        static ChatCommand spectateCommandTable[] = {
            { "player", SEC_PLAYER, true, &HandleSpectateCommand, "", NULL},
            { "view", SEC_PLAYER, true, &HandleSpectateViewCommand, "", NULL},
            { "reset", SEC_PLAYER, true, &HandleSpectateResetCommand, "", NULL},
            { "leave", SEC_PLAYER, true, &HandleSpectateLeaveCommand, "", NULL},
            { NULL, 0, false, NULL, "", NULL}
        };

        static ChatCommand commandTable[] = {
            { "spectate", SEC_PLAYER, false, NULL, "", spectateCommandTable},
            { NULL, 0, false, NULL, "", NULL}
        };
        return commandTable;
    }
};

enum NpcSpectatorSenders {
    NPC_SPECTATOR_NEW_ARENA_TEAM = 1,
    NPC_SPECTATOR_JOIN_ARENA_QUEUE,
    NPC_SPECTATOR_SENDER_LIST_GAMES,
    NPC_SPECTATOR_SENDER_SPECTATE_PLAYER
};

const uint8 GamesOnPage = 5;

class stronkcc_arena_npc : public CreatureScript {
public:
    stronkcc_arena_npc() : CreatureScript("stronkcc_arena_npc") { }

    bool OnGossipHello(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "I'd like to create a new arena team.", 0, NPC_SPECTATOR_NEW_ARENA_TEAM);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "I'd like to join an arena queue.", 0, NPC_SPECTATOR_JOIN_ARENA_QUEUE);
        if (HasSpectatableGames())
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I'd like to spectate an arena game.", 0, NPC_SPECTATOR_SENDER_LIST_GAMES);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        switch (uiAction) {
            case NPC_SPECTATOR_NEW_ARENA_TEAM:
                player->GetSession()->SendPetitionShowList(creature->GetGUID());
                break;
            case NPC_SPECTATOR_JOIN_ARENA_QUEUE:
                player->GetSession()->SendBattleGroundList(creature->GetGUID(), BATTLEGROUND_AA);
                break;
            case NPC_SPECTATOR_SENDER_LIST_GAMES:
                if (ShowPage(player, uiSender))
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                else
                    return OnGossipHello(player, creature);
                break;
            case NPC_SPECTATOR_SENDER_SPECTATE_PLAYER:
                if (Player *target = ObjectAccessor::FindPlayer(MAKE_NEW_GUID(uiSender, 0, HIGHGUID_PLAYER))) {
                    ChatHandler handler(player->GetSession());
                    arena_spectator_commands::HandleSpectateCommand(&handler, target->GetName().c_str());
                }
                break;
            default:
                return OnGossipHello(player, creature);
        }
        
        return true;
    }

    bool ShowPage(Player *player, uint32 page) {
        uint32 gamesShown = 0;
        bool sentTeamEntry = false;
        bool haveNextPage = false;
        for (uint8 i = BATTLEGROUND_NA; i <= BATTLEGROUND_RV; ++i) {
            if (!sBattlegroundMgr->IsArenaType((BattlegroundTypeId)i))	
                continue;
            BattlegroundContainer bgs = sBattlegroundMgr->GetBattlegroundsByType((BattlegroundTypeId)i);
            for (BattlegroundContainer::iterator itr = bgs.begin(); itr != bgs.end(); ++itr) {
                Battleground* arena = itr->second;
                if (!arena->GetPlayersSize())
                    continue;
                if (arena->GetStatus() != STATUS_IN_PROGRESS)
                    continue;
                gamesShown++;
                if (gamesShown > (page + 1) * GamesOnPage) {
                    haveNextPage = true;
                    break;
                } 
                else if (gamesShown >= page * GamesOnPage) {
                    sentTeamEntry = true;
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, GetGamesStringData(arena), 
                                            GetFirstPlayerGuid(arena), NPC_SPECTATOR_SENDER_SPECTATE_PLAYER);
                }
            }
            if (haveNextPage)
                break;
        }
        if (sentTeamEntry) {
            if (page > 1)
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Take me back to the previous page.", page - 1, NPC_SPECTATOR_SENDER_LIST_GAMES);
            if (haveNextPage)
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Show me more games.", page + 1, NPC_SPECTATOR_SENDER_LIST_GAMES);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
            return true;
        }
        else if (page > 1) {
            player->PlayerTalkClass->ClearMenus();
            return ShowPage(player, page - 1);
        }
        else {
            ChatHandler(player->GetSession()).PSendSysMessage("No arena games have been found. Please try again later.");
            return false;
        }
    }
    
    bool HasSpectatableGames() {
        for (uint8 i = BATTLEGROUND_NA; i <= BATTLEGROUND_RV; ++i) {
            if (!sBattlegroundMgr->IsArenaType((BattlegroundTypeId)i))	
                continue;
            BattlegroundContainer bgs = sBattlegroundMgr->GetBattlegroundsByType((BattlegroundTypeId)i);
            for (BattlegroundContainer::iterator itr = bgs.begin(); itr != bgs.end(); ++itr) {
                Battleground* arena = itr->second;
                if (!arena->GetPlayersSize())
                    continue;
                if (arena->GetStatus() != STATUS_IN_PROGRESS)
                    continue;
                return true;
            }
        }
        
        return false;
    }
    
    uint32 GetFirstPlayerGuid(Battleground *arena) {
        for (Battleground::BattlegroundPlayerMap::const_iterator itr = arena->GetPlayers().begin(); itr != arena->GetPlayers().end(); ++itr)
            if (Player * player = ObjectAccessor::FindPlayer(itr->first))
                if(!player->isSpectator())
                    return GUID_LOPART(itr->first);
        return 0;
    }
    
    std::string GetClassNameById(uint8 id) {
        std::string sClass = "";
        switch (id) {
            case CLASS_WARRIOR: sClass = "Warr";
                break;
            case CLASS_PALADIN: sClass = "Pal";
                break;
            case CLASS_HUNTER: sClass = "Hunter";
                break;
            case CLASS_ROGUE: sClass = "Rogue";
                break;
            case CLASS_PRIEST: sClass = "Priest";
                break;
            case CLASS_DEATH_KNIGHT: sClass = "DK";
                break;
            case CLASS_SHAMAN: sClass = "Sham";
                break;
            case CLASS_MAGE: sClass = "Mage";
                break;
            case CLASS_WARLOCK: sClass = "Lock";
                break;
            case CLASS_DRUID: sClass = "Druid";
                break;
        }
        return sClass;
    }
    
    std::string GetTeamStringData(Battleground* arena, uint32 team) {
        std::stringstream ss;
        if (arena->isRated()) {
            if (ArenaTeam* arenaTeam = sArenaTeamMgr->GetArenaTeamById(arena->GetArenaTeamIdForTeam(team)))
                ss << "[" << arenaTeam->GetRating() << "]"; }
        
        return ss.str();
    }
    
    std::string GetPlayerStringData(Battleground* /*arena*/, uint32 /*team*/, Player* player) {
        uint32 const* talentTabIds = GetTalentTabPages(player->getClass());
        uint16 talentTabPointCount[MAX_TALENT_TABS] = {0};
        uint8 mainTalentTab = 0;
        std::string mainTalentTabName = "Unspecified";
        // Get talent information
        for (uint8 i = 0; i < MAX_TALENT_TABS; ++i) {
            TalentTabEntry const* talentTabInfo = sTalentTabStore.LookupEntry(talentTabIds[i]);
            if (!talentTabInfo)
                continue;
            for (uint32 talentId = 0; talentId < sTalentStore.GetNumRows(); ++talentId) {
                TalentEntry const* talentInfo = sTalentStore.LookupEntry(talentId);
                if (!talentInfo)
                    continue;
                // skip another tab talents
                if (talentInfo->TalentTab != talentTabInfo->TalentTabID)
                    continue;
                // find points spent on a talent and add to point count for talent tab
                for (int8 rank = MAX_TALENT_RANK-1; rank >= 0; --rank) {
                    if (talentInfo->RankID[rank] && player->HasTalent(talentInfo->RankID[rank], player->GetActiveSpec())) {
                        talentTabPointCount[i] += rank + 1;
                        break;
                    }
                }
            }
            
            if (!talentTabPointCount[i])
                continue;
            if (talentTabPointCount[mainTalentTab] <= talentTabPointCount[i]) {
                mainTalentTab = i;
                mainTalentTabName = talentTabInfo->name[0];
            }
        }
        
        std::string mainTalentTabNameString = "";
        if (mainTalentTabName == "Unspecified") mainTalentTabNameString = "Uns";
        else if (mainTalentTabName == "Arms") mainTalentTabNameString = "Arms";
        else if (mainTalentTabName == "Fury") mainTalentTabNameString = "Fury";
        else if (mainTalentTabName == "Protection") mainTalentTabNameString = "Prot";
        else if (mainTalentTabName == "Affliction") mainTalentTabNameString = "Aff";
        else if (mainTalentTabName == "Demonology") mainTalentTabNameString = "Demo";
        else if (mainTalentTabName == "Destruction") mainTalentTabNameString = "Destro";
        else if (mainTalentTabName == "Elemental") mainTalentTabNameString = "Ele";
        else if (mainTalentTabName == "Enhancement") mainTalentTabNameString = "Enh";
        else if (mainTalentTabName == "Restoration") mainTalentTabNameString = "R";
        else if (mainTalentTabName == "Assassination") mainTalentTabNameString = "Assa";
        else if (mainTalentTabName == "Combat") mainTalentTabNameString = "Comb";
        else if (mainTalentTabName == "Subtlety") mainTalentTabNameString = "Sub";
        else if (mainTalentTabName == "Discipline") mainTalentTabNameString = "Disc";
        else if (mainTalentTabName == "Holy") mainTalentTabNameString = "H";
        else if (mainTalentTabName == "Shadow") mainTalentTabNameString = "S";
        else if (mainTalentTabName == "Retribution") mainTalentTabNameString = "Ret";
        else if (mainTalentTabName == "Arcane") mainTalentTabNameString = "Arc";
        else if (mainTalentTabName == "Fire") mainTalentTabNameString = "Fire";
        else if (mainTalentTabName == "Frost") mainTalentTabNameString = "Frost";
        else if (mainTalentTabName == "Beast Mastery") mainTalentTabNameString = "BM";
        else if (mainTalentTabName == "Marksmanship") mainTalentTabNameString = "MM";
        else if (mainTalentTabName == "Survival") mainTalentTabNameString = "Surv";
        else if (mainTalentTabName == "Balance") mainTalentTabNameString = "Bal";
        else if (mainTalentTabName == "Feral Combat") mainTalentTabNameString = "Feral";
        else if (mainTalentTabName == "Blood") mainTalentTabNameString = "Blood";
        else if (mainTalentTabName == "Unholy") mainTalentTabNameString = "UH";
        
        std::stringstream ss;
        ss << " " << mainTalentTabNameString << GetClassNameById(player->getClass());
        
        return ss.str();
    }
    
    std::string GetGamesStringData(Battleground *arena) {
        std::string teamMembers[BG_TEAMS_COUNT];
        std::string teamName[BG_TEAMS_COUNT];
        uint32 firstTeamId = 0;
        for (Battleground::BattlegroundPlayerMap::const_iterator itr = arena->GetPlayers().begin(); itr != arena->GetPlayers().end(); ++itr)
            if (Player * player = ObjectAccessor::FindPlayer(itr->first)) {
                if (player->isSpectator())
                    continue;
                uint32 team = itr->second.Team;
                if (!firstTeamId)
                    firstTeamId = team;
                teamName[firstTeamId == team] = GetTeamStringData(arena, team);
                teamMembers[firstTeamId == team] += GetPlayerStringData(arena, team, player);
            }
        std::stringstream ss;
        for (uint8 i = 0; i < BG_TEAMS_COUNT; ++i)
            ss << teamName[i] << teamMembers[i] << "\n";
        
        return ss.str();
    }
};

void AddSC_stronkcc_arena() {
    new arena_spectator_commands();
    new stronkcc_arena_npc();
}