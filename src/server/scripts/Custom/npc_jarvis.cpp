#include "ScriptPCH.h"

class npc_jarvis : public CreatureScript
{
public:
    npc_jarvis() : CreatureScript("npc_jarvis") { }
    
    bool OnGossipHello(Player* player, Creature* creature)
    {
        Unit* Jarvis = creature->ToUnit();
        if (!Jarvis || !Jarvis->IsSummon())
            return true;
        if (player != Jarvis->GetOwner()) {
            creature->MonsterWhisper("You are not my owner!", player->GetGUID());
            return true;
        }
        creature->MonsterWhisper("Your wish is my command!", player->GetGUID());
        return true;
    }
    /*
    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
    {
        
        return true;
    }
    */
};

void AddSC_npc_jarvis()
{
    new npc_jarvis;
}