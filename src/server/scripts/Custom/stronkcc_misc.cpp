#include "Guild.h"
#include "GuildMgr.h"
#include "ScriptPCH.h"
#include "SpellScript.h"
#include "WorldPvPMgr.h"
#include "CustomGossipText.h"

class stronkcc_world_pvp_scout : public CreatureScript
{
public:
    stronkcc_world_pvp_scout() : CreatureScript("stronkcc_world_pvp_scout") { }
    
    bool OnGossipHello(Player* player, Creature* creature)
    {
        std::ostringstream os;
        os << "Greetings, " << player->GetName() << ". ";
        os << "Reconnaissance missions conducted by my trusted sources have revealed the "
              "following information about the contestable bases just outside of here:\n\n";
        
        WorldPvPMgr::AreaDataMap const* aDataMap = sWorldPvPMgr->GetAreaDataMap();
        for (WorldPvPMgr::AreaDataMap::const_iterator itr = aDataMap->begin();
             itr != aDataMap->end(); ++itr) {
            uint32 areaId = itr->first;
            AreaTableEntry const* areaEntry = GetAreaEntryByAreaID(areaId);
            os << (areaEntry ? areaEntry->area_name[sWorld->GetDefaultDbcLocale()] : "<Unknown>");
            os << " (Level: " << uint16(sWorldPvPMgr->GetAreaDifficulty(itr->first)) << ")\n";
            os << "Held By: |cff323232";
            if (Guild* holdingGuild = sGuildMgr->GetGuildById(itr->second.holdingGuildId))
                os << holdingGuild->GetName();
            else
                os << "NA";
            os << "|r\nStatus: |cff323232";
            bool hasStatusText = false;
            if (Guild* challengingGuild = sGuildMgr->GetGuildById(itr->second.challengingGuildId)) {
                hasStatusText = true;
                os << "Currently being challenged by <" << challengingGuild->GetName() << ">. ";
            }
            if (Guild* guildTakingOver = sGuildMgr->GetGuildById(itr->second.guildIdAwaitingTakeOver)) {
                hasStatusText = true;
                os << "Successfully assaulted by <" << guildTakingOver->GetName() << ">. ";
                os << "Take over will occur in" << GetTimeString(itr->second.takeOverTime - sWorld->GetGameTime()) << ". ";
            }
            if (uint32 rewardTime = itr->second.rewardTime) {
                hasStatusText = true;
                os << "Resets in" << GetTimeString(rewardTime - sWorld->GetGameTime()) << ". ";
            }
            if (!hasStatusText)
                os << "No information available.";
            os << "|r\n\n";
        }
        
        UpdateGossipText(player, STRONKCC_WORLD_PVP_SCOUT, os.str());
        player->SEND_GOSSIP_MENU(STRONKCC_WORLD_PVP_SCOUT, creature->GetGUID());
        return true;
    }
    
    std::string GetTimeString(uint32 time) {
        uint32 days = time / DAY, hours = (time % DAY) / HOUR, minutes = (time % HOUR) / MINUTE, seconds = (time % MINUTE);
        std::ostringstream ss;
        if (days) ss << " " << days << (days == 1 ? " day" : " days");
        if (hours) ss << " " << hours << (hours == 1 ? " hour" : " hours");
        if (minutes) ss << " " << minutes << (minutes == 1 ? " minute" : " minutes");
        if (!days && !hours && !minutes) ss << " " << seconds << (seconds == 1 ? " second" : " seconds");
        return ss.str();
    }
};

enum HearthStoneEnum {
    SPELL_HEARTHSTONE = 8690
};

class spell_hearthstone : public SpellScriptLoader
{
public:
    spell_hearthstone() : SpellScriptLoader("spell_hearthstone") {}
    
    class spell_hearthstone_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_hearthstone_SpellScript);
        
        bool Load() {
            if (!sSpellMgr->GetSpellInfo(SPELL_HEARTHSTONE))
                return false;
            
            return GetCaster()->GetTypeId() == TYPEID_PLAYER;
        }
        
        void HandleScript() {
            SpellInfo const* spellInfo = GetSpellInfo();
            Player * player = GetCaster()->ToPlayer();
            if (!spellInfo || !player)
                return;
            Guild* guild = player->GetGuild();
            if (guild && guild->GetTalentModifierValue(HS_COOLDOWN))
                player->RemoveSpellCategoryCooldown(spellInfo->Category, true);
        }
        
        void Register() {
            AfterCast += SpellCastFn(spell_hearthstone_SpellScript::HandleScript);
        }
    };
    
    SpellScript* GetSpellScript() const {
        return new spell_hearthstone_SpellScript();
    }
};

enum MassResurrectionEnum {
    SPELL_MASS_RESURRECTION             = 72429,
    SPELL_MASS_RESURRECTION_TRIGGER     = 72423,
    SPELL_MASS_RESURRECTION_COOLDOWN    = 1800
};

class spell_mass_resurrection : public SpellScriptLoader
{
public:
    spell_mass_resurrection() : SpellScriptLoader("spell_mass_resurrection") {}
    
    class spell_mass_resurrection_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_mass_resurrection_SpellScript);
        
        bool Load() {
            if (!sSpellMgr->GetSpellInfo(SPELL_MASS_RESURRECTION) || !sSpellMgr->GetSpellInfo(SPELL_MASS_RESURRECTION_TRIGGER))
                return false;
            
            return GetCaster()->GetTypeId() == TYPEID_PLAYER;
        }
        
        void FilterTargets(std::list<WorldObject*>& targets) {
            Player* caster = GetCaster()->ToPlayer();
            if (!caster || caster->InBattleground() || !caster->GetGuild() || !caster->GetGuild()->GetTalentModifierValue(MASS_RES)) {
                targets.clear();
                return;
            }
            for (std::list<WorldObject*>::iterator itr = targets.begin(); itr != targets.end();) {
                if ((*itr)->ToPlayer() && (*itr)->ToPlayer()->isDead() && (*itr)->ToPlayer()->IsInSameGuildWith(caster))
                    ++itr;
                else
                    targets.erase(itr++);
            }
        }
        
        void HandleTrigger() {
            GetCaster()->CastSpell(GetHitUnit(), SPELL_MASS_RESURRECTION_TRIGGER, true);
            if (Player* caster = GetCaster()->ToPlayer())
                caster->AddSpellCooldown(SPELL_MASS_RESURRECTION, 0, sWorld->GetGameTime() + SPELL_MASS_RESURRECTION_COOLDOWN);
        }
        
        void Register() {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_mass_resurrection_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENTRY);
            AfterHit += SpellHitFn(spell_mass_resurrection_SpellScript::HandleTrigger);
        }
    };
    
    SpellScript* GetSpellScript() const {
        return new spell_mass_resurrection_SpellScript();
    }
};

class item_stone_of_resurrection : public ItemScript
{
public:
    item_stone_of_resurrection() : ItemScript("item_stone_of_resurrection") { }
    
    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) {
        Guild* guild = player->GetGuild();
        if (!guild || !guild->GetTalentModifierValue(MASS_RES)) {
            ChatHandler(player->GetSession()).PSendSysMessage("Only members of guilds that have purchased the Mass Resurrection talent may use the Stone of Resurrection.");
            player->DestroyItemCount(item->GetEntry(), player->GetItemCount(item->GetEntry(), false), true, false);
            player->SendEquipError(EQUIP_ERR_CANT_DO_RIGHT_NOW, item, NULL);
            return true;
        }
        if (player->InBattleground()) {
            ChatHandler(player->GetSession()).PSendSysMessage("You may not use the Stone of Resurrection in battlegrounds or arenas.");
            player->SendEquipError(EQUIP_ERR_CANT_DO_RIGHT_NOW, item, NULL);
            return true;
        }
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).PSendSysMessage("You may not use the Stone of Resurrection while in combat.");
            player->SendEquipError(EQUIP_ERR_CANT_DO_RIGHT_NOW, item, NULL);
            return true;
        }
        if (uint32 massRessCooldown = player->GetSpellCooldownDelay(SPELL_MASS_RESURRECTION)) {
            ChatHandler(player->GetSession()).PSendSysMessage("You may not use the Stone of Resurrection for another %s.",
                                                              GetTimeString(massRessCooldown).c_str());
            player->SendEquipError(EQUIP_ERR_CANT_DO_RIGHT_NOW, item, NULL);
            return true;
        }
            
        return false;
    }
    
    std::string GetTimeString(uint32 time) {
        uint32 days = time / DAY, hours = (time % DAY) / HOUR, minutes = (time % HOUR) / MINUTE, seconds = (time % MINUTE);
        std::ostringstream ss;
        if (days) ss << days << (days == 1 ? " day" : " days");
        else if (hours) ss << hours << (hours == 1 ? " hour" : " hours");
        else if (minutes) ss << minutes << (minutes == 1 ? " minute" : " minutes");
        else ss << seconds << (seconds == 1 ? " second" : " seconds");
        return ss.str();
    }
};

enum SpiritualImmunityEnum {
    SPELL_SPIRITUAL_IMMUNITY = 58729
};

class spell_spiritual_immunity : public SpellScriptLoader
{
public:
    spell_spiritual_immunity() : SpellScriptLoader("spell_spiritual_immunity") { }
    
    class spell_spiritual_immunity_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_spiritual_immunity_AuraScript);
        
        bool Load() {
            if (!sSpellMgr->GetSpellInfo(SPELL_SPIRITUAL_IMMUNITY))
                return false;
            
            return true;
        }
        
        bool CheckTarget(Unit* target)
        {
            if (target->GetCharmerOrOwnerOrSelf() && target->GetCharmerOrOwnerOrSelf() != GetCaster())
                return false;
            
            return true;
        }
        
        void HandleAuraApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/) {
            if (GetTarget())
                GetTarget()->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1);
        }
        
        void HandleAuraRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/) {
            if (GetTarget())
                GetTarget()->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1);
        }
        
        void Register() {
            DoCheckAreaTarget += AuraCheckAreaTargetFn(spell_spiritual_immunity_AuraScript::CheckTarget);
            OnEffectApply += AuraEffectApplyFn(spell_spiritual_immunity_AuraScript::HandleAuraApply, EFFECT_0, SPELL_AURA_SCHOOL_IMMUNITY, AURA_EFFECT_HANDLE_REAL);
            OnEffectRemove += AuraEffectRemoveFn(spell_spiritual_immunity_AuraScript::HandleAuraRemove, EFFECT_0, SPELL_AURA_SCHOOL_IMMUNITY, AURA_EFFECT_HANDLE_REAL);
        }
    };
    
    AuraScript* GetAuraScript() const {
        return new spell_spiritual_immunity_AuraScript();
    }
};

class TargetDistanceCheck
{
public:
    TargetDistanceCheck(WorldObject* source, float dist) : _source(source), _dist(dist) { }
    
    bool operator()(WorldObject* target) const
    {
        return target->IsWithinDist(_source, _dist, true);
    }
    
private:
    WorldObject* _source;
    float _dist;
};

enum FireCannonEnums
{
    SPELL_WORLD_PVP_FIRE_CANNON  = 57610
};

class spell_gen_wpvp_fire_cannon : public SpellScriptLoader
{
public:
    spell_gen_wpvp_fire_cannon() : SpellScriptLoader("spell_gen_wpvp_fire_cannon") { }
    
    class spell_gen_wpvp_fire_cannon_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_gen_wpvp_fire_cannon_SpellScript);
        
        bool Validate(SpellInfo const* /*spellInfo*/)
        {
            if (!sSpellMgr->GetSpellInfo(SPELL_WORLD_PVP_FIRE_CANNON))
                return false;
            
            return true;
        }
        
        void FilterTargets(std::list<WorldObject*>& targets)
        {
            targets.remove_if(TargetDistanceCheck(GetCaster(), 10));
        }
        
        void SetCorrectHitDamage()
        {
            if (GetHitUnit() && GetHitUnit()->IsControlledByPlayer())
                SetHitDamage(GetHitDamage() * sWorldPvPMgr->GetModifierValue(GetCaster()->GetAreaId(), 1.0f, 1.2f));
        }
        
        void Register()
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_gen_wpvp_fire_cannon_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            BeforeHit += SpellHitFn(spell_gen_wpvp_fire_cannon_SpellScript::SetCorrectHitDamage);
        }
    };
    
    SpellScript* GetSpellScript() const
    {
        return new spell_gen_wpvp_fire_cannon_SpellScript();
    }
};

enum StronkccWorldPvPPortals {
    WESTERN_HFP = 600003
};
 
class stronkcc_world_pvp_portal : public GameObjectScript
{
public:
    stronkcc_world_pvp_portal() : GameObjectScript("stronkcc_world_pvp_portal") { }
     
    bool OnGossipHello(Player* player, GameObject* go)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        switch(go->GetEntry()) {
            case WESTERN_HFP:
                player->TeleportTo(530, -194.381f, 3969.44f, 107.903f, 4.79126f);
                break;
            default:
                break;
        }
         
        return true;
    }
};

void AddSC_stronkcc_misc()
{
    new stronkcc_world_pvp_scout();
    new spell_hearthstone();
    new spell_mass_resurrection();
    new item_stone_of_resurrection();
    new spell_spiritual_immunity();
    new spell_gen_wpvp_fire_cannon();
    new stronkcc_world_pvp_portal();
}