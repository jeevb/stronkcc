enum CustomGossipText {
    STRONKCC_GUILD_GUILDMASTER = 1000000,
    STRONKCC_INNKEEPER,
    STRONKCC_WORLD_PVP_SCOUT
};

static inline void UpdateGossipText(Player* player, uint32 gossipTextId, std::string text) {
    WorldPacket data(SMSG_NPC_TEXT_UPDATE, 100);
    data << gossipTextId;
    for (uint32 i = 0; i < MAX_GOSSIP_TEXT_OPTIONS; ++i) {
        data << float(0);
        data << text;
        data << text;
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
    }
    player->GetSession()->SendPacket(&data);
}