#include "ScriptPCH.h"
#include "CustomGossipText.h"
#include "Config.h"
#include "SiteMgr.h"

#include <algorithm>

#define RETURN_TO_MAIN_MENU_ICON "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo.blp:30:30:-18:1|tReturn to the main menu"
#define MAX_LINE_CHARS 35

enum CustomInnkeeperGossipOptions {
    OPT_SET_HOME = 1,
    OPT_BROWSE_GOODS,
    OPT_RESET_TALENTS,
    OPT_RESET_PET_TALENTS,
    OPT_FINANCIAL,
    OPT_BANK,
    OPT_CURRENCY_EXCHANGE_MENU,
    OPT_CURRENCY_EXCHANGE_SUBMENU,
    OPT_CURRENCY_EXCHANGE,
    OPT_VIEW_SITE_CURRENCIES,
    OPT_CHAR,
    OPT_RECUSTOM,
    OPT_RACE_CHANGE,
    OPT_FACTION_CHANGE,
};

class stronkcc_innkeeper : public CreatureScript, public WorldScript
{
private:
    uint32 recustomCost;
    uint32 raceChangeCost;
    uint32 factionChangeCost;
    struct CurrencyExchangeRate {
        uint32 index;
        uint32 oldCurrency;
        uint32 oldCurrencyAmount;
        uint32 newCurrency;
        uint32 newCurrencyAmount;
    };
    typedef std::vector<CurrencyExchangeRate> CurrencyExchangeRates;
    CurrencyExchangeRates m_currencyExchangeRates;
    typedef std::vector<uint32> Currencies;
    Currencies m_currencies;
public:
    stronkcc_innkeeper() : 
    CreatureScript("stronkcc_innkeeper"),
    WorldScript("stronkcc_innkeeper_WS") {
        PreparedStatement* stmt = NULL;
        m_currencyExchangeRates.clear();
        stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_CURRENCY_EXCHANGE_RATES);
        if (PreparedQueryResult result = WorldDatabase.Query(stmt))
            do {
                Field* fields = result->Fetch();
                CurrencyExchangeRate m_currencyExchangeRate;
                m_currencyExchangeRate.index                = fields[0].GetUInt32();
                m_currencyExchangeRate.oldCurrency          = fields[1].GetUInt32();
                m_currencyExchangeRate.oldCurrencyAmount    = fields[2].GetUInt32();
                m_currencyExchangeRate.newCurrency          = fields[3].GetUInt32();
                m_currencyExchangeRate.newCurrencyAmount    = fields[4].GetUInt32();
                m_currencyExchangeRates.push_back(m_currencyExchangeRate);
            } while (result->NextRow());
        m_currencies.clear();
        stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_CURRENCY);
        if (PreparedQueryResult result = WorldDatabase.Query(stmt))
            do {
                Field* fields = result->Fetch();
                m_currencies.push_back(fields[0].GetUInt32());
            } while (result->NextRow());
    }
    
    void OnConfigLoad(bool /*reload*/) {
        recustomCost = sConfigMgr->GetIntDefault("Innkeeper.RecustomizationCost", 2);
        raceChangeCost = sConfigMgr->GetIntDefault("Innkeeper.RaceChangeCost", 3);
        factionChangeCost = sConfigMgr->GetIntDefault("Innkeeper.FactionChangeCost", 4);
    }
    
    bool OnGossipHello(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Make this my home.", 0, OPT_SET_HOME);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "I'd like to browse your goods.", 0, OPT_BROWSE_GOODS);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "I wish to unlearn my talents.", 0, OPT_RESET_TALENTS);
        if (player->getClass() == CLASS_HUNTER) // Only hunters should get the option to reset pet talents
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TRAINER, "I wish to untrain my pet.", 0, OPT_RESET_PET_TALENTS,
                                             "Are you sure you want to untrain your pet?", 0, false);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "I'd like to view your financial services.", 0, OPT_FINANCIAL);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I'd like to view your character services.", 0, OPT_CHAR);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
    }
    
    bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        switch (uiAction) {
            case OPT_SET_HOME:
                player->SetBindPoint(creature->GetGUID());
                break;
            case OPT_BROWSE_GOODS:
                player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            case OPT_RESET_TALENTS:
                player->SendTalentWipeConfirm(creature->GetGUID());
                break;
            case OPT_RESET_PET_TALENTS:
            {
                Pet* pet = player->GetPet();
                if (!pet || pet->getPetType() != HUNTER_PET) {
                    creature->MonsterWhisper("You do not have a valid pet summoned!", player->GetGUID());
                    break;
                }
                player->ResetPetTalents();
                break;
            }
            case OPT_FINANCIAL:
            {
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "I'd like to access my bank account.", 0, OPT_BANK);
                bool displayCurrencyExchangeOption = false;
                for (Currencies::const_iterator itr = m_currencies.begin(); itr != m_currencies.end(); ++itr)
                    if (CanExchange(player, (*itr))) {
                        displayCurrencyExchangeOption = true;
                        break;
                    }
                if (displayCurrencyExchangeOption)
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "I'd like to exchange some currency.", 0, OPT_CURRENCY_EXCHANGE_MENU);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "I'd like to view my donation and vote points.", 0, OPT_VIEW_SITE_CURRENCIES);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
            case OPT_BANK:
                player->GetSession()->SendShowBank(player->GetGUID());
                break;
            case OPT_CURRENCY_EXCHANGE_MENU:
                return SendCurrencyExchangeMenu(player, creature);
            case OPT_CURRENCY_EXCHANGE_SUBMENU:
            {
                if (!uiSender) {
                    ChatHandler(player->GetSession()).PSendSysMessage("An unexpected error has occurred. Please try again.");
                    return SendCurrencyExchangeMenu(player, creature);
                }
                uint8 menuItemCounter = 0;
                for (CurrencyExchangeRates::const_iterator itr = m_currencyExchangeRates.begin();
                     itr != m_currencyExchangeRates.end(); ++itr) {
                    if ((*itr).oldCurrency != uiSender || player->GetItemCount((*itr).oldCurrency, false) < (*itr).oldCurrencyAmount)
                        continue;
                    ItemTemplate const* oldCurrencyProto = sObjectMgr->GetItemTemplate((*itr).oldCurrency);
                    ItemTemplate const* newCurrencyProto = sObjectMgr->GetItemTemplate((*itr).newCurrency);
                    if (!oldCurrencyProto || !newCurrencyProto)
                        continue;
                    ++menuItemCounter;
                    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, GetTokenDescription(newCurrencyProto, true),
                                                     (*itr).index, OPT_CURRENCY_EXCHANGE, "", 0, true);
                }
                if (!menuItemCounter) {
                    ChatHandler(player->GetSession()).PSendSysMessage("An unexpected error has occurred. Please try again.");
                    return SendCurrencyExchangeMenu(player, creature);
                }
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|TInterface\\Icons\\inv_misc_coin_16.blp:30:30:-18:1|tExchange another currency", 0, OPT_CURRENCY_EXCHANGE_MENU);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|TInterface\\Icons\\inv_misc_coin_01.blp:30:30:-18:1|tView other financial services", 0, OPT_FINANCIAL);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
                UpdateGossipText(player, STRONKCC_INNKEEPER, GetTokenExchangeInfo(player, uiSender));
                player->SEND_GOSSIP_MENU(STRONKCC_INNKEEPER, creature->GetGUID());
                break;
            }
            case OPT_VIEW_SITE_CURRENCIES:
            {
                uint32 donationPoints = 0, votePoints = 0;
                sSiteMgr->GetRewardCurrencies(player, votePoints, donationPoints);
                std::stringstream menuText;
                menuText << "You currently have the following amount of points on your account:\n\n"
                         << "    Donation Points: " << donationPoints << "\n"
                         << "    Vote Points: " << votePoints << "\n";
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "View other financial services.", 0, OPT_FINANCIAL);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
                UpdateGossipText(player, STRONKCC_INNKEEPER, menuText.str());
                player->SEND_GOSSIP_MENU(STRONKCC_INNKEEPER, creature->GetGUID());
                break;
            }
            case OPT_CHAR:
            {
                std::stringstream recustom;
                recustom<<"Are you sure you want to exchange |cffffcc00"<<recustomCost<<"|r vote points for the following?\n\n";
                recustom<<"|cff5DFC0ACharacter Recustomization|r\n";
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "I'd like to recustomize my character.", 0, OPT_RECUSTOM,
                                                 recustom.str(), 0, false);
                std::stringstream raceChange;
                raceChange<<"Are you sure you want to exchange |cffffcc00"<<raceChangeCost<<"|r vote points for the following?\n\n";
                raceChange<<"|cff5DFC0ACharacter Race Change|r\n";
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "I'd like to change my character's race.", 0, OPT_RACE_CHANGE,
                                                 raceChange.str(), 0, false);
                std::stringstream factionChange;
                factionChange<<"Are you sure you want to exchange |cffffcc00"<<factionChangeCost<<"|r vote points for the following?\n\n";
                factionChange<<"|cff5DFC0ACharacter Faction Change|r\n";
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "I'd like to change my character's faction.", 0, OPT_FACTION_CHANGE,
                                                 factionChange.str(), 0, false);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
            case OPT_RECUSTOM:
                if (!sSiteMgr->PerformTransaction(player, recustomCost, 0)) {
                    creature->MonsterWhisper("You do not have the required vote points to complete this transaction!", player->GetGUID());
                    break;
                }
                player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
                creature->MonsterWhisper("Log out and return to the character selection screen to complete the recustomization.", player->GetGUID());
                break;
            case OPT_RACE_CHANGE:
                if (!sSiteMgr->PerformTransaction(player, raceChangeCost, 0)) {
                    creature->MonsterWhisper("You do not have the required vote points to complete this transaction!", player->GetGUID());
                    break;
                }
                player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
                creature->MonsterWhisper("Log out and return to the character selection screen to complete the race change.", player->GetGUID());
                break;
            case OPT_FACTION_CHANGE:
                if (!sSiteMgr->PerformTransaction(player, factionChangeCost, 0)) {
                    creature->MonsterWhisper("You do not have the required vote points to complete this transaction!", player->GetGUID());
                    break;
                }
                player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                creature->MonsterWhisper("Log out and return to the character selection screen to complete the faction change.", player->GetGUID());
                break;
            default:
                return OnGossipHello(player, creature);
        }
        
        return true;
    }
    
    bool OnGossipSelectCode(Player * player, Creature* creature, uint32 uiSender, uint32 uiAction, const char *ccode)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (!uiAction || !uiSender) {
            ChatHandler(player->GetSession()).PSendSysMessage("An unexpected error has occurred. Please try again.");
            return SendCurrencyExchangeMenu(player, creature);
        }
        switch (uiAction) {
            case OPT_CURRENCY_EXCHANGE:
            {
                uint32 amount = atoi((char*)ccode);
                uint32 oldCurrency = 0, oldCurrencyAmount = 0, newCurrency = 0, newCurrencyAmount = 0;
                for (CurrencyExchangeRates::const_iterator itr = m_currencyExchangeRates.begin();
                     itr != m_currencyExchangeRates.end(); ++itr)
                    if (uiSender == (*itr).index) {
                        oldCurrency = (*itr).oldCurrency;
                        oldCurrencyAmount = (*itr).oldCurrencyAmount;
                        newCurrency = (*itr).newCurrency;
                        newCurrencyAmount = (*itr).newCurrencyAmount;
                        break;
                    }
                ItemTemplate const* oldCurrencyProto = sObjectMgr->GetItemTemplate(oldCurrency);
                ItemTemplate const* newCurrencyProto = sObjectMgr->GetItemTemplate(newCurrency);
                if (!oldCurrencyProto || !oldCurrencyAmount || 
                    !newCurrencyProto || !newCurrencyAmount || 
                    player->GetItemCount(oldCurrency, false) < oldCurrencyAmount) {
                    ChatHandler(player->GetSession()).PSendSysMessage("An unexpected error has occurred. Please try again.");
                    return SendCurrencyExchangeMenu(player, creature);
                }
                if (!amount || amount < oldCurrencyAmount) {
                    ChatHandler(player->GetSession()).PSendSysMessage("You have entered an invalid number. Please try again.");
                    return OnGossipSelect(player, creature, oldCurrency, OPT_CURRENCY_EXCHANGE_SUBMENU);
                }
                if (amount > player->GetItemCount(oldCurrency, false)) {
                    creature->MonsterWhisper("You do not have the required currency tokens to complete this transaction.", player->GetGUID());
                    return OnGossipSelect(player, creature, oldCurrency, OPT_CURRENCY_EXCHANGE_SUBMENU);
                }
                uint32 tokensToDestroy = (amount / oldCurrencyAmount) * oldCurrencyAmount;
                uint32 tokensToAdd = (amount / oldCurrencyAmount) * newCurrencyAmount;
                if (!tokensToDestroy || !tokensToAdd) {
                    ChatHandler(player->GetSession()).PSendSysMessage("An unexpected error has occurred. Please try again.");
                    return SendCurrencyExchangeMenu(player, creature);
                }
                ItemPosCountVec dest;
                InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, newCurrencyProto->ItemId, tokensToAdd);
                if (msg == EQUIP_ERR_OK) {
                    Item* item = player->StoreNewItem(dest, newCurrencyProto->ItemId, true, Item::GenerateItemRandomPropertyId(newCurrencyProto->ItemId));
                    if (item) {
                        player->SendNewItem(item, tokensToAdd, true, false);
                        player->DestroyItemCount(oldCurrencyProto->ItemId, tokensToDestroy, true, false);
                        std::ostringstream os;
                        os  << "You have successfully exchanged " << tokensToDestroy << " "
                            << GetTokenDescription(oldCurrencyProto) << " " << "for " << tokensToAdd << " "
                            << GetTokenDescription(newCurrencyProto) << ".";
                        creature->MonsterWhisper(os.str().c_str(), player->GetGUID());
                        return SendCurrencyExchangeMenu(player, creature);
                    }
                    else {
                        ChatHandler(player->GetSession()).PSendSysMessage("An unexpected error has occurred. Please try again.");
                        return SendCurrencyExchangeMenu(player, creature);
                    }
                }
                else {
                    player->SendEquipError(msg, NULL, NULL);
                    return SendCurrencyExchangeMenu(player, creature);
                }
                break;
            }
            default:
                return OnGossipHello(player, creature);
        }
        
        return true;
    }
    
    bool SendCurrencyExchangeMenu(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        for (Currencies::const_iterator itr = m_currencies.begin(); itr != m_currencies.end(); ++itr) {
            ItemTemplate const* oldCurrencyProto = sObjectMgr->GetItemTemplate((*itr));
            if (!oldCurrencyProto || !CanExchange(player, (*itr)))
                continue;
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetTokenDescription(oldCurrencyProto, true), (*itr), OPT_CURRENCY_EXCHANGE_SUBMENU);
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|TInterface\\Icons\\inv_misc_coin_01.blp:30:30:-18:1|tView other financial services", 0, OPT_FINANCIAL);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return true;
    }
    
    bool CanExchange(Player* player, uint32 tokenId) {
        ASSERT(std::find(m_currencies.begin(), m_currencies.end(), tokenId) != m_currencies.end());
        for (CurrencyExchangeRates::const_iterator itr = m_currencyExchangeRates.begin();
             itr != m_currencyExchangeRates.end(); ++itr)
            if ((*itr).oldCurrency == tokenId && player->GetItemCount((*itr).oldCurrency, false) >= (*itr).oldCurrencyAmount)
                return true;
        
        return false;
    }
    
    std::string GetTokenExchangeInfo(Player* player, uint32 tokenId) {
        ItemTemplate const* token = sObjectMgr->GetItemTemplate(tokenId);
        ASSERT(token);
        std::ostringstream os;
        os  << "Greetings " << player->GetName() << "! You have chosen to exchange:\n\n    "
            << GetTokenDescription(token, true, 32, -7, -11) << "\n\n"
            << "I can offer you the following rates:\n\n";
        for (CurrencyExchangeRates::const_iterator itr = m_currencyExchangeRates.begin();
             itr != m_currencyExchangeRates.end(); ++itr) {
            if ((*itr).oldCurrency != tokenId || player->GetItemCount((*itr).oldCurrency, false) < (*itr).oldCurrencyAmount)
                continue;
            if (ItemTemplate const* newToken = sObjectMgr->GetItemTemplate((*itr).newCurrency))
                os  << "  " << (*itr).oldCurrencyAmount << " to " << (*itr).newCurrencyAmount
                    << " |cff303030[" << newToken->Name1 << "]|r\n"
                    << "      Minimum exchange: " << (*itr).oldCurrencyAmount << " tokens\n"
                    << "      Maximum exchange: " << player->GetItemCount((*itr).oldCurrency, false) << " tokens\n\n";
        }
        os  << "Select one of the options below and enter the number of "
            << "|cff303030[" << token->Name1 << "]|r you would like to exchange.";
        
        return os.str();
    }
    
    std::string GetTokenDescription(ItemTemplate const* itemTemplate, bool withIcon = false, uint32 iconSize = 30, int32 horizontalOffset = -18, int32 verticalOffset = 1) {
        std::ostringstream os;
        std::string itemName = itemTemplate->Name1;
        if (withIcon) {
            if (itemName.size() > MAX_LINE_CHARS) {
                itemName.resize(MAX_LINE_CHARS - 3);
                itemName += "...";
            }
            std::string iconPath = "INV_Misc_QuestionMark";
            if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(itemTemplate->DisplayInfoID))
                iconPath = displayInfo->iconPath;
            os  << "|TInterface\\Icons\\" << iconPath << ".blp:"
                << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t"
                << "|Hitem:" << itemTemplate->ItemId << ":0:0:0:0:0:0:0:0:0|h|cff303030" << itemName << "|r|h";
        }
        else
            os  << "|Hitem:" << itemTemplate->ItemId << ":0:0:0:0:0:0:0:0:0|h|c"
                << std::hex << ItemQualityColors[itemTemplate->Quality] << std::dec
                << "[" << itemTemplate->Name1 << "]|r|h";
        
        return os.str();
    }
};
    
    void AddSC_stronkcc_innkeeper()
{
    new stronkcc_innkeeper;
}