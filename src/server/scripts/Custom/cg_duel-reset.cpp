#include "ScriptPCH.h"
#include "ScriptMgr.h"
#include "Config.h"
#include <set>


struct SpellCooldownInfo
{
    time_t end;
    uint32 spellId;
};

struct SavedPlayerState
{
    uint32 health;
    uint32 mana;
    std::vector<SpellCooldownInfo> cooldowns;
};

typedef std::map<uint64, SavedPlayerState> PlayerStateMap;

class Reset_OnDuelEnd : public PlayerScript, public DisableScript
{
private:
     PlayerStateMap m_SavedState;
public:
    Reset_OnDuelEnd() : PlayerScript("Reset_OnDuelEnd"), DisableScript("Reset_OnDuelEnd_DisableScript") {}
        
    inline bool NotPermAppliedAura(Player* player, uint32 spellId) 
    {
        // We check to see if the spell is an aura (Like Cold Blood, Inner Focus)
        if(Aura *aura = player->GetAura(spellId, player->GetGUID())) 
            return !aura->IsPermanent(); // And if it's not permanent (I..e. expires on cast) we track its cooldown.
        
        return true;
    }

    void SavePlayerState(Player* player) 
    {
        SavedPlayerState& state = m_SavedState[player->GetGUID()];

        //  Save player health & mana.
        state.health = player->GetHealth();
        if(player->getPowerType() == POWER_MANA)
            state.mana = player->GetPower(POWER_MANA);
        else
            state.mana = 0;

        // Save cooldowns.
        
        if(state.cooldowns.size() > 0)
            state.cooldowns.clear();
        
        time_t curTime = time(NULL);
        
        SpellCooldowns const cooldowns = player->GetSpellCooldowns();
        for (SpellCooldowns::const_iterator itr = cooldowns.begin(); itr != cooldowns.end(); ++itr) 
        {
            SpellInfo const* entry = sSpellMgr->GetSpellInfo(itr->first);
            if( entry && 
                itr->second.itemid == 0 && // We don't store or deal with item cooldowns.
                itr->second.end < curTime + MONTH && // These cooldowns are way too long, TC considers them infinite. 
                itr->second.end > curTime && // What if for some odd reason it's already expired? 
                entry->RecoveryTime <= 10 * MINUTE * IN_MILLISECONDS && // We only reset cooldowns under 10 minutes.
                entry->CategoryRecoveryTime <= 10 * MINUTE * IN_MILLISECONDS &&
                NotPermAppliedAura(player, itr->first) // We don't care about auras which are applied and are permanent. 
            ) {
                SpellCooldownInfo si;
                si.end = itr->second.end;
                si.spellId = itr->first;
                state.cooldowns.push_back(si);
                player->RemoveSpellCooldown(itr->first, true);
            }
        }
    }

    void RemoveExpiringPlayerBuffs(Player* player, int32 within) 
    {
        // remove auras with duration lower than 30s
        Unit::AuraApplicationMap & auraMap = player->GetAppliedAuras();
        for (Unit::AuraApplicationMap::iterator iter = auraMap.begin(); iter != auraMap.end();)
        {
            AuraApplication * aurApp = iter->second;
            Aura* aura = aurApp->GetBase();
            if (aura->GetId() == 25771 /* Remove FORBERANCE */
                || (!aura->IsPermanent()
                && aura->GetDuration() <= within
                && aurApp->IsPositive()
                && (!(aura->GetSpellInfo()->Attributes & SPELL_ATTR0_UNAFFECTED_BY_INVULNERABILITY))
                && (!aura->HasEffectType(SPELL_AURA_MOD_INVISIBILITY))))
                player->RemoveAura(iter);
            else
                ++iter;
        }
    }

    void RestorePlayerState(Player* player) 
    {
        uint64 playerGuid = player->GetGUID();
        
        PlayerStateMap::iterator itr = m_SavedState.find(playerGuid);
        // no save state. 
        if(itr == m_SavedState.end())
            return;
        
        SavedPlayerState& state = itr->second;

        // restore health and mana
        player->SetHealth(state.health);
        if(state.mana)
            player->SetPower(POWER_MANA, state.mana);
        
        // restore cooldowns.
        
        time_t curTime = time(NULL);
        // Let's populate a set of spellIds we're going to ovewrite and not reset. 

        std::map<uint32, uint32> willReset;
        for(std::vector<SpellCooldownInfo>::iterator itr = state.cooldowns.begin(); itr != state.cooldowns.end(); ++itr)
            if(itr->end > curTime)
                willReset[itr->spellId] = itr->end;

        std::vector< std::pair<SpellInfo const*, uint32> > startCooldowns; // Perma auras (Cold Blood, Inner Focus, ..etc) have to be handled specially.

        SpellCooldowns const cooldowns = player->GetSpellCooldowns();
        SpellCooldowns::const_iterator sitr, next;

        WorldPacket data(SMSG_SPELL_COOLDOWN, 8+1+willReset.size()*8); // The packet should be at most the size of our cooldowns & reset.
        data << uint64(player->GetGUID());
        data << uint8(0x00);

        for (sitr = cooldowns.begin(); sitr != cooldowns.end(); sitr = next) 
        {
            next = sitr;
            ++next;
            SpellInfo const* entry = sSpellMgr->GetSpellInfo(sitr->first);
            if(!entry || !NotPermAppliedAura(player, sitr->first)) // We don't care about auras which are applied and are permanent. 
                continue;

            std::map<uint32, uint32>::iterator _wr = willReset.find(sitr->first);
            if( _wr == willReset.end() && // If we're not going to re-add the cooldown, then we can reset it if ...
                entry->RecoveryTime <= 10 * MINUTE * IN_MILLISECONDS && // It is under 10 minutes.
                entry->CategoryRecoveryTime <= 10 * MINUTE * IN_MILLISECONDS
            ) {
                player->RemoveSpellCooldown(sitr->first, true);
            }
        }

        // Re-add cooldowns. 
        for(std::map<uint32, uint32>::iterator _wr = willReset.begin(); _wr != willReset.end(); ++_wr) 
        {
            if(NotPermAppliedAura(player, _wr->first)) 
            {
                if(SpellInfo const* entry = sSpellMgr->GetSpellInfo(_wr->first)) 
                    if(entry->GetDuration() == -1) // Permanant auras are a pain in the ass. We have to use a little hack to get them to reset.
                        startCooldowns.push_back(std::pair<SpellInfo const*, uint32>(entry, _wr->second));
                    data << uint32(_wr->first);
                    data << uint32((_wr->second - curTime) * IN_MILLISECONDS);
                    player->AddSpellCooldown(_wr->first, 0, _wr->second); 
            }
        }

        player->GetSession()->SendPacket(&data);

        // We avoid sending or even consturcting the packet if we don't have to.
        if(startCooldowns.size()) 
        {
            // For permanent cooldowns, what we have to do is send a reset to the client, then resend the remaining time.
            // What a pain!
            WorldPacket sc_data(SMSG_SPELL_COOLDOWN, 8+1+startCooldowns.size()*8); // The packet should be at most the size of our cooldowns & reset.
            sc_data << uint64(player->GetGUID());
            sc_data << uint8(0x00);
            for(std::vector< std::pair<SpellInfo const*, uint32> >::iterator _sc = startCooldowns.begin(); _sc != startCooldowns.end(); ++_sc) 
            {
                player->SendCooldownEvent(_sc->first, 0, NULL, false);
                data << uint32(_sc->first->Id);
                data << uint32((_sc->second - curTime) * IN_MILLISECONDS);
            }
            player->GetSession()->SendPacket(&sc_data);
        }

        m_SavedState.erase(playerGuid);
    }

    void HandleDuelAccept(Player* player)
    {
        if (player->IsInMainMall()) {
            RemoveExpiringPlayerBuffs(player, 30*IN_MILLISECONDS);
            SavePlayerState(player);
            player->ResetAllPowers();
        }
    }
    
    void OnDuelAccept(Player* player1, Player* player2) 
    {
        if(sWorld->getBoolConfig(CONFIG_DUEL_RESET_COOLDOWN)) 
        {
            HandleDuelAccept(player1);
            HandleDuelAccept(player2);
        }
    }

    void OnDuelEnd(Player *winner, Player *loser, DuelCompleteType /*type*/)
    {
        // reset cooldowns
        if (sWorld->getBoolConfig(CONFIG_DUEL_RESET_COOLDOWN))
        {
            RestorePlayerState(winner);
            RestorePlayerState(loser);
        } 
    }
    
    void OnLogout(Player* player) 
    {
        m_SavedState.erase(player->GetGUID());
    }
    
    // Disable FLying Mounts while Dueling...
    
    bool CanUseFlyingMount(Player *player, AreaTableEntry const* /*area*/) 
    {
        return !(player->duel && player->duel->startTime);
    }
};

void AddSC_DuelReset()
{
    new Reset_OnDuelEnd;
}
