#include "ScriptPCH.h"
#include "Config.h"
#include "ObjectMgr.h"
#include "Battleground.h"
#include "BattlegroundAV.h"
#include "BattlegroundWS.h"
#include "BattlegroundAB.h"
#include "BattlegroundEY.h"
#include "BattlegroundSA.h"
#include "BattlegroundIC.h"

enum Tokens {
    BG_RATING_TOKEN = 37711
};

class cg_bgrating : public PlayerScript, public WorldScript
{
private:
    struct TopScores {
        TopScores() : topKillingBlows(0), topHonorableKills(0), topDamageDone(0), topHealingDone(0) { }
        
        uint32 topKillingBlows;
        uint32 topHonorableKills;
        uint32 topDamageDone;
        uint32 topHealingDone;
    };
    typedef std::map<Battleground*, TopScores> BgTopScores;
    BgTopScores m_bgTopScores;
    float ratingCoeff;
public:
    cg_bgrating() : PlayerScript("cg_bgrating_ps"), WorldScript("cg_bgrating_ws") { }

    void OnConfigLoad(bool /*reload*/) {
        ratingCoeff = sConfigMgr->GetFloatDefault("Battleground.Rating.Coefficient", 24.0f);
        
        return;
    }

    void OnLogin(Player* player) {
        SyncBattlegroundRatingTokens(player, 0);
        
        return;
    }

    void BeforePlayerRemoveFromBattlegroundPlayerMap(Battleground* bg, uint64 guid) {
        if (!bg->isBattleground() ||
            bg->GetStatus() < STATUS_WAIT_JOIN ||
            bg->GetStatus() > STATUS_IN_PROGRESS)
            return;

        Battleground::BattlegroundPlayerMap::const_iterator itr = bg->GetPlayers().find(guid);
        if (itr == bg->GetPlayers().end() || itr->second.EndedMatch)
            return;

        Player* player = sObjectAccessor->FindPlayer(guid);

        if (player && (player->IsGameMaster() || player->IsBeingSummonedByGM()))
            return;

        int32 ratingChange = (int32)ceilf(-1.0f * ratingCoeff);
        if (ratingChange > 0) // Just to make sure...
            return;

        SQLTransaction trans = CharacterDatabase.BeginTransaction();

        int32 pvpRatingChange = ratingChange * (bg->isPremade() ? 5.0f : 3.0f);
        int32 newPlayerRating = 0;

        if (player) {
            player->AddPvPRating(pvpRatingChange);

            if (bg->isPremade()) {
                int32 newPlayerRating = (int32)itr->second.BgRating + ratingChange;
                if (newPlayerRating < 0) newPlayerRating = 0;

                player->SetBattlegroundRating((uint32)newPlayerRating);
                SyncBattlegroundRatingTokens(player, ratingChange);

                std::ostringstream ss;
                ss<<"Your new battleground rating is ";
                ss<<"|cffff0000"<<(uint32)newPlayerRating<<" ("<<ratingChange<<")|r!";

                player->SendPvPMessage(5, ss.str().c_str());
            }
        }
        else
            sObjectMgr->UpdatePlayerPvPRatingByGUID(guid, pvpRatingChange, trans);

        if (bg->isPremade())
            sObjectMgr->UpdatePlayerBGRatingByGUID(guid, (uint32)newPlayerRating, trans);

        CharacterDatabase.CommitTransaction(trans);
        return;
    }

    void OnEndBattleground(Battleground* bg) {
        if (!bg->isBattleground() ||
            bg->IsEndedPrematurely()) // Do not award/remove rating for BGs that end prematurely
            return;
        uint32 winnerTeam = 0;
        switch (bg->GetWinner()) {
            case WINNER_HORDE:
                winnerTeam = HORDE;
                break;
            case WINNER_ALLIANCE:
                winnerTeam = ALLIANCE;
                break;
            default:
                break;
        }
        uint32 winnerScore = 0;
        uint32 loserScore = 0;
        int32 winnerBonus = 0;
        float winMargin = 0.0f;
        // Calculate win margin only if the bg did not end prematurely
        if (winnerTeam) { 
            winnerScore = bg->GetTeamScore(bg->GetTeamIndexByTeamId(winnerTeam));
            loserScore = bg->GetTeamScore(bg->GetTeamIndexByTeamId(bg->GetOtherTeam(winnerTeam)));
            winnerBonus = GetWinnerTeamRatingBonus(bg, winnerTeam);
            if (winnerScore) // To ensure there is no division by 0
                winMargin = float(winnerScore - loserScore) / float(winnerScore);
            if (winMargin < 0.0f) // Should not happen, but just in case...
                winMargin = 0.0f;
        }
        uint32 allianceTeamRating = GetTeamBGRating(bg, ALLIANCE);
        uint32 hordeTeamRating = GetTeamBGRating(bg, HORDE);
        int32 allianceRatingChange = GetRatingChangeForTeam(allianceTeamRating, hordeTeamRating, winMargin, winnerBonus, winnerTeam == ALLIANCE);
        int32 hordeRatingChange = GetRatingChangeForTeam(hordeTeamRating, allianceTeamRating, winMargin, winnerBonus, winnerTeam == HORDE);
        GetBattlegroundTopScores(bg);
        
        SetBGRatingForTeamPlayers(bg, allianceRatingChange, ALLIANCE);
        SetBGRatingForTeamPlayers(bg, hordeRatingChange, HORDE);
        
        if (bg->isPremade()) //Log match statistics to the database
            CharacterDatabase.PExecute("INSERT INTO bg_rating_stats (startTime, endTime, winnerTeam, winnerScore, loserScore, a_teamRating, h_teamRating, a_ratingChange, h_ratingChange) VALUES (%u, %u, %u, %u, %u, %u, %u, %i, %i)",
                                        bg->GetBgStartTimeStamp(), bg->GetBgEndTimeStamp(), winnerTeam, winnerScore, loserScore, allianceTeamRating, hordeTeamRating, allianceRatingChange, hordeRatingChange);

        m_bgTopScores.erase(bg);
        return;
    }

    uint32 GetTeamBGRating(Battleground* bg, uint32 team) {
        Battleground::BgTeamRatingTimeMap::const_iterator itr = bg->GetBgTeamRatingTimeData().find(team);
        if (itr != bg->GetBgTeamRatingTimeData().end())
            return itr->second / GetBGMaxPossiblePlayedTime(bg);

        return 0;
    }

    uint32 GetBGMaxPossiblePlayedTime(Battleground* bg) {
        return (bg->GetBgEndTimeStamp() - bg->GetBgStartTimeStamp()) * bg->GetMaxPlayersPerTeam();
    }

    int32 GetRatingChangeForTeam(uint32 ownRating, uint32 opponentRating, float winMargin, int32 winnerBonus, bool isWinner) {
        float chance = 1.0f / (1.0f + exp(log(10.0f) * (float)((float)opponentRating - (float)ownRating) / 650.0f));
        float winFactor = isWinner ? 1.0f : 0.0f;

        return (int32)ceilf((winFactor - chance) * ratingCoeff * (1 + 0.25f * winMargin)) + (int32)(winFactor * winnerBonus);
    }

    void SetBGRatingForTeamPlayers(Battleground* bg, int32 _ratingChange, uint32 team) {
        SQLTransaction trans = CharacterDatabase.BeginTransaction();

        for (Battleground::BattlegroundPlayerMap::const_iterator itr = bg->GetPlayers().begin(); itr != bg->GetPlayers().end(); ++itr) {
            if (bg->GetPlayerTeam(itr->first) != team || !IsEligibleForRatingChange(bg, itr->first))
                continue;

            int32 ratingChange = _ratingChange;
            AddIndividualRatingBonus(bg, itr->first, ratingChange);

            int32 pvpRatingChange = ratingChange * (bg->isPremade() ? 5.0f : 3.0f);
            int32 newPlayerRating = 0;

            if (Player* player = sObjectAccessor->FindPlayer(itr->first)) {
                // Apply PvP Rating changes
                player->AddPvPRating(pvpRatingChange);

                // Apply guild reputation and token gains
                if (pvpRatingChange > 0)
                    player->GiveGuildRep(pvpRatingChange);

                if (bg->isPremade()) {
                    int32 newPlayerRating = (int32)itr->second.BgRating + ratingChange;
                    if (newPlayerRating < 0) newPlayerRating = 0;

                    player->SetBattlegroundRating((uint32)newPlayerRating);
                    SyncBattlegroundRatingTokens(player, ratingChange);

                    std::ostringstream ss;
                    ss<<"Your new battleground rating is ";
                    if (ratingChange < 0)
                        ss<<"|cffff0000"<<(uint32)newPlayerRating<<" (-";
                    else if (ratingChange >= 0)
                        ss<<"|cff00ff00"<<(uint32)newPlayerRating<<" (+";
                    ss<<abs(ratingChange)<<")|r!";

                    player->SendPvPMessage(5, ss.str().c_str());
                }
            }
            else
                sObjectMgr->UpdatePlayerPvPRatingByGUID(itr->first, pvpRatingChange, trans);

            if (bg->isPremade())
                sObjectMgr->UpdatePlayerBGRatingByGUID(itr->first, (uint32)newPlayerRating, trans);
        }

        CharacterDatabase.CommitTransaction(trans);
        return;
    }

    bool IsEligibleForRatingChange(Battleground* bg, uint64 guid) {
        Battleground::BattlegroundPlayerMap::const_iterator itr = bg->GetPlayers().find(guid);
        if (itr == bg->GetPlayers().end() || !itr->second.EndedMatch)
            return false;
        uint32 playedTime = 0;
        if (itr->second.OfflineTime && itr->second.OfflineTime > bg->GetBgStartTimeStamp())
            playedTime = itr->second.OfflineTime - itr->second.JoinedTime;
        else if (!itr->second.OfflineTime)
            playedTime = bg->GetBgEndTimeStamp() - itr->second.JoinedTime;
        if (itr->second.NullifiedTime) {
            ASSERT(itr->second.NullifiedTime <= playedTime);
            playedTime -= itr->second.NullifiedTime;
        }
        uint32 bgLength = bg->GetBgEndTimeStamp() - bg->GetBgStartTimeStamp();
        uint32 noRatingWindow = (uint32)ceilf(float(bgLength) * 0.15f);
        if (noRatingWindow < 3 * MINUTE)
            noRatingWindow = 3 * MINUTE;

        return playedTime > noRatingWindow;
    }

    void SyncBattlegroundRatingTokens(Player* player, int32 ratingChange) {
        if (ratingChange > 0)
            player->AddItem(BG_RATING_TOKEN, (uint32)abs(ratingChange));
        else if (ratingChange < 0)
            player->DestroyItemCount(BG_RATING_TOKEN, (uint32)abs(ratingChange), true, false);
        if (player->GetItemCount(BG_RATING_TOKEN, false) == player->GetBattlegroundRating())
            return;
        else {
            player->DestroyItemCount(BG_RATING_TOKEN, player->GetItemCount(BG_RATING_TOKEN, false), true, false);
            player->AddItem(BG_RATING_TOKEN, player->GetBattlegroundRating());
        }

        return;
    }

    void GetBattlegroundTopScores(Battleground* bg) {
        TopScores ts;
        for (Battleground::BattlegroundScoreMap::const_iterator itr = bg->GetPlayerScoresBegin();
             itr != bg->GetPlayerScoresEnd(); ++itr) {
            if (itr->second->KillingBlows > ts.topKillingBlows) ts.topKillingBlows = itr->second->KillingBlows;
            if (itr->second->HonorableKills > ts.topHonorableKills) ts.topHonorableKills = itr->second->HonorableKills;
            if (itr->second->DamageDone > ts.topDamageDone) ts.topDamageDone = itr->second->DamageDone;
            if (itr->second->HealingDone > ts.topHealingDone) ts.topHealingDone = itr->second->HealingDone;
        }
        // Populate top scores fields for bg
        m_bgTopScores[bg] = ts;
        
        return;
    }

    void AddIndividualRatingBonus(Battleground* bg, uint64 guid, int32& ratingChange) {
        Battleground::BattlegroundScoreMap::const_iterator itr = bg->GetPlayerScore(guid);
        BgTopScores::const_iterator itr1 = m_bgTopScores.find(bg);
        if (itr == bg->GetPlayerScoresEnd() || itr1 == m_bgTopScores.end())
            return;
        // Top killing blows
        if (itr->second->KillingBlows && itr->second->KillingBlows == (itr1->second).topKillingBlows)
            ratingChange += 2;
        // Top honorable kills
        if (itr->second->HonorableKills && itr->second->HonorableKills == (itr1->second).topHonorableKills)
            ratingChange += 1;
        // Top damage done
        if (itr->second->DamageDone && itr->second->DamageDone == (itr1->second).topDamageDone)
            ratingChange += 2;
        // Top healing done
        if (itr->second->HealingDone && itr->second->HealingDone == (itr1->second).topHealingDone)
            ratingChange += 2;
        // No deaths
        if (itr->second->Deaths == 0)
            ratingChange += 1;
        // Battleground-specific bonuses
        switch (bg->GetTypeID()) {
            case BATTLEGROUND_AV: // Alterac Valley
            {
                if (int32 graveyardsAssaulted = (int32)((BattlegroundAVScore*)itr->second)->GraveyardsAssaulted)
                    ratingChange += graveyardsAssaulted;
                if (int32 graveyardsDefended = (int32)((BattlegroundAVScore*)itr->second)->GraveyardsDefended)
                    ratingChange += graveyardsDefended / 2;
                if (int32 towersAssaulted = (int32)((BattlegroundAVScore*)itr->second)->TowersAssaulted)
                    ratingChange += towersAssaulted;
                if (int32 towersDefended = (int32)((BattlegroundAVScore*)itr->second)->TowersDefended)
                    ratingChange += towersDefended / 2;
                if (int32 minesCaptured = (int32)((BattlegroundAVScore*)itr->second)->MinesCaptured)
                    ratingChange += minesCaptured;
                if (int32 leadersKilled = (int32)((BattlegroundAVScore*)itr->second)->LeadersKilled)
                    ratingChange += leadersKilled;
                if (int32 secondaryObjectives = (int32)((BattlegroundAVScore*)itr->second)->SecondaryObjectives)
                    ratingChange += secondaryObjectives;
                break;
            }
            case BATTLEGROUND_WS: // Warsong Gulch
            {
                if (int32 flagsCaptured = (int32)((BattlegroundWGScore*)itr->second)->FlagCaptures)
                    ratingChange += flagsCaptured;
                if (int32 flagsReturned = (int32)((BattlegroundWGScore*)itr->second)->FlagReturns)
                    ratingChange += flagsReturned / 2;
                break;
            }
            case BATTLEGROUND_AB: // Arathi Basin
            {
                if (int32 basesAssaulted = (int32)((BattlegroundABScore*)itr->second)->BasesAssaulted)
                    ratingChange += basesAssaulted;
                if (int32 basesDefended = (int32)((BattlegroundABScore*)itr->second)->BasesDefended)
                    ratingChange += basesDefended / 2;
                break;
            }
            case BATTLEGROUND_EY: // Eye of the Storm
            {
                if (int32 flagsCaptured = (int32)((BattlegroundEYScore*)itr->second)->FlagCaptures)
                    ratingChange += flagsCaptured;
                break;
            }
            case BATTLEGROUND_SA: // Strand of the Ancients
            {
                if (int32 demolishersDestroyed = (int32)((BattlegroundSAScore*)itr->second)->demolishers_destroyed)
                    ratingChange += demolishersDestroyed / 2;
                if (int32 gatesDestroyed = (int32)((BattlegroundSAScore*)itr->second)->gates_destroyed)
                    ratingChange += gatesDestroyed;                
                break;
            }
            case BATTLEGROUND_IC: // Isle of Conquest
            {
                if (int32 basesAssaulted = (int32)((BattlegroundICScore*)itr->second)->BasesAssaulted)
                    ratingChange += basesAssaulted;
                if (int32 basesDefended = (int32)((BattlegroundICScore*)itr->second)->BasesDefended)
                    ratingChange += basesDefended / 2;
                break;
            }
            default:
                break;
        }

        return;
    }

    int32 GetWinnerTeamRatingBonus(Battleground* bg, uint32 winnerTeam) {
        int32 winnerBonus = 0;
        // Calculate match length bonuses
        uint32 matchLength = bg->GetBgEndTimeStamp() - bg->GetBgStartTimeStamp();
        if (matchLength <= 8 * MINUTE)
            winnerBonus += 3;
        else if (matchLength <= 12 * MINUTE)
            winnerBonus += 2;
        else if (matchLength <= 16 * MINUTE)
            winnerBonus += 1;
        // Calculate bonuses for ending match with maximum bases possible
        // Only for Arathi Basin, Eye of the Storm and Isle of Conquest
        switch (bg->GetTypeID()) {
            case BATTLEGROUND_AB:
            case BATTLEGROUND_EY:
            case BATTLEGROUND_IC:
                if (bg->IsAllNodesConrolledByTeam(winnerTeam))
                    winnerBonus += 3;
                break;
            default:
                break;
        }
        // Calculate bonus for no deaths in team
        for (Battleground::BattlegroundScoreMap::const_iterator itr = bg->GetPlayerScoresBegin();
             itr != bg->GetPlayerScoresEnd(); ++itr)
            if (bg->GetPlayerTeam(itr->first) == winnerTeam && itr->second->Deaths)
                return winnerBonus;
        // We can only get this far if there were no deaths in winnerTeam
        winnerBonus += 5;

        return winnerBonus;
    }
};

void AddSC_cg_bgrating()
{
    new cg_bgrating();
}
