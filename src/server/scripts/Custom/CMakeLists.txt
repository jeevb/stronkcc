# Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

set(scripts_STAT_SRCS
  ${scripts_STAT_SRCS}
  Custom/npc_jarvis.cpp
  Custom/cg_AHBot.cpp
  Custom/cg_xfactionbg.cpp
  Custom/cg_autolearn_spells.cpp
  Custom/cg_bgrating.cpp
  Custom/cg_duel-reset.cpp
  Custom/cg_guru-ffa.cpp
  Custom/stronkcc_world_pvp_guardians.cpp
  Custom/stronkcc_world_pvp_guards.cpp
  Custom/stronkcc_guild.cpp
  Custom/stronkcc_misc.cpp
  Custom/stronkcc_killstreak.cpp
  Custom/stronkcc_vendors.cpp
  Custom/stronkcc_transmogrifier.cpp
  Custom/stronkcc_opcode_flood_filter.cpp
  Custom/stronkcc_arena.cpp
  Custom/stronkcc_innkeeper.cpp
)

message("  -> Prepared: Custom")
