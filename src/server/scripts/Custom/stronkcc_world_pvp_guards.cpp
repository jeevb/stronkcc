#include "ScriptPCH.h"
#include "WorldPvPMgr.h"

# define CHECK_BOUNDARY_TIMER 3000
# define BOUNDARY 120.0f

enum GuardEvents {
    // Priest
    EVENT_SHADOW_WORD_PAIN = 1,
    EVENT_CIRCLE_OF_DESTRUCTION,
    EVENT_COWER_IN_FEAR,
    EVENT_DARK_MENDING,
    
    // Mage
    EVENT_FIREBALL,
    EVENT_FLAMESTRIKE,
    EVENT_FROSTBOLT,
    EVENT_CHAINS_OF_ICE,
    
    // Rogue
    EVENT_SHADOW_STEP,
    EVENT_DEADLY_POISON,
    EVENT_ENVENOMED_DAGGER_THROW,
    EVENT_KIDNEY_SHOT,
    
    // Warrior
    EVENT_MORTAL_STRIKE,
    EVENT_SMASH,
    EVENT_STAGGERING_ROAR,
    EVENT_ENRAGE,
    
    // Hunter
    EVENT_SHOOT,
    EVENT_CURSED_ARROW,
    EVENT_FROST_TRAP,
    EVENT_ICE_SHOT
};

enum GuardSpells {
    SPELL_RESISTANT_SKIN            = 72723,
    // Priest
    SPELL_SHADOW_WORD_PAIN          = 72318,
    SPELL_CIRCLE_OF_DESTRUCTION     = 72320,
    SPELL_COWER_IN_FEAR             = 72321,
    SPELL_DARK_MENDING              = 72322,
    
    // Mage
    SPELL_FIREBALL                  = 72163,
    SPELL_FLAMESTRIKE               = 72169,
    SPELL_FROSTBOLT                 = 72166,
    SPELL_CHAINS_OF_ICE             = 72121,
    
    // Rogue
    SPELL_SHADOW_STEP               = 72326,
    SPELL_DEADLY_POISON             = 72329,
    SPELL_ENVENOMED_DAGGER_THROW    = 72333,
    SPELL_KIDNEY_SHOT               = 72335,
    
    // Warrior
    SPELL_MORTAL_STRIKE             = 44268,
    SPELL_SMASH                     = 42669,
    SPELL_STAGGERING_ROAR           = 42708,
    SPELL_ENRAGE                    = 42705,
    
    // Hunter
    SPELL_SHOOT                     = 72208,
    SPELL_CURSED_ARROW              = 72222,
    SPELL_FROST_TRAP                = 72215,
    SPELL_ICE_SHOT                  = 72268
};

class FriendlyTargetCheck
{
public:
    FriendlyTargetCheck(Unit* source) : _source(source) { }

    bool operator()(Unit* target) const
    {
        return target->GetReactionTo(_source) >= REP_FRIENDLY;
    }

private:
    Unit* _source;
};

struct WorldPvPGuardAI: public ScriptedAI
{
    WorldPvPGuardAI(Creature* creature) : ScriptedAI(creature), checkBoundaryTimer(CHECK_BOUNDARY_TIMER)
    {
        me->SetIgnoreLOS(true);
        areaAffiliation = me->GetAreaAffiliation();

        healthMod = sWorldPvPMgr->GetModifierValue(areaAffiliation, 0.18, 1.2);
        dmgMod = sWorldPvPMgr->GetModifierValue(areaAffiliation, 0.3, 1.2);

        maxHealth = me->GetMaxHealth() * healthMod;
    }

    void Reset()
    {
        me->SetMaxHealth(maxHealth);
        me->SetFullHealth();

        events.Reset();
    }

    void _EnterCombat(Unit* /*who*/)
    {
        DoCast(me, SPELL_RESISTANT_SKIN);
        sWorldPvPMgr->BroadcastAreaUnderAttack(areaAffiliation);
    }

    void ModDamageDone(Unit* /*victim*/, uint32& damage)
    {
        damage *= dmgMod;
    }

    bool IsWithinBoundary(uint32 diff)
    {
        checkBoundaryTimer.Update(diff);
        if (checkBoundaryTimer.Passed()) {
            checkBoundaryTimer.Reset(CHECK_BOUNDARY_TIMER);
            if (me->GetAreaId() != areaAffiliation &&
                me->GetDistance(me->GetHomePosition()) > BOUNDARY)
                return false;
        }

        return true;
    }

    void IsSummonedBy(Unit* /*summoner*/)
    {
        std::list<Player*> playerList = me->GetNearestPlayersList(50.0f);
        playerList.remove_if(FriendlyTargetCheck(me));
        std::list<Player*>::const_iterator randIt = playerList.begin();
        std::advance(randIt, urand(0, playerList.size() - 1));
        AttackStart(*randIt);
        me->AddThreat(*randIt, 1000000.0f);
    }

protected:
    EventMap events;
    TimeTracker checkBoundaryTimer;
    uint32 areaAffiliation;
    float healthMod;
    float dmgMod;
    uint32 maxHealth;
};

class guard_priest : public CreatureScript
{
public:
    guard_priest() : CreatureScript("guard_priest") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new guard_priestAI(creature);
    }

    struct guard_priestAI: public WorldPvPGuardAI
    {
        guard_priestAI(Creature* creature) : WorldPvPGuardAI(creature) { }

        void EnterCombat(Unit* who)
        {
            _EnterCombat(who);
            events.ScheduleEvent(EVENT_SHADOW_WORD_PAIN, urand(8000, 10000));
            events.ScheduleEvent(EVENT_CIRCLE_OF_DESTRUCTION, urand(12000, 15000));
            events.ScheduleEvent(EVENT_COWER_IN_FEAR, urand(25000, 30000));
            events.ScheduleEvent(EVENT_DARK_MENDING, urand(20000, 25000));
        }
    
        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            if (!IsWithinBoundary(diff)) {
                DoResetThreat();
                EnterEvadeMode();
            }

            events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_SHADOW_WORD_PAIN:
                        DoCastVictim(SPELL_SHADOW_WORD_PAIN);
                        events.ScheduleEvent(EVENT_SHADOW_WORD_PAIN, urand(8000, 10000));
                        return;
                    case EVENT_CIRCLE_OF_DESTRUCTION:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM))
                            DoCast(target, SPELL_CIRCLE_OF_DESTRUCTION);
                        events.ScheduleEvent(EVENT_CIRCLE_OF_DESTRUCTION, urand(12000, 15000));
                        return;
                    case EVENT_COWER_IN_FEAR:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM))
                            DoCast(target, SPELL_COWER_IN_FEAR);
                        events.ScheduleEvent(EVENT_COWER_IN_FEAR, urand(25000, 30000));
                        return;
                    case EVENT_DARK_MENDING:
                        if (Unit* target = DoSelectLowestHpFriendly(40, 50000))
                        {
                            DoCast(target, SPELL_DARK_MENDING);
                            events.ScheduleEvent(EVENT_DARK_MENDING, urand(20000, 25000));
                        }
                        else
                            events.ScheduleEvent(EVENT_DARK_MENDING, urand(5000, 7000));
                        return;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

class guard_mage : public CreatureScript
{
public:
    guard_mage() : CreatureScript("guard_mage") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new guard_mageAI(creature);
    }

    struct guard_mageAI: public WorldPvPGuardAI
    {
        guard_mageAI(Creature* creature) : WorldPvPGuardAI(creature) { }

        void EnterCombat(Unit* who)
        {
            _EnterCombat(who);
            events.ScheduleEvent(EVENT_FIREBALL, urand(10000, 15000));
            events.ScheduleEvent(EVENT_FLAMESTRIKE, urand(15000, 20000));
            events.ScheduleEvent(EVENT_FROSTBOLT, urand(5000, 10000));
            events.ScheduleEvent(EVENT_CHAINS_OF_ICE, urand(12000, 15000));
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            if (!IsWithinBoundary(diff)) {
                DoResetThreat();
                EnterEvadeMode();
            }

            events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_FIREBALL:
                        DoCastVictim(SPELL_FIREBALL);
                        events.ScheduleEvent(EVENT_FIREBALL, urand(10000, 15000));
                        return;
                    case EVENT_FLAMESTRIKE:
                        DoCast(SPELL_FLAMESTRIKE);
                        events.ScheduleEvent(EVENT_FLAMESTRIKE, urand(15000, 20000));
                        return;
                    case EVENT_FROSTBOLT:
                        DoCastVictim(SPELL_FROSTBOLT);
                        events.ScheduleEvent(EVENT_FROSTBOLT, urand(5000, 10000));
                        return;
                    case EVENT_CHAINS_OF_ICE:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM))
                            DoCast(target, SPELL_CHAINS_OF_ICE);
                        events.ScheduleEvent(EVENT_CHAINS_OF_ICE, urand(12000, 15000));
                        return;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

class guard_rogue : public CreatureScript
{
public:
    guard_rogue() : CreatureScript("guard_rogue") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new guard_rogueAI(creature);
    }

    struct guard_rogueAI: public WorldPvPGuardAI
    {
        guard_rogueAI(Creature* creature) : WorldPvPGuardAI(creature) { }

        void EnterCombat(Unit* who)
        {
            _EnterCombat(who);
            events.ScheduleEvent(EVENT_SHADOW_STEP, urand(8000, 12000));
            events.ScheduleEvent(EVENT_DEADLY_POISON, urand(5000, 10000));
            events.ScheduleEvent(EVENT_ENVENOMED_DAGGER_THROW, urand(15000, 20000));
            events.ScheduleEvent(EVENT_KIDNEY_SHOT, urand(25000, 30000));
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            if (!IsWithinBoundary(diff)) {
                DoResetThreat();
                EnterEvadeMode();
            }

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_SHADOW_STEP:
                        DoCast(SPELL_SHADOW_STEP);
                        events.ScheduleEvent(EVENT_SHADOW_STEP, urand(8000, 12000));
                        return;
                    case EVENT_DEADLY_POISON:
                        DoCastVictim(SPELL_DEADLY_POISON);
                        events.ScheduleEvent(EVENT_DEADLY_POISON, urand(5000, 10000));
                        return;
                    case EVENT_ENVENOMED_DAGGER_THROW:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM))
                            DoCast(target, SPELL_ENVENOMED_DAGGER_THROW);
                        events.ScheduleEvent(EVENT_ENVENOMED_DAGGER_THROW, urand(15000, 20000));
                        return;
                    case EVENT_KIDNEY_SHOT:
                        DoCastVictim(SPELL_KIDNEY_SHOT);
                        events.ScheduleEvent(EVENT_KIDNEY_SHOT, urand(25000, 30000));
                        return;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

class guard_warrior : public CreatureScript
{
public:
    guard_warrior() : CreatureScript("guard_warrior") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new guard_warriorAI(creature);
    }

    struct guard_warriorAI: public WorldPvPGuardAI
    {
        guard_warriorAI(Creature* creature) : WorldPvPGuardAI(creature) { }

        void EnterCombat(Unit* who)
        {
            _EnterCombat(who);
            events.ScheduleEvent(EVENT_MORTAL_STRIKE, urand(5000, 10000));
            events.ScheduleEvent(EVENT_STAGGERING_ROAR, urand(30000, 35000));
            events.ScheduleEvent(EVENT_ENRAGE, urand(7000, 14000));
            events.ScheduleEvent(EVENT_SMASH, urand(12000, 16000));
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            if (!IsWithinBoundary(diff)) {
                DoResetThreat();
                EnterEvadeMode();
            }

            events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_MORTAL_STRIKE:
                        DoCastVictim(SPELL_MORTAL_STRIKE);
                        events.ScheduleEvent(EVENT_MORTAL_STRIKE, urand(5000, 10000));
                        break;
                    case EVENT_STAGGERING_ROAR:
                        DoCast(me, SPELL_STAGGERING_ROAR);
                        events.ScheduleEvent(EVENT_STAGGERING_ROAR, urand(30000, 35000));
                        break;
                    case EVENT_ENRAGE:
                        DoCast(me, SPELL_ENRAGE);
                        events.ScheduleEvent(EVENT_ENRAGE, urand(7000, 14000));
                        break;
                    case EVENT_SMASH:
                        DoCastAOE(SPELL_SMASH);
                        events.ScheduleEvent(EVENT_SMASH, urand(12000, 16000));
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

class guard_hunter : public CreatureScript
{
public:
    guard_hunter() : CreatureScript("guard_hunter") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new guard_hunterAI(creature);
    }

    struct guard_hunterAI  : public WorldPvPGuardAI
    {
        guard_hunterAI(Creature* creature) : WorldPvPGuardAI(creature) { }

        void EnterCombat(Unit* who)
        {
            _EnterCombat(who);
            events.ScheduleEvent(EVENT_SHOOT, urand(2000, 5000));
            events.ScheduleEvent(EVENT_CURSED_ARROW, urand(10000, 15000));
            events.ScheduleEvent(EVENT_FROST_TRAP, urand(1000, 3000));
            events.ScheduleEvent(EVENT_ICE_SHOT, urand(30000, 35000));
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            if (!IsWithinBoundary(diff)) {
                DoResetThreat();
                EnterEvadeMode();
            }

            events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_SHOOT:
                        DoCastVictim(SPELL_SHOOT);
                        events.ScheduleEvent(EVENT_SHOOT, urand(2000, 5000));
                        return;
                    case EVENT_CURSED_ARROW:
                        DoCastVictim(SPELL_CURSED_ARROW);
                        events.ScheduleEvent(EVENT_CURSED_ARROW, urand(10000, 15000));
                        return;
                    case EVENT_FROST_TRAP:
                        DoCast(SPELL_FROST_TRAP);
                        events.ScheduleEvent(EVENT_FROST_TRAP, urand(20000, 25000));
                        return;
                    case EVENT_ICE_SHOT:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM))
                            DoCast(target, SPELL_ICE_SHOT);
                        events.ScheduleEvent(EVENT_ICE_SHOT, urand(30000, 35000));
                        return;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

void AddSC_stronkcc_world_pvp_guards()
{
    new guard_priest();
    new guard_mage();
    new guard_rogue();
    new guard_warrior();
    new guard_hunter();
}