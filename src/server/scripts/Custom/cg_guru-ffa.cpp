/*
 * Crusade Gaming, -- Gurubashi Free For All Event Script --
 * (c) 2012 Crusade Gaming
 * By Nyse (Edgeworth, e@cacaw.net) w. Help from Valcorb
 * 
 */

#include "ScriptPCH.h"
#include "MapManager.h"
#include "Group.h"
#include <set>
#include <map>
#include <list>
#include <vector>
#include <algorithm>

namespace GurubashiFreeForAll
{

    enum PlayerState
    {
        IN_SPAWN_POINT  = 1,
        ACTIVE_PLAYER   = 2,
        INACTIVE_PLAYER = 3,
    };
    
    class EventScript;
    
    EventScript *guruFfaEventScript = NULL;
    
    struct PlayerInfo
    {
        PlayerState state;
        
        TimeTrackerSmall nextEventTimer;
        
        uint32 deaths;
        uint32 killingBlows;
        uint32 killStreak;
        uint32 kills;
        uint32 healingDone;
        uint32 damageDone;
        uint32 lastKiller;
        uint32 highestKillStreak;
        
        // map player guid -> damage done.
        std::map<uint32, uint32> attackers;
        
        
        PlayerInfo() : state(ACTIVE_PLAYER), deaths(0), killingBlows(0), killStreak(0),
        kills(0), healingDone(0), damageDone(0), lastKiller(0), highestKillStreak(0) { }
        
        inline bool hasGreaterKillStreak(PlayerInfo *other)
        {
            if(killStreak != other->killStreak)
                return killStreak > other->killStreak;
            
            if(deaths != other->deaths)
                return deaths < other->deaths;
            
            return kills > other->kills;
            
        }
        
        inline bool isGreaterOverall(PlayerInfo *other)
        {
            if(killingBlows != other->killingBlows)
                return killingBlows > other->killingBlows;
            
            if(highestKillStreak != other->highestKillStreak)
                return highestKillStreak > other->highestKillStreak;
            
            if(damageDone != other->damageDone)
                return damageDone > other->damageDone;
            
            return deaths < other->deaths;
            
        }
        
        // used by std::sort(), we sort by biggest first, so the < is a cheat.
        bool operator< (PlayerInfo *other)
        {
            if(killingBlows != other->killingBlows)
                return killingBlows > other->killingBlows;
            
            if(highestKillStreak != other->highestKillStreak)
                return highestKillStreak > other->highestKillStreak;
            
            return deaths < other->deaths;
            
        }
        
    };
    
    typedef std::pair<Player *, PlayerInfo> PlayerInfoPair;
    
    bool sortPlayerInfoPair(PlayerInfoPair a, PlayerInfoPair b)
    {
        return a.second.isGreaterOverall(&b.second);
    }
    
    const int numFogSpawns = 24;
    const Position fogSpawns[numFogSpawns] =
    {
        {-13188.5f, 292.977f, 21.8572f, 2.21561f},
        {-13183.1f, 290.225f, 21.8572f, 3.32695f},
        {-13174.7f, 286.763f, 21.8572f, 2.01141f},
        {-13174.9f, 282.364f, 21.8572f, 0.974681f},
        {-13181.6f, 271.655f, 21.8572f, 1.19459f},
        {-13190.4f, 251.55f, 21.8572f, 4.45793f},
        {-13188.2f, 258.188f, 21.8572f, 4.38724f},
        {-13186.7f, 263.657f, 21.8572f, 5.33757f},
        {-13193.9f, 263.073f, 21.8572f, 0.817602f},
        {-13198.6f, 257.868f, 21.8572f, 0.868653f},
        {-13206.6f, 254.175f, 21.8572f, 6.26434f},
        {-13216.7f, 251.233f, 21.8572f, 5.73027f},
        {-13230.6f, 249.511f, 21.8572f, 5.82452f},
        {-13232.8f, 253.601f, 21.8572f, 4.11628f},
        {-13225.0f, 261.064f, 21.8572f, 3.88459f},
        {-13219.7f, 265.722f, 21.8572f, 3.82176f},
        {-13211.7f, 270.43f, 21.8572f, 3.5665f},
        {-13196.8f, 273.681f, 21.8572f, 4.95273f},
        {-13206.9f, 283.147f, 21.8572f, 5.79702f},
        {-13220.3f, 294.959f, 21.8572f, 5.4161f},
        {-13191.2f, 301.473f, 21.8572f, 2.45123f},
        {-13198.0f, 303.901f, 21.8572f, 3.1306f},
        {-13205.0f, 299.3f, 21.8572f, 4.20659f},
        {-13216.0f, 303.817f, 21.8572f, 2.40018f},
    };
    
    const int numFireworkSpawns = 30;
    
    const float arenaSpawnRadius = 32.5f;
    const float spawnRadius =  8.0f;
    const float nearbyMessageRadius = 125.0f;
    
    
    const uint32 eventId = 91;
    const uint32 gurubashiZoneId = 33; // Gurubashi Arena
    const uint32 gurubashiAreaId = 2177; // Stranglethorn Vale
    const uint32 gurubashiMapId = 0; // Eastern Kingdoms
    
    const uint32 silenceDebuff = 27559;
    const uint32 invulnBuff = 9438;
    const uint32 hasteBuff = 43242;
    const uint32 speedBuff = 23451;
    const uint32 berserkBuff = 23505;
    const uint32 restorationBuff = 23493;
    const uint32 persuitBuff = 68987;
    
    const uint32 fogGameObjectId = 191371;
    const uint32 fireworkGameObjectId = 181886;
    const uint32 respawnWaitInterval = 6 * IN_MILLISECONDS;
    const uint32 disqualifyWaitInterval = 30 * IN_MILLISECONDS;
    
    
    const WorldLocation centerOfArena(gurubashiMapId, -13204.2f, 278.436f, 21.8579f, 0.0f);
    const Position allianceSpawn = {-13207.225f, 327.787f, 21.857f,  4.71f };
    const Position hordeSpawn =    {-13158.56f,  301.19f,  21.857f,  3.76f };
    
    inline bool playerInArenaAndFlagged(Player *player)
    {
        uint32 zoneId, areaId;
        player->GetZoneAndAreaId(zoneId, areaId);
        
        return (
            player->IsInWorld() &&
            player->GetMapId() == gurubashiMapId && 
            zoneId == gurubashiZoneId && 
            areaId == gurubashiAreaId && 
            player->HasByteFlag(UNIT_FIELD_BYTES_2, 1, UNIT_BYTE2_FLAG_FFA_PVP)
        );
    }
    
    inline void teleportPlayerToSpawn(Player *player)
    {
        const Position destination = 
                player->GetTeam() == ALLIANCE ? allianceSpawn : hordeSpawn;
        
        float x, y, z, o;
        destination.GetPosition(x, y, z, o);
        player->TeleportTo(gurubashiMapId, x, y, z, o);
        
    }
    
    inline bool isNearSpawnPoint(Player *player)
    {
        Position const* spawnPoint = player->GetTeam() == ALLIANCE ? &allianceSpawn : &hordeSpawn;
        
        return (
                player->IsInWorld() &&
                player->GetMapId() == gurubashiMapId &&
                player->GetZoneId() == gurubashiZoneId &&
                player->IsInDist2d(spawnPoint, spawnRadius)
        );
        
    }
    
    inline void randomArenaPoint(float &x, float &y, float &z, float &o)
    {
        o = frand(0.0f, 6.28f);
        
        float h, k;
        centerOfArena.GetPosition(h, k, z);
        
        float radians = frand(0.0f, 6.28f);
        float radius = arenaSpawnRadius * sqrt(rand_norm());
        
        x = radius * cos(radians) + h;
        y = radius * sin(radians) + k;
        
    }
    
    inline void notifyImpendingDisqualify(PlayerInfo *state, Player* player)
    {
        state->state = INACTIVE_PLAYER;
        state->nextEventTimer.Reset(disqualifyWaitInterval);
        player->GetSession()->SendAreaTriggerMessage("You have left the arena zone, "
                "and will be removed from the event in 30 seconds if you do not return!");
        
    }
    
    void sendServerMessage(Player *player, const char* Text, ...)
    {
        va_list ap;
        char szStr [1024];
        szStr[0] = '\0';

        va_start(ap, Text);
        vsnprintf(szStr, 1024, Text, ap);
        va_end(ap);

        uint32 length = strlen(szStr)+1;
        WorldPacket data(SMSG_SERVER_MESSAGE, 4+length);
        data << uint32(SERVER_MSG_STRING);
        data << szStr;
        
        player->GetSession()->SendPacket(&data);
    }
    
    void makeServerMessage(WorldPacket &data, const char* Text, ...)
    {
        va_list ap;
        char szStr [1024];
        szStr[0] = '\0';

        va_start(ap, Text);
        vsnprintf(szStr, 1024, Text, ap);
        va_end(ap);

        uint32 length = strlen(szStr)+1;
        data.Initialize(SMSG_SERVER_MESSAGE, 4+length);
        data << uint32(SERVER_MSG_STRING);
        data << szStr;
    }
    
    void makeAreaTriggerMessage(WorldPacket &data, const char* Text, ...)
    {
        va_list ap;
        char szStr [1024];
        szStr[0] = '\0';

        va_start(ap, Text);
        vsnprintf(szStr, 1024, Text, ap);
        va_end(ap);

        uint32 length = strlen(szStr)+1;
        data.Initialize(SMSG_AREA_TRIGGER_MESSAGE, 4+length);
        data << length;
        data << szStr;
    }
    
    class EventScript : public ::PlayerScript, public ::WorldScript
    {
    private:
        bool m_isActive;
        bool m_fogIsSpawned;
        bool m_fireworksActive;
        
        PeriodicTimer m_periodicTimer;
        TimeTracker m_gameRemainingTime;
        TimeTracker m_fogTimer;
        TimeTrackerSmall m_fireworksTimer;
        
        GameObject * m_invisibleAnnouncer;
        
        int m_startTime;
        int m_totalTime;
        
        uint32 diffBacklog;
        
        Player *m_mostKillStreakPlayer;
        
        std::map<Player *, PlayerInfo> m_players;
        std::list<GameObject *> m_spawnedGameObjects;
        
    public:
        EventScript() : 
            PlayerScript("GurubashiFreeForAllEventScriptPlayerScript"),
            WorldScript("GurubashiFreeForAllEventScriptWorldScript"), 
            m_isActive(false),
            m_fogIsSpawned(false),
            m_fireworksActive(false),
            m_periodicTimer(1000, 0),
            m_gameRemainingTime(0),
            m_fogTimer(0),
            m_fireworksTimer(0),
            m_invisibleAnnouncer(NULL),
            diffBacklog(0),
            m_mostKillStreakPlayer(NULL)
        { 
            
        }
            
        void OnLogout(Player *player)
        {
            if(!m_isActive) return;
            
            std::map<Player*, PlayerInfo>::iterator iter = m_players.find(player);
            if(iter != m_players.end())
            {
                notifyDisqualifyPlayer(&iter->second, iter->first, false);
                m_players.erase(iter);
            }
            
            if(player == m_mostKillStreakPlayer)
            {
                m_mostKillStreakPlayer = findHighestKillStreakPlayer();
                notifyNewHighestKillStreakPlayer();
            }
        }
        
        //void OnUpdateZone(Player * player, uint32, uint32) {
        //    
        //}
        
        void OnUpdate(uint32 diff)
        {
            if(!m_isActive) return;
            diffBacklog += diff;
            m_gameRemainingTime.Update(diff);
            if(m_gameRemainingTime.Passed()) {
                stopEvent();
                return;
            }
            if(!m_periodicTimer.Update(diff)) return;
            
            m_fogTimer.Update(diffBacklog);
            if(m_fogTimer.Passed()) {
                procFogTimer();
            }
            
            if(m_fireworksActive)
            {
                m_fireworksTimer.Update(diffBacklog);
                if(m_fireworksTimer.Passed()) {
                    m_fireworksActive = false;
                    deleteGameObjectsOfEntry(fireworkGameObjectId);
                }
            }
            
            for(std::map<Player *, PlayerInfo>::iterator iter = m_players.begin(); iter != m_players.end(); ++iter)
            {
                PlayerInfo *state = &iter->second;
                Player *player = iter->first;
                if(Group *group = player->GetGroup())
                {
                    sendServerMessage(player, "You have been removed from your group. You can join it when the event is over, or if you leave the event.");
                    group->RemoveMember(player->GetGUID(), GROUP_REMOVEMETHOD_LEAVE);
                }
                
                switch(state->state)
                {
                    case IN_SPAWN_POINT:
                        if(isNearSpawnPoint(player))
                        {
                            // if the player is in the spawn point and near it, update their timer.
                            state->nextEventTimer.Update(diffBacklog);
                            if(state->nextEventTimer.Passed())
                            {
                                makePlayerActive(state, player);
                                player->AddAura(silenceDebuff, player);
                            }
                        }
                        else
                        {
                            sLog->outInfo(LOG_FILTER_MAPSCRIPTS,
                                          "%s is not near the spawn! Is %f away.", player->GetName().c_str(),
                                          player->GetExactDist2d((player->GetTeam() == ALLIANCE) ? &allianceSpawn : &hordeSpawn));
                            teleportPlayerToSpawn(player);
                        }
                        break;
                    
                    case INACTIVE_PLAYER:
                        if(playerInArenaAndFlagged(player))
                        {
                            // if the player was inactive, and has returned to the arena,
                            // mark them as active.
                            state->state = ACTIVE_PLAYER;
                            // notify they are active again.
                            player->GetSession()->SendAreaTriggerMessage("You have re-entered the arena area!");
                        }
                        else
                        {
                            state->nextEventTimer.Update(diffBacklog);
                            if(state->nextEventTimer.Passed())
                            {
                                notifyDisqualifyPlayer(state, player);
                                m_players.erase(iter);
                            }
                        }
                        break;
            
                    case ACTIVE_PLAYER:
                        if(!playerInArenaAndFlagged(player))
                            // if the player was once active, and left the arena,
                            // mark them as inactive, and schedule their disqualification.
                            notifyImpendingDisqualify(state, player);
                        
                        else if (!player->IsAlive())
                            // sometimes player doesn't rezz, this will check if they're active
                            // and dead, which means they still need to be rezzed.
                            resurrectPlayer(player, state);
                        
                        else if (player == m_mostKillStreakPlayer)
                            // renew the persuit buff on the highest KS player.
                            player->AddAura(persuitBuff, player);

                        break;
                }
            
            }
            diffBacklog = 0;
        }
        
        void OnDealDamage(Player* attacker, Player* victim, uint32 amount)
        {
            if(
                !m_isActive || 
                !playerInArenaAndFlagged(attacker) || 
                !playerInArenaAndFlagged(victim)
            ) return;
            
            PlayerInfo &victimInfo = getPlayerInfo(victim);
            victimInfo.attackers[attacker->GetGUIDLow()] += amount;
            
            PlayerInfo &attackerInfo = getPlayerInfo(attacker);
            attackerInfo.damageDone += amount;
            
        }
        
        void OnPVPKill(Player* killer, Player* killed)
        {
            if(
                !m_isActive || 
                !playerInArenaAndFlagged(killer) || 
                !playerInArenaAndFlagged(killed)
            ) return;
            
            PlayerInfo &killerInfo = getPlayerInfo(killer);
            PlayerInfo &killedInfo = getPlayerInfo(killed);
            uint32 killerGuid = killer->GetGUIDLow();
            uint32 killedGuid = killed->GetGUIDLow();
            
            killedInfo.deaths++;
            killedInfo.killStreak = 0;
            
            if(killer != killed)
            {
                killedInfo.lastKiller = killerGuid;
                killerInfo.killingBlows++;
                killerInfo.killStreak++;
                killerInfo.kills++;
                if(killerInfo.killStreak > killerInfo.highestKillStreak)
                    killerInfo.highestKillStreak = killerInfo.killStreak;
            }
            
            // We loop over everyone who's dealt damage to this poor soul,
            // and award them a kill.
            for(std::map<uint32, uint32>::const_iterator iter = killedInfo.attackers.begin(), 
                                                         end = killedInfo.attackers.end(); 
                                                         iter != end; ++iter)
            {
                
                if(Player *player = sObjectAccessor->FindPlayer(iter->first))
                {
                    if(player == killer)
                        continue;
                    if(PlayerInfo *playerInfo = findPlayerInfo(player))
                        playerInfo->kills++;
                }
                
            }
            
            killedInfo.attackers.clear();
            
            // Revive the player in the spawn point.
            resurrectPlayer(killed, &killedInfo);
            
            if(killer == killed)
                sendServerMessage(killed, "You have killed yourself???");
            else
                sendServerMessage(killed, "You have been killed by %s.", killer->GetName().c_str());
            
            sendServerMessage(killed, "You have been revived in a safe zone and will be ported back into the arena in 6 seconds.");
            
            if(killer == killed)
                return;
            
            uint32 overallRank = calculateOverallRank(&killerInfo, killer);
            uint32 killStreakRank = calculateKillStreakRank(&killerInfo, killer);
            
            sendServerMessage(killer, "You have landed the killing blow on %s (%u kills, %u killing blows, %u deaths, %u kill-streak)", 
                    killed->GetName().c_str(), killedInfo.kills, killedInfo.killingBlows, killedInfo.deaths, killedInfo.killStreak);
            sendServerMessage(killer, "You are now rank %u, with a kill-streak rank of %u. Your stats are as follows:", overallRank, killStreakRank);
            sendServerMessage(killer, "%u kills, %u killing blows, %u deaths, %u kill-streak, %u damage done", 
                    killerInfo.kills, killerInfo.killingBlows, killerInfo.deaths, killerInfo.killStreak, killerInfo.damageDone);
            
            if(killed == m_mostKillStreakPlayer)
            {
                killer->AddAura(berserkBuff, killer);
                sendServerMessage(killer, "You have earned Berserking for killing the player with the highest kill-streak!");
                spawnFireWorks();
            }
            
            if(killedGuid == killerInfo.lastKiller)
            {
                killer->AddAura(speedBuff, killer);
                sendServerMessage(killer, "You have earned Speed for exacting revenge on your killer!");
                killerInfo.lastKiller = 0;
            }
            
            if(killerInfo.killStreak % 4 == 0)
            {
                killer->AddAura(restorationBuff, killer);
                sendServerMessage(killer, "You have earned Restoration for reaching a %u kill-streak!", killerInfo.killStreak);
            }
            
            Player * oldMostKillStreakPlayer = m_mostKillStreakPlayer;
            m_mostKillStreakPlayer = findHighestKillStreakPlayer();
            if(oldMostKillStreakPlayer != m_mostKillStreakPlayer)
            {
                if(oldMostKillStreakPlayer)
                    oldMostKillStreakPlayer->RemoveAura(persuitBuff);
                notifyNewHighestKillStreakPlayer();
            }
            
            WorldPacket data;
            makeAreaTriggerMessage(data, "%s has killed %s and now has a %u kill streak!", killer->GetName().c_str(), killed->GetName().c_str(), killerInfo.killStreak);
            SendPacketToPlayersNearArena(data, killer);
            killer->GetSession()->SendAreaTriggerMessage("You have killed %s, your kill-streak is now %u!", killed->GetName().c_str(), killerInfo.killStreak);
            
        }
        
        void startEvent(uint32 eventDuration)
        {
            if(m_isActive) return;
            
            m_mostKillStreakPlayer = NULL;
            
            m_fogTimer.Reset(urand(2 * MINUTE * IN_MILLISECONDS, 5 * MINUTE * IN_MILLISECONDS));
            m_gameRemainingTime.Reset(eventDuration);
            float x, y, z, o; 
            centerOfArena.GetPosition(x, y, z, o);
            
            // this Game Object serves as the object which our visitor uses to send notifications out
            m_invisibleAnnouncer = spawnGameObject(185879, gurubashiMapId, x, y, z, o);
            
            WorldPacket data;
            makeAreaTriggerMessage(data, "The Gurubashi Arena FFA-PvP event has started, and will last for %u minutes!", eventDuration / (IN_MILLISECONDS * MINUTE));
            SendPacketToPlayersNearArena(data);
            m_isActive = true;
            
            startStopEvent(true);
            spawnFireWorks(15000);
            
        }
        
        void stopEvent(bool IsGMStop = false)
        {
            if(!m_isActive) return;
            m_isActive = false;
            
            WorldPacket data;
            if(IsGMStop)
                makeAreaTriggerMessage(data, "THE EVENT HAS BEEN ABORTED!!! Check your chat log for the scoreboard!");
            else
                makeAreaTriggerMessage(data, "THE EVENT IS OVER!!! Check your chat log for the scoreboard!");
            SendPacketToPlayersNearArena(data);
            
            AnnounceStatus();
            
            float x, y, z, o;
            
            for(std::map<Player *, PlayerInfo>::iterator iter = m_players.begin(); iter != m_players.end(); ++iter)
            {
                if(!iter->first->IsAlive()) // if the player is dead, revive them with full health.
                    iter->first->ResurrectPlayer(1.0f, false);
                else
                {
                    // else, pseudo-kill them, removing all the buffs/debuffs that shouldn't persist thru death, like they're dead.
                    iter->first->RemoveAllAurasOnDeath();
                    iter->first->SetHealth(iter->first->GetMaxHealth());
                }
                // If the player isn't in the arena, or is in the spawning state, teleport them to
                // a point in the arena before freezing them.
                if(!playerInArenaAndFlagged(iter->first) || iter->second.state == IN_SPAWN_POINT)
                {
                    randomArenaPoint(x, y, z, o);
                    iter->first->TeleportTo(gurubashiMapId, x, y, z, o);
                }
                
                freezePlayer(iter->first);
            }
            
            startStopEvent(false);
            cleanUpEvent();
        }
        
        bool isActive() { return m_isActive; }
        
        bool isFogSpawned() { return m_fogIsSpawned; }
        
        void toggleFogManually() { procFogTimer(); }
        
        void AnnounceStatus(Player *target = NULL)
        {
            std::vector<PlayerInfoPair> players(m_players.begin(), m_players.end());
            std::sort(players.begin(), players.end(), sortPlayerInfoPair);
            
            uint32 i = 1;
            WorldPacket data;
            
            for(std::vector<PlayerInfoPair>::iterator iter = players.begin(); iter != players.end(); ++iter)
            {
                makeServerMessage(data, "%u: %s (%u killing blows, %u kills, %u deaths, %u highest kill-streak, %u damage done).",
                        i, iter->first->GetName().c_str(), iter->second.killingBlows, iter->second.kills, iter->second.deaths, 
                        iter->second.highestKillStreak, iter->second.damageDone);
                
                if(target)
                    target->GetSession()->SendPacket(&data);
                else 
                    SendPacketToPlayersNearArena(data);
                
                i++;
            }
        }
        
    private:
        void resurrectPlayer(Player *player, PlayerInfo *state)
        {
            state->state = IN_SPAWN_POINT;
            state->nextEventTimer.Reset(respawnWaitInterval);
            teleportPlayerToSpawn(player);
            player->ResurrectPlayer(.85f, false);
            player->AddAura(invulnBuff, player);
            player->AddAura(hasteBuff, player);
            player->AddAura(restorationBuff, player);
        }
        
        void startStopEvent(bool start = true)
        {
            GameEventMgr::GameEventDataMap const& events = sGameEventMgr->GetEventMap();

            if (eventId >= events.size()) return;

            GameEventData const& eventData = events[eventId];
            if (!eventData.isValid()) return;
            
            GameEventMgr::ActiveEvents const& activeEvents = sGameEventMgr->GetActiveEventList();
            bool eventIsActive = activeEvents.find(eventId) != activeEvents.end();

            if (start && !eventIsActive)
                sGameEventMgr->StartEvent(eventId, true);
            else if(!start && eventIsActive)
                sGameEventMgr->StopEvent(eventId, true);
        }
        
        void procFogTimer()
        {
            uint32 fogDuration;
            
            WorldPacket data;
            if(m_fogIsSpawned)
            {
                deleteGameObjectsOfEntry(fogGameObjectId);
                m_fogIsSpawned = false;
                makeAreaTriggerMessage(data, "The fog disappears leaving the battlefield visible!");
            }
            else
            {
                bool heavyFog = irand(0, 1);
                spawnFog(heavyFog);
                if(heavyFog)
                    makeAreaTriggerMessage(data, "A very heavy fog appears in the arena, obscuring your view.");
                else
                    makeAreaTriggerMessage(data, "A fog appears in the arena, obscuring your view.");
            }
            
            SendPacketToPlayersNearArena(data);
            
            if(m_fogIsSpawned) 
                fogDuration = urand(30 * IN_MILLISECONDS, 45 * IN_MILLISECONDS);
            else
                fogDuration = urand(MINUTE * IN_MILLISECONDS, 3 * MINUTE * IN_MILLISECONDS);
            
            m_fogTimer.Reset(fogDuration);
        }
        
        uint32 calculateOverallRank(PlayerInfo *state, Player *player)
        {
            uint32 rank = m_players.size();
            
            for(std::map<Player*, PlayerInfo>::iterator iter = m_players.begin(); iter != m_players.end(); ++iter)
            {
                if(iter->first == player)
                    continue;
                if(state->isGreaterOverall(&iter->second))
                    rank--;
                
            }
            return rank;
            
        }
        
        uint32 calculateKillStreakRank(PlayerInfo *state, Player *player)
        {
            uint32 rank = m_players.size();
            
            for(std::map<Player*, PlayerInfo>::iterator iter = m_players.begin(); iter != m_players.end(); ++iter)
            {
                if(iter->first == player)
                    continue;
                if(state->hasGreaterKillStreak(&iter->second))
                    rank--;
                
            }
            return rank;
            
        }
        
        void SendPacketToPlayersNearArena(WorldPacket &packet, Player const* skipped = NULL)
        {
            if(!m_invisibleAnnouncer)
                return;
            
            std::list<Player* > nearbyPlayers = m_invisibleAnnouncer->GetNearestPlayersList(nearbyMessageRadius, false);
            if(nearbyPlayers.empty())
                return;
            
            for(std::list<Player *>::iterator iter = nearbyPlayers.begin(),end = nearbyPlayers.end(); iter != end; ++iter)
            {
                if(*iter == skipped)
                    continue;
                
                if(WorldSession *s = (*iter)->GetSession())
                    s->SendPacket(&packet);
            }
        }
        
        inline void makePlayerActive(PlayerInfo *state, Player *player)
        {
            // send respawn message
            state->state = ACTIVE_PLAYER;

            float x, y, z, o;
            randomArenaPoint(x, y, z, o);
            player->RemoveAura(hasteBuff);
            player->RemoveAura(restorationBuff);
            
            if(m_mostKillStreakPlayer != NULL)
            {
                player->SetTarget(m_mostKillStreakPlayer->GetGUID());
                Position destPos = {x, y, z, o};
                o = destPos.GetAngle(m_mostKillStreakPlayer); 
            }    
            player->TeleportTo(gurubashiMapId, x, y, z, o);
        
        }
        
        void notifyDisqualifyPlayer(PlayerInfo *state, Player* player, bool toSelf = true)
        {
            if(toSelf)
                player->GetSession()->SendAreaTriggerMessage("You have left the arena for too long and have been disqualified!");
            
            WorldPacket data;
            makeAreaTriggerMessage(data, "%s (%u kills, %u killing blows, %u deaths, %u kill-streak) "
                    "has left the arena for too long and has been disqualified!", 
                    player->GetName().c_str(), state->kills, state->killingBlows, state->deaths, state->killStreak);
            
            SendPacketToPlayersNearArena(data, player);
        
        }
        
        void notifyNewHighestKillStreakPlayer()
        {
            if(!m_mostKillStreakPlayer) return;
            PlayerInfo *state = findPlayerInfo(m_mostKillStreakPlayer);
            if(!state) return;
            
            m_mostKillStreakPlayer->GetSession()->SendAreaTriggerMessage("You are now the character with the highest kill streak (%u kill-streak), watch out! You're going to be the default target of all re-spawned players.", state->killStreak);
            m_mostKillStreakPlayer->AddAura(persuitBuff, m_mostKillStreakPlayer);
            
            WorldPacket data;
            makeAreaTriggerMessage(data, "%s (%u kill-streak) "
                    "is now the player with the highest kill-streak! Kill him quickly for a power-up!!!", 
                    m_mostKillStreakPlayer->GetName().c_str(), state->killStreak);
            
            SendPacketToPlayersNearArena(data, m_mostKillStreakPlayer);
            
        }
        
        inline PlayerInfo& getPlayerInfo(Player* player)
        {
            return m_players[player];
        }

        inline PlayerInfo* findPlayerInfo(Player *player)
        {
            std::map<Player *, PlayerInfo>::iterator iter = m_players.find(player);
            return (iter != m_players.end()) ? &iter->second : NULL;
        }
        
        inline Player* findHighestKillStreakPlayer()
        {
            Player *player = NULL;
            PlayerInfo *highestState = NULL;
            
            for(std::map<Player*, PlayerInfo>::iterator iter = m_players.begin(); iter != m_players.end(); ++iter)
            {
                if(iter->second.state != ACTIVE_PLAYER)
                    continue;
                
                if(!highestState)
                {
                    highestState = &iter->second;
                    player = iter->first;
                }
                else
                {
                    if(iter->second.hasGreaterKillStreak(highestState))
                    {
                        player = iter->first;
                        highestState = &iter->second;                        
                    }
                }
            }
            
            return (highestState && highestState->killStreak) ? player : NULL;
            
        }
        
        void cleanUpEvent()
        {
            m_mostKillStreakPlayer = NULL;
            m_invisibleAnnouncer = NULL;
            m_fireworksActive = false;
            m_fogIsSpawned = false;
            m_players.clear();
            deleteGameObjects();
        }
        
        void freezePlayer(Player *player)
        {
            if(!player) return;
            player->setFaction(35);
            player->CombatStop();
            if (player->IsNonMeleeSpellCasted(true))
                player->InterruptNonMeleeSpells(true);
            player->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);

            //if player class = hunter || warlock remove pet if alive
            if ((player->getClass() == CLASS_HUNTER) || (player->getClass() == CLASS_WARLOCK))
            {
                if (Pet* pet = player->GetPet())
                {
                    pet->SavePetToDB(PET_SAVE_AS_CURRENT);
                    // not let dismiss dead pet
                    if (pet && pet->IsAlive())
                        player->RemovePet(pet, PET_SAVE_NOT_IN_SLOT);
                }
            }

            //m_session->GetPlayer()->CastSpell(player, spellID, false);
            if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(9454))
                Aura::TryRefreshStackOrCreate(spellInfo, MAX_EFFECT_MASK, player, player);

            //save player
            player->SaveToDB();
        }
        
        GameObject *spawnGameObject(uint32 entry, uint32 mapId, float x, float y, float z, float o)
        {
            Map* map = sMapMgr->FindMap(mapId, 0);
            if(!map)
                return NULL;
            
            GameObjectTemplate const* goInfo = sObjectMgr->GetGameObjectTemplate(entry);
            if(!goInfo)
                return NULL;
            
            float rot2 = sin(o/2);
            float rot3 = cos(o/2);

            
            GameObject *go = new GameObject();
            uint32 guid = sObjectMgr->GenerateLowGuid(HIGHGUID_GAMEOBJECT);
            if(!go->Create(guid, entry, map, PHASEMASK_NORMAL, x, y, z, o, 0, 0, rot2, rot3, 100, GO_STATE_READY))
            {
                delete go;
                return NULL;
            }
            go->SetRespawnTime(300);
            go->SetSpawnedByDefault(false);
            
            map->AddToMap(go);
            
            m_spawnedGameObjects.push_back(go);
            
            return go;
        }
        
        void deleteGameObjectsOfEntry(uint32 entry)
        {
            for(std::list<GameObject *>::iterator iter = m_spawnedGameObjects.begin(); iter != m_spawnedGameObjects.end();)
            {
                GameObject *obj = *iter;
                if(obj->GetEntry() == entry)
                {
                    obj->SetRespawnTime(0);
                    obj->Delete();
                    iter = m_spawnedGameObjects.erase(iter);
                } else
                    iter++;
            }
        }
        
        void deleteGameObjects()
        {
            for(std::list<GameObject *>::iterator iter = m_spawnedGameObjects.begin(); iter != m_spawnedGameObjects.end();)
            {
                (*iter)->Delete();
                iter = m_spawnedGameObjects.erase(iter);
            }
        }
        
        void spawnFog(bool heavyFog)
        {
            if(!m_isActive || m_fogIsSpawned) return;
            float x, y, z, o;
            for(int i = 0; i < numFogSpawns; i++)
            {
                fogSpawns[i].GetPosition(x, y, z, o);
                spawnGameObject(fogGameObjectId, gurubashiMapId, x, y, z, o);
            }
            
            if(heavyFog)
                for(int i = 0; i < numFogSpawns; i++)
                {
                    fogSpawns[i].GetPosition(x, y, z, o);
                    spawnGameObject(fogGameObjectId, gurubashiMapId, x, y, z + 2.0f, o);
                }
            
            m_fogIsSpawned = true;
            
        }
        
        void spawnFireWorks(int32 spawnDuration = 15000)
        {
            if(!m_isActive) return;
            if (!m_fireworksActive)
            {
                float x, y, z, o;
                for(int i = 0; i < numFireworkSpawns; i++)
                {
                    randomArenaPoint(x, y, z, o);
                    spawnGameObject(fireworkGameObjectId, gurubashiMapId, x, y, z, o);
                }

                m_fireworksActive = true;
            }
            if(m_fireworksTimer.GetExpiry() < spawnDuration)
                m_fireworksTimer.Reset(spawnDuration);
        }
    };

    class CommandScript : public ::CommandScript
    {
    public:
        CommandScript() : ::CommandScript("GurubashiFreeForAllCommandScript") { }
        
        ChatCommand* GetCommands() const
        {
            static ChatCommand guruFfaTable[] =
            {
                { "start",      SEC_GAMEMASTER,         false, &HandleStartEventCommand, "", NULL },
                { "stop",       SEC_GAMEMASTER,         false, &HandleStopEventCommand,  "", NULL },
                { "status",     SEC_GAMEMASTER,         false, &HandleStatusEventCommand,"", NULL },
                { "fog",        SEC_GAMEMASTER,         false, &HandleToggleFogCommand,  "", NULL },
                { NULL,         0,                      false, NULL,                     "", NULL }
            };
            
            static ChatCommand commandTable[] =
            {
                { "guru-ffa",   SEC_GAMEMASTER,         true,  NULL,    "", guruFfaTable          },
                { NULL,         0,                      false, NULL,                     "", NULL }
            };
            
            return commandTable;
            
        }
        
        static bool HandleStartEventCommand(ChatHandler *handler, const char* args)
        {
            if(guruFfaEventScript->isActive())
            {
                handler->SendSysMessage("The event is currently active, you cannot start it again!");
                handler->SetSentErrorMessage(true);
                return false;
            }
            
            if(!args) return false;
            
            uint32 eventDuration = atoi(args);
            
            if(eventDuration > 0 && eventDuration <= 45)
            {
                guruFfaEventScript->startEvent(eventDuration * MINUTE * IN_MILLISECONDS);
                return true;
            }
            else
            {
                handler->SendSysMessage("event duration must be between 0 and 45.");
                handler->SetSentErrorMessage(true);
                return false;
            }
            
            
        }
        
        static bool HandleStopEventCommand(ChatHandler *handler, const char* /*args*/) {
            if(!guruFfaEventScript->isActive())
            {
                handler->SendSysMessage("The event not currently active.");
                handler->SetSentErrorMessage(true);
                return false;
            }
            
            guruFfaEventScript->stopEvent(true);
            return true;
        }
        
        static bool HandleStatusEventCommand(ChatHandler *handler, const char* /*args*/) {
            if(!guruFfaEventScript->isActive())
            {
                handler->SendSysMessage("The event not currently active.");
                handler->SetSentErrorMessage(true);
                return false;
            }
            
            handler->PSendSysMessage("Fog is %s", guruFfaEventScript->isFogSpawned() ? "on" : "off");
            Player *p = handler->GetSession()->GetPlayer();
            guruFfaEventScript->AnnounceStatus(p);
            return true;
            
        }
        
        static bool HandleToggleFogCommand(ChatHandler *handler, const char* /*args*/)
        {
            if(!guruFfaEventScript->isActive())
            {
                handler->SendSysMessage("The event not currently active.");
                handler->SetSentErrorMessage(true);
                return false;
            }
            
            guruFfaEventScript->toggleFogManually();
            handler->PSendSysMessage("Fog is now %s", guruFfaEventScript->isFogSpawned() ? "on" : "off");
            return true;
        }

    };
}

void AddSC_GurubashiFreeForAll()
{
    GurubashiFreeForAll::guruFfaEventScript = new GurubashiFreeForAll::EventScript();
    new GurubashiFreeForAll::CommandScript();
}
