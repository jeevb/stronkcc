#include "Guild.h"
#include "ScriptPCH.h"

#define TIME_LIMIT 7000 // 7 seconds maximum between each kill for it to count
#define MAKE_MESSAGE(type) ChatHandler::FillMessageData(&data, session, type, LANG_UNIVERSAL, NULL, 0, msg2, NULL)

typedef std::map<uint64, uint32> PlayerDamage;
typedef std::deque<uint64> PlayerList;

enum KillStreakTypes {
    KS_SELF,
    KS_AREA,
    KS_AREA_NOSELF,
    KS_WORLD,
    KS_WORLD_NOSELF,
    KS_RAIDWARN,
    KS_WORLD_RAIDWARN,
    KS_WORLD_RAIDWARN_NOSELF
};

struct KillStreakInfo {
    uint16 m_killingSpree;
    PlayerDamage m_damagers;
    PlayerDamage m_healers;
    PlayerList m_lastVictims;
    PlayerList m_lastKillers;
    uint32 m_lastTime;
    uint8 m_multikillCount;
};

class stronkcc_killstreak : public PlayerScript 
{
private:
    std::map<uint64, KillStreakInfo> m_killStreakData;
    
    void CleanupKillstreakInfo(KillStreakInfo* killStreakData, bool resetAll) {
        if(!killStreakData)
            return;
        killStreakData->m_killingSpree = 0;
        killStreakData->m_damagers.clear();
        killStreakData->m_healers.clear();
        killStreakData->m_lastTime = 0;
        killStreakData->m_multikillCount = 0;
        if (resetAll) {
            killStreakData->m_lastKillers.clear();
            killStreakData->m_lastVictims.clear();
        }
    }
    
    std::string CreateGoldString(uint32 money) {
        uint32 gold = money / 10000, silver = (money % 10000) / 100, copper = money % 100;
        std::ostringstream os;
        if (gold) os << gold << " gold ";
        if (silver) os << silver << " silver ";
        if (copper) os << copper << " copper ";
        
        return os.str();
    }
    
    void SendPvPMessage(Player* player, KillStreakTypes type, const char* format, ...) {
        va_list args;
        va_start(args,format);
        
        char* msg = new char[1000];
        char* msg2 = new char[1000];
        vsprintf(msg,format,args);
        
        sprintf(msg2,"|cfff2583e[PvP]:|r %s",msg);
        
        WorldSession *session = player->GetSession();
        WorldPacket data;
        
        switch(type) {
            case KS_SELF:
                MAKE_MESSAGE(CHAT_MSG_SYSTEM);
                session->SendPacket(&data);
                break;
            case KS_AREA:
                MAKE_MESSAGE(CHAT_MSG_SYSTEM);
                player->SendMessageToSet(&data, true);
                break;
            case KS_AREA_NOSELF:
                MAKE_MESSAGE(CHAT_MSG_SYSTEM);
                player->SendMessageToSet(&data, false);
                break;
            case KS_WORLD:
                MAKE_MESSAGE(CHAT_MSG_SYSTEM);
                sWorld->SendGlobalMessage(&data);
                break;
            case KS_WORLD_NOSELF:
                MAKE_MESSAGE(CHAT_MSG_SYSTEM);
                sWorld->SendGlobalMessage(&data, session);
                break;
            case KS_RAIDWARN:
                MAKE_MESSAGE(CHAT_MSG_RAID_WARNING);
                session->SendPacket(&data);
                break;
            case KS_WORLD_RAIDWARN:
                MAKE_MESSAGE(CHAT_MSG_RAID_WARNING);
                sWorld->SendGlobalMessage(&data);
                break;
            case KS_WORLD_RAIDWARN_NOSELF:
                MAKE_MESSAGE(CHAT_MSG_RAID_WARNING);
                sWorld->SendGlobalMessage(&data, session);
                break;
        }
        
        delete [] msg;
        delete [] msg2;
        msg = NULL;
        msg2 = NULL;
        va_end(args);
    }

    void GiveRating(Player* player, Player* victim, bool isKiller = true, float mod = 1.0f)
    {
        uint32 playerRating = player->GetPvPRating();
        uint32 victimRating = victim->GetPvPRating();

        float chance = 1.0f / (1.0f + exp(log(10.0f) * (float)((float)victimRating - (float)playerRating) / 650.0f));
        int32 reward = (int32)floorf((1.0f - chance) * 10.0f) * mod;

        player->GiveGuildRep(reward);

        if (!player->InBattleground())
            player->AddPvPRating(reward);

        if (isKiller && !victim->InBattleground())
            victim->AddPvPRating(-1 * reward * 0.5);
    }

public:
    stronkcc_killstreak() : PlayerScript("stronkcc_killstreak") {}
    
    void OnMapChanged(Player* player) {
        if (!player)
            return;
        if (KillStreakInfo* killStreakData_player = &m_killStreakData[player->GetGUID()]) {
            // clear damagers/healers on map change
            killStreakData_player->m_damagers.clear();
            killStreakData_player->m_healers.clear();
            killStreakData_player->m_lastKillers.clear();
            killStreakData_player->m_lastVictims.clear();
        }
    }
    
    void AfterPlayerRemoveFromBattleground(Player* player, Battleground* bg) {
        if (!player || player->IsBeingSummonedByGM() || !bg || !bg->isBattleground() || 
            bg->GetStatus() < STATUS_WAIT_JOIN || bg->GetStatus() > STATUS_IN_PROGRESS)
            return;
        if (KillStreakInfo* killStreakData = &m_killStreakData[player->GetGUID()]) {
            if (killStreakData->m_killingSpree)
                SendPvPMessage(player, KS_SELF, "Your killstreak will be ended for prematurely leaving the battleground!");
            //reset killstreak data on premature leave from battleground
            CleanupKillstreakInfo(killStreakData, true);
        }
    }
    
    void OnLogin(Player* player) { //reset data on login
        if (!player)
            return;
        if (KillStreakInfo* killStreakData = &m_killStreakData[player->GetGUID()])
            CleanupKillstreakInfo(killStreakData, true);
    }
    
    void OnDealDamage(Player* attacker, Player* victim, uint32 amount) {
        if (attacker != victim) 
            m_killStreakData[victim->GetGUID()].m_damagers[attacker->GetGUID()] += amount;
    }
    
    void OnDealHeal(Player* healer, Player* victim, uint32 amount) {
        if (healer != victim) 
            m_killStreakData[victim->GetGUID()].m_healers[healer->GetGUID()] += amount;
    }
    
    void OnLogout(Player* player) {
        if (player)
            m_killStreakData.erase(player->GetGUID());
    }
    
    void OnPVPKill(Player* killer, Player* victim) {
        //initial sanity checks
        if (!killer || !victim || m_killStreakData[victim->GetGUID()].m_damagers.empty())
            return;
        KillStreakInfo* killStreakData_victim = &m_killStreakData[victim->GetGUID()];
        if (!killStreakData_victim)
            return;
        //check buffs, honorless target, spirit heal, preparation, arena preparation, rez sickness
        if (victim->HasAura(44521) || victim->HasAura(44535) || victim->HasAura(32727) || victim->HasAura(46705) || ((victim->HasAura(15007) && !victim->InBattleground())))
            return;
        //who did the most damage?
        PlayerDamage::const_iterator highDamager = killStreakData_victim->m_damagers.begin();
        PlayerDamage::const_iterator itr = highDamager;
        for (;itr != killStreakData_victim->m_damagers.end(); itr++) {
            if(itr->second >= highDamager->second)
                highDamager = itr;
        }
        //reward the actual 'killer', not just the one who did killing blow
        killer = sObjectAccessor->FindPlayer(highDamager->first);
        if (!killer)
            return;
        //more sanity checks
        if (killer != victim && killer->IsInSameGuildWith(victim))
            return;
        KillStreakInfo* killStreakData_killer = &m_killStreakData[killer->GetGUID()];
        if (!killStreakData_killer)
            return;
        //system messages
        const char * kcolor = "|cffff0000";
        const char * vcolor = "|cff00afff";
        //suicide
        if (killer == victim && killStreakData_killer == killStreakData_victim) {
            //If suicide then reset count and tell player or announce if killstreak was high
            SendPvPMessage(killer,KS_SELF,"You have ended your own %u-kill spree!",killStreakData_killer->m_killingSpree);
            if (killStreakData_killer->m_killingSpree >= 50) {
                if (!killer->getGender())
                    SendPvPMessage(killer, KS_WORLD_RAIDWARN, "Alas, %s%s|r has ended his own %u-kill spree!", vcolor, killer->GetName().c_str(), killStreakData_killer->m_killingSpree);
                else
                    SendPvPMessage(killer, KS_WORLD_RAIDWARN, "Alas, %s%s|r has ended her own %u-kill spree!", vcolor, killer->GetName().c_str(), killStreakData_killer->m_killingSpree);
            }
            
            //Remove killstreak and other kill info
            CleanupKillstreakInfo(killStreakData_killer, true);
            killer->UpdateStreakData(STREAK_KILLSTREAK, false);
            return;
        }
        //Ip check
        if (killer->GetSession()->GetRemoteAddress().compare(victim->GetSession()->GetRemoteAddress()) == 0)
            return;
        //check stale killer
        if (!victim->InBattleground()) {
            uint8 stale = 0;
            for(uint8 i = 0; i < killStreakData_victim->m_lastKillers.size() && i <= 3; i++) {
                if (killStreakData_victim->m_lastKillers[i] == killer->GetGUID())
                    stale++;
                if (stale >= 3) {
                    //clear victim's damagers/healers
                    CleanupKillstreakInfo(killStreakData_victim, false);
                    return;
                }
            }
        }
        //check stale targets
        uint8 stale = 0;
        for (uint8 i = 0; i < killStreakData_killer->m_lastVictims.size() && i <= 3; i++) {
            if (killStreakData_killer->m_lastVictims[i] == victim->GetGUID())
                stale++;
        }
        if (stale <= 3) {
            killStreakData_killer->m_killingSpree++; //increment killing spree
            killer->UpdateStreakData(STREAK_KILLSTREAK);
        }
        //determine kill reward
        float factor = 0.0f;
        if (stale == 0)
            factor = 1.0f;
        else if (stale == 1)
            factor = 0.5f;
        else if (stale == 2)
            factor = 0.25f;
        else if (stale == 3)
            factor = 0.125f;
        uint32 reward = 10000; //1g reward
        //gold for killing spree
        if(killStreakData_killer->m_killingSpree >= 5)
            reward += 1000 * (killStreakData_killer->m_killingSpree - 5);
        reward = reward * factor; //add in factor
        //limit reward - limited to 5g, seriously...
        if(reward >= 50000)
            reward = 50000;
        //apply Cash Flow guild talent bonus
        if (Guild* guild = killer->GetGuild())
            AddPct(reward, guild->GetTalentModifierValue(INCR_GOLD_RATE));
        SendPvPMessage(killer, KS_SELF, "You have earned |cffffffff%s|rfor slaying %s%s|r!", CreateGoldString(reward).c_str(), vcolor, victim->GetName().c_str());
        SendPvPMessage(killer, KS_SELF, "Kills: |cffffffff%u|r", killStreakData_killer->m_killingSpree);
        //go through and award assists
        uint32 totaldamage = 0;
        //get total
        for(PlayerDamage::const_iterator itr = killStreakData_victim->m_damagers.begin(); itr != killStreakData_victim->m_damagers.end(); itr++)
            totaldamage += itr->second;
        //award assist
        for(PlayerDamage::const_iterator itr = killStreakData_victim->m_damagers.begin(); itr != killStreakData_victim->m_damagers.end(); itr++) {
            Player *assistant = ObjectAccessor::FindPlayer(itr->first);
            if(!assistant || assistant == killer || itr->second < 2000) // must have done at least 2000 damage
                continue;
            float percent = (float)itr->second / (float)totaldamage;
            if(percent < 0.10f) //must have done at least 10% of damage
                continue;
            float assistMod = 0.5 * percent; //half of killer's reward, modded by percent of damage done
            uint32 assistReward = reward * assistMod;
            //apply Cash Flow guild talent bonus
            if (Guild* guild = assistant->GetGuild())
                AddPct(assistReward, guild->GetTalentModifierValue(INCR_GOLD_RATE));
            SendPvPMessage(assistant, KS_SELF, "You have earned |cffffffff%s|rfor assisting %s%s|r!", CreateGoldString(assistReward).c_str(), kcolor, killer->GetName().c_str());
            assistant->ModifyMoney(assistReward);
            GiveRating(assistant, victim, false, assistMod);
        }
        //loop and award healing
        if(!killStreakData_killer->m_healers.empty()) {
            //get total healing
            uint32 totalhealing = 0;
            for(PlayerDamage::const_iterator itr = killStreakData_killer->m_healers.begin(); itr != killStreakData_killer->m_healers.end(); itr++)
                totalhealing += itr->second;
            for(PlayerDamage::const_iterator itr = killStreakData_killer->m_healers.begin(); itr != killStreakData_killer->m_healers.end(); itr++) {
                Player *healer = ObjectAccessor::FindPlayer(itr->first);
                if(!healer || healer == victim || healer == killer || itr->second < 5000) //some limitations
                    continue;
                float percent = (float)itr->second / (float)totalhealing;
                if(percent < 0.30f) //skip if you did less than 30% total healing
                    continue;
                if(healer->GetExactDist2d(killer->GetPositionX(), killer->GetPositionY()) > 30.0f || healer->GetInstanceId() != killer->GetInstanceId()) //skip if out of range
                    continue;
                float healingMod = 0.5 * percent; //half of killer's reward, modded by percent of healing done to killer
                uint32 healingReward = reward * healingMod;
                //apply Cash Flow guild talent bonus
                if (Guild* guild = healer->GetGuild())
                    AddPct(healingReward, guild->GetTalentModifierValue(INCR_GOLD_RATE));
                SendPvPMessage(healer, KS_SELF, "You have earned |cffffffff%s|rfor healing %s%s|r!", CreateGoldString(healingReward).c_str(), kcolor, killer->GetName().c_str());
                healer->ModifyMoney(healingReward);
                GiveRating(healer, victim, false, healingMod);
            }
        }
        //C-C-C-COMBO BREAKER!
        if(killStreakData_victim->m_killingSpree >= 5) {
            if(!killer->InArena()) {
                uint32 spreeReward = 100 * killStreakData_victim->m_killingSpree;
                //apply Cash Flow guild talent bonus
                if (Guild* guild = killer->GetGuild())
                    AddPct(spreeReward, guild->GetTalentModifierValue(INCR_GOLD_RATE));
                killer->CastSpell(killer, 62994, true); //Foam Sword Defeat
                SendPvPMessage(killer,KS_SELF,"You have earned |cffffffff%s|rfor ending %s%s|r's %u-kill spree!",CreateGoldString(spreeReward).c_str(),vcolor,victim->GetName().c_str(),killStreakData_victim->m_killingSpree);
                reward += spreeReward;
            }
            else
                SendPvPMessage(killer, KS_SELF, "You have ended %s%s|r's %u-kill spree!", vcolor, victim->GetName().c_str(), killStreakData_victim->m_killingSpree);
            SendPvPMessage(victim, KS_SELF, "%s%s|r has ended your %u-kill spree.", kcolor, killer->GetName().c_str(), killStreakData_victim->m_killingSpree);
            if(killStreakData_victim->m_killingSpree >= 50)
                SendPvPMessage(killer, KS_WORLD_RAIDWARN, "%s%s|r's %u-kill spree was just ended by %s%s|r!", vcolor, victim->GetName().c_str(), killStreakData_victim->m_killingSpree, kcolor, killer->GetName().c_str());
        }
        //clear victim's damagers/healers
        CleanupKillstreakInfo(killStreakData_victim, false);
        victim->UpdateStreakData(STREAK_KILLSTREAK, false);
        //give gold/rating/tokens
        killer->ModifyMoney(reward);
        GiveRating(killer, victim);
        //update list of victims
        killStreakData_killer->m_lastVictims.push_front(victim->GetGUID());
        //update list of killers
        killStreakData_victim->m_lastKillers.push_front(killer->GetGUID());
        if(stale >= 3)
            return;
        //special effect and messages
        //Confetti every 5 kills
        if (killStreakData_killer->m_killingSpree % 5 == 0)
            killer->CastSpell(killer, 62994, true); //Confetti
        //Restoration every 10 kills
        if ((killStreakData_killer->m_killingSpree % 10 == 0) && !killer->InArena())
            killer->CastSpell(killer, 23493, true); //Restoration
        //Charred Earth AoE every 15 kills
        if ((killStreakData_killer->m_killingSpree % 15 == 0) && (!killer->InArena())) {
            SendPvPMessage(killer, KS_AREA, "The ground beneath %s%s|r goes up in flames.", kcolor, killer->GetName().c_str());
            killer->CastSpell(killer->GetPositionX(), killer->GetPositionY(), killer->GetPositionZ(), 30129, true); //Charred Earth
        }
        //Growth every 30 kills
        if ((killStreakData_killer->m_killingSpree % 30 == 0) && (!killer->InArena()))
            killer->CastSpell(killer, 36300, true); //Growth
        //Announce every thirty kills
        if ((killStreakData_killer->m_killingSpree <= 200) && (killStreakData_killer->m_killingSpree % 30 == 0))
            SendPvPMessage(killer, KS_WORLD_RAIDWARN, "%s%s|r has a killing spree of %u kills!|r", kcolor, killer->GetName().c_str(), killStreakData_killer->m_killingSpree);
        //Announce every ten kills over 200
        if ((killStreakData_killer->m_killingSpree > 200) && (killStreakData_killer->m_killingSpree % 10 == 0))
            SendPvPMessage(killer, KS_WORLD_RAIDWARN, "%s%s|r has a GODLIKE killing spree of %u kills!|r", kcolor, killer->GetName().c_str(), killStreakData_killer->m_killingSpree);
        //Multi-kills
        if(!killStreakData_killer->m_multikillCount) {
            //No multi-kill yet just set it to one kill so far
            killStreakData_killer->m_multikillCount = 1;
            killStreakData_killer->m_lastTime = getMSTime();
        }
        else if(getMSTimeDiff(killStreakData_killer->m_lastTime, getMSTime()) < TIME_LIMIT) {
            killStreakData_killer->m_multikillCount++;
            killStreakData_killer->m_lastTime = getMSTime();
            switch(killStreakData_killer->m_multikillCount) {
                case 2:
                    SendPvPMessage(killer, KS_AREA, "%s%s|r has gotten a doublekill!", kcolor, killer->GetName().c_str());
                    break;
                case 3:
                    SendPvPMessage(killer, KS_AREA, "%s%s|r has gotten a triplekill!", kcolor, killer->GetName().c_str());
                    if(!killer->InArena())
                        killer->CastSpell(killer, 23505, true); //Berserking
                    break;
                case 4:
                    SendPvPMessage(killer, KS_AREA, "%s%s|r has gotten a quadrakill!", kcolor, killer->GetName().c_str());
                    break;
                case 5:
                    SendPvPMessage(killer, KS_AREA, "%s%s|r has gotten a pentakill!", kcolor, killer->GetName().c_str());
                    break;
                case 6:
                    SendPvPMessage(killer, KS_AREA, "%s%s|r has gotten a hexakill!", kcolor, killer->GetName().c_str());
                    break;
                default:
                    break;
            }
        }
        else //time has ran out, set to 0
            killStreakData_killer->m_multikillCount = 0;
    }
};

void AddSC_stronkcc_killstreak()
{
    new stronkcc_killstreak();
}

#undef TIME_LIMIT
#undef MAKE_MESSAGE