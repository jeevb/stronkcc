#include "ScriptPCH.h"
#include "Config.h"

enum trainerTypes {
    WEAPON_MASTER               = 10256,
    RIDING_TRAINER              = 31238,
    CLASS_DRUID_TRAINER         = 26324,
    CLASS_HUNTER_TRAINER        = 26325,
    CLASS_MAGE_TRAINER          = 26326,
    CLASS_PALADIN_TRAINER       = 26327,
    CLASS_PRIEST_TRAINER        = 26328,
    CLASS_ROGUE_TRAINER         = 26329,
    CLASS_SHAMAN_TRAINER        = 26330,
    CLASS_WARLOCK_TRAINER       = 26331,
    CLASS_WARRIOR_TRAINER       = 26332,
    CLASS_DEATH_KNIGHT_TRAINER  = 28471
};
class cg_autolearn_spells : public PlayerScript, public WorldScript
{
private:
    bool learnSpellsOnLogin;
    bool learnSpellsOnTalentPointUse;
    struct spellFlagUnlearn {
        uint32 raceMask;
        uint32 classMask;
        uint32 spellId;
    };
    typedef std::vector<spellFlagUnlearn> spellsFlagUnlearn;
    spellsFlagUnlearn m_spellsFlagUnlearn;
public:
    cg_autolearn_spells() :
    PlayerScript("cg_autolearn_spells_PS"),
    WorldScript("cg_autolearn_spells_WS") { }
    
    void OnConfigLoad(bool /*reload*/)
    {
        learnSpellsOnLogin = sConfigMgr->GetBoolDefault("Auto-learnSpells.OnLogin", false);
        learnSpellsOnTalentPointUse = sConfigMgr->GetBoolDefault("Auto-learnSpells.OnTalentPointUse", false);
        // Load spells to unlearn on login
        m_spellsFlagUnlearn.clear();
        QueryResult result = WorldDatabase.Query("SELECT racemask, classmask, Spell FROM spell_flag_unlearn");
        if (!result)
            return;
        do
        {
            Field* fields = result->Fetch();
            spellFlagUnlearn m_spellFlagUnlearn;
            m_spellFlagUnlearn.raceMask = fields[0].GetUInt32();
            m_spellFlagUnlearn.classMask = fields[1].GetUInt32();
            m_spellFlagUnlearn.spellId = fields[2].GetUInt32();
            if (!sSpellMgr->GetSpellInfo(m_spellFlagUnlearn.spellId))
                continue;
            m_spellsFlagUnlearn.push_back(m_spellFlagUnlearn);
        } while (result->NextRow());
    }

    void OnLogin(Player* player)
    {
        if (!player || !learnSpellsOnLogin)
            return;
        player->SilenceAchievementAnnounce(true);
        // Unlearn flagged spells on login
        UnlearnFlaggedSpells(player);
        // Learn dual specialization
        if (player->GetSpecsCount() == 1 &&
            player->getLevel() >= sWorld->getIntConfig(CONFIG_MIN_DUALSPEC_LEVEL)) {
            player->CastSpell(player, 63680, true, NULL, NULL, player->GetGUID());
            player->CastSpell(player, 63624, true, NULL, NULL, player->GetGUID());
        }
        // Learn all weapon skills
        AutolearnSpells(player, WEAPON_MASTER);
        // Learn all riding spells
        AutolearnSpells(player, RIDING_TRAINER);
        // Learn all class spells
        AutolearnAllClassSpells(player);
        // Learn all secondary professions
        AutolearnSkill(player, SKILL_FIRST_AID);
        AutolearnSkill(player, SKILL_COOKING);
        AutolearnSkill(player, SKILL_FISHING);
        // Max all skills
        player->UpdateSkillsToMaxSkillsForLevel();
        player->SilenceAchievementAnnounce(false);
    }
    
    void OnFreeTalentPointsChanged(Player* player, uint32 points)
    {
        if (!player || !(points < player->GetUInt32Value(PLAYER_CHARACTER_POINTS1)) || !learnSpellsOnTalentPointUse)
            return;
        AutolearnAllClassSpells(player);
    }
    
    void AutolearnAllClassSpells(Player* player)
    {
        if (!player)
            return;
        switch (player->getClass()) {
            case CLASS_DRUID:           return AutolearnSpells(player, CLASS_DRUID_TRAINER);
            case CLASS_HUNTER:          return AutolearnSpells(player, CLASS_HUNTER_TRAINER);
            case CLASS_MAGE:            return AutolearnSpells(player, CLASS_MAGE_TRAINER);
            case CLASS_PALADIN:         return AutolearnSpells(player, CLASS_PALADIN_TRAINER);
            case CLASS_PRIEST:          return AutolearnSpells(player, CLASS_PRIEST_TRAINER);
            case CLASS_ROGUE:           return AutolearnSpells(player, CLASS_ROGUE_TRAINER);
            case CLASS_SHAMAN:          return AutolearnSpells(player, CLASS_SHAMAN_TRAINER);
            case CLASS_WARLOCK:         return AutolearnSpells(player, CLASS_WARLOCK_TRAINER);
            case CLASS_WARRIOR:         return AutolearnSpells(player, CLASS_WARRIOR_TRAINER);
            case CLASS_DEATH_KNIGHT:    return AutolearnSpells(player, CLASS_DEATH_KNIGHT_TRAINER);
            default:                    return;
        }
    }
    
    void AutolearnSpells(Player* player, uint32 trainerEntry)
    {
        if (!player || !trainerEntry)
            return;
        const TrainerSpellData *trainer_spells = sObjectMgr->GetNpcTrainerSpells(trainerEntry);
        if (!trainer_spells) {
            sLog->outError(LOG_FILTER_WORLDSERVER, "cg_autolearn_spells could not find trainer entry %u.", trainerEntry);
            return;
        }
        uint32 pRaceMask = player->getRaceMask();
        uint32 pClassMask = player->getClassMask();
        uint32 totalLearnedSpells = 0;
        for (uint8 tries = 0; tries < 10; tries++) {
            uint32 learnedSpells = 0;
            for (TrainerSpellMap::const_iterator itr = trainer_spells->spellList.begin(); itr != trainer_spells->spellList.end(); ++itr) {
                TrainerSpell const* tSpell = &itr->second;
                bool valid = true;
                // Exclude tSpell->spell if on list of spells flagged for unlearn.
                for (spellsFlagUnlearn::iterator itr = m_spellsFlagUnlearn.begin(); itr != m_spellsFlagUnlearn.end(); ++itr) {
                    spellFlagUnlearn & m_spellFlagUnlearn = *itr;
                    if (!m_spellFlagUnlearn.raceMask || 
                        (m_spellFlagUnlearn.raceMask && (m_spellFlagUnlearn.raceMask & pRaceMask)))
                        if (!m_spellFlagUnlearn.classMask || 
                            (m_spellFlagUnlearn.classMask && (m_spellFlagUnlearn.classMask & pClassMask)))
                            if (tSpell->spell && (tSpell->spell == m_spellFlagUnlearn.spellId)) {
                                valid = false;
                                break;
                            }
                }
                if (!valid)
                    continue;
                for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i) {
                    if (!tSpell->learnedSpell[i])
                        continue;
                    if (!player->IsSpellFitByClassAndRace(tSpell->learnedSpell[i])) {
                        valid = false;
                        break;
                    }
                }
                if (!valid)
                    continue;
                TrainerSpellState state = player->GetTrainerSpellState(tSpell);
                if (state == TRAINER_SPELL_GREEN) {
                    if (tSpell->IsCastable())
                        player->CastSpell(player, tSpell->spell, true);
                    else
                        player->learnSpell(tSpell->spell, false);
                    learnedSpells++;
                }
            }
            if (!learnedSpells)
                break;
            totalLearnedSpells += learnedSpells;
        }
        if (totalLearnedSpells)
            player->SaveToDB();
        
        return;
    }
    
    void AutolearnSkill(Player* player, uint32 skillId)
    {
        uint32 pRaceMask = player->getRaceMask();
        uint32 pClassMask = player->getClassMask();
        for (uint32 j = 0; j < sSkillLineAbilityStore.GetNumRows(); ++j) {
            SkillLineAbilityEntry const* skillLine = sSkillLineAbilityStore.LookupEntry(j);
            if (!skillLine)
                continue;
            // wrong skill
            if (skillLine->skillId != skillId)
                continue;
            // not high rank
            if (skillLine->forward_spellid)
                continue;
            // skip racial skills
            if (skillLine->racemask != 0)
                continue;
            // skip wrong class skills
            if (skillLine->classmask && (skillLine->classmask & pClassMask) == 0)
                continue;
            SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(skillLine->spellId);
            if (!spellInfo || !SpellMgr::IsSpellValid(spellInfo, player, false))
                continue;
            // Exclude skillLine->spellId if on list of spells flagged for unlearn.
            bool valid = true;
            for (spellsFlagUnlearn::iterator itr = m_spellsFlagUnlearn.begin(); itr != m_spellsFlagUnlearn.end(); ++itr) {
                spellFlagUnlearn & m_spellFlagUnlearn = *itr;
                if (!m_spellFlagUnlearn.raceMask || 
                    (m_spellFlagUnlearn.raceMask && (m_spellFlagUnlearn.raceMask & pRaceMask)))
                    if (!m_spellFlagUnlearn.classMask || 
                        (m_spellFlagUnlearn.classMask && (m_spellFlagUnlearn.classMask & pClassMask)))
                        if (skillLine->spellId && (skillLine->spellId == m_spellFlagUnlearn.spellId)) {
                            valid = false;
                            break;
                        }
            }
            if (!valid)
                continue;
            player->learnSpell(skillLine->spellId, false);
        }
        uint16 maxLevel = player->GetPureMaxSkillValue(skillId);
        player->SetSkill(skillId, player->GetSkillStep(skillId), maxLevel, maxLevel);
        
        return;
    }
    
    void UnlearnFlaggedSpells(Player* player)
    {
        if (!player)
            return;
        uint32 pRaceMask = player->getRaceMask();
        uint32 pClassMask = player->getClassMask();
        if (!pRaceMask || !pClassMask) // Just in case...
            return;
        uint32 unlearnedSpells = 0;
        for (spellsFlagUnlearn::iterator itr = m_spellsFlagUnlearn.begin(); itr != m_spellsFlagUnlearn.end(); ++itr) {
            spellFlagUnlearn & m_spellFlagUnlearn = *itr;
            if (!m_spellFlagUnlearn.raceMask || 
                (m_spellFlagUnlearn.raceMask && (m_spellFlagUnlearn.raceMask & pRaceMask)))
                if (!m_spellFlagUnlearn.classMask || 
                    (m_spellFlagUnlearn.classMask && (m_spellFlagUnlearn.classMask & pClassMask)))
                    if (player->HasSpell(m_spellFlagUnlearn.spellId)) {
                        player->removeSpell(m_spellFlagUnlearn.spellId, false, false);
                        unlearnedSpells++;
                    }
        }
        if (unlearnedSpells)
            player->SaveToDB();
        
        return;
    }
};

void AddSC_cg_autolearn_spells()
{
    new cg_autolearn_spells;
}