#include "CustomGossipText.h"
#include "Guild.h"
#include "GuildMgr.h"
#include "ScriptPCH.h"

enum StronkccGuildMasterOptions {
    OPT_TALENT_TREE = MAX_GUILD_TALENT,
    OPT_RESET_TALENTS,
    OPT_GUILD_TABARD,
    OPT_GUILD_CREATE,
    OPT_GUILD_RENAME
};

struct RankInfo {
    const char* icon;
    const char* name;
    const char* description;
    const uint32* ranks;

    RankInfo(const char* _icon, const char* _name, const char* _description, const uint32* _ranks = NULL) :
            icon(_icon), name(_name), description(_description), ranks(_ranks) { }
};


const RankInfo RankInfoList[MAX_GUILD_TALENT] = {
    // MOUNTED_SPEED_INCR
    RankInfo("Interface\\Icons\\Spell_DeathKnight_SummonDeathCharger.blp", "Mount Up!",
            "Increase speed while mounted", mountedSpeedIncreaseRanks),
    // PLAYER_SPEED_INCR
    RankInfo("Interface\\Icons\\Ability_Rogue_Sprint.blp", "Speedy Gonzales",
            "Increase speed while on foot", playerSpeedIncreaseRanks),
    // INCR_HEALING_RECEIVED
    RankInfo("Interface\\Icons\\Spell_Holy_BlindingHeal.blp", "Heavenly Touch",
            "Increase healing received", increasedHealingReceivedRanks),
    // INCR_DAMAGE_DONE
    RankInfo("Interface\\Icons\\Ability_Warrior_IntensifyRage.blp", "Ancestral Might",
            "Increase damage done", increasedDamageDoneRanks),
    // DECR_DAMAGE_TAKEN
    RankInfo("Interface\\Icons\\Ability_Warrior_DefensiveStance.blp", "Reinforced Armor",
            "Decrease damage taken", decreasedDamageTakenRanks),
    // INCR_PLAYER_STATS
    RankInfo("Interface\\Icons\\Spell_Shadow_TwistedFaith.blp", "Divine Blessing",
            "Increase total stats", increasedPlayerStatsRanks),
    // INCR_REP_BONUS
    RankInfo("Interface\\Icons\\Achievement_Reputation_01.blp", "Mr. Popularity",
            "Increase reputation gained", increasedRepBonusRanks),
    // INCR_GOLD_RATE
    RankInfo("Interface\\Icons\\inv_misc_coin_01.blp", "Cash Flow",
            "Increase gold gained", increasedGoldRateRanks),
    // INCR_HONOR_RATE
    RankInfo("Interface\\Icons\\Achievement_PVP_A_15.blp", "Honorable Mention",
            "Increase honor gained", increasedHonorRateRanks),
    // ALLOW_FLYING_MOUNT
    RankInfo("Interface\\Icons\\ability_mount_gryphon_01.blp", "Mile High Club",
            "Allows the use of flying mounts"),
    // HS_COOLDOWN
    RankInfo("Interface\\Icons\\INV_Misc_Rune_01.blp", "Hasty Hearth",
            "Remove the cooldown on Hearthstone"),
    // MASS_RES
    RankInfo("Interface\\Icons\\Spell_Holy_Resurrection.blp", "Mass Resurrection",
            "Resurrect all nearby allies")
};


class stronkcc_guildmaster : public CreatureScript, public GuildScript
{
public:
    stronkcc_guildmaster() : CreatureScript("stronkcc_guildmaster"), GuildScript("stronkcc_guildmaster_GS") { }
    
    void OnAddMember(Guild* /*guild*/, Player* player, uint8& /*plRank*/) {
        if (player)
            player->ClearCloseGossip();
    }
    
    void OnRemoveMember(Guild* /*guild*/, Player* player, bool /*isDisbanding*/, bool /*isKicked*/) {
        if (player)
            player->ClearCloseGossip();
    }

    bool OnGossipHello(Player* player, Creature* creature) {
        player->ClearCloseGossip();

        Guild* guild = player->GetGuild();
        if (!guild) {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "Create a guild crest.", 0, OPT_GUILD_TABARD);
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK, "Register a new guild. Specify the desired name of your guild at the prompt.", 0, OPT_GUILD_CREATE, 
                                             "" , 0, true);
            UpdateGossipText(player, STRONKCC_GUILD_GUILDMASTER,
                             "My affiliation with Dalaran's Guild Registry "
                             "allows me to act in its stead in these forsaken lands. "
                             "How may I help you?"); // Updates gossip text corresponding to STRONKCC_GUILD_GUILDMASTER
            player->SEND_GOSSIP_MENU(STRONKCC_GUILD_GUILDMASTER, creature->GetGUID());
            return true;
        }
        
        std::ostringstream os;
        os  << "Greetings " << player->GetName() << "!\n\n"
            << "You are " << GetGuildRepRankDesc(player->GetGuildRepRank())
            << " (" << player->GetGuildRep() << "/"
            << GetRequiredGuildRepForRank(player->GetGuildRepRank() + 1 < GUILD_REP_MAX_RANK ?
                player->GetGuildRepRank() + 1 : GUILD_REP_EXALTED) << ")"
            << " with "<< guild->GetName() <<" (Level " << (uint16)guild->GetGuildLevel() << ").\n\n";

        if (guild->HasActiveTalents()) {
            os << "These are your guild's active talents:";
            _BuildActiveTalentsList(os, guild, 20, -6, 1);
        }
        else
            os << "Your guild has no active talents.";
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "I would like to view my guild\'s talent tree.", 0, OPT_TALENT_TREE);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "I would like to change my guild crest.", 0, OPT_GUILD_TABARD);
        if (guild->CanBeRenamed() && player->GetRank() == GR_GUILDMASTER)
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "I would like to rename my guild.", 0, OPT_GUILD_RENAME, 
                                             "" , 0, true);
        UpdateGossipText(player, STRONKCC_GUILD_GUILDMASTER, os.str());
        player->SEND_GOSSIP_MENU(STRONKCC_GUILD_GUILDMASTER, creature->GetGUID());
        return true;
    }
    
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction) {
        player->ClearCloseGossip();
        
        if (!uiAction)
            return OnGossipHello(player, creature);
        WorldSession* session = player->GetSession();
        if (uiAction == OPT_GUILD_TABARD) {
            session->SendTabardVendorActivate(creature->GetGUID());
            return true;
        }
        Guild* guild = player->GetGuild();
        ChatHandler handler(session);
        if (!guild) {
            handler.PSendSysMessage("You are not in a guild!");
            return OnGossipHello(player, creature);
        }
        if (uiAction == OPT_TALENT_TREE)
            return SendGuildTalentTree(player, creature, guild);
        
        if (player->GetRank() > GR_OFFICER) {
            handler.PSendSysMessage("Alas, only the Guild Master or Officers may buy or reset talents!");
            return true;
        }
        if (uiAction == OPT_RESET_TALENTS) {
            guild->ResetTalents();
            handler.PSendSysMessage("All your guild's talents have been reset.");
            return OnGossipHello(player, creature);
        }
        if (!guild->GetRemainingTalentPoints()) {
            handler.PSendSysMessage("Your guild does not have sufficient talent points to complete this upgrade.");
            return true;
        }
        // Extract talent and new rank from uiAction (uint32)
        uint8 talent = uiAction;
        uint8 newRank = uiAction >> 8;
        uint32 goldPrice = GetTalentRankCost(talent, newRank);
        if (guild->ModifyBankMoney(goldPrice, false)) {
            guild->SetTalentRank(talent, newRank);
            PurchaseSuccessSummary(player, guild, creature, talent, newRank);
            return true;
        }
        else
            handler.PSendSysMessage("Your guild vault does not have sufficient gold for this transaction.");
        
        return OnGossipHello(player, creature);
    }
    
    bool OnGossipSelectCode(Player * player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction, const char *ccode) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (!uiAction)
            return OnGossipHello(player, creature);
        WorldSession* session = player->GetSession();
        std::string guildName = (std::string)ccode;
        if (guildName.empty() || guildName.length() > 24 ||
            sObjectMgr->IsReservedName(guildName) || !sObjectMgr->IsValidCharterName(guildName)) {
            Guild::SendCommandResult(session, GUILD_COMMAND_CREATE, ERR_GUILD_NAME_INVALID, guildName);
            return OnGossipHello(player, creature);
        }
        // Check if guild name is already taken
        if (sGuildMgr->GetGuildByName(guildName)) {
            Guild::SendCommandResult(session, GUILD_COMMAND_CREATE, ERR_GUILD_NAME_EXISTS_S, guildName);
            return OnGossipHello(player, creature);
        }
        switch (uiAction) {
            case OPT_GUILD_CREATE:
            {
                // Check if player is already in a guild
                if (player->GetGuildId()) {
                    WorldPacket data;
                    data.Initialize(SMSG_TURN_IN_PETITION_RESULTS, 4);
                    data << (uint32)PETITION_TURN_ALREADY_IN_GUILD;
                    session->SendPacket(&data);
                    break;
                }
                // Create guild
                Guild* guild = new Guild;
                if (!guild->Create(player, guildName)) {
                    delete guild;
                    break;
                }
                
                // Register guild and add guild master
                sGuildMgr->AddGuild(guild);
                guild->CreateNewBankTab();
                Guild::SendCommandResult(session, GUILD_COMMAND_CREATE, ERR_GUILD_COMMAND_SUCCESS, guildName);
                break;
            }
            case OPT_GUILD_RENAME:
            {
                Guild* guild = player->GetGuild();
                ChatHandler handler(session);
                if (!guild || player->GetRank() > GR_GUILDMASTER) {
                    handler.PSendSysMessage("Alas, only Guild Masters may rename their guilds!");
                    break;
                }
                std::string oldName = guild->GetName();
                if (!guild->SetName(guildName, false)) {
                    Guild::SendCommandResult(session, GUILD_COMMAND_CREATE, ERR_GUILD_NAME_INVALID, guildName);
                    break;
                }
                
                handler.PSendSysMessage(LANG_GUILD_RENAME_DONE, oldName.c_str(), guildName.c_str());
                break;
            }
            default:
                break;
        }
        
        return OnGossipHello(player, creature);
    }
    
    bool SendGuildTalentTree(Player* player, Creature* creature, Guild* guild) {
        player->ClearCloseGossip();
        
        for (uint8 i = MOUNTED_SPEED_INCR; i < MAX_GUILD_TALENT; i++) {
            uint8 curRank = guild->GetTalentRank(i);
            std::ostringstream os;
            os << GetTalentRankTitle(i, curRank, 40, -17, 0) << "\n";
            for (uint8 j = 1; j < GetMaxGuildTalentRank(i); j++)
                os << (curRank == j ? "|cff000000" : "|cff666362") << GetTalentRankDesc(i, j) << "|r\n";
            if (curRank + 1 < GetMaxGuildTalentRank(i)) {
                std::stringstream os1;
                os1 << "Do you wish to spend the following amount of your guild's gold on:\n\n"
                << "|cffffcc00" << GetTalentRankTitle(i, curRank + 1, 30, -7, -10) << "|r?\n\n"
                << GetGoldString(GetTalentRankCost(i, curRank + 1));
                // Package talent and new rank into uiAction (uint32)
                uint32 newTalentRank = ((curRank + 1) << 8) | i;
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, os.str(), 0, newTalentRank, os1.str(), 0, false);
            }
            else
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, os.str(), 0, 0);
        }
        // Use MAX_GUILD_TALENT as the option identifier that will reset all talents.
        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_INTERACT_1, "Reset all my guild's talents.", 0, OPT_RESET_TALENTS, 
                                         "Are you sure you want to reset all your guild's talents?" , 0, false);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
        UpdateGossipText(player, STRONKCC_GUILD_GUILDMASTER, GetGuildData(guild));
        player->SEND_GOSSIP_MENU(STRONKCC_GUILD_GUILDMASTER, creature->GetGUID());
        
        return true;
    }

    void PurchaseSuccessSummary(Player* player, Guild* guild, Creature* creature, uint8 type, uint8 rank) {
        player->ClearCloseGossip();

        std::ostringstream summary;
        summary << "You have successfully purchased:\n\n  |cff363636" << GetTalentRankTitle(type, rank, 32, -7, -11) << "|r\n\n"
                << "These are your guild's active talents:";

        _BuildActiveTalentsList(summary, guild, 20, -6, -11);

        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Return to my guild's talent tree.", 0, OPT_TALENT_TREE);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
        UpdateGossipText(player, STRONKCC_GUILD_GUILDMASTER, summary.str());
        player->SEND_GOSSIP_MENU(STRONKCC_GUILD_GUILDMASTER, creature->GetGUID());
    }

    void _BuildActiveTalentsList(std::ostringstream &output, Guild *guild, uint32 iconSize, int32 horizontalOffset,
                                 int32 verticalOffset) {
        for (uint8 i = MOUNTED_SPEED_INCR; i < MAX_GUILD_TALENT; i++)
            if (uint8 curRank = guild->GetTalentRank(i))
                output << "\n\n  |cff363636"
                       << GetTalentRankTitle(i, curRank, iconSize, horizontalOffset, verticalOffset)
                       << "\n  "
                       << GetTalentRankDesc(i, curRank) << "|r";
    }

    std::string GetTalentRankTitle(uint8 type, uint8 rank, uint32 iconSize, int32 horizontalOffset, int32 verticalOffset) {
        if (type > MAX_GUILD_TALENT)
            return "";

        RankInfo const* rankInfo = &RankInfoList[type];
        std::ostringstream talentRankTitle;
        talentRankTitle << "|T" << rankInfo->icon << ':'
                        << iconSize << ':' << iconSize << ':' << horizontalOffset << ':' << verticalOffset
                        << "|t" << rankInfo->name;

        if (rank)
            talentRankTitle << " (Rank " << uint16(rank) << "/" << uint16(GetMaxGuildTalentRank(type) - 1) << ")";

        return talentRankTitle.str();
    }
    
    std::string GetTalentRankDesc(uint8 type, uint8 rank) {
        if (type > MAX_GUILD_TALENT)
            return "";

        RankInfo const* rankInfo = &RankInfoList[type];
        std::ostringstream talentRankDesc;

        talentRankDesc << rankInfo->description;
        if (rankInfo->ranks)
            talentRankDesc << " by " << rankInfo->ranks[rank] << '%';

        return talentRankDesc.str();
    }
    
    std::string GetGuildData(Guild* guild) {
        std::ostringstream os;
        os << "Guild: " << guild->GetName() << "\nLevel: " << (uint16)guild->GetGuildLevel();
        if (guild->GetGuildLevel() < sWorld->getIntConfig(CONFIG_GUILD_LEVEL_CAP)) {
            os  << " (Experience: " << guild->GetGuildExperience() << "/"
                << guild->GetXPForLevel(guild->GetGuildLevel() + 1) << ")";
        }
        os << "\nUnspent Talents: " << (uint16)guild->GetRemainingTalentPoints();
        return os.str();
    }
    
    uint32 GetTalentRankCost(uint8 type, uint8 value) const {
        switch (type) {
            case MOUNTED_SPEED_INCR:
            case PLAYER_SPEED_INCR:
            case INCR_HEALING_RECEIVED:
            case INCR_DAMAGE_DONE:
            case DECR_DAMAGE_TAKEN:
            case INCR_PLAYER_STATS:
            case INCR_REP_BONUS:
            case INCR_GOLD_RATE:
            case INCR_HONOR_RATE:
                return pow(2, value) * 1000 * GOLD;
            case ALLOW_FLYING_MOUNT:
            case HS_COOLDOWN:
            case MASS_RES:
                return pow(2, value * 5) * 1000 * GOLD; // Treat as 5th rank
            default:
                return 0;
        }
    }
    
    std::string GetGoldString(uint32 amount) {
        uint32 gold = amount / GOLD, silver = (amount % GOLD) / SILVER, copper = (amount % SILVER);
        std::ostringstream os;
        if (gold) os << gold << "|TInterface\\MoneyFrame\\UI-GoldIcon.blp:14:14:2:-10|t ";
        if (silver) os << silver << "|TInterface\\MoneyFrame\\UI-SilverIcon.blp:14:14:2:-10|t ";
        if (copper) os << copper << "|TInterface\\MoneyFrame\\UI-CopperIcon.blp:14:14:2:-10|t";
        return os.str();
    }
};

class stronkcc_flyingmountcheck : public DisableScript
{
public:
    stronkcc_flyingmountcheck() : DisableScript ("stronkcc_flyingmountcheck") { }
    
    bool CanUseFlyingMount(Player* player, AreaTableEntry const* /*area*/) {
        Guild* guild = player->GetGuild();
        return guild && guild->GetTalentRank(ALLOW_FLYING_MOUNT);
    }
};

void AddSC_stronkcc_guild()
{
    new stronkcc_guildmaster;
    new stronkcc_flyingmountcheck;
}