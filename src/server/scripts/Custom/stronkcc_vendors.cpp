#include "ScriptPCH.h"
#include "Config.h"
#include "World.h"
#include "Language.h"
#include "SiteMgr.h"
#include "WorldPvPMgr.h"
#include "Pet.h"

#define RETURN_TO_MAIN_MENU_ICON "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo.blp:36:36:-16:1|tReturn to the main menu"
#define MAX_LINE_CHARS 35
#define MAX_GOSSIP_ITEMS 50

class stronkcc_gear_vendor : public CreatureScript, public PlayerScript
{
private:
    struct itemData {
        uint32 InvType;
        uint32 SubClass;
        uint32 ItemId;
    };
    typedef std::vector<itemData> availableItemTypes;
    typedef std::map<Creature*, availableItemTypes> creatureData;
    creatureData m_creatureData;
    typedef std::map<Player*, uint32> equipmentSlotSelection;
    equipmentSlotSelection m_equipmentSlotSelection;
public:
    stronkcc_gear_vendor() :
    CreatureScript("stronkcc_gear_vendor"),
    PlayerScript("stronkcc_gear_vendor_PS") { }
    
    void OnLogout(Player *player) {
        // Clear player data on logout
        m_equipmentSlotSelection.erase(player);
    }
    
    bool OnGossipHello(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_equipmentSlotSelection.erase(player);
        FillCreatureData(creature);
        if (!sWorldPvPMgr->CanInteract(player, creature))
            return true;
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i) {
            if (i == EQUIPMENT_SLOT_BODY || i == EQUIPMENT_SLOT_FINGER2 ||
                i == EQUIPMENT_SLOT_TRINKET2)
                continue;
            if (ShowEquipmentSlot(player, creature, i))
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetItemSlotDescription(i), 0, i);
        }
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return true;
    }
    
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction) {
        if (uiAction == EQUIPMENT_SLOT_END)
            return OnGossipHello(player, creature);
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_equipmentSlotSelection[player] = uiAction;
        player->GetSession()->SendListInventory(creature->GetGUID());
        return true;
    }
    
    bool ShouldDisplayVendorItem(Player* player, Creature* vendor, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/) {
        return  sWorldPvPMgr->IsItemAvailableInArea(item_template->ItemId, vendor->GetAreaAffiliation()) && 
                IsUsableGearItem(player, item_template, m_equipmentSlotSelection[player]);
    }
    
    bool OnBeforePurchaseItem(Player *player, Creature *vendor, ItemTemplate const* /*item_template*/, VendorItem const* /*vendor_item*/) {
        if (!sWorldPvPMgr->CanInteract(player, vendor)) {
            player->PlayerTalkClass->ClearMenus();
            player->PlayerTalkClass->SendCloseGossip();
            m_equipmentSlotSelection.erase(player);
            return false;
        }
        
        return true;
    }
    
    void OnPurchasedItem(Player* player, Creature* vendor, Item* item, VendorItem const* /*vendor_item*/) {
        return SendPurchaseSuccessMenu(player, vendor, item);
    }
    
    void FillCreatureData(Creature* creature) { // Run once
        creatureData::iterator iter = m_creatureData.find(creature);
        if(iter == m_creatureData.end())
            if (VendorItemData const* items = creature->GetVendorItems())
                for (VendorItemList::const_iterator iter1 = items->m_items.begin(); iter1 != items->m_items.end(); ++iter1)
                    if (VendorItem const* item = *iter1)
                        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(item->item)) {
                            if (!sWorldPvPMgr->IsItemAvailableInArea(itemTemplate->ItemId, creature->GetAreaAffiliation()))
                                continue;
                            itemData m_itemData;
                            m_itemData.InvType = itemTemplate->InventoryType;
                            m_itemData.SubClass = itemTemplate->SubClass;
                            m_itemData.ItemId = itemTemplate->ItemId;
                            availableItemTypes::iterator iter2 = m_creatureData[creature].begin();
                            for (; iter2 != m_creatureData[creature].end(); ++iter2)
                                if (iter2->InvType == m_itemData.InvType && iter2->SubClass == m_itemData.SubClass)
                                    break;
                            if (iter2 == m_creatureData[creature].end())
                                m_creatureData[creature].push_back(m_itemData);
                        }
        
        return;
    }
    
    bool ShowEquipmentSlot(Player* player, Creature* creature, uint32 equipmentSlot) {
        for (availableItemTypes::iterator iter3 = m_creatureData[creature].begin(); iter3 != m_creatureData[creature].end(); ++iter3) {
            itemData & m_itemData = *iter3;
            if (IsUsableGearItem(player, sObjectMgr->GetItemTemplate(m_itemData.ItemId), equipmentSlot))
                return true;
        }
        
        return false;
    }
    
    bool IsUsableGearItem(Player* player, ItemTemplate const* item_template, uint32 equipmentSlot) {
        const static uint32 item_weapon_skills[MAX_ITEM_SUBCLASS_WEAPON] =
        {
            SKILL_AXES,     SKILL_2H_AXES,  SKILL_BOWS,          SKILL_GUNS,      SKILL_MACES,
            SKILL_2H_MACES, SKILL_POLEARMS, SKILL_SWORDS,        SKILL_2H_SWORDS, 0,
            SKILL_STAVES,   0,              0,                   SKILL_FIST_WEAPONS,   0,
            SKILL_DAGGERS,  SKILL_THROWN,   SKILL_ASSASSINATION, SKILL_CROSSBOWS, SKILL_WANDS,
            SKILL_FISHING
        }; // Copied from function Item::GetSkill() 
        
        const static uint32 item_armor_skills[MAX_ITEM_SUBCLASS_ARMOR] =
        {
            0, SKILL_CLOTH, SKILL_LEATHER, SKILL_MAIL, SKILL_PLATE_MAIL, 0, SKILL_SHIELD, 0, 0, 0, 0
        }; // Copied from function Item::GetSkill()
        
        bool hasSkill = true;
        switch (item_template->Class) {
            case ITEM_CLASS_WEAPON:
                if (item_weapon_skills[item_template->SubClass])
                    hasSkill = player->GetSkillValue(item_weapon_skills[item_template->SubClass]) != 0;
                break;
            case ITEM_CLASS_ARMOR:
                if (item_armor_skills[item_template->SubClass])
                    hasSkill = player->GetSkillValue(item_armor_skills[item_template->SubClass]) != 0;
                break;
            default: // Gear vendor should not have any other item classes...
                return false;
        }
        
        return CanEquipInSlot(player, item_template, equipmentSlot) && hasSkill;
    }
    
    bool CanEquipInSlot(Player* player, ItemTemplate const* proto, uint32 slot) {
        uint8 playerClass = player->getClass();
        
        uint8 slots[2];
        slots[0] = NULL_SLOT;
        slots[1] = NULL_SLOT;
        switch (proto->InventoryType)
        {
            case INVTYPE_HEAD:
                slots[0] = EQUIPMENT_SLOT_HEAD;
                break;
            case INVTYPE_NECK:
                slots[0] = EQUIPMENT_SLOT_NECK;
                break;
            case INVTYPE_SHOULDERS:
                slots[0] = EQUIPMENT_SLOT_SHOULDERS;
                break;
            case INVTYPE_BODY:
                slots[0] = EQUIPMENT_SLOT_BODY;
                break;
            case INVTYPE_CHEST:
                slots[0] = EQUIPMENT_SLOT_CHEST;
                break;
            case INVTYPE_ROBE:
                slots[0] = EQUIPMENT_SLOT_CHEST;
                break;
            case INVTYPE_WAIST:
                slots[0] = EQUIPMENT_SLOT_WAIST;
                break;
            case INVTYPE_LEGS:
                slots[0] = EQUIPMENT_SLOT_LEGS;
                break;
            case INVTYPE_FEET:
                slots[0] = EQUIPMENT_SLOT_FEET;
                break;
            case INVTYPE_WRISTS:
                slots[0] = EQUIPMENT_SLOT_WRISTS;
                break;
            case INVTYPE_HANDS:
                slots[0] = EQUIPMENT_SLOT_HANDS;
                break;
            case INVTYPE_FINGER:
                slots[0] = EQUIPMENT_SLOT_FINGER1;
                slots[1] = EQUIPMENT_SLOT_FINGER2;
                break;
            case INVTYPE_TRINKET:
                slots[0] = EQUIPMENT_SLOT_TRINKET1;
                slots[1] = EQUIPMENT_SLOT_TRINKET2;
                break;
            case INVTYPE_CLOAK:
                slots[0] = EQUIPMENT_SLOT_BACK;
                break;
            case INVTYPE_WEAPON:
            {
                slots[0] = EQUIPMENT_SLOT_MAINHAND;
                
                // suggest offhand slot only if know dual wielding
                // (this will be replace mainhand weapon at auto equip instead unwonted "you don't known dual wielding" ...
                if (player->CanDualWield())
                    slots[1] = EQUIPMENT_SLOT_OFFHAND;
                break;
            }
            case INVTYPE_SHIELD:
                slots[0] = EQUIPMENT_SLOT_OFFHAND;
                break;
            case INVTYPE_RANGED:
                slots[0] = EQUIPMENT_SLOT_RANGED;
                break;
            case INVTYPE_2HWEAPON:
                slots[0] = EQUIPMENT_SLOT_MAINHAND;
                if (player->CanDualWield() && player->CanTitanGrip() && proto->SubClass != ITEM_SUBCLASS_WEAPON_POLEARM && proto->SubClass != ITEM_SUBCLASS_WEAPON_STAFF)
                    slots[1] = EQUIPMENT_SLOT_OFFHAND;
                break;
            case INVTYPE_TABARD:
                slots[0] = EQUIPMENT_SLOT_TABARD;
                break;
            case INVTYPE_WEAPONMAINHAND:
                slots[0] = EQUIPMENT_SLOT_MAINHAND;
                break;
            case INVTYPE_WEAPONOFFHAND:
                // suggest offhand slot only if know dual wielding
                if (player->CanDualWield())
                    slots[0] = EQUIPMENT_SLOT_OFFHAND;
                break;
            case INVTYPE_HOLDABLE:
                slots[0] = EQUIPMENT_SLOT_OFFHAND;
                break;
            case INVTYPE_THROWN:
                slots[0] = EQUIPMENT_SLOT_RANGED;
                break;
            case INVTYPE_RANGEDRIGHT:
                slots[0] = EQUIPMENT_SLOT_RANGED;
                break;
            case INVTYPE_RELIC:
            {
                switch (proto->SubClass) {
                    case ITEM_SUBCLASS_ARMOR_LIBRAM:
                        if (playerClass == CLASS_PALADIN)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                    case ITEM_SUBCLASS_ARMOR_IDOL:
                        if (playerClass == CLASS_DRUID)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                    case ITEM_SUBCLASS_ARMOR_TOTEM:
                        if (playerClass == CLASS_SHAMAN)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                    case ITEM_SUBCLASS_ARMOR_MISC:
                        if (playerClass == CLASS_WARLOCK)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                    case ITEM_SUBCLASS_ARMOR_SIGIL:
                        if (playerClass == CLASS_DEATH_KNIGHT)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                }
                break;
            }
            default:
                return false;
        }
        
        if (slot != NULL_SLOT)
            for (uint8 i = 0; i < 2; ++i)
                if (slots[i] == slot)
                    return true;
        
        return false;
    }
    
    void SendErrorMenu(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_equipmentSlotSelection.erase(player);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, EQUIPMENT_SLOT_END);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return;
    }
    
    void SendPurchaseSuccessMenu(Player* player, Creature* creature, Item* purchasedItem) {
        if (!purchasedItem)
            return SendErrorMenu(player, creature);
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_equipmentSlotSelection.erase(player);
        std::stringstream ss;
        ss<<"You have successfully purchased |cff303030["<<purchasedItem->GetTemplate()->Name1<<"]|r. Browse more items?";
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), 0, EQUIPMENT_SLOT_END);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return;
    }
    
    std::string GetItemSlotDescription(uint8 equipmentSlot) {
        std::ostringstream os;
        os << " |TInterface\\Icons\\";
        switch (equipmentSlot) {
            case EQUIPMENT_SLOT_HEAD:       os << "inv_helmet_03.blp:36:36:-22:1|tHead"; break;
            case EQUIPMENT_SLOT_NECK:       os << "inv_jewelry_amulet_05.blp:36:36:-22:1|tNeck"; break;
            case EQUIPMENT_SLOT_SHOULDERS:  os << "inv_shoulder_28.blp:36:36:-22:1|tShoulders"; break;
            case EQUIPMENT_SLOT_CHEST:      os << "inv_chest_chain_07.blp:36:36:-22:1|tChest"; break;
            case EQUIPMENT_SLOT_WAIST:      os << "inv_belt_03.blp:36:36:-22:1|tWaist"; break;
            case EQUIPMENT_SLOT_LEGS:       os << "inv_pants_02.blp:36:36:-22:1|tLegs"; break;
            case EQUIPMENT_SLOT_FEET:       os << "inv_boots_01.blp:36:36:-22:1|tFeet"; break;
            case EQUIPMENT_SLOT_WRISTS:     os << "inv_bracer_06.blp:36:36:-22:1|tWrist"; break;
            case EQUIPMENT_SLOT_HANDS:      os << "inv_gauntlets_05.blp:36:36:-22:1|tHands"; break;
            case EQUIPMENT_SLOT_FINGER1:    os << "inv_jewelry_ring_03.blp:36:36:-22:1|tFinger"; break;
            case EQUIPMENT_SLOT_TRINKET1:   os << "inv_jewelry_talisman_05.blp:36:36:-22:1|tTrinket"; break;
            case EQUIPMENT_SLOT_BACK:       os << "inv_misc_cape_18.blp:36:36:-22:1|tBack"; break;
            case EQUIPMENT_SLOT_MAINHAND:   os << "inv_throwingaxe_06.blp:36:36:-22:1|tMain hand"; break;
            case EQUIPMENT_SLOT_OFFHAND:    os << "inv_misc_book_11.blp:36:36:-22:1|tOff Hand"; break;
            case EQUIPMENT_SLOT_RANGED:     os << "inv_jewelry_talisman_06.blp:36:36:-22:1|tRanged/Relic"; break;
            case EQUIPMENT_SLOT_TABARD:     os << "inv_shirt_guildtabard_01.blp:36:36:-22:1|tTabard"; break;
            default:                        return "";
        }
        
        return os.str();
    }
};

enum gossipOptions {
    OPT_NEW_ITEM_MENU               = 1,
    OPT_EXISTING_ITEM_MENU          = 2,
    OPT_REFUNDABLE_PURCHASES_MENU   = 3,
    OPT_VIEW_INVENTORY              = 4,
    OPT_EXISTING_ITEM_OPTIONS       = 5,
    OPT_REQUEST_REFUND              = 6,
    OPT_RENT_NEW_ITEM               = 7,
    OPT_PURCHASE_NEW_ITEM           = 8,
    OPT_RENT_EXISTING_ITEM          = 9,
    OPT_PURCHASE_EXISTING_ITEM      = 10
};

class stronkcc_quartermaster : public CreatureScript, public PlayerScript, public WorldScript
{
private:
    struct itemData {
        uint32 InvType;
        uint32 SubClass;
        uint32 ItemId;
    };
    typedef std::vector<itemData> availableItemTypes;
    typedef std::map<Creature*, availableItemTypes> creatureData;
    creatureData m_creatureData;
    struct Cost {
        uint32 votePointCost;
        uint32 donationPointCost;
    };
    typedef std::map<uint32, Cost> ItemCost;
    ItemCost m_itemCost;
    typedef std::map<Player*, uint32> PlayerSelection;
    PlayerSelection m_playerSelection;
    uint32 leaseTime;
    uint32 gracePeriod;
    struct RentalData {
        RentalData(): itemGUID(0), itemEntry(0), playerGUID(0), expirationTime(0), endGracePeriod(0) { }
        
        uint64 itemGUID;
        uint32 itemEntry;
        uint64 playerGUID;
        uint32 expirationTime;
        uint32 endGracePeriod;
    };
    typedef std::list<RentalData*> RentalDataContainer;
    typedef std::map<uint64, RentalDataContainer> PlayerRentalData;
    PlayerRentalData m_playerRentalData;
    RentalDataContainer m_leaseExpirationQueue;
    RentalDataContainer m_itemRemovalQueue;
    uint32 refundTime;
    struct RefundablePurchaseData {
        RefundablePurchaseData(): purchaseTime(0), itemGUID(0), itemEntry(0), playerGUID(0), 
        votePointCost(0), donationPointCost(0), refundExpiration(0), isRefunded(0) { }
        
        uint32 purchaseTime;
        uint64 itemGUID;
        uint32 itemEntry;
        uint64 playerGUID;
        uint32 votePointCost;
        uint32 donationPointCost;
        uint32 refundExpiration;
        uint8 isRefunded;
    };
    typedef std::list<RefundablePurchaseData*> RefundablePurchases;
    RefundablePurchases m_refundablePurchases;
public:
    stronkcc_quartermaster() :
    CreatureScript("stronkcc_quartermaster"),
    PlayerScript("stronkcc_quartermaster_PS"),
    WorldScript("stronkcc_quartermaster_WS") {
        PreparedStatement* stmt = NULL;
        m_itemCost.clear();
        stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_STORE_ITEM_COST);
        if (PreparedQueryResult result = WorldDatabase.Query(stmt)) {
            do {
                Field* fields = result->Fetch();
                Cost m_cost;
                m_cost.votePointCost        = fields[1].GetUInt32();
                m_cost.donationPointCost    = fields[2].GetUInt32();
                m_itemCost[fields[0].GetUInt32()] = m_cost;
            }
            while (result->NextRow());
        }
        m_playerRentalData.clear();
        m_leaseExpirationQueue.clear();
        m_itemRemovalQueue.clear();
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_RENTAL_DATA);
        if (PreparedQueryResult result = CharacterDatabase.Query(stmt)) {
            do {
                Field* fields = result->Fetch();
                RentalData* m_rentalData        = new RentalData;
                m_rentalData->itemGUID          = MAKE_NEW_GUID(fields[0].GetUInt32(), 0, HIGHGUID_ITEM);
                m_rentalData->itemEntry         = fields[1].GetUInt32();
                m_rentalData->playerGUID        = MAKE_NEW_GUID(fields[2].GetUInt32(), 0, HIGHGUID_PLAYER);
                m_rentalData->expirationTime    = fields[3].GetUInt32();
                m_rentalData->endGracePeriod    = fields[4].GetUInt32();
                m_playerRentalData[m_rentalData->playerGUID].push_back(m_rentalData);
                if (m_rentalData->expirationTime)
                    AddRentalDataToQueue(m_rentalData, m_leaseExpirationQueue);
                else
                    AddRentalDataToQueue(m_rentalData, m_itemRemovalQueue);
            }
            while (result->NextRow());
        }
        CharacterDatabase.Execute(CharacterDatabase.GetPreparedStatement(CHAR_DEL_BAD_RENTAL_DATA));
        m_refundablePurchases.clear();
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_REFUNDABLE_PURCHASE_DATA);
        stmt->setUInt32(0, sWorld->GetGameTime());
        if (PreparedQueryResult result = CharacterDatabase.Query(stmt)) {
            do {
                Field* fields = result->Fetch();
                RefundablePurchaseData* m_refundData    = new RefundablePurchaseData;
                m_refundData->purchaseTime              = fields[0].GetUInt32();
                m_refundData->itemGUID                  = MAKE_NEW_GUID(fields[1].GetUInt32(), 0, HIGHGUID_ITEM);
                m_refundData->itemEntry                 = fields[2].GetUInt32();
                m_refundData->playerGUID                = MAKE_NEW_GUID(fields[3].GetUInt32(), 0, HIGHGUID_PLAYER);
                m_refundData->votePointCost             = fields[4].GetUInt32();
                m_refundData->donationPointCost         = fields[5].GetUInt32();
                m_refundData->refundExpiration          = fields[6].GetUInt32();
                m_refundData->isRefunded                = fields[7].GetUInt8();
                m_refundablePurchases.push_back(m_refundData);
            }
            while (result->NextRow());
        }
        CharacterDatabase.Execute(CharacterDatabase.GetPreparedStatement(CHAR_UPD_BAD_REFUNDABLE_PURCHASE_DATA));
    }
    
    void OnConfigLoad(bool /*reload*/) {
        leaseTime = sConfigMgr->GetIntDefault("StronkCC.Quartermaster.LeaseTime", 1440);
        gracePeriod = sConfigMgr->GetIntDefault("StronkCC.Quartermaster.GracePeriod", 15);
        refundTime = sConfigMgr->GetIntDefault("StronkCC.Quartermaster.RefundTime", 15);
    }
    
    void OnUpdate(Creature* /*creature*/, uint32 /*diff*/) { }
    
    void OnUpdate(uint32 /*diff*/) {
        if (!m_leaseExpirationQueue.empty())
            ProcessLeaseExpiration(*(m_leaseExpirationQueue.begin()));
        if (!m_itemRemovalQueue.empty())
            ProcessItemRemoval(*(m_itemRemovalQueue.begin()));
    }
    
    void ProcessLeaseExpiration(RentalData* rentalData) {
        if (rentalData->expirationTime > sWorld->GetGameTime())
            return;
        rentalData->expirationTime = 0;
        RemoveFromLeaseExpirationQueue(rentalData);
        rentalData->endGracePeriod  = sWorld->GetGameTime() + gracePeriod * MINUTE;
        AddRentalDataToQueue(rentalData, m_itemRemovalQueue);
        SaveRentalDataToDB(rentalData);
        if (Player* player = sObjectAccessor->FindPlayer(rentalData->playerGUID))
            if (Item* item = player->GetItemByGuid(rentalData->itemGUID))
                ChatHandler(player->GetSession()).PSendSysMessage("Your lease on %s will expire in%s.", GetItemDescription(item->GetTemplate(), true).c_str(), 
                                                                  GetTimeString(rentalData->endGracePeriod - sWorld->GetGameTime()).c_str());
    }
    
    void ProcessItemRemoval(RentalData* rentalData) {
        if (rentalData->endGracePeriod > sWorld->GetGameTime())
            return;
        if (Player* player = sObjectAccessor->FindPlayer(rentalData->playerGUID)) {
            if (Item* item = player->GetItemByGuid(rentalData->itemGUID)) {
                ChatHandler(player->GetSession()).PSendSysMessage("Your lease on %s has expired.", GetItemDescription(item->GetTemplate(), true).c_str());
                player->DestroyItem(item->GetBagSlot(), item->GetSlot(), true);
                SavePlayerInventory(player);
            }
        }
        else {
            PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_ITEM_INSTANCE);
            stmt->setUInt32(0, GUID_LOPART(rentalData->itemGUID));
            CharacterDatabase.Execute(stmt);
        }
        RemoveFromItemRemovalQueue(rentalData);
        RemovePlayerRentalData(rentalData);
    }
    
    void OnLogout(Player *player) {
        // Clear player data on logout
        m_playerSelection.erase(player);
    }
    
    bool OnGossipHello(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_playerSelection.erase(player);
        FillCreatureData(creature);
        if (!sWorldPvPMgr->CanInteract(player, creature))
            return true;
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "I'd like to purchase or rent a new item.", 0, OPT_NEW_ITEM_MENU);
        if (HasLeasedItems(player))
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "I'd like to purchase or extend a lease on an existing item.", 0, OPT_EXISTING_ITEM_MENU);
        if (HasRefundableItems(player))
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I'd like to request a refund for a recently purchased item.", 0, OPT_REFUNDABLE_PURCHASES_MENU);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());

        return true;
    }
    
    bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction) {
        if (!uiAction)
            return OnGossipHello(player, creature);
        switch (uiAction) {
            case OPT_NEW_ITEM_MENU:
                SendNewItemMenu(player, creature);
                break;
            case OPT_EXISTING_ITEM_MENU:
                SendExistingItemMenu(player, creature);
                break;
            case OPT_REFUNDABLE_PURCHASES_MENU:
                SendRefundablePurchasesMenu(player, creature);
                break;
            case OPT_VIEW_INVENTORY:
                m_playerSelection[player] = uiSender;
                player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            case OPT_EXISTING_ITEM_OPTIONS:
            {
                if (!uiSender) {
                    SendErrorMenu(player, creature);
                    break;
                }
                if (Item* item = player->GetItemByGuid(MAKE_NEW_GUID(uiSender, 0, HIGHGUID_ITEM)))
                    SendExistingItemOptionsMenu(player, creature, item);
                else {
                    EraseRentalData(GetRentalDataByItemGUID(player, MAKE_NEW_GUID(uiSender, 0, HIGHGUID_ITEM)));
                    SendErrorMenu(player, creature);
                }
                break;
            }
            case OPT_REQUEST_REFUND:
            {
                if (!uiSender) {
                    SendErrorMenu(player, creature);
                    break;
                }
                if (Item* item = player->GetItemByGuid(MAKE_NEW_GUID(uiSender, 0, HIGHGUID_ITEM))) {
                    RefundPurchasedItem(player, creature, item);
                    return OnGossipHello(player, creature);
                }
                else {
                    EraseRefundablePurchaseData(GetRefundablePurchaseDataByItemGUID(player, MAKE_NEW_GUID(uiSender, 0, HIGHGUID_ITEM)));
                    SendErrorMenu(player, creature);
                }
                break;
            }
            case OPT_RENT_NEW_ITEM:
                if (!uiSender) {
                    SendErrorMenu(player, creature);
                    break;
                }
                AddItemToPlayer(player, creature, uiSender, NULL, true);
                break;
            case OPT_PURCHASE_NEW_ITEM:
                if (!uiSender) {
                    SendErrorMenu(player, creature);
                    break;
                }
                AddItemToPlayer(player, creature, uiSender, NULL, false);
                break;
            case OPT_RENT_EXISTING_ITEM:
            {
                if (!uiSender) {
                    SendErrorMenu(player, creature);
                    break;
                }
                if (Item* item = player->GetItemByGuid(MAKE_NEW_GUID(uiSender, 0, HIGHGUID_ITEM)))
                    AddItemToPlayer(player, creature, item->GetEntry(), item, true);
                else {
                    EraseRentalData(GetRentalDataByItemGUID(player, MAKE_NEW_GUID(uiSender, 0, HIGHGUID_ITEM)));
                    SendErrorMenu(player, creature);
                }
                break;
            }
            case OPT_PURCHASE_EXISTING_ITEM:
            {
                if (!uiSender) {
                    SendErrorMenu(player, creature);
                    break;
                }
                if (Item* item = player->GetItemByGuid(MAKE_NEW_GUID(uiSender, 0, HIGHGUID_ITEM)))
                    AddItemToPlayer(player, creature, item->GetEntry(), item, false);
                else {
                    EraseRentalData(GetRentalDataByItemGUID(player, MAKE_NEW_GUID(uiSender, 0, HIGHGUID_ITEM)));
                    SendErrorMenu(player, creature);
                }
                break;
            }
            default:
                SendErrorMenu(player, creature);
                break;
        }
        
        return true;
    }
    
    void SendNewItemMenu(Player* player, Creature* creature) {
        m_playerSelection.erase(player);
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i) {
            if (i == EQUIPMENT_SLOT_BODY || i == EQUIPMENT_SLOT_FINGER2 ||
                i == EQUIPMENT_SLOT_TRINKET2)
                continue;
            if (ShowEquipmentSlot(player, creature, i))
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetItemSlotDescription(i), i, OPT_VIEW_INVENTORY);
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
    }
    
    void SendExistingItemMenu(Player* player, Creature* creature) {
        m_playerSelection.erase(player);
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        PlayerRentalData::iterator itr = m_playerRentalData.find(player->GetGUID());
        if (itr == m_playerRentalData.end() || itr->second.empty())
            return SendErrorMenu(player, creature);
        uint8 itemLimit = 0;
        for (RentalDataContainer::iterator itr1 = itr->second.begin(); itr1 != itr->second.end();) {
            if (itemLimit >= MAX_GOSSIP_ITEMS)
                break;
            Item* item = player->GetItemByGuid((*itr1)->itemGUID);
            if (item && item->GetEntry() == (*itr1)->itemEntry) {
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetItemDescription(item->GetTemplate(), false, true), item->GetGUIDLow(), OPT_EXISTING_ITEM_OPTIONS);
                ++itemLimit;
                ++itr1;
            }
            else {
                RentalData* rentalData = GetRentalDataByItemGUID(player, (*itr1)->itemGUID);
                RemoveFromLeaseExpirationQueue(rentalData);
                RemoveFromItemRemovalQueue(rentalData);
                itr->second.erase(itr1++);
                DeleteRentalDataFromDB(rentalData);
                delete rentalData;
            }
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
    }
    
    void SendRefundablePurchasesMenu(Player* player, Creature* creature) {
        m_playerSelection.erase(player);
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        for (RefundablePurchases::iterator itr = m_refundablePurchases.begin(); itr != m_refundablePurchases.end();)
            if ((*itr)->playerGUID != player->GetGUID())
                ++itr;
            else {
                Item* item = player->GetItemByGuid((*itr)->itemGUID);
                if (item && item->GetEntry() == (*itr)->itemEntry && (*itr)->refundExpiration > sWorld->GetGameTime() && !(*itr)->isRefunded) {
                    uint32 votePointCost = 0, donationPointCost = 0;
                    GetItemCost(item->GetTemplate()->ItemId, votePointCost, donationPointCost);
                    std::ostringstream os;
                    os  << "Are you sure that you wish to be refunded |cffffcc00" << donationPointCost << " donation points|r for the following item:\n\n"
                        << GetItemDescription(item->GetTemplate(), true, true, 30, -4, -11) << "\n\n"
                        << "This action cannot be undone and will result in a permanent loss of the item.";
                    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                                     GetItemDescription(item->GetTemplate(), false, true),
                                                     item->GetGUIDLow(),
                                                     OPT_REQUEST_REFUND,
                                                     os.str(),
                                                     0,
                                                     false);
                    ++itr;
                }
                else {
                    RefundablePurchaseData* refundData = GetRefundablePurchaseDataByItemGUID(player, (*itr)->itemGUID);
                    m_refundablePurchases.erase(itr++);
                    delete refundData;
                }
            }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
    }    
    
    void SendExistingItemOptionsMenu(Player* player, Creature* creature, Item* item) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        uint32 votePointCost = 0, donationPointCost = 0;
        GetItemCost(item->GetEntry(), votePointCost, donationPointCost);
        if (!votePointCost && !donationPointCost)
            return SendErrorMenu(player, creature);
        if (votePointCost) {
            std::stringstream rentItem;
            rentItem    << "Extend your lease on |cff303030[" << item->GetTemplate()->Name1 << "]|r for " << votePointCost << " vote points."
                        << " Your new lease will expire in" << GetTimeString(GetRemainingLeaseTime(player, item) + leaseTime * MINUTE) << ".";
            std::stringstream rentConfirm;
            rentConfirm << "Are you sure that you wish to extend the lease on the following item for |cffffcc00" 
                        << votePointCost << " vote points|r?\n\n"
                        << GetItemDescription(item->GetTemplate(), true, true, 30, -4, -7) << "\n";
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT,
                                             rentItem.str().c_str(),
                                             item->GetGUIDLow(),
                                             OPT_RENT_EXISTING_ITEM,
                                             rentConfirm.str().c_str(),
                                             0,
                                             false);
        }
        if (donationPointCost) {
            std::stringstream purchaseItem;
            purchaseItem << "Purchase |cff303030[" << item->GetTemplate()->Name1 << "]|r for " << donationPointCost << " donation points.";
            std::stringstream purchaseConfirm;
            purchaseConfirm << "Are you sure that you wish to purchase the following item for |cffffcc00" 
                            << donationPointCost << " donation points|r?\n\n"
                            << GetItemDescription(item->GetTemplate(), true, true, 30, -4, -7) << "\n";
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT,
                                             purchaseItem.str().c_str(),
                                             item->GetGUIDLow(),
                                             OPT_PURCHASE_EXISTING_ITEM,
                                             purchaseConfirm.str().c_str(),
                                             0,
                                             false);
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
    }
    
    bool ShouldDisplayVendorItem(Player* player, Creature* vendor, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/) {
        return  sWorldPvPMgr->IsItemRewardAvailableInArea(item_template->ItemId, vendor->GetAreaAffiliation()) && 
                IsUsableGearItem(player, item_template, m_playerSelection[player]);
    }
    
    bool OnBeforePurchaseItem(Player *player, Creature *vendor, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (!sWorldPvPMgr->CanInteract(player, vendor)) {
            m_playerSelection.erase(player);
            return false;
        }
        uint32 votePointCost = 0, donationPointCost = 0;
        GetItemCost(item_template->ItemId, votePointCost, donationPointCost);
        if (!votePointCost && !donationPointCost) {
            SendErrorMenu(player, vendor);
            return false;
        }
        if (votePointCost) {
            std::stringstream rentItem;
            rentItem << "Lease |cff303030[" << item_template->Name1 << "]|r for " << votePointCost << " vote points.";
            std::stringstream rentConfirm;
            rentConfirm << "Are you sure that you wish to lease the following item for |cffffcc00" 
                        << votePointCost << " vote points|r?\n\n"
                        << GetItemDescription(item_template, true, true, 30, -4, -7) << "\n";
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT,
                                             rentItem.str().c_str(),
                                             item_template->ItemId,
                                             OPT_RENT_NEW_ITEM,
                                             rentConfirm.str().c_str(),
                                             0,
                                             false);
        }
        if (donationPointCost) {
            std::stringstream purchaseItem;
            purchaseItem << "Purchase |cff303030[" << item_template->Name1 << "]|r for " << donationPointCost << " donation points.";
            std::stringstream purchaseConfirm;
            purchaseConfirm << "Are you sure that you wish to purchase the following item for |cffffcc00" 
                            << donationPointCost << " donation points|r?\n\n"
                            << GetItemDescription(item_template, true, true, 30, -4, -7) << "\n";
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT,
                                             purchaseItem.str().c_str(),
                                             item_template->ItemId,
                                             OPT_PURCHASE_NEW_ITEM,
                                             purchaseConfirm.str().c_str(),
                                             0,
                                             false);
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, vendor->GetGUID());
        
        return false;
    }
    
    void AddItemToPlayer(Player* player, Creature* vendor, uint32 itemId, Item* item, bool isRental) {
        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
        if (!itemTemplate)
            SendErrorMenu(player, vendor);
        uint32 votePointCost = 0, donationPointCost = 0;
        GetItemCost(itemId, votePointCost, donationPointCost);
        if (item) {
            RentalData* rentalData = GetRentalDataByItemGUID(player, item->GetGUID());
            if (!rentalData || item->GetEntry() != rentalData->itemEntry)
                SendErrorMenu(player, vendor);
            if (!CanAffordItem(player, votePointCost, donationPointCost, isRental))
                return;
            if (isRental) {
                RemoveFromLeaseExpirationQueue(rentalData);
                RemoveFromItemRemovalQueue(rentalData);
                rentalData->expirationTime = std::max(rentalData->expirationTime, (uint32)sWorld->GetGameTime()) + leaseTime * MINUTE;
                AddRentalDataToQueue(rentalData, m_leaseExpirationQueue);
                SaveRentalDataToDB(rentalData);
            }
            else {
                RefundablePurchaseData* m_refundData = new RefundablePurchaseData;
                m_refundData->purchaseTime = sWorld->GetGameTime();
                m_refundData->itemGUID = item->GetGUID();
                m_refundData->itemEntry = item->GetEntry();
                m_refundData->playerGUID = player->GetGUID();
                m_refundData->votePointCost = 0;
                m_refundData->donationPointCost = donationPointCost;
                m_refundData->refundExpiration = sWorld->GetGameTime() + refundTime * MINUTE;
                m_refundData->isRefunded = 0;
                m_refundablePurchases.push_back(m_refundData);
                SaveRefundablePurchaseDataToDB(m_refundData);
                EraseRentalData(rentalData);
            }
            SendPurchaseSuccessMenu(player, vendor, item, isRental, true);
            return;
        }
        else {
            ItemPosCountVec dest;
            InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemId, 1);
            if (msg == EQUIP_ERR_OK) {
                if (!CanAffordItem(player, votePointCost, donationPointCost, isRental))
                    return;
                if (Item* item = player->StoreNewItem(dest, itemId, true, Item::GenerateItemRandomPropertyId(itemId))) {
                    player->SendNewItem(item, 1, true, false);
                    if (isRental) {
                        RentalData* m_rentalData = new RentalData;
                        m_rentalData->itemGUID = item->GetGUID();
                        m_rentalData->itemEntry = item->GetEntry();
                        m_rentalData->playerGUID = player->GetGUID();
                        m_rentalData->expirationTime = sWorld->GetGameTime() + leaseTime * MINUTE;
                        m_playerRentalData[m_rentalData->playerGUID].push_back(m_rentalData);
                        AddRentalDataToQueue(m_rentalData, m_leaseExpirationQueue);
                        SaveRentalDataToDB(m_rentalData);
                    }
                    else {
                        RefundablePurchaseData* m_refundData = new RefundablePurchaseData;
                        m_refundData->purchaseTime = sWorld->GetGameTime();
                        m_refundData->itemGUID = item->GetGUID();
                        m_refundData->itemEntry = item->GetEntry();
                        m_refundData->playerGUID = player->GetGUID();
                        m_refundData->votePointCost = 0;
                        m_refundData->donationPointCost = donationPointCost;
                        m_refundData->refundExpiration = sWorld->GetGameTime() + refundTime * MINUTE;
                        m_refundData->isRefunded = 0;
                        m_refundablePurchases.push_back(m_refundData);
                        SaveRefundablePurchaseDataToDB(m_refundData);
                    }
                    SavePlayerInventory(player);
                    SendPurchaseSuccessMenu(player, vendor, item, isRental, false);
                    return;
                }
            }
            player->SendEquipError(msg, NULL, NULL);
            player->PlayerTalkClass->SendCloseGossip();
            m_playerSelection.erase(player);
            return;
        }
    }
    
    void RefundPurchasedItem(Player* player, Creature* creature, Item* item) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_playerSelection.erase(player);
        RefundablePurchaseData* refundData = GetRefundablePurchaseDataByItemGUID(player, item->GetGUID());
        if (!refundData || item->GetEntry() != refundData->itemEntry || 
            refundData->refundExpiration <= sWorld->GetGameTime() || refundData->isRefunded)
            SendErrorMenu(player, creature);
        uint32 donationPointCost = refundData->donationPointCost;
        if (sSiteMgr->ModifyDonationPoints(player, donationPointCost)) {
            ChatHandler(player->GetSession()).PSendSysMessage("You will be refunded %u donation points for %s.", 
                                                              donationPointCost, GetItemDescription(item->GetTemplate(), true).c_str());
            player->DestroyItem(item->GetBagSlot(), item->GetSlot(), true);
            SavePlayerInventory(player);
            refundData->isRefunded = 1;
            SaveRefundablePurchaseDataToDB(refundData);
            EraseRefundablePurchaseData(refundData);
        }
    }
    
    void SendErrorMenu(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_playerSelection.erase(player);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return;
    }
    
    void SendPurchaseSuccessMenu(Player* player, Creature* creature, Item* purchasedItem, bool isRental, bool isExistingItem) {
        if (!purchasedItem)
            return SendErrorMenu(player, creature);
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_playerSelection.erase(player);
        std::stringstream ss;
        ss  << "You have successfully " << (isExistingItem && isRental ? "extended your lease on " : (isRental ? "leased " : "purchased ")) 
            << "|cff303030["<<purchasedItem->GetTemplate()->Name1<<"]|r.";
        if (isRental)
            ss << " Your rental will expire in" << GetTimeString(GetRemainingLeaseTime(player, purchasedItem)) << ".";
        ss << " Browse more items?";
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), 0, OPT_NEW_ITEM_MENU);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return;
    }
    
    void FillCreatureData(Creature* creature) { // Run once
        creatureData::iterator iter = m_creatureData.find(creature);
        if(iter == m_creatureData.end())
            if (VendorItemData const* items = creature->GetVendorItems())
                for (VendorItemList::const_iterator iter1 = items->m_items.begin(); iter1 != items->m_items.end(); ++iter1)
                    if (VendorItem const* item = *iter1)
                        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(item->item)) {
                            if (!sWorldPvPMgr->IsItemRewardAvailableInArea(itemTemplate->ItemId, creature->GetAreaAffiliation()))
                                continue;
                            itemData m_itemData;
                            m_itemData.InvType = itemTemplate->InventoryType;
                            m_itemData.SubClass = itemTemplate->SubClass;
                            m_itemData.ItemId = itemTemplate->ItemId;
                            availableItemTypes::iterator iter2 = m_creatureData[creature].begin();
                            for (; iter2 != m_creatureData[creature].end(); ++iter2)
                                if (iter2->InvType == m_itemData.InvType && iter2->SubClass == m_itemData.SubClass)
                                    break;
                            if (iter2 == m_creatureData[creature].end())
                                m_creatureData[creature].push_back(m_itemData);
                        }
        
        return;
    }
    
    bool ShowEquipmentSlot(Player* player, Creature* creature, uint32 equipmentSlot) {
        
        for (availableItemTypes::iterator iter3 = m_creatureData[creature].begin(); iter3 != m_creatureData[creature].end(); ++iter3) {
            itemData & m_itemData = *iter3;
            if (IsUsableGearItem(player, sObjectMgr->GetItemTemplate(m_itemData.ItemId), equipmentSlot))
                return true;
        }
        
        return false;
    }
    
    bool IsUsableGearItem(Player* player, ItemTemplate const* item_template, uint32 equipmentSlot) {
        const static uint32 item_weapon_skills[MAX_ITEM_SUBCLASS_WEAPON] =
        {
            SKILL_AXES,     SKILL_2H_AXES,  SKILL_BOWS,          SKILL_GUNS,      SKILL_MACES,
            SKILL_2H_MACES, SKILL_POLEARMS, SKILL_SWORDS,        SKILL_2H_SWORDS, 0,
            SKILL_STAVES,   0,              0,                   SKILL_FIST_WEAPONS,   0,
            SKILL_DAGGERS,  SKILL_THROWN,   SKILL_ASSASSINATION, SKILL_CROSSBOWS, SKILL_WANDS,
            SKILL_FISHING
        }; // Copied from function Item::GetSkill() 
        
        const static uint32 item_armor_skills[MAX_ITEM_SUBCLASS_ARMOR] =
        {
            0, SKILL_CLOTH, SKILL_LEATHER, SKILL_MAIL, SKILL_PLATE_MAIL, 0, SKILL_SHIELD, 0, 0, 0, 0
        }; // Copied from function Item::GetSkill()
        
        bool hasSkill = true;
        switch (item_template->Class) {
            case ITEM_CLASS_WEAPON:
                if (item_weapon_skills[item_template->SubClass])
                    hasSkill = player->GetSkillValue(item_weapon_skills[item_template->SubClass]) != 0;
                break;
            case ITEM_CLASS_ARMOR:
                if (item_armor_skills[item_template->SubClass])
                    hasSkill = player->GetSkillValue(item_armor_skills[item_template->SubClass]) != 0;
                break;
            default: // Gear vendor should not have any other item classes...
                return false;
        }
        
        return player->getLevel() >= item_template->RequiredLevel && CanEquipInSlot(player, item_template, equipmentSlot) && hasSkill;
    }
    
    bool CanEquipInSlot(Player* player, ItemTemplate const* proto, uint32 slot) {
        uint8 playerClass = player->getClass();
        
        uint8 slots[2];
        slots[0] = NULL_SLOT;
        slots[1] = NULL_SLOT;
        switch (proto->InventoryType) {
            case INVTYPE_HEAD:
                slots[0] = EQUIPMENT_SLOT_HEAD;
                break;
            case INVTYPE_NECK:
                slots[0] = EQUIPMENT_SLOT_NECK;
                break;
            case INVTYPE_SHOULDERS:
                slots[0] = EQUIPMENT_SLOT_SHOULDERS;
                break;
            case INVTYPE_BODY:
                slots[0] = EQUIPMENT_SLOT_BODY;
                break;
            case INVTYPE_CHEST:
                slots[0] = EQUIPMENT_SLOT_CHEST;
                break;
            case INVTYPE_ROBE:
                slots[0] = EQUIPMENT_SLOT_CHEST;
                break;
            case INVTYPE_WAIST:
                slots[0] = EQUIPMENT_SLOT_WAIST;
                break;
            case INVTYPE_LEGS:
                slots[0] = EQUIPMENT_SLOT_LEGS;
                break;
            case INVTYPE_FEET:
                slots[0] = EQUIPMENT_SLOT_FEET;
                break;
            case INVTYPE_WRISTS:
                slots[0] = EQUIPMENT_SLOT_WRISTS;
                break;
            case INVTYPE_HANDS:
                slots[0] = EQUIPMENT_SLOT_HANDS;
                break;
            case INVTYPE_FINGER:
                slots[0] = EQUIPMENT_SLOT_FINGER1;
                slots[1] = EQUIPMENT_SLOT_FINGER2;
                break;
            case INVTYPE_TRINKET:
                slots[0] = EQUIPMENT_SLOT_TRINKET1;
                slots[1] = EQUIPMENT_SLOT_TRINKET2;
                break;
            case INVTYPE_CLOAK:
                slots[0] = EQUIPMENT_SLOT_BACK;
                break;
            case INVTYPE_WEAPON:
            {
                slots[0] = EQUIPMENT_SLOT_MAINHAND;
                
                // suggest offhand slot only if know dual wielding
                // (this will be replace mainhand weapon at auto equip instead unwonted "you don't known dual wielding" ...
                if (player->CanDualWield())
                    slots[1] = EQUIPMENT_SLOT_OFFHAND;
                break;
            }
            case INVTYPE_SHIELD:
                slots[0] = EQUIPMENT_SLOT_OFFHAND;
                break;
            case INVTYPE_RANGED:
                slots[0] = EQUIPMENT_SLOT_RANGED;
                break;
            case INVTYPE_2HWEAPON:
                slots[0] = EQUIPMENT_SLOT_MAINHAND;
                if (player->CanDualWield() && player->CanTitanGrip() && proto->SubClass != ITEM_SUBCLASS_WEAPON_POLEARM && proto->SubClass != ITEM_SUBCLASS_WEAPON_STAFF)
                    slots[1] = EQUIPMENT_SLOT_OFFHAND;
                break;
            case INVTYPE_TABARD:
                slots[0] = EQUIPMENT_SLOT_TABARD;
                break;
            case INVTYPE_WEAPONMAINHAND:
                slots[0] = EQUIPMENT_SLOT_MAINHAND;
                break;
            case INVTYPE_WEAPONOFFHAND:
                // suggest offhand slot only if know dual wielding
                if (player->CanDualWield())
                    slots[0] = EQUIPMENT_SLOT_OFFHAND;
                break;
            case INVTYPE_HOLDABLE:
                slots[0] = EQUIPMENT_SLOT_OFFHAND;
                break;
            case INVTYPE_THROWN:
                slots[0] = EQUIPMENT_SLOT_RANGED;
                break;
            case INVTYPE_RANGEDRIGHT:
                slots[0] = EQUIPMENT_SLOT_RANGED;
                break;
            case INVTYPE_RELIC:
            {
                switch (proto->SubClass) {
                    case ITEM_SUBCLASS_ARMOR_LIBRAM:
                        if (playerClass == CLASS_PALADIN)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                    case ITEM_SUBCLASS_ARMOR_IDOL:
                        if (playerClass == CLASS_DRUID)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                    case ITEM_SUBCLASS_ARMOR_TOTEM:
                        if (playerClass == CLASS_SHAMAN)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                    case ITEM_SUBCLASS_ARMOR_MISC:
                        if (playerClass == CLASS_WARLOCK)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                    case ITEM_SUBCLASS_ARMOR_SIGIL:
                        if (playerClass == CLASS_DEATH_KNIGHT)
                            slots[0] = EQUIPMENT_SLOT_RANGED;
                        break;
                }
                break;
            }
            default:
                return false;
        }
        
        if (slot != NULL_SLOT)
            for (uint8 i = 0; i < 2; ++i)
                if (slots[i] == slot)
                    return true;
        
        return false;
    }
    
    std::string GetItemSlotDescription(uint8 equipmentSlot) {
        std::ostringstream os;
        os << "|TInterface\\Icons\\";
        switch (equipmentSlot) {
            case EQUIPMENT_SLOT_HEAD:       os << "inv_helmet_03.blp:36:36:-16:1|tHead"; break;
            case EQUIPMENT_SLOT_NECK:       os << "inv_jewelry_amulet_05.blp:36:36:-16:1|tNeck"; break;
            case EQUIPMENT_SLOT_SHOULDERS:  os << "inv_shoulder_28.blp:36:36:-16:1|tShoulders"; break;
            case EQUIPMENT_SLOT_CHEST:      os << "inv_chest_chain_07.blp:36:36:-16:1|tChest"; break;
            case EQUIPMENT_SLOT_WAIST:      os << "inv_belt_03.blp:36:36:-16:1|tWaist"; break;
            case EQUIPMENT_SLOT_LEGS:       os << "inv_pants_02.blp:36:36:-16:1|tLegs"; break;
            case EQUIPMENT_SLOT_FEET:       os << "inv_boots_01.blp:36:36:-16:1|tFeet"; break;
            case EQUIPMENT_SLOT_WRISTS:     os << "inv_bracer_06.blp:36:36:-16:1|tWrist"; break;
            case EQUIPMENT_SLOT_HANDS:      os << "inv_gauntlets_05.blp:36:36:-16:1|tHands"; break;
            case EQUIPMENT_SLOT_FINGER1:    os << "inv_jewelry_ring_03.blp:36:36:-16:1|tFinger"; break;
            case EQUIPMENT_SLOT_TRINKET1:   os << "inv_jewelry_talisman_05.blp:36:36:-16:1|tTrinket"; break;
            case EQUIPMENT_SLOT_BACK:       os << "inv_misc_cape_18.blp:36:36:-16:1|tBack"; break;
            case EQUIPMENT_SLOT_MAINHAND:   os << "inv_throwingaxe_06.blp:36:36:-16:1|tMain hand"; break;
            case EQUIPMENT_SLOT_OFFHAND:    os << "inv_misc_book_11.blp:36:36:-16:1|tOff Hand"; break;
            case EQUIPMENT_SLOT_RANGED:     os << "inv_jewelry_talisman_06.blp:36:36:-16:1|tRanged/Relic"; break;
            case EQUIPMENT_SLOT_TABARD:     os << "inv_shirt_guildtabard_01.blp:36:36:-16:1|tTabard"; break;
            default:                        return "";
        }
        
        return os.str();
    }

    void GetItemCost(uint32 itemId, uint32 &votePointCost, uint32 &donationPointCost) {
        ItemCost::const_iterator itr = m_itemCost.find(itemId);
        if (itr != m_itemCost.end()) {
            votePointCost = itr->second.votePointCost;
            donationPointCost = itr->second.donationPointCost;
        }
    }
    
    uint32 GetRemainingLeaseTime(Player* player, Item* item) {
        if (RentalData* rentalData = GetRentalDataByItemGUID(player, item->GetGUID()))
            return rentalData->expirationTime > sWorld->GetGameTime() ? rentalData->expirationTime - sWorld->GetGameTime() : 0;
        
        return 0;
    }
    
    std::string GetTimeString(uint32 time) {
        uint32 days = time / DAY, hours = (time % DAY) / HOUR, minutes = (time % HOUR) / MINUTE, seconds = (time % MINUTE);
        std::ostringstream ss;
        if (days) ss << " " << days << (days == 1 ? " day" : " days");
        if (hours) ss << " " << hours << (hours == 1 ? " hour" : " hours");
        if (minutes) ss << " " << minutes << (minutes == 1 ? " minute" : " minutes");
        if (!days && !hours && !minutes) ss << " " << seconds << (seconds == 1 ? " second" : " seconds");
        return ss.str();
    }
    
    RentalData* GetRentalDataByItemGUID(Player* player, uint64 itemGUID) {
        PlayerRentalData::const_iterator itr = m_playerRentalData.find(player->GetGUID());
        if (itr != m_playerRentalData.end() && !itr->second.empty())
            for (RentalDataContainer::const_iterator itr1 = itr->second.begin(); itr1 != itr->second.end(); ++itr1)
                if ((*itr1)->itemGUID == itemGUID)
                    return (*itr1);
        
        return NULL;
    }
    
    RefundablePurchaseData* GetRefundablePurchaseDataByItemGUID(Player* player, uint64 itemGUID) {
        for (RefundablePurchases::const_iterator itr = m_refundablePurchases.begin(); itr != m_refundablePurchases.end(); ++itr)
            if ((*itr)->playerGUID == player->GetGUID() && (*itr)->itemGUID == itemGUID)
                return (*itr);
        
        return NULL;
    }
    
    void AddRentalDataToQueue(RentalData* rentalData, RentalDataContainer &rentalDataQueue) {
        RentalDataContainer::iterator itr = rentalDataQueue.begin();
        for (; itr != rentalDataQueue.end(); ++itr)
            if (rentalData->expirationTime < (*itr)->expirationTime)
                break;
        rentalDataQueue.insert(itr, rentalData);
    }
    
    void RemovePlayerRentalData(RentalData* rentalData) {
        if (!rentalData)
            return;
        PlayerRentalData::iterator itr = m_playerRentalData.find(rentalData->playerGUID);
        if (itr != m_playerRentalData.end() && !itr->second.empty())
            for (RentalDataContainer::iterator itr1 = itr->second.begin(); itr1 != itr->second.end(); ++itr1)
                if ((*itr1) == rentalData) {
                    itr->second.erase(itr1);
                    DeleteRentalDataFromDB(rentalData);
                    delete rentalData;
                    return;
                }
    }
    
    void RemoveFromLeaseExpirationQueue(RentalData* rentalData) {
        if (!rentalData)
            return;
        RentalDataContainer::iterator itr;
        for (itr = m_leaseExpirationQueue.begin(); itr != m_leaseExpirationQueue.end(); ++itr)
            if ((*itr) == rentalData) {
                m_leaseExpirationQueue.erase(itr);
                return;
            }
    }
    
    void RemoveFromItemRemovalQueue(RentalData* rentalData) {
        if (!rentalData)
            return;
        RentalDataContainer::iterator itr;
        for (itr = m_itemRemovalQueue.begin(); itr != m_itemRemovalQueue.end(); ++itr)
            if ((*itr) == rentalData) {
                m_itemRemovalQueue.erase(itr);
                return;
            }
    }
    
    void EraseRentalData(RentalData* rentalData) {
        RemoveFromLeaseExpirationQueue(rentalData);
        RemoveFromItemRemovalQueue(rentalData);
        RemovePlayerRentalData(rentalData);
    }
    
    void EraseRefundablePurchaseData(RefundablePurchaseData* refundData) {
        if (!refundData)
            return;
        RefundablePurchases::iterator itr = m_refundablePurchases.begin();
        for (; itr != m_refundablePurchases.end(); ++itr)
            if ((*itr) == refundData)
                break;
        
        if (itr != m_refundablePurchases.end())
            m_refundablePurchases.erase(itr);
        
        delete refundData;
    }
    
    bool HasLeasedItems(Player* player) {
        PlayerRentalData::iterator itr = m_playerRentalData.find(player->GetGUID());
        if (itr == m_playerRentalData.end() || itr->second.empty())
            return false;
        for (RentalDataContainer::iterator itr1 = itr->second.begin(); itr1 != itr->second.end();) {
            Item* item = player->GetItemByGuid((*itr1)->itemGUID);
            if (item && item->GetEntry() == (*itr1)->itemEntry)
                return true;
            else {
                RentalData* rentalData = GetRentalDataByItemGUID(player, (*itr1)->itemGUID);
                RemoveFromLeaseExpirationQueue(rentalData);
                RemoveFromItemRemovalQueue(rentalData);
                itr->second.erase(itr1++);
                DeleteRentalDataFromDB(rentalData);
                delete rentalData;
            }
        }
        
        return false;
    }
    
    bool HasRefundableItems(Player* player) {
        for (RefundablePurchases::iterator itr = m_refundablePurchases.begin(); itr != m_refundablePurchases.end();)
            if ((*itr)->playerGUID != player->GetGUID())
                ++itr;
            else {
                Item* item = player->GetItemByGuid((*itr)->itemGUID);
                if (item && item->GetEntry() == (*itr)->itemEntry && (*itr)->refundExpiration > sWorld->GetGameTime() && !(*itr)->isRefunded)
                    return true;
                else {
                    RefundablePurchaseData* refundData = GetRefundablePurchaseDataByItemGUID(player, (*itr)->itemGUID);
                    m_refundablePurchases.erase(itr++);
                    delete refundData;
                }
            }
        
        return false;
    }
        
    bool CanAffordItem(Player* player, uint32 votePointCost, uint32 donationPointCost, bool isRental) {
        if ((isRental && votePointCost && !sSiteMgr->PerformTransaction(player, votePointCost, 0)) ||
            (!isRental && donationPointCost && !sSiteMgr->PerformTransaction(player, 0, donationPointCost))) {
            ChatHandler(player->GetSession()).PSendSysMessage("You do not have the points required to complete this transaction.");
            player->PlayerTalkClass->SendCloseGossip();
            m_playerSelection.erase(player);
            return false;
        }
        
        return true;
    }
    
    void SavePlayerInventory(Player* player) {
        SQLTransaction trans = CharacterDatabase.BeginTransaction();
        player->SaveInventoryAndGoldToDB(trans);
        CharacterDatabase.CommitTransaction(trans);
    }
    
    void SaveRentalDataToDB(RentalData* rentalData) {
        if (!rentalData)
            return;
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_RENTAL_DATA);
        stmt->setUInt32(0, GUID_LOPART(rentalData->itemGUID));
        stmt->setUInt32(1, rentalData->itemEntry);
        stmt->setUInt32(2, GUID_LOPART(rentalData->playerGUID));
        stmt->setUInt32(3, rentalData->expirationTime);
        stmt->setUInt32(4, rentalData->endGracePeriod);
        CharacterDatabase.Execute(stmt);
    }
    
    void DeleteRentalDataFromDB(RentalData* rentalData) {
        if (!rentalData)
            return;
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_RENTAL_DATA);
        stmt->setUInt32(0, GUID_LOPART(rentalData->itemGUID));
        stmt->setUInt32(1, rentalData->itemEntry);
        stmt->setUInt32(2, GUID_LOPART(rentalData->playerGUID));
        CharacterDatabase.Execute(stmt);
    }
    
    void SaveRefundablePurchaseDataToDB(RefundablePurchaseData* refundData) {
        if (!refundData)
            return;
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_REFUNDABLE_PURCHASE_DATA);
        stmt->setUInt32(0, refundData->purchaseTime);
        stmt->setUInt32(1, GUID_LOPART(refundData->itemGUID));
        stmt->setUInt32(2, refundData->itemEntry);
        stmt->setUInt32(3, GUID_LOPART(refundData->playerGUID));
        stmt->setUInt32(4, refundData->votePointCost);
        stmt->setUInt32(5, refundData->donationPointCost);
        stmt->setUInt32(6, refundData->refundExpiration);
        stmt->setUInt8(7, refundData->isRefunded);
        CharacterDatabase.Execute(stmt);
    }
    
    std::string GetItemDescription(ItemTemplate const* itemTemplate, bool coloredText = false, bool withIcon = false, uint32 iconSize = 36, int32 horizontalOffset = -16, int32 verticalOffset = 1) {
        std::ostringstream os;
        std::string itemName = itemTemplate->Name1;
        if (withIcon) {
            if (itemName.size() > MAX_LINE_CHARS) {
                itemName.resize(MAX_LINE_CHARS - 3);
                itemName += "...";
            }
            std::string iconPath = "INV_Misc_QuestionMark";
            if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(itemTemplate->DisplayInfoID))
                iconPath = displayInfo->iconPath;
            os  << "|TInterface\\Icons\\" << iconPath << ".blp:" 
                << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
        }
        os  << "|Hitem:" << itemTemplate->ItemId << ":0:0:0:0:0:0:0:0:0|h";
        if (coloredText)
            os << "|c" << std::hex << ItemQualityColors[itemTemplate->Quality] << std::dec << "[";
        os  << itemName;
        if (coloredText)
            os << "]|r";
        os  << "|h";
        
        return os.str();
    }
};

enum ProfessionVendorOptions {
    OPT_LEARN_PROFESSION_MENU = 1,
    OPT_UPGRADE_PROFESSION_MENU,
    OPT_BROWSE_GOODS,
    OPT_LEARN_PROFESSION,
    OPT_UPGRADE_PROFESSION
};
enum ProfessionSpells {
    PROFESSION_ALCHEMY         = 2259,
    PROFESSION_BLACKSMITHING   = 2018,
    PROFESSION_ENCHANTING      = 7411,
    PROFESSION_ENGINEERING     = 4036,
    PROFESSION_HERBALISM       = 2366,
    PROFESSION_INSCRIPTION     = 45357,
    PROFESSION_JEWELCRAFTING   = 25229,
    PROFESSION_LEATHERWORKING  = 2108,
    PROFESSION_MINING          = 2575,
    PROFESSION_SKINNING        = 8613,
    PROFESSION_TAILORING       = 3908
};
class stronkcc_profession_vendor : public CreatureScript
{
public:
    stronkcc_profession_vendor() : CreatureScript("stronkcc_profession_vendor") { }
    
    bool OnGossipHello(Player* player, Creature* creature)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        if (!sWorldPvPMgr->CanInteract(player, creature))
            return true;
        if (player->GetFreePrimaryProfessionPoints())
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "I'd like to learn a new profession.", 0, OPT_LEARN_PROFESSION_MENU);
        if (HasUpgradeableProfessions(player, creature))
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "I'd like to upgrade an existing profession with some new recipes.", 0, OPT_UPGRADE_PROFESSION_MENU);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, GOSSIP_TEXT_BROWSE_GOODS, 0, OPT_BROWSE_GOODS);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return true;
    }
    
    bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (!uiAction)
            return OnGossipHello(player, creature);
        switch (uiAction) {
            case OPT_LEARN_PROFESSION_MENU:
                return SendLearnProfessionMenu(player, creature);
            case OPT_UPGRADE_PROFESSION_MENU:
                return SendUpgradeProfessionMenu(player, creature);
            case OPT_BROWSE_GOODS:
                player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            case OPT_LEARN_PROFESSION:
            {
                if (!uiSender) // ERROR
                    return OnGossipHello(player, creature);
                LearnProfession(player, creature, uint8(uiSender));
                if (player->GetFreePrimaryProfessionPoints()) {
                    std::stringstream ss;
                    ss<<"You may still learn "<<player->GetFreePrimaryProfessionPoints()<<" more profession(s).";
                    ss<<" Continue browsing?";
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), 0, OPT_LEARN_PROFESSION_MENU);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                }
                else
                    return OnGossipHello(player, creature);
                break;
            }
            case OPT_UPGRADE_PROFESSION:
            {
                if (!uiSender)
                    return OnGossipHello(player, creature);
                LearnProfession(player, creature, uint8(uiSender), false);
                break;
            }
            default:
                break;
        }
        
        return true;
    }
    
    bool SendLearnProfessionMenu(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (player->GetFreePrimaryProfessionPoints())
            for (uint8 tradeSkill = TRADESKILL_ALCHEMY; tradeSkill <= TRADESKILL_INSCRIPTION; ++tradeSkill) {
                if (tradeSkill == TRADESKILL_COOKING || tradeSkill == TRADESKILL_FIRSTAID ||
                    tradeSkill == TRADESKILL_POISONS || tradeSkill == TRADESKILL_FISHING)
                    continue;
                if (GetProfessionSpellId(tradeSkill) && !player->HasSpell(GetProfessionSpellId(tradeSkill))) {
                    std::ostringstream os;
                    os << "Do you wish to learn this as your ";
                    switch (player->GetFreePrimaryProfessionPoints()) {
                        case 1: os << "second"; break;
                        case 2: os << "first"; break;
                        default: break;
                    }
                    os << " profession?\n\n" << GetProfessionDescription(tradeSkill, true, 30, -7) << "|r";
                    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, GetProfessionDescription(tradeSkill, false, 36, -16, 1),
                                                     uint32(tradeSkill), OPT_LEARN_PROFESSION, os.str(),
                                                     0, false);
                }
            }
        
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
    }
    
    bool SendUpgradeProfessionMenu(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        for (uint8 tradeSkill = TRADESKILL_ALCHEMY; tradeSkill <= TRADESKILL_INSCRIPTION; ++tradeSkill) {
            if (tradeSkill == TRADESKILL_COOKING || tradeSkill == TRADESKILL_FIRSTAID ||
                tradeSkill == TRADESKILL_POISONS || tradeSkill == TRADESKILL_FISHING)
                continue;
            if (CanUpgradeProfession(player, creature, tradeSkill)) {
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetProfessionDescription(tradeSkill, false, 36, -16, 1),
                                        uint32(tradeSkill), OPT_UPGRADE_PROFESSION);
            }
        }
        
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
    }
    
    bool HasUpgradeableProfessions(Player* player, Creature* creature) {
        if (creature->GetAreaAffiliation())
            for (uint8 tradeSkill = TRADESKILL_ALCHEMY; tradeSkill <= TRADESKILL_INSCRIPTION; ++tradeSkill) {
                if (tradeSkill == TRADESKILL_COOKING || tradeSkill == TRADESKILL_FIRSTAID ||
                    tradeSkill == TRADESKILL_POISONS || tradeSkill == TRADESKILL_FISHING)
                    continue;
                if (CanUpgradeProfession(player, creature, tradeSkill))
                    return true;
            }
        
        return false;
    }
    
    bool CanUpgradeProfession(Player* player, Creature* creature, uint8 tradeSkill) {
        return  GetProfessionSpellId(tradeSkill) && GetProfessionSkillId(tradeSkill) &&
                player->HasSpell(GetProfessionSpellId(tradeSkill)) &&
                sWorldPvPMgr->CanUpgradeProfSpellsInArea(player, GetProfessionSkillId(tradeSkill), creature->GetAreaAffiliation());
    }
    
    void LearnProfession(Player* player, Creature* creature, uint8 tradeSkill, bool isLearningProfession = true)
    {
        ChatHandler handler(player->GetSession());
        if (!tradeSkill || !GetProfessionSpellId(tradeSkill) || !GetProfessionSkillId(tradeSkill)) {
            handler.PSendSysMessage("An error has occurred. Please try again.");
            return;
        }
        if (!sWorldPvPMgr->CanInteract(player, creature))
            return;
        if (isLearningProfession) {
            if (player->HasSpell(GetProfessionSpellId(tradeSkill))) {
                handler.PSendSysMessage("You already know %s!", GetProfessionDescription(tradeSkill).c_str());
                return;
            }
            if (!player->GetFreePrimaryProfessionPoints()) {
                handler.PSendSysMessage("You may not learn any more professions!");
                return;
            }
            player->SilenceAchievementAnnounce(true);
            player->learnSpellHighRank(GetProfessionSpellId(tradeSkill));
            uint16 maxLevel = player->GetPureMaxSkillValue(GetProfessionSkillId(tradeSkill));
            player->SetSkill(GetProfessionSkillId(tradeSkill), player->GetSkillStep(GetProfessionSkillId(tradeSkill)), maxLevel, maxLevel);
            player->SilenceAchievementAnnounce(false);
        }
        else if (!player->HasSpell(GetProfessionSpellId(tradeSkill))) { // Shouldn't happen. But check it just in case...
            handler.PSendSysMessage("Alas, you do not have the necessary skills for this!");
            return;
        }
        sWorldPvPMgr->LearnProfSpellsForArea(player, GetProfessionSkillId(tradeSkill), creature->GetAreaAffiliation());
        std::stringstream ss;
        ss<<"You have successfully " << (isLearningProfession ? "learned " : "upgraded ") << GetProfessionDescription(tradeSkill)<<"!";
        creature->MonsterWhisper(ss.str().c_str(), player->GetGUID(), true);
    }
    
    uint32 GetProfessionSpellId(uint8 tradeSkill)
    {
        switch (tradeSkill) {
            case TRADESKILL_ALCHEMY:        return PROFESSION_ALCHEMY;
            case TRADESKILL_BLACKSMITHING:  return PROFESSION_BLACKSMITHING;
            case TRADESKILL_ENCHANTING:     return PROFESSION_ENCHANTING;
            case TRADESKILL_ENGINEERING:    return PROFESSION_ENGINEERING;
            case TRADESKILL_HERBALISM:      return PROFESSION_HERBALISM;
            case TRADESKILL_LEATHERWORKING: return PROFESSION_LEATHERWORKING;
            case TRADESKILL_TAILORING:      return PROFESSION_TAILORING;
            case TRADESKILL_MINING:         return PROFESSION_MINING;
            case TRADESKILL_SKINNING:       return PROFESSION_SKINNING;
            case TRADESKILL_JEWLCRAFTING:   return PROFESSION_JEWELCRAFTING;
            case TRADESKILL_INSCRIPTION:    return PROFESSION_INSCRIPTION;
            default:                        return 0;
        }
    }
    
    std::string GetProfessionDescription(uint8 tradeSkill, bool coloredText = false, uint32 iconSize = 0, int32 horizontalOffset = 0, int32 verticalOffset = 0)
    {
        std::ostringstream os;
        switch (tradeSkill) {
            case TRADESKILL_ALCHEMY:
                if (iconSize)
                    os  << "|TInterface\\Icons\\trade_alchemy.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Alchemy|r" : "Alchemy");
                break;
            case TRADESKILL_BLACKSMITHING:
                if (iconSize)
                    os  << "|TInterface\\Icons\\trade_blacksmithing.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Blacksmithing|r" : "Blacksmithing");
                break;
            case TRADESKILL_ENCHANTING:
                if (iconSize)
                    os  << "|TInterface\\Icons\\trade_engraving.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Enchanting|r" : "Enchanting");
                break;
            case TRADESKILL_ENGINEERING:
                if (iconSize)
                    os  << "|TInterface\\Icons\\trade_engineering.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Engineering|r" : "Engineering");
                break;
            case TRADESKILL_HERBALISM:
                if (iconSize)
                    os  << "|TInterface\\Icons\\spell_nature_naturetouchgrow.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Herbalism|r" : "Herbalism");
                break;
            case TRADESKILL_LEATHERWORKING:
                if (iconSize)
                    os  << "|TInterface\\Icons\\inv_misc_armorkit_17.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Leatherworking|r" : "Leatherworking");
                break;
            case TRADESKILL_TAILORING:
                if (iconSize)
                    os  << "|TInterface\\Icons\\trade_tailoring.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Tailoring|r" : "Tailoring");
                break;
            case TRADESKILL_MINING:
                if (iconSize)
                    os  << "|TInterface\\Icons\\trade_mining.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Mining|r" : "Mining");
                break;
            case TRADESKILL_SKINNING:
                if (iconSize)
                    os  << "|TInterface\\Icons\\inv_misc_pelt_wolf_01.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Skinning|r" : "Skinning");
                break;
            case TRADESKILL_JEWLCRAFTING:
                if (iconSize)
                    os  << "|TInterface\\Icons\\inv_misc_gem_01.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Jewelcrafting|r" : "Jewelcrafting");
                break;
            case TRADESKILL_INSCRIPTION:
                if (iconSize)
                    os  << "|TInterface\\Icons\\inv_inscription_tradeskill01.blp:"
                        << iconSize << ":" << iconSize << ":" << horizontalOffset << ":" << verticalOffset << "|t";
                os << (coloredText ? "|cffffcc00Inscription|r" : "Inscription");
                break;
            default:
                return "";
        }
        
        return os.str();
    }
    
    uint32 GetProfessionSkillId(uint8 tradeSkill)
    {
        switch (tradeSkill) {
            case TRADESKILL_ALCHEMY:        return SKILL_ALCHEMY;
            case TRADESKILL_BLACKSMITHING:  return SKILL_BLACKSMITHING;
            case TRADESKILL_ENCHANTING:     return SKILL_ENCHANTING;
            case TRADESKILL_ENGINEERING:    return SKILL_ENGINEERING;
            case TRADESKILL_HERBALISM:      return SKILL_HERBALISM;
            case TRADESKILL_LEATHERWORKING: return SKILL_LEATHERWORKING;
            case TRADESKILL_TAILORING:      return SKILL_TAILORING;
            case TRADESKILL_MINING:         return SKILL_MINING;
            case TRADESKILL_SKINNING:       return SKILL_SKINNING;
            case TRADESKILL_JEWLCRAFTING:   return SKILL_JEWELCRAFTING;
            case TRADESKILL_INSCRIPTION:    return SKILL_INSCRIPTION;
            default:                        return 0;
        }
    }
};

enum vendorGossipOptionTypes {
    VENDORGOSSIP_ENCHANT              = 1,
    VENDORGOSSIP_GLYPH,
    VENDORGOSSIP_GEM,
    VENDORGOSSIP_PRIMARY_MAX,
    VENDORGOSSIP_ENCHANT_SECONDARY,
    VENDORGOSSIP_ENCHANT_STANDARD,
    VENDORGOSSIP_ENCHANT_PROF,
    VENDORGOSSIP_GEM_SECONDARY,
    VENDORGOSSIP_SECONDARY_MAX
};
uint32 profEnchantSpellIds[] = {
    // engineering 
    54998, // hand mounted pyro rocket 
    54999, // hyperspeed accelerators
    55016, // nitro boots
    67839, // mind amplification dish
    63765, // springy arachnoweave
    55002, // flexweave underlay
    63770, // reticulated armor webbing
    54793, // frag belt
    // enchanting
    44645, // ring - assault
    44636, // ring - spellpower
    59636, // ring - stamina
    // inscription
    61117, // inscription of the axe
    61118, // inscription of the craig
    61119, // inscription of the pinnacle
    61120, // inscription of the storm
    // leatherworking
    57683, // fur lining - attack power
    57690, // fur lining - stamina
    57691, // fur lining - spell power
    // tailoring
    55777, // swordguard embroidery
    55642, // lightweave embroidery
    55769, // darkglow embroidery
    // blacksmithing
    55641, // socket gloves
    55628, // socket bracer
};

class stronkcc_enhancement_vendor : public CreatureScript, public PlayerScript
{
private:
    struct itemData {
        uint32 ItemId;
        uint32 Class;
        uint32 SubClass;
        uint32 Flags2;
        uint32 AllowableClass;
        uint32 AllowableRace;
        uint32 RequiredLevel;
        uint32 RequiredSkill;
        uint32 RequiredSkillRank;
        uint32 RequiredSpell;
        int32 itemProtoSpell_EquippedItemClass;
        int32 itemProtoSpell_EquippedItemSubClassMask;
        int32 itemProtoSpell_EquippedItemInventoryTypeMask;
        
        itemData(): ItemId(0), Class(0), SubClass(0), Flags2(0), AllowableClass(0), AllowableRace(0), RequiredLevel(0), RequiredSkill(0), RequiredSkillRank(0),
        RequiredSpell(0), itemProtoSpell_EquippedItemClass(0), itemProtoSpell_EquippedItemSubClassMask(0), itemProtoSpell_EquippedItemInventoryTypeMask(0) { }
    };
    typedef std::vector<itemData> availableItemTypes;
    typedef std::map<Creature*, availableItemTypes> creatureData;
    creatureData m_creatureData;
    struct playerSelection_t {
        uint8 vendorGossipOptionTypeSelection;
        uint8 enchantItemPos;
        uint8 gemType;
    };
    typedef std::map<Player*, playerSelection_t> playerSelection;
    playerSelection m_playerSelection;
public:
    stronkcc_enhancement_vendor() :
    CreatureScript("stronkcc_enhancement_vendor"),
    PlayerScript("stronkcc_enhancement_vendor_PS") { }
    
    void OnLogout(Player *player)
    {
        // Clear player data on logout
        m_playerSelection.erase(player);
    }
    
    bool OnGossipHello(Player* player, Creature* creature)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_playerSelection.erase(player);
        FillCreatureData(creature);
        if (!sWorldPvPMgr->CanInteract(player, creature))
            return true;
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        for (uint8 i = VENDORGOSSIP_ENCHANT; i < VENDORGOSSIP_PRIMARY_MAX; ++i) {
            if (ShowGossipOption(player, creature, (vendorGossipOptionTypes)i, 0))
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetVendorItemTypeDescription((vendorGossipOptionTypes)i), 0, i);
        }
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return true;
    }
    
    bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction)
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (!uiAction)
            return OnGossipHello(player, creature);
        switch ((vendorGossipOptionTypes)uiAction) {
            case VENDORGOSSIP_ENCHANT:
            {
                m_playerSelection.erase(player);
                for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i) {
                    if (i == EQUIPMENT_SLOT_TRINKET1 || i == EQUIPMENT_SLOT_TRINKET2 ||
                        i == EQUIPMENT_SLOT_NECK     || i == EQUIPMENT_SLOT_BODY ||
                        i == EQUIPMENT_SLOT_TABARD)
                        continue;
                    if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i)) {
                        if (item->GetTemplate()->InventoryType == INVTYPE_RELIC) // Exceptions for items that should not be displayed on gossip menu
                            continue;
                        if (ShowGossipOption(player, creature, VENDORGOSSIP_ENCHANT_SECONDARY, i + 1)) {
                            std::stringstream ss;
                            bool hasEnchant = false;
                            bool hasEnchantDescription = false;
                            if (uint32 enchantId = item->GetEnchantmentId(PERM_ENCHANTMENT_SLOT)) {
                                hasEnchant = true;
                                if (SpellItemEnchantmentEntry const* itemEnchant = sSpellItemEnchantmentStore.LookupEntry(enchantId))
                                    if (itemEnchant->description[0]) {
                                        hasEnchantDescription = true;
                                        ss<<GetColoredItemName(item->GetTemplate())<<" is already enchanted with:\n";
                                        ss<<"|cff5DFC0A"<<itemEnchant->description[0]<<"|r\n";
                                        ss<<"Are you sure you want to replace this enchant with another?";
                                    }
                            }
                            if (!hasEnchant)
                                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetItemDescription(player, item, i), i + 1, (uint32)VENDORGOSSIP_ENCHANT_SECONDARY);
                            else {
                                if (!hasEnchantDescription) {
                                    ss<<GetColoredItemName(item->GetTemplate())<<" is already enchanted.\n";
                                    ss<<"Are you sure you want to replace the enchant with another?";
                                }
                                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, GetItemDescription(player, item, i), i + 1, 
                                                                 (uint32)VENDORGOSSIP_ENCHANT_SECONDARY, ss.str(), 0, false);
                            }
                        }
                    }
                }
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
            case VENDORGOSSIP_GLYPH:
                m_playerSelection[player].vendorGossipOptionTypeSelection = uiAction;
                player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            case VENDORGOSSIP_GEM:
            {
                m_playerSelection.erase(player);
                for (uint8 i = ITEM_SUBCLASS_GEM_RED; i < MAX_ITEM_SUBCLASS_GEM; ++i)
                    if (ShowGossipOption(player, creature, VENDORGOSSIP_GEM_SECONDARY, i + 1))
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetGemTypeDescription(i), i + 1, (uint32)VENDORGOSSIP_GEM_SECONDARY);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
            case VENDORGOSSIP_ENCHANT_SECONDARY:
            {
                if (!uiSender || !player->GetItemByPos(INVENTORY_SLOT_BAG_0, uiSender - 1)) {
                    SendErrorMenu(player, creature);
                    return true;
                }
                m_playerSelection[player].vendorGossipOptionTypeSelection = uiAction;
                m_playerSelection[player].enchantItemPos = uiSender;
                if (ShowProfEnchantMenu(player, creature)) {
                    if (ShowGossipOption(player, creature, VENDORGOSSIP_ENCHANT_STANDARD, m_playerSelection[player].enchantItemPos))
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Show me your available enchants instead.", m_playerSelection[player].enchantItemPos, (uint32)VENDORGOSSIP_ENCHANT_STANDARD);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
                    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                }
                else
                    player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            }
            case VENDORGOSSIP_ENCHANT_STANDARD:
            {
                if (!uiSender || !player->GetItemByPos(INVENTORY_SLOT_BAG_0, uiSender - 1)) {
                    SendErrorMenu(player, creature);
                    return true;
                }
                m_playerSelection[player].vendorGossipOptionTypeSelection = uiAction;
                m_playerSelection[player].enchantItemPos = uiSender;
                player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            }
            case VENDORGOSSIP_ENCHANT_PROF:
            {
                if (!uiSender || !m_playerSelection[player].vendorGossipOptionTypeSelection ||
                    (vendorGossipOptionTypes)m_playerSelection[player].vendorGossipOptionTypeSelection != VENDORGOSSIP_ENCHANT_SECONDARY ||
                    !m_playerSelection[player].enchantItemPos) {
                    SendErrorMenu(player, creature);
                    return true;
                }
                SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(uiSender);
                Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, m_playerSelection[player].enchantItemPos - 1);
                if (!spellInfo || !item || !ItemFitsSpellRequirements(item, spellInfo)) {
                    SendErrorMenu(player, creature);
                    return true;
                }
                SpellCastTargets targets;
                targets.SetItemTarget(item);
                TriggerCastFlags triggerFlag = (TriggerCastFlags)(TRIGGERED_FULL_MASK & ~TRIGGERED_CAST_DIRECTLY);
                player->CastSpell(targets, spellInfo, NULL, triggerFlag, NULL, NULL, 0);
                std::stringstream ss;
                ss<<"You have successfully enchanted "<<item->GetTemplate()->Name1<<" with |cff303030[";
                ss<<spellInfo->SpellName[0]<<"]|r. Enchant another item?";
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), 0, (uint32)VENDORGOSSIP_ENCHANT);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
            case VENDORGOSSIP_GEM_SECONDARY:
            {
                if (!uiSender) {
                    SendErrorMenu(player, creature);
                    return true;
                }
                m_playerSelection[player].vendorGossipOptionTypeSelection = uiAction;
                m_playerSelection[player].gemType = uiSender;
                player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            }
            default:
                handler.PSendSysMessage("An error has occurred. Please try again.");
                return OnGossipHello(player, creature);
        }
        
        return true;
    }
    
    bool ShouldDisplayVendorItem(Player* player, Creature* vendor, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/)
    {
        if (!(vendorGossipOptionTypes)m_playerSelection[player].vendorGossipOptionTypeSelection) {
            SendErrorMenu(player, vendor);
            return false;
        }
        vendorGossipOptionTypes m_vendorGossipOptionType = (vendorGossipOptionTypes)m_playerSelection[player].vendorGossipOptionTypeSelection;
        uint32 secondaryOption = 0;
        switch (m_vendorGossipOptionType) {
            case VENDORGOSSIP_ENCHANT_SECONDARY:
            case VENDORGOSSIP_ENCHANT_STANDARD:
                if (!m_playerSelection[player].enchantItemPos) {
                    SendErrorMenu(player, vendor);
                    return false;
                }
                secondaryOption = m_playerSelection[player].enchantItemPos;
                break;
            case VENDORGOSSIP_GEM_SECONDARY:
                if (!m_playerSelection[player].gemType) {
                    SendErrorMenu(player, vendor);
                    return false;
                }
                secondaryOption = m_playerSelection[player].gemType;
                break;
            default:
                break;
        }
        
        return IsUsableItemForType(player, item_template, m_vendorGossipOptionType, secondaryOption);
    }
    
    bool OnBeforePurchaseItem(Player *player, Creature *vendor, ItemTemplate const* /*item_template*/, VendorItem const* /*vendor_item*/) {
        if (!sWorldPvPMgr->CanInteract(player, vendor)) {
            player->PlayerTalkClass->ClearMenus();
            player->PlayerTalkClass->SendCloseGossip();
            m_playerSelection.erase(player);
            return false;
        }
        
        return true;
    }
    
    void OnPurchasedItem(Player* player, Creature* vendor, Item* item, VendorItem const* /*vendor_item*/)
    {
        if (!(vendorGossipOptionTypes)m_playerSelection[player].vendorGossipOptionTypeSelection)
            return SendErrorMenu(player, vendor);
        vendorGossipOptionTypes m_vendorGossipOptionType = (vendorGossipOptionTypes)m_playerSelection[player].vendorGossipOptionTypeSelection;
        switch (m_vendorGossipOptionType) {
            case VENDORGOSSIP_ENCHANT_SECONDARY:
            case VENDORGOSSIP_ENCHANT_STANDARD:
            {
                if (!item || !item->GetTemplate() || !item->GetTemplate()->Spells[0].SpellId ||
                    item->GetTemplate()->Spells[0].SpellTrigger || !m_playerSelection[player].enchantItemPos)
                    return SendErrorMenu(player, vendor);
                SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(item->GetTemplate()->Spells[0].SpellId);
                Item* targetItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, m_playerSelection[player].enchantItemPos - 1);
                if (!spellInfo || !(spellInfo->Targets & TARGET_FLAG_ITEM) ||
                    !targetItem || !ItemFitsSpellRequirements(targetItem, spellInfo))
                    return SendErrorMenu(player, vendor);
                SpellCastTargets targets;
                targets.SetItemTarget(targetItem);
                TriggerCastFlags triggerFlag = (TriggerCastFlags)(TRIGGERED_FULL_MASK &
                                                                  ~(TRIGGERED_IGNORE_POWER_AND_REAGENT_COST |
                                                                    TRIGGERED_IGNORE_CAST_ITEM |
                                                                    TRIGGERED_CAST_DIRECTLY));
                Spell* spell = new Spell(player, spellInfo, triggerFlag);
                spell->m_CastItem = item;
                spell->m_cast_count = 0;
                spell->m_glyphIndex = 0;
                spell->prepare(&targets);
                break;
            }
            default:
                break;
        }
        
        return SendPurchaseSuccessMenu(player, vendor, m_vendorGossipOptionType, item);
    }
    
    void FillCreatureData(Creature* creature) // Run once
    {
        creatureData::iterator iter = m_creatureData.find(creature);
        if(iter == m_creatureData.end())
            if (VendorItemData const* items = creature->GetVendorItems())
                for (VendorItemList::const_iterator iter1 = items->m_items.begin(); iter1 != items->m_items.end(); ++iter1)
                    if (VendorItem const* item = *iter1)
                        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(item->item)) {
                            // Disregard all equipped items
                            if (itemTemplate->InventoryType != INVTYPE_NON_EQUIP)
                                continue;
                            itemData m_itemData;
                            m_itemData.ItemId = itemTemplate->ItemId;
                            m_itemData.Class = itemTemplate->Class;
                            m_itemData.SubClass = itemTemplate->SubClass;
                            m_itemData.Flags2 = itemTemplate->Flags2;
                            m_itemData.AllowableClass = itemTemplate->AllowableClass;
                            m_itemData.AllowableRace = itemTemplate->AllowableRace;
                            m_itemData.RequiredLevel = itemTemplate->RequiredLevel;
                            m_itemData.RequiredSkill = itemTemplate->RequiredSkill;
                            m_itemData.RequiredSkillRank = itemTemplate->RequiredSkillRank;
                            m_itemData.RequiredSpell = itemTemplate->RequiredSpell;
                            if (uint32 itemProtoSpellId = itemTemplate->Spells[0].SpellId)
                                if (SpellInfo const* itemProtoSpellInfo = sSpellMgr->GetSpellInfo(itemProtoSpellId)) {
                                    m_itemData.itemProtoSpell_EquippedItemClass = itemProtoSpellInfo->EquippedItemClass;
                                    m_itemData.itemProtoSpell_EquippedItemSubClassMask = itemProtoSpellInfo->EquippedItemSubClassMask;
                                    m_itemData.itemProtoSpell_EquippedItemInventoryTypeMask = itemProtoSpellInfo->EquippedItemInventoryTypeMask;
                                }
                            availableItemTypes::iterator iter2 = m_creatureData[creature].begin();
                            for (; iter2 != m_creatureData[creature].end(); ++iter2)
                                if (iter2->Class == m_itemData.Class && 
                                    iter2->SubClass == m_itemData.SubClass &&
                                    iter2->Flags2 == m_itemData.Flags2 &&
                                    iter2->AllowableClass == m_itemData.AllowableClass &&
                                    iter2->AllowableRace == m_itemData.AllowableRace &&
                                    iter2->RequiredLevel == m_itemData.RequiredLevel &&
                                    iter2->RequiredSkill == m_itemData.RequiredSkill &&
                                    iter2->RequiredSkillRank == m_itemData.RequiredSkillRank &&
                                    iter2->RequiredSpell == m_itemData.RequiredSpell &&
                                    iter2->itemProtoSpell_EquippedItemClass == m_itemData.itemProtoSpell_EquippedItemClass &&
                                    iter2->itemProtoSpell_EquippedItemSubClassMask == m_itemData.itemProtoSpell_EquippedItemSubClassMask &&
                                    iter2->itemProtoSpell_EquippedItemInventoryTypeMask == m_itemData.itemProtoSpell_EquippedItemInventoryTypeMask)
                                    break;
                            if (iter2 == m_creatureData[creature].end())
                                m_creatureData[creature].push_back(m_itemData);
                        }
        
        return;
    }
    
    bool ShowGossipOption(Player* player, Creature* creature, vendorGossipOptionTypes m_vendorGossipOptionType, uint32 secondaryOption)
    {
        for (availableItemTypes::iterator iter3 = m_creatureData[creature].begin(); iter3 != m_creatureData[creature].end(); ++iter3) {
            itemData & m_itemData = *iter3;
            if (IsUsableItemForType(player, sObjectMgr->GetItemTemplate(m_itemData.ItemId), m_vendorGossipOptionType, secondaryOption))
                return true;
        }
        
        return HasValidProfEnchant(player, m_vendorGossipOptionType, secondaryOption);
    }
    
    bool IsUsableItemForType(Player* player, ItemTemplate const* item_template, vendorGossipOptionTypes m_vendorGossipOptionType, uint32 secondaryOption)
    {
        if (!item_template)
            return false;
        if (player->CanUseItem(item_template) != EQUIP_ERR_OK)
            return false;
        switch (m_vendorGossipOptionType) {
            case VENDORGOSSIP_ENCHANT:
            {
                if (!item_template->Spells[0].SpellId || item_template->Spells[0].SpellTrigger)
                    return false;
                SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(item_template->Spells[0].SpellId);
                if (!spellInfo)
                    return false;
                // Recursively search player's equipped items to find one that can be enchanted
                // with this item
                bool hasValidItemEquipped = false;
                for(uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i) {
                    if (i == EQUIPMENT_SLOT_TRINKET1 || i == EQUIPMENT_SLOT_TRINKET2 ||
                        i == EQUIPMENT_SLOT_NECK     || i == EQUIPMENT_SLOT_BODY ||
                        i == EQUIPMENT_SLOT_TABARD)
                        continue;
                    if(Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
                        if(ItemFitsSpellRequirements(item, spellInfo)) {
                            hasValidItemEquipped = true;
                            break;
                        }
                }
                return (spellInfo->Targets & TARGET_FLAG_ITEM) && hasValidItemEquipped;
            }
            case VENDORGOSSIP_GLYPH:
                return item_template->Class == ITEM_CLASS_GLYPH; 
            case VENDORGOSSIP_GEM:
                return item_template->Class == ITEM_CLASS_GEM;
            case VENDORGOSSIP_ENCHANT_SECONDARY:
            case VENDORGOSSIP_ENCHANT_STANDARD:
            {
                if (!item_template->Spells[0].SpellId || item_template->Spells[0].SpellTrigger || !secondaryOption)
                    return false;
                SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(item_template->Spells[0].SpellId);
                if (!spellInfo)
                    return false;
                Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, secondaryOption - 1);
                if (!item)
                    return false;
                return (spellInfo->Targets & TARGET_FLAG_ITEM) && ItemFitsSpellRequirements(item, spellInfo);
            }
            case VENDORGOSSIP_GEM_SECONDARY:
                if (!secondaryOption)
                    return false;
                return item_template->Class == ITEM_CLASS_GEM && item_template->SubClass == (secondaryOption - 1);
            default:
                return false;
        }
    }
    
    bool ItemFitsSpellRequirements(Item* item, SpellInfo const* spellInfo) const
    {
        if (!item || !spellInfo)
            return false;
        ItemTemplate const* proto = item->GetTemplate();
        if (!proto)
            return false;
        
        return item->IsFitToSpellRequirements(spellInfo) && (spellInfo->BaseLevel ? (proto->ItemLevel >= spellInfo->BaseLevel) : true);
    }
    
    bool ShowProfEnchantMenu(Player* player, Creature* creature)
    {
        if (!m_playerSelection[player].vendorGossipOptionTypeSelection ||
            (vendorGossipOptionTypes)m_playerSelection[player].vendorGossipOptionTypeSelection != VENDORGOSSIP_ENCHANT_SECONDARY ||
            !m_playerSelection[player].enchantItemPos) {
            SendErrorMenu(player, creature);
            return false;
        }
        Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, m_playerSelection[player].enchantItemPos - 1);
        if (!item) {
            SendErrorMenu(player, creature);
            return false;
        }
        uint8 limit = 0;
        uint8 max = 64;
        for(uint32 *spellIdPtr = profEnchantSpellIds; limit < max && spellIdPtr < profEnchantSpellIds + sizeof(profEnchantSpellIds) / sizeof(uint32); spellIdPtr++) {
            if(player->HasSpell(*spellIdPtr))
                if(SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(*spellIdPtr))
                    if (spellInfo->Targets & TARGET_FLAG_ITEM)
                        if(ItemFitsSpellRequirements(item, spellInfo)) {
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER,(std::string)spellInfo->SpellName[0], spellInfo->Id, VENDORGOSSIP_ENCHANT_PROF);
                            limit++;
                        }
        }
        if(limit)
            return true;
        
        return false;
    }
    
    bool HasValidProfEnchant(Player* player, vendorGossipOptionTypes m_vendorGossipOptionType, uint32 secondaryOption)
    {
        switch (m_vendorGossipOptionType) {
            case VENDORGOSSIP_ENCHANT:
            {
                for(uint32 *spellIdPtr = profEnchantSpellIds; spellIdPtr < profEnchantSpellIds + sizeof(profEnchantSpellIds) / sizeof(uint32); spellIdPtr++) {
                    if(player->HasSpell(*spellIdPtr))
                        if(SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(*spellIdPtr))
                            if (spellInfo->Targets & TARGET_FLAG_ITEM)
                                for(uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i) {
                                    if (i == EQUIPMENT_SLOT_TRINKET1 || i == EQUIPMENT_SLOT_TRINKET2 ||
                                        i == EQUIPMENT_SLOT_NECK     || i == EQUIPMENT_SLOT_BODY ||
                                        i == EQUIPMENT_SLOT_TABARD)
                                        continue;
                                    if(Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
                                        if(ItemFitsSpellRequirements(item, spellInfo))
                                            return true;
                                }
                }
                break;
            }
            case VENDORGOSSIP_ENCHANT_SECONDARY:
            {
                for(uint32 *spellIdPtr = profEnchantSpellIds; spellIdPtr < profEnchantSpellIds + sizeof(profEnchantSpellIds) / sizeof(uint32); spellIdPtr++) {
                    if(player->HasSpell(*spellIdPtr))
                        if(SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(*spellIdPtr))
                            if (spellInfo->Targets & TARGET_FLAG_ITEM)
                                if(Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, secondaryOption - 1))
                                    if(ItemFitsSpellRequirements(item, spellInfo))
                                        return true;
                }
                break;
            }
            default:
                break;
        }
        
        return false;
    }
    
    void SendErrorMenu(Player* player, Creature* creature)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_playerSelection.erase(player);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return;
    }
    
    void SendPurchaseSuccessMenu(Player* player, Creature* creature, vendorGossipOptionTypes m_vendorGossipOptionType, Item* purchasedItem)
    {
        if (!m_vendorGossipOptionType || !purchasedItem)
            return SendErrorMenu(player, creature);
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        std::stringstream ss;
        switch (m_vendorGossipOptionType) {
            case VENDORGOSSIP_ENCHANT_SECONDARY:
            case VENDORGOSSIP_ENCHANT_STANDARD:
            {
                if (!m_playerSelection[player].enchantItemPos)
                    return SendErrorMenu(player, creature);
                Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, m_playerSelection[player].enchantItemPos - 1);
                if (!item)
                    return SendErrorMenu(player, creature);
                ss<<"You have successfully enchanted "<<item->GetTemplate()->Name1<<" with |cff303030[";
                ss<<purchasedItem->GetTemplate()->Name1<<"]|r. Enchant another item?";
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), 0, (uint32)VENDORGOSSIP_ENCHANT);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu", 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
            case VENDORGOSSIP_GLYPH:
            {
                ss<<"You have successfully purchased |cff303030["<<purchasedItem->GetTemplate()->Name1<<"]|r. Browse more glyphs?";
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), 0, (uint32)VENDORGOSSIP_GLYPH);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
            case VENDORGOSSIP_GEM_SECONDARY:
            {
                ss<<"You have successfully purchased |cff303030["<<purchasedItem->GetTemplate()->Name1<<"]|r. Purchase more gems?";
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), 0, (uint32)VENDORGOSSIP_GEM);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
            default:
                break;
        }
        
        m_playerSelection.erase(player);
        return;
    }
    
    std::string GetVendorItemTypeDescription(vendorGossipOptionTypes m_vendorGossipOptionType)
    {
        std::ostringstream os;
        switch (m_vendorGossipOptionType) {
            case VENDORGOSSIP_ENCHANT:    os << "|TInterface\\Icons\\inv_rod_titanium.blp:36:36:-16:1|tEnchant equipped items"; break;
            case VENDORGOSSIP_GLYPH:      os << "|TInterface\\Icons\\inv_glyph_majordruid.blp:36:36:-16:1|tBrowse some glyphs"; break;
            case VENDORGOSSIP_GEM:        os << "|TInterface\\Icons\\inv_misc_gem_01.blp:36:36:-16:1|tPurchase some gems"; break;
            default:                      return "";
        }
        
        return os.str();
    }
    
    std::string GetItemDescription(Player* player, Item* item, uint8 slot) {
        std::string itemName = item->GetTemplate()->Name1;
        if (itemName.size() > MAX_LINE_CHARS) {
            itemName.resize(MAX_LINE_CHARS - 3);
            itemName += "...";
        }
        std::string iconPath = "INV_Misc_QuestionMark";
        uint32 itemEntry = player->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID + (slot * 2));
        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemEntry))
            if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(itemTemplate->DisplayInfoID))
                iconPath = displayInfo->iconPath;
        std::ostringstream os;
        os  << "|TInterface\\Icons\\" << iconPath << ".blp:36:36:-16:1|t" 
            << "|Hitem:" << item->GetTemplate()->ItemId << ":0:0:0:0:0:0:0:0:0|h" << itemName << "|h";
        
        return os.str();
    }
    
    std::string GetColoredItemName(ItemTemplate const* itemTemplate) {
        std::ostringstream os;
        os << "|c" << std::hex << ItemQualityColors[itemTemplate->Quality] << std::dec << "[" << itemTemplate->Name1 << "]|r";
        return os.str();
    }
    
    std::string GetGemTypeDescription(uint8 gemType)
    {
        std::ostringstream os;
        switch (gemType) {
            case ITEM_SUBCLASS_GEM_RED:         os << "|TInterface\\Icons\\inv_jewelcrafting_gem_37.blp:36:36:-16:1|tRed Gems"; break;
            case ITEM_SUBCLASS_GEM_BLUE:        os << "|TInterface\\Icons\\inv_jewelcrafting_gem_42.blp:36:36:-16:1|tBlue Gems"; break;
            case ITEM_SUBCLASS_GEM_YELLOW:      os << "|TInterface\\Icons\\inv_jewelcrafting_gem_38.blp:36:36:-16:1|tYellow Gems"; break;
            case ITEM_SUBCLASS_GEM_PURPLE:      os << "|TInterface\\Icons\\inv_jewelcrafting_gem_40.blp:36:36:-16:1|tPurple Gems"; break;
            case ITEM_SUBCLASS_GEM_GREEN:       os << "|TInterface\\Icons\\inv_jewelcrafting_gem_41.blp:36:36:-16:1|tGreen Gems"; break;
            case ITEM_SUBCLASS_GEM_ORANGE:      os << "|TInterface\\Icons\\inv_jewelcrafting_gem_39.blp:36:36:-16:1|tOrange Gems"; break;
            case ITEM_SUBCLASS_GEM_META:        os << "|TInterface\\Icons\\inv_jewelcrafting_icediamond_02.blp:36:36:-16:1|tMeta Gems"; break;
            case ITEM_SUBCLASS_GEM_SIMPLE:      os << "|TInterface\\Icons\\inv_misc_gem_pearl_09.blp:36:36:-16:1|tSimple Gems"; break;
            case ITEM_SUBCLASS_GEM_PRISMATIC:   os << "|TInterface\\Icons\\inv_misc_gem_pearl_12.blp:36:36:-16:1|tPrismatic Gems"; break;
            default:                            return "";
        }
        
        return os.str();
    }
};

enum PetClass {
    PET_CLASS_CUNNING = 1,
    PET_CLASS_FEROCITY,
    PET_CLASS_TENACITY,
    PET_CLASS_EXOTIC
};

enum PetSubClass {
    // Cunning Pets
    PET_SUBCLASS_BAT = 1,
    PET_SUBCLASS_BIRD_OF_PREY,
    PET_SUBCLASS_DRAGONHAWK,
    PET_SUBCLASS_NETHER_RAY,
    PET_SUBCLASS_RAVAGER,
    PET_SUBCLASS_SERPENT,
    PET_SUBCLASS_SPIDER,
    PET_SUBCLASS_SPOREBAT,
    PET_SUBCLASS_WIND_SERPENT,
    // Ferocity Pets
    PET_SUBCLASS_CARRION_BIRD,
    PET_SUBCLASS_CAT,
    PET_SUBCLASS_HYENA,
    PET_SUBCLASS_MOTH,
    PET_SUBCLASS_RAPTOR,
    PET_SUBCLASS_TALLSTRIDER,
    PET_SUBCLASS_WASP,
    PET_SUBCLASS_WOLF,
    // Tenacity Pets
    PET_SUBCLASS_BEAR,
    PET_SUBCLASS_BOAR,
    PET_SUBCLASS_CRAB,
    PET_SUBCLASS_CROCOLISK,
    PET_SUBCLASS_GORILLA,
    PET_SUBCLASS_SCORPID,
    PET_SUBCLASS_TURTLE,
    PET_SUBCLASS_WARP_STALKER,
    // Exotic Pets
    PET_SUBCLASS_CHIMAERA,
    PET_SUBCLASS_SILITHID,
    PET_SUBCLASS_CORE_HOUND,
    PET_SUBCLASS_DEVILSAUR,
    PET_SUBCLASS_SPIRIT_BEAST,
    PET_SUBCLASS_RHINO,
    PET_SUBCLASS_WORM,
    
    MAX_PET_SUBCLASS
};

enum BeastMasterOptions {
    BM_OPT_PICK_NEW_PET = 1,
    BM_OPT_STABLE,
    BM_OPT_BROWSE_GOODS,
    BM_OPT_SHOW_PET_SUBCLASS,
    BM_OPT_SHOW_PETS,
    BM_OPT_TAME_PET
};

class stronkcc_beastmaster : public CreatureScript
{
private:
    struct PetData {
        uint32 entry;
        uint8 petClass;
        uint8 petSubClass;
        std::string description;
    };
    typedef std::vector<PetData> Pets;
    Pets m_pets;
public:
	stronkcc_beastmaster() : CreatureScript("stronkcc_beastmaster") {
        m_pets.clear();
        PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_TAMEABLE_PETS);
        if (PreparedQueryResult result = WorldDatabase.Query(stmt)) {
            do {
                Field* fields = result->Fetch();
                PetData m_petData;
                m_petData.entry         = fields[0].GetUInt32();
                m_petData.petClass      = fields[1].GetUInt8();
                m_petData.petSubClass   = fields[2].GetUInt8();
                m_petData.description   = fields[3].GetString();
                m_pets.push_back(m_petData);
            }
            while (result->NextRow());
        }
    }
    
	bool OnGossipHello(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (player->getClass() != CLASS_HUNTER) {
			creature->MonsterWhisper("Only hunters may tame pets.", player->GetGUID());
			return true;
		}
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "I'd like to tame a new pet.", 0, BM_OPT_PICK_NEW_PET);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "I'd like to access my stabled pets.", 0, BM_OPT_STABLE);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, GOSSIP_TEXT_BROWSE_GOODS, 0, BM_OPT_BROWSE_GOODS);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
		return true;
	}
    
	bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        if (!uiAction)
            return OnGossipHello(player, creature);
        switch (uiAction) {
            case BM_OPT_PICK_NEW_PET:
                for (uint8 i = PET_CLASS_CUNNING; i < PET_CLASS_EXOTIC; ++i)
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetPetClassDescription(i), i, BM_OPT_SHOW_PET_SUBCLASS);
                if (player->CanTameExoticPets())
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetPetClassDescription(PET_CLASS_EXOTIC), PET_CLASS_EXOTIC, BM_OPT_SHOW_PET_SUBCLASS);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, RETURN_TO_MAIN_MENU_ICON, 0, 0);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            case BM_OPT_STABLE:
                player->GetSession()->SendStablePet(creature->GetGUID());
                break;
            case BM_OPT_BROWSE_GOODS:
                player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            case BM_OPT_SHOW_PET_SUBCLASS:
                if (!uiSender) {
                    ChatHandler(player->GetSession()).PSendSysMessage("An error has occurred. Please try again.");
                    return OnGossipHello(player, creature);
                }
                for (uint8 i = PET_SUBCLASS_BAT; i < MAX_PET_SUBCLASS; ++i)
                    for (Pets::const_iterator itr = m_pets.begin(); itr != m_pets.end(); ++itr) {
                        if (i != (*itr).petSubClass)
                            continue;
                        if (uiSender == (*itr).petClass)
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GetPetSubClassDescription(i), i, BM_OPT_SHOW_PETS);
                        break;
                    }
                
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, 
                                        "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo.blp:36:36:-16:1|tReturn to the pet selection menu.", 
                                        0, BM_OPT_PICK_NEW_PET);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            case BM_OPT_SHOW_PETS:
                if (!uiSender) {
                    ChatHandler(player->GetSession()).PSendSysMessage("An error has occurred. Please try again.");
                    return OnGossipHello(player, creature);
                }
                for (Pets::const_iterator itr = m_pets.begin(); itr != m_pets.end(); ++itr)
                    if (uiSender == (*itr).petSubClass)
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, (*itr).description, (*itr).entry, BM_OPT_TAME_PET);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the pet selection menu.", 0, BM_OPT_PICK_NEW_PET);
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            case BM_OPT_TAME_PET:
                if (!uiSender) {
                    ChatHandler(player->GetSession()).PSendSysMessage("An error has occurred. Please try again.");
                    return OnGossipHello(player, creature);
                }
                CreatePet(player, creature, uiSender);
                break;
            default:
                break;
        }
		
		return true;
    }
    
    void CreatePet(Player *player, Creature * creature, uint32 entry) {
        
		if (player->getClass() != CLASS_HUNTER) { // This should never happen, just do it anyway...
			creature->MonsterWhisper("Only hunters may tame pets.", player->GetGUID());
			return;
		}
        
		if (player->GetPet()) {
			creature->MonsterWhisper("Dismiss or stable your currently summoned pet before taming a new one.", player->GetGUID());
			return;
		}
        
        if (!sObjectMgr->GetCreatureTemplate(entry) ||
            !sObjectMgr->GetCreatureTemplate(entry)->IsTameable(player->CanTameExoticPets())) {
			creature->MonsterWhisper("You may not tame this pet.", player->GetGUID());
			return;
		}
        
		Creature *creatureTarget = creature->SummonCreature(entry, player->GetPositionX(), player->GetPositionY()+2, player->GetPositionZ(), player->GetOrientation(), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 500);
		if(!creatureTarget) return;
        
		Pet* pet = player->CreateTamedPetFrom(creatureTarget, 0);
		if(!pet) return;
        
		creatureTarget->setDeathState(JUST_DIED);
		creatureTarget->RemoveCorpse();
		creatureTarget->SetHealth(0);                       // just for nice GM-mode view
        
		pet->SetPower(POWER_HAPPINESS, pet->GetMaxPower(POWER_HAPPINESS));
        
		pet->SetUInt64Value(UNIT_FIELD_CREATEDBY, player->GetGUID());
		pet->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, player->getFaction());
        
		pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel() - 1);
		pet->GetMap()->AddToMap(pet->ToCreature());
        
		pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel());
        
		pet->GetCharmInfo()->SetPetNumber(sObjectMgr->GeneratePetNumber(), true);
		if(!pet->InitStatsForLevel(player->getLevel()))
			TC_LOG_ERROR(LOG_FILTER_UNITS, "Pet::InitStatsForLevel() failed for creature (Entry: %u)!", pet->GetEntry());
        
		pet->UpdateAllStats();
        
		player->SetMinion(pet, true);
        
		pet->SavePetToDB(PET_SAVE_AS_CURRENT);
		pet->InitTalentForLevel();
		player->PetSpellInitialize();
        
		creature->MonsterWhisper("You have successfully tamed a new pet.", player->GetGUID());
	}
    
    std::string GetPetClassDescription(uint8 petClass) {
        std::ostringstream os;
        switch (petClass) {
            case PET_CLASS_CUNNING:     os << "|TInterface\\Icons\\ability_hunter_animalhandler.blp:36:36:-16:1|tCunning Pets"; break;
            case PET_CLASS_FEROCITY:    os << "|TInterface\\Icons\\ability_druid_primaltenacity.blp:36:36:-16:1|tFerocity Pets"; break;
            case PET_CLASS_TENACITY:    os << "|TInterface\\Icons\\spell_nature_shamanRage.blp:36:36:-16:1|tTenacity Pets"; break;
            case PET_CLASS_EXOTIC:      os << "|TInterface\\Icons\\ability_hunter_beastmastery.blp:36:36:-16:1|tExotic Pets"; break;
            default:                    return "";
        }
        
        return os.str();
    }
    
    std::string GetPetSubClassDescription(uint8 petSubClass) {
        std::ostringstream os;
        switch (petSubClass) {
            // Cunning Pets
            case PET_SUBCLASS_BAT:          os << "|TInterface\\Icons\\ability_hunter_pet_bat.blp:36:36:-16:1|tBats"; break;
            case PET_SUBCLASS_BIRD_OF_PREY: os << "|TInterface\\Icons\\ability_hunter_pet_owl.blp:36:36:-16:1|tBird of Prey"; break;
            case PET_SUBCLASS_DRAGONHAWK:   os << "|TInterface\\Icons\\ability_hunter_pet_dragonhawk.blp:36:36:-16:1|tDragonhawks"; break;
            case PET_SUBCLASS_NETHER_RAY:   os << "|TInterface\\Icons\\ability_hunter_pet_netherray.blp:36:36:-16:1|tNether Rays"; break;
            case PET_SUBCLASS_RAVAGER:      os << "|TInterface\\Icons\\ability_hunter_pet_ravager.blp:36:36:-16:1|tRavagers"; break;
            case PET_SUBCLASS_SERPENT:      os << "|TInterface\\Icons\\spell_nature_guardianward.blp:36:36:-16:1|tSerpents"; break;
            case PET_SUBCLASS_SPIDER:       os << "|TInterface\\Icons\\ability_hunter_pet_spider.blp:36:36:-16:1|tSpiders"; break;
            case PET_SUBCLASS_SPOREBAT:     os << "|TInterface\\Icons\\ability_hunter_pet_sporebat.blp:36:36:-16:1|tSporebats"; break;
            case PET_SUBCLASS_WIND_SERPENT: os << "|TInterface\\Icons\\ability_hunter_pet_windserpent.blp:36:36:-16:1|tWind Serpents"; break;
            // Ferocity Pets
            case PET_SUBCLASS_CARRION_BIRD: os << "|TInterface\\Icons\\ability_hunter_pet_vulture.blp:36:36:-16:1|tCarrion Birds"; break;
            case PET_SUBCLASS_CAT:          os << "|TInterface\\Icons\\ability_hunter_pet_cat.blp:36:36:-16:1|tCats"; break;
            case PET_SUBCLASS_HYENA:        os << "|TInterface\\Icons\\ability_hunter_pet_hyena.blp:36:36:-16:1|tHyenas"; break;
            case PET_SUBCLASS_MOTH:         os << "|TInterface\\Icons\\ability_hunter_pet_moth.blp:36:36:-16:1|tMoths"; break;
            case PET_SUBCLASS_RAPTOR:       os << "|TInterface\\Icons\\ability_hunter_pet_raptor.blp:36:36:-16:1|tRaptors"; break;
            case PET_SUBCLASS_TALLSTRIDER:  os << "|TInterface\\Icons\\ability_hunter_pet_tallstrider.blp:36:36:-16:1|tTallstriders"; break;
            case PET_SUBCLASS_WASP:         os << "|TInterface\\Icons\\ability_hunter_pet_wasp.blp:36:36:-16:1|tWasps"; break;
            case PET_SUBCLASS_WOLF:         os << "|TInterface\\Icons\\ability_hunter_pet_wolf.blp:36:36:-16:1|tWolves"; break;
            // Tenacity Pets
            case PET_SUBCLASS_BEAR:         os << "|TInterface\\Icons\\ability_hunter_pet_bear.blp:36:36:-16:1|tBears"; break;
            case PET_SUBCLASS_BOAR:         os << "|TInterface\\Icons\\ability_hunter_pet_boar.blp:36:36:-16:1|tBoars"; break;
            case PET_SUBCLASS_CRAB:         os << "|TInterface\\Icons\\ability_hunter_pet_crab.blp:36:36:-16:1|tCrabs"; break;
            case PET_SUBCLASS_CROCOLISK:    os << "|TInterface\\Icons\\ability_hunter_pet_crocolisk.blp:36:36:-16:1|tCrocolisks"; break;
            case PET_SUBCLASS_GORILLA:      os << "|TInterface\\Icons\\ability_hunter_pet_gorilla.blp:36:36:-16:1|tGorillas"; break;
            case PET_SUBCLASS_SCORPID:      os << "|TInterface\\Icons\\ability_hunter_pet_scorpid.blp:36:36:-16:1|tScorpids"; break;
            case PET_SUBCLASS_TURTLE:       os << "|TInterface\\Icons\\ability_hunter_pet_turtle.blp:36:36:-16:1|tTurtles"; break;
            case PET_SUBCLASS_WARP_STALKER: os << "|TInterface\\Icons\\ability_hunter_pet_warpstalker.blp:36:36:-16:1|tWarp Stalkers"; break;
            // Exotic Pets
            case PET_SUBCLASS_CHIMAERA:     os << "|TInterface\\Icons\\ability_hunter_pet_chimera.blp:36:36:-16:1|tChimaerae"; break;
            case PET_SUBCLASS_SILITHID:     os << "|TInterface\\Icons\\ability_hunter_pet_silithid.blp:36:36:-16:1|tSilithids"; break;
            case PET_SUBCLASS_CORE_HOUND:   os << "|TInterface\\Icons\\ability_hunter_pet_corehound.blp:36:36:-16:1|tCore Hounds"; break;
            case PET_SUBCLASS_DEVILSAUR:    os << "|TInterface\\Icons\\ability_hunter_pet_devilsaur.blp:36:36:-16:1|tDevilsaurs"; break;
            case PET_SUBCLASS_SPIRIT_BEAST: os << "|TInterface\\Icons\\ability_druid_primalprecision.blp:36:36:-16:1|tSpirit Beasts"; break;
            case PET_SUBCLASS_RHINO:        os << "|TInterface\\Icons\\ability_hunter_pet_rhino.blp:36:36:-16:1|tRhinos"; break;
            case PET_SUBCLASS_WORM:         os << "|TInterface\\Icons\\ability_hunter_pet_worm.blp:36:36:-16:1|tWorms"; break;
            default:                        return "";
        }
        
        return os.str();
    }
};

void AddSC_stronkcc_vendors()
{
    new stronkcc_gear_vendor;
    new stronkcc_quartermaster;
    new stronkcc_profession_vendor;
    new stronkcc_enhancement_vendor;
    new stronkcc_beastmaster;
}