#include "WorldPvPMgr.h"

# define CHECK_BOUNDARY_TIMER 3000
# define BOUNDARY 120.0f

struct WorldPvPGuardianAI: public ScriptedAI
{
    WorldPvPGuardianAI(Creature* creature) : ScriptedAI(creature), checkBoundaryTimer(CHECK_BOUNDARY_TIMER)
    {
        me->SetIgnoreLOS(true);
        areaAffiliation = me->GetAreaAffiliation();
        areaDifficulty = sWorldPvPMgr->GetAreaDifficulty(areaAffiliation);

        healthMod = sWorldPvPMgr->GetModifierValue(areaDifficulty, 0.3, 1.6);
        dmgMod = sWorldPvPMgr->GetModifierValue(areaDifficulty, 0.6, 1.2);

        maxHealth = me->GetMaxHealth() * healthMod;
    }

    void Reset()
    {
        me->SetMaxHealth(maxHealth);
        me->SetFullHealth();

        events.Reset();
    }

    void ModDamageDone(Unit* /*victim*/, uint32& damage)
    {
        damage *= dmgMod;
    }

    void JustReachedHome()
    {
        me->DespawnOrUnsummon();
        sWorldPvPMgr->AreaGuardianDespawned(areaAffiliation);
    }

    void JustDied(Unit* /*killer*/)
    {
        sWorldPvPMgr->ProcessAreaTakeOver(areaAffiliation, true);
    }

    void KilledUnit(Unit* /*victim*/)
    {
        DoResetThreat();
    }

    bool IsWithinBoundary(uint32 diff)
    {
        checkBoundaryTimer.Update(diff);
        if (checkBoundaryTimer.Passed()) {
            checkBoundaryTimer.Reset(CHECK_BOUNDARY_TIMER);
            if (me->GetAreaId() != areaAffiliation &&
                me->GetDistance(me->GetHomePosition()) > BOUNDARY)
                return false;
        }

        return true;
    }

protected:
    EventMap events;
    TimeTracker checkBoundaryTimer;
    uint32 areaAffiliation;
    uint8 areaDifficulty;
    float healthMod;
    float dmgMod;
    uint32 maxHealth;
};

enum GuardianPreciousEvents {
    EVENT_FEAR = 1,
    EVENT_SUMMON,
    EVENT_ENRAGE,
    EVENT_CHANGE_TARGET,
    EVENT_MORTAL_WOUND,
    EVENT_DESPAWN
};

enum GuardianPreciousSpells {
    SPELL_ENRAGE = 54427,
    SPELL_MORTAL_WOUND = 71127,
    SPELL_INCITE_TERROR = 73070,
    SPELL_IMPALING_CHARGE = 59827
};

class guardian_precious : public CreatureScript
{
public:
    guardian_precious() : CreatureScript("guardian_precious") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new guardian_preciousAI(creature);
    }

    struct guardian_preciousAI : public WorldPvPGuardianAI
    {
        guardian_preciousAI(Creature* creature) : WorldPvPGuardianAI(creature) { }

        void EnterCombat(Unit* /*target*/)
        {
            events.ScheduleEvent(EVENT_FEAR, urand(45000, 60000));
            events.ScheduleEvent(EVENT_SUMMON, urand(20000, 40000));
            events.ScheduleEvent(EVENT_ENRAGE, urand(12000, 15000));
            events.ScheduleEvent(EVENT_CHANGE_TARGET, urand(10000, 12000));
            events.ScheduleEvent(EVENT_MORTAL_WOUND, urand(3000, 5000));
            events.ScheduleEvent(EVENT_DESPAWN, 8*60000);
        }

        void UpdateAI(uint32 diff)
        {
            if (!UpdateVictim())
                return;

            if (!IsWithinBoundary(diff))
                return JustReachedHome();

            events.Update(diff);

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_FEAR:
                        DoCastVictim(SPELL_INCITE_TERROR);
                        events.ScheduleEvent(EVENT_FEAR, urand(45000, 60000));
                        break;
                    case EVENT_SUMMON:
                    {
                        uint8 guardsToSpawn = urand(1, areaDifficulty);
                        for (uint8 i = 0; i < guardsToSpawn; ++i)
                            DoSummon(sWorldPvPMgr->GetRandomGuardForArea(areaAffiliation),
                                     me, 15.0f, 5000, TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT);
                        events.ScheduleEvent(EVENT_SUMMON, urand(20000, 40000));
                        break;
                    }
                    case EVENT_ENRAGE:
                        DoCast(me, SPELL_ENRAGE);
                        events.ScheduleEvent(EVENT_ENRAGE, urand(12000, 15000));
                        break;
                    case EVENT_CHANGE_TARGET:
                        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 1, 50.0f, true)) {
                            DoResetThreat();
                            me->AddThreat(target, 1000000.0f);
                            DoCast(target, SPELL_IMPALING_CHARGE);
                        }
                        events.ScheduleEvent(EVENT_CHANGE_TARGET, urand(10000, 12000));
                        break;
                    case EVENT_MORTAL_WOUND:
                        DoCastVictim(SPELL_MORTAL_WOUND);
                        events.ScheduleEvent(EVENT_MORTAL_WOUND, urand(3000, 7000));
                        break;
                    case EVENT_DESPAWN:
                        return JustReachedHome();
                    default:
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };
};

void AddSC_stronkcc_world_pvp_guardians()
{
    new guardian_precious();
}