#include "Player.h"
#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "Group.h"

class cg_xfactionbg_PlayerScript : public PlayerScript
{
    public:
        cg_xfactionbg_PlayerScript() : PlayerScript("cg_xfactionbg_PlayerScript") { }
    
    
    void GetValidFakeDisplay(Player const* player, uint8 &raceId, uint32 &morphId)
    {
        // Our possible races.
        const static uint8 allianceRaces[5]         = {RACE_HUMAN, RACE_GNOME, RACE_NIGHTELF, RACE_DWARF, RACE_DRAENEI};
        const static uint32 allianceMaleMorphs[5]   = { 19723, 20580, 20318, 20317, 20323 }; // Draenei morph copied from female (for shamans)
        const static uint32 allianceFemaleMorphs[5] = { 19724, 20581, 20318,     0, 20323 }; // NightElf morph copied from male (for druids)
        const static uint8 hordeRaces[5]            = {RACE_ORC, RACE_TROLL, RACE_TAUREN, RACE_BLOODELF, RACE_UNDEAD_PLAYER};
        const static uint32 hordeMaleMorphs[5]      = {     0, 20321, 20585, 20578,     0 };
        const static uint32 hordeFemaleMorphs[5]    = { 20316,     0, 20584, 20579,     0 };

        // Pointer to one of the above based on BGTeam.
        uint8 const* availableRaces = player->GetBGTeam() == ALLIANCE ? allianceRaces : hordeRaces;
        uint32 const* availableMorphs = player->GetBGTeam() == ALLIANCE ? 
            (player->getGender() == GENDER_MALE ? allianceMaleMorphs : allianceFemaleMorphs) :
            (player->getGender() == GENDER_MALE ? hordeMaleMorphs : hordeFemaleMorphs);
        uint8 validRaces[5];
        uint32 validMorphs[5];
        uint8 foundInfoSlots = 0;
        
        // Find the races that are valid for this class.
        for(uint8 i = 0; i < 5; i++)
            if(sObjectMgr->GetPlayerInfo(availableRaces[i], player->getClass()) && availableMorphs[i]) {
                validRaces[foundInfoSlots] = availableRaces[i];
                validMorphs[foundInfoSlots++] = availableMorphs[i];
            }
        
        ASSERT(foundInfoSlots != 0); // we should always find something, if not, there's something direly wrong.
        
        uint8 chosenInfoSlot = urand(0, --foundInfoSlots);
        raceId = validRaces[chosenInfoSlot];
        morphId = validMorphs[chosenInfoSlot];
    }
    
    void OnPlayerJoinedBattleground(Player* player, Battleground* bg)
    {
        if (!bg->isBattleground())
            return;
        
        // Set correct faction, fake race and morphs for cross-faction players based on their respective BG teams
        if (player->GetBGTeam() != player->TeamForRace(player->getRace()))
        {
            player->setFaction(player->GetBGTeam() == ALLIANCE ? 1 : 2);
            player->SendPvPMessage(5, "You have been temporarily faction changed to %s character to balance teams.", 
                                   (player->GetBGTeam() == ALLIANCE ? "an |cff00afffALLIANCE|r" : "a |cffff0000HORDE|r"));
            
            uint8 fakeRaceId;
            uint32 fakeMorphId;
            GetValidFakeDisplay(player, fakeRaceId, fakeMorphId);
            
            player->SetFakeRace(fakeRaceId);
            player->SetNativeDisplayId(fakeMorphId);
            player->RestoreDisplayId();
        }
        
        // Recache
        WorldSession* psession = player->GetSession();
        // Recache player info for self
        if (psession)
            psession->ReCachePlayerName(player->GetGUID());
        // Recache player info for bg players, and vice versa
        for (Battleground::BattlegroundPlayerMap::const_iterator itr = bg->GetPlayers().begin(); itr != bg->GetPlayers().end(); ++itr)
            if (Player* bgPlayer = ObjectAccessor::FindPlayer(itr->first))
            {
                // Recache "player" info for "bgPlayer"
                if (WorldSession* bgPsession = bgPlayer->GetSession())
                    bgPsession->ReCachePlayerName(player->GetGUID());

                // Recache "bgPlayer" info for "player"
                if (psession)
                    psession->ReCachePlayerName(bgPlayer->GetGUID());
            }
    }

    void OnLogin(Player* player)
    {
        Battleground* bg = player->GetBattleground();
        if (!bg) {
            player->SetBattlegroundId(0, BATTLEGROUND_TYPE_NONE);
            player->SetBGTeam(0);
            return;
        }

        return OnPlayerJoinedBattleground(player, bg);
    }

    void OnEventPlayerLoggedOutOfBattleground(Player* player, Battleground* bg)
    {
        if (!player->GetBattleground() || player->GetBattleground() != bg)
            return;

        return OnPlayerRemoveFromBattleground(player, bg);
    }

    void OnPlayerRemoveFromBattleground(Player* player, Battleground* bg)
    {
        if (!bg->isBattleground())
            return;
        
        // Reset player's team, faction, display to default values
        player->RestoreFaction();
        player->SetFakeRace(0);
        player->InitDisplayIds();
        player->RestoreDisplayId();
        
        // Recache this player's info for all members of m_recacheRecipients list
        for (Player::recacheRecipients::const_iterator itr = player->GetRecacheRecipients().begin(); itr != player->GetRecacheRecipients().end(); ++itr)
            if (Player* recacheRecipient = sObjectAccessor->FindPlayer(*itr))
                if (WorldSession* rRsession = recacheRecipient->GetSession())
                    rRsession->ReCachePlayerName(player->GetGUID());
        
        player->ClearRecacheRecipients(); // Clear this player's m_recacheRecipients list
    }
    
    void OnLogout(Player* player)
    {
        // Reset player's team, faction, display to default values
        player->RestoreFaction();
        player->SetFakeRace(0);
        player->InitDisplayIds();
        player->RestoreDisplayId();
        
        // Recache this player's info for all members of m_recacheRecipients list
        for (Player::recacheRecipients::const_iterator itr = player->GetRecacheRecipients().begin(); itr != player->GetRecacheRecipients().end(); ++itr)
            if (Player* recacheRecipient = sObjectAccessor->FindPlayer(*itr))
                if (WorldSession* rRsession = recacheRecipient->GetSession())
                    rRsession->ReCachePlayerName(player->GetGUID());
        
        player->ClearRecacheRecipients(); // Clear this player's m_recacheRecipients list
    }
};

void AddSC_cg_xfactionbg()
{
    new cg_xfactionbg_PlayerScript();
}