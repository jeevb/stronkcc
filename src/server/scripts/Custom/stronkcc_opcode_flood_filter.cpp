#include "ScriptPCH.h"
#include "Opcodes.h"
#include "SiteMgr.h"
#include "World.h"
#include "WorldSession.h"
#include "WorldSocket.h"

#define DEFAULT_CHECK_INTERVAL 1
#define DEFAULT_MAX_OPCODE_COUNT 100

class stronkcc_opcode_flood_filter : public ServerScript 
{
public:
    stronkcc_opcode_flood_filter() : ServerScript("stronkcc_opcode_flood_filter") { }
    
    void OnPacketReceive(WorldSocket* socket, WorldPacket& packet) {
        if (!socket) // Just in case...
            return;
        WorldSession* session = socket->GetSession();
        if (!session)
            return;
        uint16 opcode = packet.GetOpcode();
        if (opcode == MSG_NULL_ACTION || opcode >= NUM_MSG_TYPES)
            return;
        switch (opcode) {
            // Bypass check for the following opcodes
            case CMSG_ITEM_QUERY_SINGLE:
            case CMSG_GAMEOBJECT_QUERY:
            case CMSG_SET_ACTION_BUTTON:
            // Movement opcodes
            case MSG_MOVE_START_FORWARD:
            case MSG_MOVE_START_BACKWARD:
            case MSG_MOVE_HEARTBEAT:
            case MSG_MOVE_STOP:
            case MSG_MOVE_SET_FACING:
            case MSG_MOVE_START_TURN_LEFT:
            case MSG_MOVE_START_TURN_RIGHT:
            case MSG_MOVE_STOP_TURN:
            case MSG_MOVE_START_STRAFE_LEFT:
            case MSG_MOVE_START_STRAFE_RIGHT:
            case MSG_MOVE_STOP_STRAFE:
            case MSG_MOVE_JUMP:
            case MSG_MOVE_FALL_LAND:
                break;
            // Add opcodes to check for flooding here...
            case CMSG_BUG:
            case CMSG_CHAR_ENUM:
                UpdateOpcodeRecvTime(session, opcode, 1, 2); // Allow a max of 2 opcodes every 1 second
                break;
            case CMSG_CHAT_IGNORED:
                UpdateOpcodeRecvTime(session, opcode, 1, 10); // Allow a max of 10 opcodes every 1 second
                break;
            default:
                UpdateOpcodeRecvTime(session, opcode, DEFAULT_CHECK_INTERVAL, DEFAULT_MAX_OPCODE_COUNT);
                break;
        }
        
        return;
    }
    
    void UpdateOpcodeRecvTime(WorldSession* session, uint16 opcode, time_t checkInterval, uint8 maxCount) {
        if (!session) // Just in case...
            return;
        WorldSession::RecvOpcodeDataMap& opcodeDataMap = session->GetRecvOpcodeDataMap();
        time_t currentTime = time(NULL);
        if (opcodeDataMap[opcode].recvTime > currentTime) {
            if (++opcodeDataMap[opcode].recvCount >= maxCount - 1) {
                TC_LOG_ERROR(LOG_FILTER_OPCODES, "Received too many %s opcodes from %s. Kicking...", 
                             GetOpcodeNameForLogging(opcode).c_str(),
                             session->GetPlayerInfo().c_str());
                sSiteMgr->LogToIRC("%s|Received too many %s opcodes from %s. Kicking...", 
                                   sWorld->GetIrcLogsChannel().c_str(),
                                   GetOpcodeNameForLogging(opcode).c_str(),
                                   session->GetPlayerInfo().c_str());
                session->KickPlayer();
                return;
            }
        }
        else
            opcodeDataMap[opcode].recvCount = 0;
        
        opcodeDataMap[opcode].recvTime = currentTime + checkInterval;
    }
};

void AddSC_stronkcc_opcode_flood_filter()
{
    new stronkcc_opcode_flood_filter;
}