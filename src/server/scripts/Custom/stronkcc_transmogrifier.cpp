#include "ObjectMgr.h"
#include "ScriptPCH.h"
#include "World.h"

enum transmogActionFlags {
    TRANSMOG_START_BIT  =    8,
    TRANSMOG_START_MASK =    (1 << 8) - 1,
    TRANSMOG_REVERT     =    1,
    TRANSMOG_CUSTOM     =    2,
    TRANSMOG_VENDOR     =    3,
    TRANSMOG_BACK       =    4,
    TRANSMOG_END        =    5
};

#define MAX_LINE_CHARS 36
#define XMOG_WITH_OPT(slot, opt) ((slot & TRANSMOG_START_MASK) | (opt << TRANSMOG_START_BIT))

class stronkcc_transmogrifier : public CreatureScript, public PlayerScript
{
private:
    struct transmogDataStorage_t {
        uint64 itemGuid;
        ItemTemplate const* itemTemplate;
        std::wstring wNamePart;
    };
    typedef std::map<Player *, transmogDataStorage_t> transmogDataStorage;
    transmogDataStorage m_transmogDataStorage;
public:
    stronkcc_transmogrifier() : 
    CreatureScript("stronkcc_transmogrifier"), 
    PlayerScript("stronkcc_transmogrifier_PS") { }
    
    void OnLogout(Player *player) {
        // if the player logs out with an incomplete transmog txn, delete it.
        m_transmogDataStorage.erase(player);
    }
    
    bool OnGossipHello(Player* player, Creature* creature) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_transmogDataStorage.erase(player);
        if (player->IsInCombat()) {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
            return true;
        }
        for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i)
            switch (i) {
                case EQUIPMENT_SLOT_FINGER1:
                case EQUIPMENT_SLOT_FINGER2:
                case EQUIPMENT_SLOT_TRINKET1:
                case EQUIPMENT_SLOT_TRINKET2:
                case EQUIPMENT_SLOT_NECK:
                case EQUIPMENT_SLOT_BODY:
                    break;
                default:
                {
                    if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
                        // Exceptions for items that should not be displayed on gossip menu
                        switch (item->GetTemplate()->InventoryType) {
                            case INVTYPE_RELIC:
                                break;
                            default:
                                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, 
                                                        GetItemDescription(player, item, i), 
                                                        item->GetGUIDLow(), 
                                                        XMOG_WITH_OPT(i, TRANSMOG_END));
                                break;
                        }
                    
                    break;
                }
            }
        
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        return true;
    }
    
    bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        
        // Special case, when they click "Transmog another item" after buying a vendor xmog.
        if (!uiSender && !uiAction)
            return OnGossipHello(player, creature);
        
        // Otherwise, they've got an item selected from OnGossipHello.
        Item* chosenItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, uiAction & TRANSMOG_START_MASK); // mask out any bits above the 8th slot, we store flags there.
        
        // Some sanity checks, make sure that item hasn't shifted in slots.
        if (!chosenItem || chosenItem->GetGUIDLow() != uiSender) {
            ChatHandler(player->GetSession()).PSendSysMessage("An error has occurred. Please try again.");
            return OnGossipHello(player, creature);
        }
        
        // We do a bit of bitwise trickery to squeeze the slot, and the action into a uint32.
        uint32 transmogActions = uiAction >> TRANSMOG_START_BIT;
        
        switch(transmogActions) {
            case TRANSMOG_REVERT:
                RevertTransmog(player, creature, chosenItem);
                return OnGossipHello(player, creature);
                
            case TRANSMOG_CUSTOM:
            {
                // Find the target item, which is the first item in the backpack.
                ItemTemplate const* targetTemplate = NULL;
                if (Item *target = player->GetItemByPos(INVENTORY_SLOT_BAG_0, INVENTORY_SLOT_ITEM_START))
                    targetTemplate = target->GetTemplate();
                
                // Apply custom transmogrification.
                if (TransmogrifyToItem(player, creature, chosenItem, targetTemplate) == FAKE_ERR_OK)
                    SendTransmogSuccessMenu(player, creature, chosenItem, targetTemplate);
                else
                    // Bring the player back to the main menu for the item, (default: case)
                    OnGossipSelect(player, creature, uiSender, XMOG_WITH_OPT(uiAction, TRANSMOG_END));
                
                return true;
            }
                
            case TRANSMOG_BACK:
                // Return to main screen.
                return OnGossipHello(player, creature);
                
            default:
            {
                std::ostringstream os;
                // Check to see if the player can do a custom transmog with the first item in their backpack.
                if (Item *trToItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, INVENTORY_SLOT_ITEM_START)) {
                    if (Item::CanSetFakeDisplay(chosenItem->GetTemplate(), trToItem->GetTemplate()) == FAKE_ERR_OK && 
                       chosenItem->GetTemplate()->ItemId != trToItem->GetTemplate()->ItemId &&
                       chosenItem->GetFakeDisplayEntry() != trToItem->GetTemplate()->ItemId) {
                        
                        os  << "Transmogrify " << GetItemName(chosenItem->GetTemplate()) << " into "
                            << GetItemName(trToItem->GetTemplate()) << ".";
                        
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1,
                                                os.str(), 
                                                uiSender,
                                                XMOG_WITH_OPT(uiAction, TRANSMOG_CUSTOM));
                    }
                }
                os.str(std::string());
                // Allow them to buy a vendor transmog.
                os << "Search for an eligible item skin by name for " << GetItemName(chosenItem->GetTemplate()) <<".";
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG,
                                                 os.str(),
                                                 uiSender,
                                                 XMOG_WITH_OPT(uiAction, TRANSMOG_VENDOR),
                                                 "",
                                                 0,
                                                 true);
                os.str(std::string());
                // Check to see if they can revert the transmog.
                if (chosenItem->GetFakeDisplayEntry()) {
                    os << "Revert the transmogrification on " << GetItemName(chosenItem->GetTemplate()) << ".";
                    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE,
                                                     os.str(),
                                                     uiSender,
                                                     XMOG_WITH_OPT(uiAction, TRANSMOG_REVERT),
                                                     "Are you sure you wish to reverse this transmogrification? This action cannot be undone.",
                                                     0,
                                                     false); 
                }
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, 
                                        "Return to the main menu.", 
                                        uiSender,
                                        XMOG_WITH_OPT(uiAction, TRANSMOG_BACK));
                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
                break;
            }
        }
        
        return true;
    }
    
    bool OnGossipSelectCode(Player * player, Creature* creature, uint32 uiSender, uint32 uiAction, const char *ccode) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        
        // Special case, when they click "Transmog another item" after buying a vendor xmog.
        if (!uiSender && !uiAction)
            return OnGossipHello(player, creature);
        
        std::wstring wNamePart;
        // converting string that we try to find to lower case
        if (!*ccode || !Utf8toWStr((std::string)ccode, wNamePart)) {
            ChatHandler(player->GetSession()).PSendSysMessage("Invalid name! Please try again.");
            OnGossipSelect(player, creature, uiSender, XMOG_WITH_OPT(uiAction, TRANSMOG_END));
        }
        wstrToLower(wNamePart);
        
        // Otherwise, they've got an item selected from OnGossipHello.
        Item* chosenItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, uiAction & TRANSMOG_START_MASK); // mask out any bits above the 8th slot, we store flags there.
        
        // Some sanity checks, make sure that item hasn't shifted in slots.
        if (!chosenItem || chosenItem->GetGUIDLow() != uiSender) {
            ChatHandler(player->GetSession()).PSendSysMessage("An error has occurred. Please try again.");
            return OnGossipHello(player, creature);
        }
        
        // We do a bit of bitwise trickery to squeeze the slot, and the action into a uint32.
        uint32 transmogActions = uiAction >> TRANSMOG_START_BIT;
        
        switch (transmogActions) {
            case TRANSMOG_VENDOR:
            {
                // Send vendor list.
                transmogDataStorage_t &meta = m_transmogDataStorage[player];
                meta.itemGuid = chosenItem->GetGUID();
                meta.itemTemplate = chosenItem->GetTemplate();
                meta.wNamePart = wNamePart;
                player->GetSession()->SendListInventory(creature->GetGUID());
                break;
            }
            default:
                return false;
        }
        
        return true;
    }
    
    bool BypassDisplayItemChecks(Player* /*player*/, Creature* /*vendor*/, ItemTemplate const* /*item_template*/, VendorItem const* /*vendor_item*/) {
        return true;
    }
    
    bool ShouldDisplayVendorItem(Player* player, Creature* /*vendor*/, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/) {
        transmogDataStorage::iterator iter = m_transmogDataStorage.find(player);
        if (iter == m_transmogDataStorage.end() || !iter->second.itemTemplate)
            return true;
        
        return  HasMatchingName(item_template, iter->second.wNamePart) &&
                Item::CanSetFakeDisplay(iter->second.itemTemplate, item_template) == FAKE_ERR_OK;
    }
    
    bool HasMatchingName(ItemTemplate const* item_template, std::wstring wNamePart) {
        std::string name = item_template->Name1;
        return !wNamePart.empty() && !name.empty() && Utf8FitTo(name, wNamePart);
    }
    
    bool OnBeforePurchaseItem(Player *player, Creature *vendor, ItemTemplate const* item_template, VendorItem const* vendor_item) {
        ChatHandler handler(player->GetSession());
        
        if (m_transmogDataStorage.find(player) == m_transmogDataStorage.end()) {
            handler.PSendSysMessage("An error has occurred. Please try again.");
            return false;
        }
        transmogDataStorage_t & transInfo = m_transmogDataStorage[player];
        
        Item *trItem = player->GetItemByGuid(transInfo.itemGuid);
        if (!trItem) {
            handler.PSendSysMessage("You don't own that item.");
            return false;
        }
        
        TransmogrifyToVendorItem(player, vendor, trItem, item_template, vendor_item);
        return false;
    }
    
    void TransmogrifyToVendorItem(Player* player, Creature* vendor, Item* fromItem, ItemTemplate const* toItem, VendorItem const* vendorItem) {
        if (TransmogrifyToItem(player, vendor, fromItem, toItem) == FAKE_ERR_OK) {
            if (vendorItem->ExtendedCost) {
                ItemExtendedCostEntry const* iece = sItemExtendedCostStore.LookupEntry(vendorItem->ExtendedCost);
                if (iece->reqhonorpoints)
                    player->ModifyHonorPoints(- int32(iece->reqhonorpoints));
                
                if (iece->reqarenapoints)
                    player->ModifyArenaPoints(- int32(iece->reqarenapoints));
                
                for (uint8 i = 0; i < MAX_ITEM_EXTENDED_COST_REQUIREMENTS; ++i)
                    if (iece->reqitem[i])
                        player->DestroyItemCount(iece->reqitem[i], iece->reqitemcount[i], true);
            }
            
            SendTransmogSuccessMenu(player, vendor, fromItem, toItem);
        }
    }
    
    FakeResult TransmogrifyToItem(Player* player, Creature* /*vendor*/, Item* trItem, ItemTemplate const* displayItem) {
        ChatHandler handler(player->GetSession());
        
        if (!trItem || !displayItem) {
            handler.PSendSysMessage("Items have not been placed correctly in your backpack!");
            return FAKE_ERR_CANT_FIND_ITEM;
        }
        
        FakeResult transmog_result = trItem->SetFakeDisplay(displayItem->ItemId);
        
        switch (transmog_result) {
            case FAKE_ERR_CANT_FIND_ITEM:
                handler.PSendSysMessage("Item cannot be found!");
                break;
            case FAKE_ERR_DIFF_SLOTS:
            case FAKE_ERR_DIFF_SUBCLASS:
                handler.PSendSysMessage("Item types do not match!");
                break;
            case FAKE_ERR_OK:
                handler.PSendSysMessage("%s has been successfully transmogrified!", 
                                        GetItemName(trItem->GetTemplate(), true).c_str());
                break;
            default:
                break;
        }
        
        return transmog_result;
    }
    
    void RevertTransmog(Player* player, Creature* /*vendor*/, Item* trItem) {
        ChatHandler handler(player->GetSession());
        if (!trItem->GetFakeDisplayEntry()) {
            handler.PSendSysMessage("%s doesn't have an active transmogrification.",
                                    GetItemName(trItem->GetTemplate(), true).c_str());
            
            return;
        }
        
        trItem->RemoveFakeDisplay();
        handler.PSendSysMessage("%s has had its transmogrification reverted.",
                                GetItemName(trItem->GetTemplate(), true).c_str());
    }
    
    void SendTransmogSuccessMenu(Player* player, Creature* creature, Item* fromItem, ItemTemplate const* toItem) {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();
        m_transmogDataStorage.erase(player);
        std::ostringstream ss;
        ss << "Your " << GetItemName(fromItem->GetTemplate()) << " has been successfully transmogrified into ";
        ss << GetItemName(toItem) << ". Transmogrify another item?";
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), 0, 0);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        
        return;
    }
    
    std::string GetItemName(ItemTemplate const* itemTemplate, bool withQualityColor = false) {
        std::ostringstream os;
        if (withQualityColor)
            os << "|c" << std::hex << ItemQualityColors[itemTemplate->Quality] << std::dec << "[" << itemTemplate->Name1 << "]|r";
        else
            os  << "|cff363636[" << itemTemplate->Name1 << "]|r";
        
        return os.str();
    }
    
    std::string GetItemDescription(Player* player, Item* item, uint8 slot) {
        std::string itemName = item->GetTemplate()->Name1;
        if (itemName.size() > MAX_LINE_CHARS) {
            itemName.resize(MAX_LINE_CHARS - 3);
            itemName += "...";
        }
        std::string iconPath = "INV_Misc_QuestionMark";
        uint32 itemEntry = player->GetUInt32Value(PLAYER_VISIBLE_ITEM_1_ENTRYID + (slot * 2));
        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemEntry))
            if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(itemTemplate->DisplayInfoID))
                iconPath = displayInfo->iconPath;
        std::ostringstream os;
        os  << "|TInterface\\Icons\\" << iconPath << ".blp:36:36:-16:1|t" 
            << "|Hitem:" << item->GetTemplate()->ItemId << ":0:0:0:0:0:0:0:0:0|h" << itemName << "|h";
        
        return os.str();
    }
};

void AddSC_stronkcc_transmogrifier()
{
    new stronkcc_transmogrifier;
}