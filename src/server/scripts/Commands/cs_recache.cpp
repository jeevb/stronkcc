#include "Chat.h"
#include "World.h"
#include "WorldSession.h"

class recache_commandscript : public CommandScript {
public:

    recache_commandscript() : CommandScript("recache_commandscript") {
    }

    ChatCommand* GetCommands() const {
        static ChatCommand recacheCommandTable[] ={
            { "item", SEC_ADMINISTRATOR, true, &HandleReCacheItemCommand, "", NULL},
            { "gobject", SEC_ADMINISTRATOR, true, &HandleReCacheGameObjectCommand, "", NULL},
            { "creature", SEC_ADMINISTRATOR, true, &HandleReCacheCreatureCommand, "", NULL},
            { "playername", SEC_ADMINISTRATOR, true, &HandleReCachePlayerNameCommand, "", NULL},
            { "itemall", SEC_ADMINISTRATOR, true, &HandleReCacheItemForAllCommand, "", NULL},
            { "gobjectall", SEC_ADMINISTRATOR, true, &HandleReCacheGameObjectForAllCommand, "", NULL},
            { "creatureall", SEC_ADMINISTRATOR, true, &HandleReCacheCreatureForAllCommand, "", NULL},
            { "playernameall", SEC_ADMINISTRATOR, true, &HandleReCachePlayerNameForAllCommand, "", NULL},
            { NULL, 0, false, NULL, "", NULL}
        };

        static ChatCommand commandTable[] ={
            { "recache", SEC_ADMINISTRATOR, true, NULL, "", recacheCommandTable},
            { NULL, 0, false, NULL, "", NULL}
        };

        return commandTable;
    }

    static bool HandleReCacheItemCommand(ChatHandler* handler, const char* args) {
        if (!args)
            return false;

        if (handler->GetSession() == NULL)
            return false;

        Player* p = handler->getSelectedPlayer();

        if (!p)
            return false;

        uint32 start, end = 0;
        if (sscanf(args, "%u %u", &start, &end) != 2)
            if (sscanf(args, "%u", &start) != 1)
                return false;

        if (end != 0) {
            if (start > end)
                return false;

            if (end - start >= 200) {
                handler->PSendSysMessage("You appear to be trying to 'accidently' the server, would you like some help?");
                return true;
            }

            handler->PSendSysMessage("ReCaching items %u through %u", start, end);
            while (start != end) {
                p->GetSession()->ReCacheItemInfo(start);
                start++;
            }
        } else {
            handler->PSendSysMessage("ReCaching item %u", start);
            p->GetSession()->ReCacheItemInfo(start);
        }

        return true;
    };

    static bool HandleReCacheCreatureCommand(ChatHandler* handler, const char* args) {
        if (!args)
            return false;

        if (handler->GetSession() == NULL)
            return false;

        Player* p = handler->getSelectedPlayer();

        if (p == NULL)
            return false;

        uint32 start, end = 0;

        if (sscanf(args, "%u %u", &start, &end) != 2)
            if (sscanf(args, "%u", &start) != 1)
                return false;

        if (end != 0) {
            if (start > end)
                return false;

            if (end - start >= 200) //lets limit this
            {
                handler->PSendSysMessage("You appear to be trying to 'accidently' yourself, would you like some help?");
                return true;
            }

            handler->PSendSysMessage("Recaching creatures %u through %u", start, end);
            while (start != end) {
                p->GetSession()->ReCacheCreatureInfo(start);
                start++;
            }
        } else {
            handler->PSendSysMessage("Recaching creature %u", start);
            p->GetSession()->ReCacheCreatureInfo(start);
        }
        return true;
    }

    static bool HandleReCachePlayerNameCommand(ChatHandler* handler, const char* args) {
        if (!args)
            return false;

        if (handler->GetSession() == NULL)
            return false;

        Player* p = handler->getSelectedPlayer();
        if (p == NULL)
            return false;

        handler->PSendSysMessage("Recaching targeted player's name");
        handler->GetSession()->ReCachePlayerName(p->GetGUID());
        return true;
    }

    static bool HandleReCacheGameObjectCommand(ChatHandler* handler, const char* args) {
        if (!args)
            return false;

        if (handler->GetSession() == NULL)
            return false;

        Player* p = handler->getSelectedPlayer();
        if (p == NULL)
            return false;

        uint32 start, end = 0;
        if (sscanf(args, "%u %u", &start, &end) != 2)
            if (sscanf(args, "%u", &start) != 1)
                return false;

        if (end != 0) {
            if (start > end)
                return false;

            if (end - start >= 200) //lets limit this
            {
                handler->PSendSysMessage("You appear to be trying to 'accidently' the server, would you like some help?");
                return true;
            }

            handler->PSendSysMessage("Recaching gameobjects %u through %u", start, end);
            while (start != end) {
                p->GetSession()->ReCacheGameObjectInfo(start);
                start++;
            }
        } else {
            handler->PSendSysMessage("Recaching gameobject %u", start);
            p->GetSession()->ReCacheGameObjectInfo(start);
        }
        return true;
    }

    static bool HandleReCacheItemForAllCommand(ChatHandler* handler, const char* args) {
        if (!args)
            return false;

        if (handler->GetSession() == NULL)
            return false;

        uint32 start, end = 0;
        if (sscanf(args, "%u %u", &start, &end) != 2)
            if (sscanf(args, "%u", &start) != 1)
                return false;

        if (end != 0) {
            if (start > end)
                return false;

            if (end - start >= 200) //lets limit this
            {
                handler->PSendSysMessage("You appear to be trying to 'accidently' the server, would you like some help?");
                return true;
            }

            handler->PSendSysMessage("Recaching items %u through %u", start, end);
            while (start != end) {
                sWorld->ReCacheForAll(1, start);
                start++;
            }
        } else {
            handler->PSendSysMessage("Recaching item %u", start);
            sWorld->ReCacheForAll(1, start);
        }
        return true;
    }

    static bool HandleReCacheCreatureForAllCommand(ChatHandler* handler, const char* args) {
        if (!args)
            return false;

        if (handler->GetSession() == NULL)
            return false;

        uint32 start, end = 0;
        if (sscanf(args, "%u %u", &start, &end) != 2)
            if (sscanf(args, "%u", &start) != 1)
                return false;

        if (end != 0) {
            if (start > end)
                return false;

            if (end - start >= 200) //lets limit this
            {
                handler->PSendSysMessage("You appear to be trying to 'accidently' yourself, would you like some help?");
                return true;
            }

            handler->PSendSysMessage("Recaching creatures %u through %u", start, end);
            while (start != end) {
                sWorld->ReCacheForAll(3, start);
                start++;
            }
        } else {
            handler->PSendSysMessage("Recaching creature %u", start);
            sWorld->ReCacheForAll(3, start);
        }
        return true;
    }

    static bool HandleReCachePlayerNameForAllCommand(ChatHandler* handler, const char* args) {
        if (!args)
            return false;

        if (handler->GetSession() == NULL)
            return false;

        Player* p = handler->getSelectedPlayer();
        if (p == NULL)
            return false;

        handler->PSendSysMessage("Recaching targeted player's name");
        sWorld->ReCacheForAll(2, p->GetGUID());
        return true;
    }

    static bool HandleReCacheGameObjectForAllCommand(ChatHandler* handler, const char* args) {
        if (!args)
            return false;

        if (handler->GetSession() == NULL)
            return false;

        uint32 start, end = 0;
        if (sscanf(args, "%u %u", &start, &end) != 2)
            if (sscanf(args, "%u", &start) != 1)
                return false;

        if (end != 0) {
            if (start > end)
                return false;

            if (end - start >= 200) //lets limit this
            {
                handler->PSendSysMessage("You appear to be trying to 'accidently' the server, would you like some help?");
                return true;
            }

            handler->PSendSysMessage("Recaching gameobjects %u through %u", start, end);
            while (start != end) {
                sWorld->ReCacheForAll(4, start);
                start++;
            }
        } else {
            handler->PSendSysMessage("Recaching gameobject %u", start);
            sWorld->ReCacheForAll(4, start);
        }
        return true;
    }
};

void AddSC_recache_commandscript() {
    new recache_commandscript();
}