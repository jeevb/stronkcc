#include "ScriptMgr.h"
#include "Chat.h"
#include "WorldPvPMgr.h"

class stronkcc_world_pvp_commandscript : public CommandScript
{
public:
    stronkcc_world_pvp_commandscript() : CommandScript("stronkcc_world_pvp_commandscript") { }

    ChatCommand* GetCommands() const
    {
        static ChatCommand wpvpCommandTable[] =
        {
            { "reset",          SEC_GAMEMASTER,     false, &HandleWpvpResetCommand,            "", NULL },
            { NULL,             0,                  false, NULL,                               "", NULL }
        };
        static ChatCommand commandTable[] =
        {
            { "wpvp",           SEC_GAMEMASTER,     false, NULL,                   "", wpvpCommandTable },
            { NULL,             0,                  false, NULL,                               "", NULL }
        };
        return commandTable;
    }

    static bool HandleWpvpResetCommand(ChatHandler* handler, const char* /*args*/)
    {
        if (Player* player = handler->GetSession()->GetPlayer()) {
            uint32 areaId = player->GetAreaId();
            if (sWorldPvPMgr->IsWorldPvPArea(areaId)) {
                sWorldPvPMgr->AreaGuardianDespawned(areaId);
                sWorldPvPMgr->RespawnNpcsAndGuards(areaId);
                handler->PSendSysMessage("[WorldPvPMgr] areaId: %u succesfully reset!", areaId);
                return true;
            }
        }

        return false;
    }
};

void AddSC_stronkcc_world_pvp_commandscript()
{
    new stronkcc_world_pvp_commandscript();
}