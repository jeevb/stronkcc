#include "ScriptMgr.h"
#include "AccountMgr.h"
#include "Chat.h"
#include "Group.h"

class team_commandscript : public CommandScript
{
public:
    team_commandscript() : CommandScript("team_commandscript") { }

    ChatCommand* GetCommands() const
    {
        static ChatCommand teamCommandTable[] =
        {
            { "summon",         SEC_GAMEMASTER,     false, &HandleTeamSummonCommand,         "", NULL },
            { "freeze",         SEC_GAMEMASTER,     false, &HandleTeamFreezeCommand,         "", NULL },
            { "unfreeze",       SEC_GAMEMASTER,     false, &HandleTeamUnFreezeCommand,       "", NULL },
            { "cooldown",       SEC_GAMEMASTER,     false, &HandleTeamCooldownCommand,       "", NULL },
            { "revive",         SEC_GAMEMASTER,     false, &HandleTeamReviveCommand,         "", NULL },
            { "unaura",         SEC_GAMEMASTER,     false, &HandleTeamUnAuraCommand,         "", NULL },
            { NULL,             0,                  false, NULL,                              "", NULL }
        };
        static ChatCommand commandTable[] =
        {
            { "team",        SEC_GAMEMASTER,         true,  NULL,     "", teamCommandTable  },
            { NULL,             0,                  false, NULL,                     "", NULL }
        };
        return commandTable;
    }
    
    static bool HandleTeamReviveCommand(ChatHandler* handler, const char* args) {
        Player* target;
        if (!handler->extractPlayerTarget((char*)args, &target))
            return false;
        Group* grp = target->GetGroup();
        
        if (!grp)
        {
            handler->PSendSysMessage(LANG_NOT_IN_GROUP, handler->GetNameLink(target).c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }
        for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* player = itr->GetSource();
            if (!player || !player->GetSession() || player->IsAlive())
                continue;
            player->ResurrectPlayer(!AccountMgr::IsPlayerAccount(player->GetSession()->GetSecurity()) ? 1.0f : 1.0f);
            player->SpawnCorpseBones();
            player->SaveToDB();
            handler->PSendSysMessage("Revived %s", handler->GetNameLink(player).c_str());
        }
        
        return true;
    }
    
    static bool HandleTeamUnAuraCommand(ChatHandler* handler, const char* args) {
        Player* target = handler->GetSession()->GetPlayer()->GetSelectedPlayer();
        if (!target)
            target = handler->GetSession()->GetPlayer();
            
        Group* grp = target->GetGroup();
        if (!grp)
        {
            handler->PSendSysMessage(LANG_NOT_IN_GROUP, handler->GetNameLink(target).c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }
        std::string argstr = args;
        uint32 spellID;
        std::string spellLink;
        if(argstr == "all") {
            spellID = 0;
            
        } else {
            int loc = handler->GetSessionDbcLocale();
            spellID = handler->extractSpellIdFromLink((char*)args);
            if (spellID) {
                if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellID)) {
                    std::ostringstream ss;
                    ss << "|cffffffff|Hspell:" << spellInfo->Id << "|h[" << spellInfo->SpellName[loc] << "]|h|r";
                    spellLink = ss.str();
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* player = itr->GetSource();
            if (!player || !player->GetSession())
                continue;
            if(spellID) {
                if(player->HasAura(spellID)) {
                    player->RemoveAurasDueToSpell(spellID);
                    handler->PSendSysMessage("Removed %s from %s", spellLink.c_str(), handler->GetNameLink(player).c_str());
                }
            } else {
                player->RemoveAllAuras();
                handler->PSendSysMessage("Removed all auras from %s", handler->GetNameLink(player).c_str());
            }
        }
        
        return true;
    }
    
    static bool HandleTeamCooldownCommand(ChatHandler* handler, const char* args) {
        Player* target;
        if (!handler->extractPlayerTarget((char*)args, &target))
            return false;
        Group* grp = target->GetGroup();
        std::string nameLink = handler->GetNameLink(target);

        if (!grp)
        {
            handler->PSendSysMessage(LANG_NOT_IN_GROUP, nameLink.c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }
        for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* player = itr->GetSource();
            if (!player || !player->GetSession())
                continue;
            
            player->RemoveAllSpellCooldown();
            handler->PSendSysMessage(LANG_REMOVEALL_COOLDOWN, handler->GetNameLink(player).c_str());
        }
        
        return true;
    }
    
    static bool HandleTeamFreezeCommand(ChatHandler* handler, const char* args) {
        Player* me = handler->GetSession()->GetPlayer();
        Player* target;
        if (!handler->extractPlayerTarget((char*)args, &target))
            return false;
        
        Group* grp = target->GetGroup();
        std::string nameLink = handler->GetNameLink(target);

        if (!grp)
        {
            handler->PSendSysMessage(LANG_NOT_IN_GROUP, nameLink.c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }
        for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* player = itr->GetSource();
            if (!player || player == me || !player->GetSession())
                continue;
            
            handler->PSendSysMessage(LANG_COMMAND_FREEZE, handler->GetNameLink(player).c_str());

            //stop combat + make player unattackable + duel stop + stop some spells
            player->setFaction(35);
            player->CombatStop();
            if (player->IsNonMeleeSpellCasted(true))
                player->InterruptNonMeleeSpells(true);
            player->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);

            //if player class = hunter || warlock remove pet if alive
            if ((player->getClass() == CLASS_HUNTER) || (player->getClass() == CLASS_WARLOCK))
            {
                if (Pet* pet = player->GetPet())
                {
                    pet->SavePetToDB(PET_SAVE_AS_CURRENT);
                    // not let dismiss dead pet
                    if (pet && pet->IsAlive())
                        player->RemovePet(pet, PET_SAVE_NOT_IN_SLOT);
                }
            }

            //m_session->GetPlayer()->CastSpell(player, spellID, false);
            if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(9454))
                Aura::TryRefreshStackOrCreate(spellInfo, MAX_EFFECT_MASK, player, player);

            //save player
            player->SaveToDB();
            
        }
        
        return true;
    }
    
    static bool HandleTeamUnFreezeCommand(ChatHandler* handler, const char* args) {
        Player* target;
        if (!handler->extractPlayerTarget((char*)args, &target))
            return false;
        
        Group* grp = target->GetGroup();
        std::string nameLink = handler->GetNameLink(target);

        if (!grp)
        {
            handler->PSendSysMessage(LANG_NOT_IN_GROUP, nameLink.c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }
        for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* player = itr->GetSource();
            if (!player || !player->GetSession())
                continue;
            
            handler->PSendSysMessage(LANG_COMMAND_UNFREEZE, handler->GetNameLink(player).c_str());
            player->setFactionForRace(player->getRace());
            player->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);

            //allow movement and spells
            player->RemoveAurasDueToSpell(9454);

            //save player
            player->SaveToDB();
        }
        return true;
    }
    
    static bool HandleTeamSummonCommand(ChatHandler* handler, const char* args) {
        Player* target;
        if (!handler->extractPlayerTarget((char*)args, &target))
            return false;

        // check online security
        if (handler->HasLowerSecurity(target, 0))
            return false;

        Group* grp = target->GetGroup();

        std::string nameLink = handler->GetNameLink(target);

        if (!grp)
        {
            handler->PSendSysMessage(LANG_NOT_IN_GROUP, nameLink.c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }
        WorldSession* m_session = handler->GetSession();
        
        Map* gmMap = m_session->GetPlayer()->GetMap();
        bool to_instance =  gmMap->Instanceable();

        // we are in instance, and can summon only player in our group with us as lead
        if (to_instance && (
            !m_session->GetPlayer()->GetGroup() || (grp->GetLeaderGUID() != m_session->GetPlayer()->GetGUID()) ||
            (m_session->GetPlayer()->GetGroup()->GetLeaderGUID() != m_session->GetPlayer()->GetGUID())))
            // the last check is a bit excessive, but let it be, just in case
        {
            handler->SendSysMessage(LANG_CANNOT_SUMMON_TO_INST);
            handler->SetSentErrorMessage(true);
            return false;
        }

        for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* player = itr->GetSource();

            if (!player || player == m_session->GetPlayer() || !player->GetSession())
                continue;

            // check online security
            if (handler->HasLowerSecurity(player, 0))
                return false;

            std::string plNameLink = handler->GetNameLink(player);

            if (player->IsBeingTeleported() == true)
            {
                handler->PSendSysMessage(LANG_IS_TELEPORTED, plNameLink.c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            if (to_instance)
            {
                Map* plMap = player->GetMap();

                if (plMap->Instanceable() && plMap->GetInstanceId() != gmMap->GetInstanceId())
                {
                    // cannot summon from instance to instance
                    handler->PSendSysMessage(LANG_CANNOT_SUMMON_TO_INST, plNameLink.c_str());
                    handler->SetSentErrorMessage(true);
                    return false;
                }
            }

            handler->PSendSysMessage(LANG_SUMMONING, plNameLink.c_str(), "");
            if (handler->needReportToTarget(player))
                ChatHandler(player->GetSession()).PSendSysMessage(LANG_SUMMONED_BY, handler->GetNameLink().c_str());

            // stop flight if need
            if (player->IsInFlight())
            {
                player->GetMotionMaster()->MovementExpired();
                player->CleanupAfterTaxiFlight();
            }
            // save only in non-flight case
            else
                player->SaveRecallPosition();

            // before GM
            float x, y, z;
            m_session->GetPlayer()->GetClosePoint(x, y, z, player->GetObjectSize());
            player->TeleportTo(m_session->GetPlayer()->GetMapId(), x, y, z, player->GetOrientation());
        }

        return true;
    }
};

void AddSC_team_commandscript()
{
    new team_commandscript();
}
