DELETE FROM `world_pvp_prof_spell_difficulty` WHERE `spellId` IN (67093, 67132, 67094, 63192);
INSERT INTO `world_pvp_prof_spell_difficulty` (`spellId`, `tradeskillId`, `spellDifficulty`)
VALUES
	(67093, 2, 2),
	(67132, 2, 2),
	(67094, 2, 2),
	(63192, 2, 1);
