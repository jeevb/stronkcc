DROP TABLE IF EXISTS `tameable_pets`;

CREATE TABLE `tameable_pets` (
  `petEntry` int(11) unsigned NOT NULL DEFAULT '0',
  `petClass` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `petSubClass` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `petDescription` tinytext NOT NULL,
  UNIQUE KEY `id` (`petEntry`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;