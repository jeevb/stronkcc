DELETE FROM `world_pvp_item_difficulty` WHERE `itemId` IN (42143, 42144);
INSERT INTO `world_pvp_item_difficulty` (`itemId`, `itemName`, `itemDifficulty`)
VALUES
	(42143, 'Delicate Dragon\'s Eye', 2),
	(42144, 'Runed Dragon\'s Eye', 2);
