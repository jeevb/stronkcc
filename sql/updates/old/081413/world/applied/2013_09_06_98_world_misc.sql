DELETE FROM `creature_template_addon` WHERE `entry` = 620000;
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`)
VALUES
	(620000, 0, 0, 0, 257, 0, NULL);

DELETE FROM `npc_spellclick_spells` WHERE `npc_entry` = 620000;
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`)
VALUES
	(620000, 46598, 1, 0);
