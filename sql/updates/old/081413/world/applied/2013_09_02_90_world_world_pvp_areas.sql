# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.32-0ubuntu0.12.04.1)
# Database: clanwars_wdb
# Generation Time: 2013-09-02 14:26:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table world_pvp_areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `world_pvp_areas`;

CREATE TABLE `world_pvp_areas` (
  `areaId` int(10) unsigned NOT NULL DEFAULT '0',
  `areaName` tinytext NOT NULL,
  `difficulty` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `gySafeLocId` int(10) unsigned NOT NULL DEFAULT '0',
  `flagMapId` int(10) unsigned NOT NULL DEFAULT '0',
  `flagPos_x` float NOT NULL DEFAULT '0',
  `flagPos_y` float NOT NULL DEFAULT '0',
  `flagPos_z` float NOT NULL DEFAULT '0',
  `flagOrientation` float NOT NULL DEFAULT '0',
  `flagRot_0` float NOT NULL DEFAULT '0',
  `flagRot_1` float NOT NULL DEFAULT '0',
  `flagRot_2` float NOT NULL DEFAULT '0',
  `flagRot_3` float NOT NULL DEFAULT '0',
  `guardianSpawn_x` float NOT NULL DEFAULT '0',
  `guardianSpawn_y` float NOT NULL DEFAULT '0',
  `guardianSpawn_z` float NOT NULL DEFAULT '0',
  `guardianSpawn_o` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`areaId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `world_pvp_areas` WRITE;
/*!40000 ALTER TABLE `world_pvp_areas` DISABLE KEYS */;

INSERT INTO `world_pvp_areas` (`areaId`, `areaName`, `difficulty`, `gySafeLocId`, `flagMapId`, `flagPos_x`, `flagPos_y`, `flagPos_z`, `flagOrientation`, `flagRot_0`, `flagRot_1`, `flagRot_2`, `flagRot_3`, `guardianSpawn_x`, `guardianSpawn_y`, `guardianSpawn_z`, `guardianSpawn_o`)
VALUES
	(3538,'Honor Hold',4,920,530,-749.844,2655.51,102.108,0.455375,0,0,0.225725,0.974191,-731.377,2658.85,95.1358,3.29197),
	(3536,'Thrallmar',4,919,530,178.488,2688.33,86.9523,3.3392,0,0,0.995123,-0.0986406,158.744,2684.96,84.9061,0.173943),
	(3554,'Falcon Watch',3,934,530,-607.714,4101.92,90.1775,2.0308,0,0,0.849692,0.52728,-596.984,4084.38,93.8228,2.04945),
	(3552,'Temple of Telhamat',3,933,530,190.484,4333.8,116.396,3.15752,0,0,0.999968,-0.00796362,209.843,4333.88,119.242,3.19614),
	(3671,'Broken Hill',1,0,530,-472.23,3450.86,35.9405,5.06602,0,0,0.571703,-0.82046,-449.763,3451.46,35.8002,3.19444),
	(3669,'The Stadium',1,0,530,-291.023,3702.86,58.1096,4.84261,0,0,0.659602,-0.751616,-292.631,3725.69,56.0067,4.79273),
	(3670,'The Overlook',1,0,530,-186.368,3451.28,38.9463,1.96013,0,0,0.830534,0.556968,-172.97,3472.08,41.3147,4.13692),
	(3555,'Mag\'har Post',3,0,530,518.531,3876.17,191.834,4.12198,0,0,0.882241,-0.470797,505.813,3861.64,194.356,1.02673),
	(3582,'Zeth\'Gor',3,0,530,-1128.42,1985.64,71.9716,1.11246,0,0,0.52799,0.849251,-1120.76,2000.41,68.5384,4.26586),
	(3551,'Ruins of Sha\'naar',2,0,530,-684.335,4777.22,49.1881,0.43455,0,0,0.21557,0.976488,-666.596,4789.92,48.9953,3.76701),
	(3546,'Expedition Armory',2,0,530,-1296.41,2699.25,-8.45748,6.02589,0,0,0.128296,-0.991736,-1320.03,2698.89,-11.9478,0.002701),
	(3798,'Haal\'eshi Gorge',2,0,530,-1322.95,4052.91,114.461,0.451005,0,0,0.223596,0.974682,-1309.48,4061.89,107.889,3.67276),
	(3553,'Pools of Aggonar',2,0,530,377.788,3284.02,74.3911,6.27963,0,0,0.00177804,-0.999998,382.953,3311.7,74.5223,5.01356);

/*!40000 ALTER TABLE `world_pvp_areas` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
