# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.32-0ubuntu0.12.04.1)
# Database: clanwars_wdb
# Generation Time: 2013-09-23 04:51:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table currency_exchange_rates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `currency_exchange_rates`;

CREATE TABLE `currency_exchange_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oldCurrency` int(10) unsigned NOT NULL DEFAULT '0',
  `oldCurrencyDesc` tinytext NOT NULL,
  `oldCurrencyAmount` int(6) unsigned NOT NULL DEFAULT '0',
  `newCurrency` int(10) unsigned NOT NULL DEFAULT '0',
  `newCurrencyDesc` tinytext NOT NULL,
  `newCurrencyAmount` int(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `currency_exchange_rates` WRITE;
/*!40000 ALTER TABLE `currency_exchange_rates` DISABLE KEYS */;

INSERT INTO `currency_exchange_rates` (`id`, `oldCurrency`, `oldCurrencyDesc`, `oldCurrencyAmount`, `newCurrency`, `newCurrencyDesc`, `newCurrencyAmount`)
VALUES
	(1,29434,'Emblem of Justice',10,40752,'Emblem of Heroism',1),
	(2,29434,'Emblem of Justice',50,40753,'Emblem of Valor',1),
	(3,29434,'Emblem of Justice',250,45624,'Emblem of Conquest',1),
	(4,29434,'Emblem of Justice',1250,47241,'Emblem of Triumph',1),
	(5,29434,'Emblem of Justice',6250,49426,'Emblem of Frost',1),
	(6,40752,'Emblem of Heroism',1,29434,'Emblem of Justice',10),
	(7,40752,'Emblem of Heroism',5,40753,'Emblem of Valor',1),
	(8,40752,'Emblem of Heroism',25,45624,'Emblem of Conquest',1),
	(9,40752,'Emblem of Heroism',125,47241,'Emblem of Triumph',1),
	(10,40752,'Emblem of Heroism',625,49426,'Emblem of Frost',1),
	(11,40753,'Emblem of Valor',1,29434,'Emblem of Justice',50),
	(12,40753,'Emblem of Valor',1,40752,'Emblem of Heroism',5),
	(13,40753,'Emblem of Valor',5,45624,'Emblem of Conquest',1),
	(14,40753,'Emblem of Valor',25,47241,'Emblem of Triumph',1),
	(15,40753,'Emblem of Valor',125,49426,'Emblem of Frost',1),
	(16,45624,'Emblem of Conquest',1,29434,'Emblem of Justice',250),
	(17,45624,'Emblem of Conquest',1,40752,'Emblem of Heroism',25),
	(18,45624,'Emblem of Conquest',1,40753,'Emblem of Valor',5),
	(19,45624,'Emblem of Conquest',5,47241,'Emblem of Triumph',1),
	(20,45624,'Emblem of Conquest',25,49426,'Emblem of Frost',1),
	(21,47241,'Emblem of Triumph',1,29434,'Emblem of Justice',1250),
	(22,47241,'Emblem of Triumph',1,40752,'Emblem of Heroism',125),
	(23,47241,'Emblem of Triumph',1,40753,'Emblem of Valor',25),
	(24,47241,'Emblem of Triumph',1,45624,'Emblem of Conquest',5),
	(25,47241,'Emblem of Triumph',5,49426,'Emblem of Frost',1),
	(26,49426,'Emblem of Frost',1,29434,'Emblem of Justice',6250),
	(27,49426,'Emblem of Frost',1,40752,'Emblem of Heroism',625),
	(28,49426,'Emblem of Frost',1,40753,'Emblem of Valor',125),
	(29,49426,'Emblem of Frost',1,45624,'Emblem of Conquest',25),
	(30,49426,'Emblem of Frost',1,47241,'Emblem of Triumph',5);

/*!40000 ALTER TABLE `currency_exchange_rates` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
