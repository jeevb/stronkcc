DELETE FROM `world_pvp_prof_spell_difficulty` WHERE `spellId` IN (60354, 60365, 60355, 60357, 60366, 60356, 56519, 54220, 62410, 54221, 54222, 53904);
INSERT INTO `world_pvp_prof_spell_difficulty` (`spellId`, `tradeskillId`, `spellDifficulty`)
VALUES
	(60354, 1, 4),
	(60365, 1, 4),
	(60355, 1, 4),
	(60357, 1, 4),
	(60366, 1, 4),
	(60356, 1, 4),
	(56519, 1, 4),
	(54220, 1, 4),
	(62410, 1, 4),
	(54221, 1, 4),
	(54222, 1, 4),
	(53904, 1, 4);
DELETE FROM `world_pvp_prof_spell_difficulty` WHERE `spellId` = 67025;
INSERT INTO `world_pvp_prof_spell_difficulty` (`spellId`, `tradeskillId`, `spellDifficulty`)
VALUES
	(67025, 1, 2);
DELETE FROM `world_pvp_prof_spell_difficulty` WHERE `spellId` = 60893;
INSERT INTO `world_pvp_prof_spell_difficulty` (`spellId`, `tradeskillId`, `spellDifficulty`)
VALUES
	(60893, 1, 1);