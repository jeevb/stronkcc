DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16871, 16873, 16907, 19422, 19424));
DELETE FROM `creature` WHERE `id` IN (16871, 16873, 16907, 19422, 19424);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (19423, 18677, 19442, 19440, 16964));
DELETE FROM `creature` WHERE `id` IN (19423, 18677, 19442, 19440, 16964);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (21134, 19458, 19459, 19457));
DELETE FROM `creature` WHERE `id` IN (21134, 19458, 19459, 19457);

DELETE FROM `pool_creature` WHERE `pool_entry` = 1082 AND `guid` IN (151921, 151922);