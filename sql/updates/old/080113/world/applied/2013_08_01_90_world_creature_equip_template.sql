DELETE FROM `creature_equip_template` WHERE `entry` IN (700051, 700052, 700053, 700054, 700055);
INSERT INTO `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`)
VALUES
	(700051, 1, 1899, 1984, 0),
	(700052, 1, 1905, 0, 50817),
	(700053, 1, 5281, 5281, 0),
	(700054, 1, 13069, 0, 0),
	(700055, 1, 2176, 0, 0);
