DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16904, 16905, 16906, 16976, 16977, 16978));
DELETE FROM `creature` WHERE `id` IN (16904, 16905, 16906, 16976, 16977, 16978);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (17060));
DELETE FROM `creature` WHERE `id` IN (17060);