DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (17058, 19354));
DELETE FROM `creature` WHERE `id` IN (17058, 19354);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16938, 20678, 20679, 20677, 19361, 16937));
DELETE FROM `creature` WHERE `id` IN (16938, 20678, 20679, 20677, 19361, 16937);