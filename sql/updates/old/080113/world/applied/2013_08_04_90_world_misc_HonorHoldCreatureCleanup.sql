DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16842, 16843, 16896, 20513, 16864, 17479, 17557, 16831));
DELETE FROM `creature` WHERE `id` IN (16842, 16843, 16896, 20513, 16864, 17479, 17557, 16831);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (-58449, -58450, -58451);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` = 16819);
DELETE FROM `creature` WHERE `id` = 16819;

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` = 16839);
DELETE FROM `creature` WHERE `id` = 16839;

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` = 16821);
DELETE FROM `creature` WHERE `id` = 16821;

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` = 16886);
DELETE FROM `creature` WHERE `id` = 16886;

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` = 19308);
DELETE FROM `creature` WHERE `id` = 19308;

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` = 19392);
DELETE FROM `creature` WHERE `id` = 19392;

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (19363, 16824, 16822, 35100, 35101));
DELETE FROM `creature` WHERE `id` IN (19363, 16824, 16822, 35100, 35101);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (22227, 18774, 18777, 16825, 16826, 18987, 16828, 18772, 22430, 22431, 22432));
DELETE FROM `creature` WHERE `id` IN (22227, 18774, 18777, 16825, 16826, 18987, 16828, 18772, 22430, 22431, 22432);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16856, 18775, 18779, 16823, 18266, 20206, 17657, 16830, 16835, 16832));
DELETE FROM `creature` WHERE `id` IN (16856, 18775, 18779, 16823, 18266, 20206, 17657, 16830, 16835, 16832);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (21209, 16827, 16820, 19309));
DELETE FROM `creature` WHERE `id` IN (21209, 16827, 16820, 19309);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16829, 18802, 18776, 18773, 30734, 30721));
DELETE FROM `creature` WHERE `id` IN (16829, 18802, 18776, 18773, 30734, 30721);