delete from gameobject where id in (select id from gameobject_involvedrelation) and id in (select id from gameobject_questrelation) and id not in (select id from game_event_gameobject_quest) and guid not in (select guid from game_event_gameobject);
delete from pool_gameobject where guid in (select guid from gameobject where id in (select entry from gameobject_template where type = 3 and data5 != 0 and flags != 4 and name not like "%Chest%" and name not like "%Coffer%"));
delete from gameobject where id in (select entry from gameobject_template where type = 3 and data5 != 0 and flags != 4 and name not like "%Chest%" and name not like "%Coffer%");
INSERT INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`)
VALUES
	(26813, 1618, 0, 1, 1, -9549.51, 116.518, 59.0369, 2.07694, 0, 0, 0.861629, 0.507538, 60, 100, 1);