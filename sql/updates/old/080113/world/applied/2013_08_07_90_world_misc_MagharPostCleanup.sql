DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16846, 16847, 16911, 16912));
DELETE FROM `creature` WHERE `id` IN (16846, 16847, 16911, 16912);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (19461, 16845, 17123, 16848));
DELETE FROM `creature` WHERE `id` IN (19461, 16845, 17123, 16848);