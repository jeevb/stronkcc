DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (19192, 16951, 16901));
DELETE FROM `creature` WHERE `id` IN (19192, 16951, 16901);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16903, 19191, 18679));
DELETE FROM `creature` WHERE `id` IN (16903, 19191, 18679);

DELETE FROM `pool_creature` WHERE `pool_entry` = 1085 AND `guid` IN (151929, 151930, 151931, 151932);


