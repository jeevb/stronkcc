DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (19032, 17794, 17795, 19029));
DELETE FROM `creature` WHERE `id` IN (19032, 17794, 17795, 19029);

DELETE FROM `gameobject` WHERE `id` IN (182525, 183514, 183515);