DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (16966, 16967, 17084));
DELETE FROM `creature` WHERE `id` IN (16966, 16967, 17084);

DELETE FROM `creature_addon` WHERE `guid` IN (SELECT `guid` FROM `creature` WHERE `id` IN (17053, 17034, 17035, 17039));
DELETE FROM `creature` WHERE `id` IN (17053, 17034, 17035, 17039);