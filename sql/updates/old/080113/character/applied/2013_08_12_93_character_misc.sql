# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: home (MySQL 5.5.32-0ubuntu0.12.04.1)
# Database: clanwars_cdb
# Generation Time: 2013-08-13 03:58:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ingame_purchases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ingame_purchases`;

CREATE TABLE `ingame_purchases` (
  `purchaseTime` int(11) unsigned NOT NULL DEFAULT '0',
  `itemGUID` int(11) unsigned NOT NULL DEFAULT '0',
  `itemEntry` int(11) unsigned NOT NULL DEFAULT '0',
  `playerGUID` int(11) unsigned NOT NULL DEFAULT '0',
  `votePointCost` int(4) unsigned NOT NULL DEFAULT '0',
  `donationPointCost` int(4) unsigned NOT NULL DEFAULT '0',
  `refundExpiration` int(11) unsigned NOT NULL DEFAULT '0',
  `isRefunded` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `isDeleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`purchaseTime`,`itemGUID`,`itemEntry`,`playerGUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table item_rental
# ------------------------------------------------------------

DROP TABLE IF EXISTS `item_rental`;

CREATE TABLE `item_rental` (
  `itemGUID` int(11) unsigned NOT NULL DEFAULT '0',
  `itemEntry` int(11) unsigned NOT NULL DEFAULT '0',
  `playerGUID` int(11) unsigned NOT NULL DEFAULT '0',
  `expirationTime` int(11) unsigned NOT NULL DEFAULT '0',
  `endGracePeriod` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemGUID`,`itemEntry`,`playerGUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
