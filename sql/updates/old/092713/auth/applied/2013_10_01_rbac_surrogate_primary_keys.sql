ALTER TABLE `rbac_account_groups`
DROP PRIMARY KEY,
ADD rbac_account_groups_id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD UNIQUE KEY uq_rbac_account_groups__accountId__realmId__groupId (`accountId`, `realmId`, `groupId`);

ALTER TABLE `rbac_account_permissions`
DROP PRIMARY KEY,
ADD rbac_account_permissions_id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD UNIQUE KEY uq_rbac_account_permissions__accountId__realmId__permissionId (`accountId`, `realmId`, `permissionId`);

ALTER TABLE `rbac_account_roles`
DROP PRIMARY KEY,
ADD rbac_account_roles_id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD UNIQUE KEY uq_rbac_account_roles__accountId__realmId__roleId (`accountId`, `realmId`, `roleId`);

ALTER TABLE `rbac_group_roles`
DROP PRIMARY KEY,
ADD rbac_group_roles_id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD UNIQUE KEY uq_rbac_group_roles__groupId__roleId (`groupId`, `roleId`);

ALTER TABLE `rbac_role_permissions`
DROP PRIMARY KEY,
ADD rbac_role_permissions_id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD UNIQUE KEY uq_rbac_role_permissions__roleId__permissionId (`roleId`, `permissionId`);

ALTER TABLE `rbac_security_level_groups`
DROP PRIMARY KEY,
ADD rbac_security_level_groups_id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD UNIQUE KEY uq_rbac_security_level_groups__secId__groupId (`secId`, `groupId`);
