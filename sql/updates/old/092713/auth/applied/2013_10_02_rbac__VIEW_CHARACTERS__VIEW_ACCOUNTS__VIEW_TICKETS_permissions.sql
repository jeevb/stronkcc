/* Add permissions */
INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES (100, "View Accounts");

INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES (101, "View Tickets");

INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES (102, "View Characters");

/* Add roles */
INSERT INTO `rbac_roles` (`id`, `name`)
VALUES (100, "View Accounts");

INSERT INTO `rbac_roles` (`id`, `name`)
VALUES (101, "View Tickets");

INSERT INTO `rbac_roles` (`id`, `name`)
VALUES (102, "View Characters");

/* Link roles and permissions */
INSERT INTO `rbac_role_permissions` (`roleId`, `permissionId`)
VALUES (100, 100);

INSERT INTO `rbac_role_permissions` (`roleId`, `permissionId`)
VALUES (101, 101);

INSERT INTO `rbac_role_permissions` (`roleId`, `permissionId`)
VALUES (102, 102);

/* Add the roles to groups */
/* Add all roles to GameMasteres */
INSERT INTO `rbac_group_roles` (`groupId`, `roleId`)
VALUES (3, 100);

INSERT INTO `rbac_group_roles` (`groupId`, `roleId`)
VALUES (3, 101);

INSERT INTO `rbac_group_roles` (`groupId`, `roleId`)
VALUES (3, 102);

/* Add all roles to Administrator */
INSERT INTO `rbac_group_roles` (`groupId`, `roleId`)
VALUES (4, 100);

INSERT INTO `rbac_group_roles` (`groupId`, `roleId`)
VALUES (4, 101);

INSERT INTO `rbac_group_roles` (`groupId`, `roleId`)
VALUES (4, 102);
