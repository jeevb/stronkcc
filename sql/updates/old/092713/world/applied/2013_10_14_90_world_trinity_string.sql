DELETE FROM `trinity_string` WHERE `entry` = 11010;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`)
VALUES
	(11010, 'Please try to be as descriptive as possible in your ticket. This will faciliate escalation to the appropriate staff members to ensure that you receive the best response. If you are reporting a bug, please do so on our bug tracker at http://bug.stronk.cc.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
