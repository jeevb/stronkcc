DELETE FROM `gameobject` WHERE `guid` = 300503;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`)
VALUES
	(300503, 700112, 530, 1, 1, -683.998, 2693.05, 93.0186, 7.11944, 0, 0, 0.990783, -0.135459, 300, 0, 1);

DELETE FROM `world_pvp_npc_spawns` WHERE `areaId` = 3538 AND `entry` = 620000;
INSERT INTO `world_pvp_npc_spawns` (`id`, `entry`, `areaId`, `mapId`, `position_x`, `position_y`, `position_z`, `orientation`, `movementType`, `pathId`)
VALUES
	(5, 620000, 3538, 530, -683.905, 2693.32, 107.15, 3.6843, 0, 0);
