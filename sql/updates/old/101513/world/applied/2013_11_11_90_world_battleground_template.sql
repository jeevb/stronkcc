ALTER TABLE `battleground_template` ADD COLUMN `PlayersPerPremadeTeam` smallint(5) unsigned NOT NULL DEFAULT '0' AFTER `MaxPlayersPerTeam`;

/*Warsong Gulch*/
UPDATE `battleground_template` SET `PlayersPerPremadeTeam` = 5 WHERE `id` = 2;

/*Arathi Basin*/
UPDATE `battleground_template` SET `PlayersPerPremadeTeam` = 7 WHERE `id` = 3;

/*Eye of the Storm*/
UPDATE `battleground_template` SET `PlayersPerPremadeTeam` = 7 WHERE `id` = 7;

/*Strand of the Ancients*/
UPDATE `battleground_template` SET `PlayersPerPremadeTeam` = 7 WHERE `id` = 9;