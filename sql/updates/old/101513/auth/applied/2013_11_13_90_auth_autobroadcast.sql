DELETE FROM `autobroadcast` WHERE `id` IN (2, 3, 4, 5, 6, 7, 8);
INSERT INTO `autobroadcast` (`realmid`, `id`, `weight`, `text`)
VALUES
	(-1, 2, 1, 'Get your guildies together for some Rated Battlegrounds - 5v5 Warsong Gulch or 7v7 Arathi Basin, Eye of the Storm or Strand of the Ancients.'),
	(-1, 3, 1, 'Equipment rentals are now available at Ocealus Vasselle, the Stronk.CC Quartermaster. You may lease armor or weapon upgrades for a very low Vote Point cost!'),
	(-1, 4, 1, 'Decimate your enemies and earn PvP Rating and new PvP Ranks (e.g. Champion, General, Grand Marshal, etc).'),
	(-1, 5, 1, 'Rack up a hefty killstreak, or perhaps an arena or battleground winning streak? You might just get your name on Stronk.CC\'s Hall of Fame!'),
	(-1, 6, 1, 'Like us on our Facebook page @ http://www.facebook.com/stronkcc'),
	(-1, 7, 1, 'Confused about how this realm works? Have a look at our guide on the forums @ http://forum.stronk.cc/index.php?/topic/24-guide/'),
	(-1, 8, 1, 'Found a bug? Report it at http://bug.stronk.cc with your forums account.');