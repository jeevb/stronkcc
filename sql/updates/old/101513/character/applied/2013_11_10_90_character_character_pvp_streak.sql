# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.31-0+wheezy1)
# Database: clanwars_cdb
# Generation Time: 2013-11-10 23:20:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table character_pvp_streak
# ------------------------------------------------------------

DROP TABLE IF EXISTS `character_pvp_streak`;

CREATE TABLE `character_pvp_streak` (
  `guid` int(10) NOT NULL,
  `maxKillStreak` int(6) unsigned NOT NULL,
  `maxKillStreakTime` int(11) unsigned NOT NULL,
  `maxArena2Streak` int(6) unsigned NOT NULL,
  `maxArena2StreakTime` int(11) unsigned NOT NULL,
  `maxArena3Streak` int(6) unsigned NOT NULL,
  `maxArena3StreakTime` int(11) unsigned NOT NULL,
  `maxArena5Streak` int(6) unsigned NOT NULL,
  `maxArena5StreakTime` int(11) unsigned NOT NULL,
  `maxNormalBGStreak` int(6) unsigned NOT NULL,
  `maxNormalBGStreakTime` int(11) unsigned NOT NULL,
  `maxRatedBGStreak` int(6) unsigned NOT NULL,
  `maxRatedBGStreakTime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
