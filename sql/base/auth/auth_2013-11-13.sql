# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: db.stk (MySQL 5.5.33a-MariaDB-1~wheezy-log)
# Database: auth
# Generation Time: 2013-11-14 01:35:42 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifier',
  `username` varchar(32) NOT NULL DEFAULT '',
  `sha_pass_hash` varchar(40) NOT NULL DEFAULT '',
  `sessionkey` varchar(80) NOT NULL DEFAULT '',
  `v` varchar(64) NOT NULL DEFAULT '',
  `s` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(254) NOT NULL DEFAULT '',
  `joindate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `failed_logins` int(10) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `lock_country` varchar(2) NOT NULL DEFAULT '00',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `online` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `expansion` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `mutetime` bigint(20) NOT NULL DEFAULT '0',
  `mutereason` varchar(255) NOT NULL DEFAULT '',
  `muteby` varchar(50) NOT NULL DEFAULT '',
  `locale` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `os` varchar(3) NOT NULL DEFAULT '',
  `recruiter` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account System';



# Dump of table account_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_access`;

CREATE TABLE `account_access` (
  `id` int(10) unsigned NOT NULL,
  `gmlevel` tinyint(3) unsigned NOT NULL,
  `RealmID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`,`RealmID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table account_banned
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_banned`;

CREATE TABLE `account_banned` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account id',
  `bandate` int(10) unsigned NOT NULL DEFAULT '0',
  `unbandate` int(10) unsigned NOT NULL DEFAULT '0',
  `bannedby` varchar(50) NOT NULL,
  `banreason` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ban List';



# Dump of table account_premium
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_premium`;

CREATE TABLE `account_premium` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT 'Account id',
  `setdate` bigint(40) NOT NULL DEFAULT '0',
  `unsetdate` bigint(40) NOT NULL DEFAULT '0',
  `premium_type` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`setdate`),
  KEY `setdate` (`setdate`,`unsetdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Premium Accounts';



# Dump of table autobroadcast
# ------------------------------------------------------------

DROP TABLE IF EXISTS `autobroadcast`;

CREATE TABLE `autobroadcast` (
  `realmid` int(11) NOT NULL DEFAULT '-1',
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `weight` tinyint(3) unsigned DEFAULT '1',
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`,`realmid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table gm_command_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gm_command_log`;

CREATE TABLE `gm_command_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `realm_id` int(11) unsigned NOT NULL,
  `command` text NOT NULL,
  `command_args` text NOT NULL,
  `gm_account_id` int(11) unsigned NOT NULL,
  `gm_char_guid` int(11) unsigned NOT NULL,
  `gm_pos_x` float NOT NULL,
  `gm_pos_y` float NOT NULL,
  `gm_pos_z` float NOT NULL,
  `gm_map_id` float unsigned NOT NULL,
  `target_type` varchar(12) DEFAULT '',
  `target_x` float DEFAULT NULL,
  `target_y` float DEFAULT NULL,
  `target_z` float DEFAULT NULL,
  `target_name` text,
  `target_guid` bigint(11) unsigned DEFAULT NULL,
  `target_guid_lopart` int(11) DEFAULT NULL,
  `target_account_id` int(11) DEFAULT NULL,
  `flagged_by` int(11) unsigned DEFAULT NULL COMMENT 'account id of the GM who flagged this command as suspicious / abusive',
  `flagged_by_timestamp` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `realm_id` (`realm_id`),
  KEY `gm_account_id` (`gm_account_id`),
  KEY `command_index` (`command`(5)),
  KEY `realm_id_2` (`realm_id`,`gm_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ip_banned
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ip_banned`;

CREATE TABLE `ip_banned` (
  `ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `bandate` int(10) unsigned NOT NULL,
  `unbandate` int(10) unsigned NOT NULL,
  `bannedby` varchar(50) NOT NULL DEFAULT '[Console]',
  `banreason` varchar(255) NOT NULL DEFAULT 'no reason',
  PRIMARY KEY (`ip`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Banned IPs';



# Dump of table ip2nation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ip2nation`;

CREATE TABLE `ip2nation` (
  `ip` int(11) unsigned NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '',
  KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ip2nationCountries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ip2nationCountries`;

CREATE TABLE `ip2nationCountries` (
  `code` varchar(4) NOT NULL DEFAULT '',
  `iso_code_2` varchar(2) NOT NULL DEFAULT '',
  `iso_code_3` varchar(3) DEFAULT '',
  `iso_country` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `lat` float NOT NULL DEFAULT '0',
  `lon` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `time` int(10) unsigned NOT NULL,
  `realm` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `string` text CHARACTER SET latin1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rbac_account_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_account_groups`;

CREATE TABLE `rbac_account_groups` (
  `rbac_account_groups_id` int(10) NOT NULL AUTO_INCREMENT,
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `groupId` int(10) unsigned NOT NULL COMMENT 'Group id',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`rbac_account_groups_id`),
  UNIQUE KEY `uq_rbac_account_groups__accountId__realmId__groupId` (`accountId`,`realmId`,`groupId`),
  KEY `fk__rbac_account_groups__rbac_groups` (`groupId`),
  CONSTRAINT `fk__rbac_account_groups__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_groups__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Group relation';



# Dump of table rbac_account_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_account_permissions`;

CREATE TABLE `rbac_account_permissions` (
  `rbac_account_permissions_id` int(10) NOT NULL AUTO_INCREMENT,
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `permissionId` int(10) unsigned NOT NULL COMMENT 'Permission id',
  `granted` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Granted = 1, Denied = 0',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`rbac_account_permissions_id`),
  UNIQUE KEY `uq_rbac_account_permissions__accountId__realmId__permissionId` (`accountId`,`realmId`,`permissionId`),
  KEY `fk__rbac_account_roles__rbac_permissions` (`permissionId`),
  CONSTRAINT `fk__rbac_account_permissions__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_roles__rbac_permissions` FOREIGN KEY (`permissionId`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Permission relation';



# Dump of table rbac_account_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_account_roles`;

CREATE TABLE `rbac_account_roles` (
  `rbac_account_roles_id` int(10) NOT NULL AUTO_INCREMENT,
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  `granted` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Granted = 1, Denied = 0',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`rbac_account_roles_id`),
  UNIQUE KEY `uq_rbac_account_roles__accountId__realmId__roleId` (`accountId`,`realmId`,`roleId`),
  KEY `fk__rbac_account_roles__rbac_roles` (`roleId`),
  CONSTRAINT `fk__rbac_account_roles__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_roles__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Role relation';



# Dump of table rbac_group_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_group_roles`;

CREATE TABLE `rbac_group_roles` (
  `rbac_group_roles_id` int(10) NOT NULL AUTO_INCREMENT,
  `groupId` int(10) unsigned NOT NULL COMMENT 'group id',
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  PRIMARY KEY (`rbac_group_roles_id`),
  UNIQUE KEY `uq_rbac_group_roles__groupId__roleId` (`groupId`,`roleId`),
  KEY `fk__rbac_group_roles__rbac_roles` (`roleId`),
  CONSTRAINT `fk__rbac_group_roles__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_group_roles__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Group Role relation';

LOCK TABLES `rbac_group_roles` WRITE;
/*!40000 ALTER TABLE `rbac_group_roles` DISABLE KEYS */;

INSERT INTO `rbac_group_roles` (`rbac_group_roles_id`, `groupId`, `roleId`)
VALUES
	(1,1,1),
	(2,1,6),
	(3,1,7),
	(4,1,19),
	(5,1,28),
	(6,1,30),
	(7,1,34),
	(8,1,35),
	(9,1,36),
	(10,2,2),
	(11,2,5),
	(12,2,8),
	(13,2,9),
	(14,2,10),
	(15,2,11),
	(16,2,12),
	(17,2,13),
	(18,2,14),
	(19,2,15),
	(20,2,16),
	(21,2,17),
	(22,2,19),
	(23,2,20),
	(24,2,21),
	(25,2,22),
	(26,2,24),
	(27,2,25),
	(28,2,26),
	(29,2,27),
	(30,2,28),
	(31,2,29),
	(32,2,30),
	(33,2,31),
	(34,2,32),
	(35,2,33),
	(36,2,35),
	(37,2,36),
	(38,2,37),
	(39,2,38),
	(40,3,3),
	(41,3,8),
	(42,3,9),
	(43,3,10),
	(44,3,11),
	(45,3,12),
	(46,3,13),
	(47,3,14),
	(48,3,15),
	(49,3,16),
	(50,3,17),
	(51,3,19),
	(52,3,20),
	(53,3,21),
	(54,3,22),
	(55,3,24),
	(56,3,25),
	(57,3,26),
	(58,3,27),
	(59,3,28),
	(60,3,29),
	(61,3,30),
	(62,3,32),
	(63,3,33),
	(64,3,35),
	(65,3,36),
	(66,3,37),
	(67,3,38),
	(99,3,100),
	(100,3,101),
	(101,3,102),
	(68,4,4),
	(69,4,8),
	(70,4,9),
	(71,4,10),
	(72,4,11),
	(73,4,12),
	(74,4,13),
	(75,4,14),
	(76,4,15),
	(77,4,16),
	(78,4,17),
	(79,4,18),
	(80,4,19),
	(81,4,20),
	(82,4,21),
	(83,4,22),
	(84,4,23),
	(85,4,24),
	(86,4,25),
	(87,4,26),
	(88,4,27),
	(89,4,28),
	(90,4,29),
	(91,4,30),
	(92,4,32),
	(93,4,33),
	(94,4,35),
	(95,4,36),
	(96,4,37),
	(97,4,38),
	(98,4,39),
	(102,4,100),
	(103,4,101),
	(104,4,102);

/*!40000 ALTER TABLE `rbac_group_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_groups`;

CREATE TABLE `rbac_groups` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Group id',
  `name` varchar(100) NOT NULL COMMENT 'Group name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Group List';

LOCK TABLES `rbac_groups` WRITE;
/*!40000 ALTER TABLE `rbac_groups` DISABLE KEYS */;

INSERT INTO `rbac_groups` (`id`, `name`)
VALUES
	(1,'Player'),
	(2,'Moderator'),
	(3,'GameMaster'),
	(4,'Administrator');

/*!40000 ALTER TABLE `rbac_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_permissions`;

CREATE TABLE `rbac_permissions` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Permission id',
  `name` varchar(100) NOT NULL COMMENT 'Permission name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Permission List';

LOCK TABLES `rbac_permissions` WRITE;
/*!40000 ALTER TABLE `rbac_permissions` DISABLE KEYS */;

INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES
	(1,'Instant logout'),
	(2,'Skip Queue'),
	(3,'Join Normal Battleground'),
	(4,'Join Random Battleground'),
	(5,'Join Arenas'),
	(6,'Join Dungeon Finder'),
	(7,'Player Commands (Temporal till commands moved to rbac)'),
	(8,'Moderator Commands (Temporal till commands moved to rbac)'),
	(9,'GameMaster Commands (Temporal till commands moved to rbac)'),
	(10,'Administrator Commands (Temporal till commands moved to rbac)'),
	(11,'Log GM trades'),
	(13,'Skip Instance required bosses check'),
	(14,'Skip character creation team mask check'),
	(15,'Skip character creation class mask check'),
	(16,'Skip character creation race mask check'),
	(17,'Skip character creation reserved name check'),
	(18,'Skip character creation heroic min level check'),
	(19,'Skip needed requirements to use channel check'),
	(20,'Skip disable map check'),
	(21,'Skip reset talents when used more than allowed check'),
	(22,'Skip spam chat check'),
	(23,'Skip over-speed ping check'),
	(24,'Two side faction characters on the same account'),
	(25,'Allow say chat between factions'),
	(26,'Allow channel chat between factions'),
	(27,'Two side mail interaction'),
	(28,'See two side who list'),
	(29,'Add friends of other faction'),
	(30,'Save character without delay with .save command'),
	(31,'Use params with .unstuck command'),
	(32,'Can be assigned tickets with .assign ticket command'),
	(33,'Notify if a command was not found'),
	(34,'Check if should appear in list using .gm ingame command'),
	(35,'See all security levels with who command'),
	(36,'Filter whispers'),
	(37,'Use staff badge in chat'),
	(38,'Resurrect with full Health Points'),
	(39,'Restore saved gm setting states'),
	(40,'Allows to add a gm to friend list'),
	(41,'Use Config option START_GM_LEVEL to assign new character level'),
	(42,'Allows to use CMSG_WORLD_TELEPORT opcode'),
	(43,'Allows to use CMSG_WHOIS opcode'),
	(44,'Receive global GM messages/texts'),
	(45,'Join channels without announce'),
	(46,'Change channel settings without being channel moderator'),
	(47,'Enables lower security than target check'),
	(48,'Enable IP, Last Login and EMail output in pinfo'),
	(100,'View Accounts'),
	(101,'View Tickets'),
	(102,'View Characters');

/*!40000 ALTER TABLE `rbac_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_role_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_role_permissions`;

CREATE TABLE `rbac_role_permissions` (
  `rbac_role_permissions_id` int(10) NOT NULL AUTO_INCREMENT,
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  `permissionId` int(10) unsigned NOT NULL COMMENT 'Permission id',
  PRIMARY KEY (`rbac_role_permissions_id`),
  UNIQUE KEY `uq_rbac_role_permissions__roleId__permissionId` (`roleId`,`permissionId`),
  KEY `fk__role_permissions__rbac_permissions` (`permissionId`),
  CONSTRAINT `fk__role_permissions__rbac_permissions` FOREIGN KEY (`permissionId`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__role_permissions__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Role Permission relation';

LOCK TABLES `rbac_role_permissions` WRITE;
/*!40000 ALTER TABLE `rbac_role_permissions` DISABLE KEYS */;

INSERT INTO `rbac_role_permissions` (`rbac_role_permissions_id`, `roleId`, `permissionId`)
VALUES
	(1,1,7),
	(2,2,8),
	(3,3,9),
	(4,4,10),
	(5,5,1),
	(6,5,2),
	(7,6,3),
	(8,6,4),
	(9,6,5),
	(10,7,6),
	(11,8,11),
	(12,9,13),
	(13,10,32),
	(14,11,30),
	(15,12,31),
	(16,13,38),
	(17,14,34),
	(18,15,37),
	(19,16,44),
	(20,17,23),
	(21,18,42),
	(22,18,43),
	(23,19,27),
	(24,20,33),
	(25,21,47),
	(26,22,20),
	(27,23,21),
	(28,24,22),
	(29,25,39),
	(30,26,41),
	(31,27,19),
	(32,28,25),
	(33,29,36),
	(34,30,26),
	(35,31,45),
	(36,32,46),
	(37,33,14),
	(38,33,15),
	(39,33,16),
	(40,33,17),
	(41,33,18),
	(42,34,24),
	(43,35,28),
	(44,36,29),
	(45,37,35),
	(46,38,40),
	(47,39,48),
	(48,100,100),
	(49,101,101),
	(50,102,102);

/*!40000 ALTER TABLE `rbac_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_roles`;

CREATE TABLE `rbac_roles` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Role id',
  `name` varchar(100) NOT NULL COMMENT 'Role name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Roles List';

LOCK TABLES `rbac_roles` WRITE;
/*!40000 ALTER TABLE `rbac_roles` DISABLE KEYS */;

INSERT INTO `rbac_roles` (`id`, `name`)
VALUES
	(1,'Player Commands'),
	(2,'Moderator Commands'),
	(3,'GameMaster Commands'),
	(4,'Administrator Commands'),
	(5,'Quick Login/Logout'),
	(6,'Use Battleground/Arenas'),
	(7,'Use Dungeon Finder'),
	(8,'Log GM trades'),
	(9,'Skip Instance required bosses check'),
	(10,'Ticket management'),
	(11,'Instant .save'),
	(12,'Allow params with .unstuck'),
	(13,'Full HP after resurrect'),
	(14,'Appear in GM ingame list'),
	(15,'Use staff badge in chat'),
	(16,'Receive global GM messages/texts'),
	(17,'Skip over-speed ping check'),
	(18,'Allows Admin Opcodes'),
	(19,'Two side mail interaction'),
	(20,'Notify if a command was not found'),
	(21,'Enables lower security than target check'),
	(22,'Skip disable map check'),
	(23,'Skip reset talents when used more than allowed check'),
	(24,'Skip spam chat check'),
	(25,'Restore saved gm setting states'),
	(26,'Use Config option START_GM_LEVEL to assign new character level'),
	(27,'Skip needed requirements to use channel check'),
	(28,'Allow say chat between factions'),
	(29,'Filter whispers'),
	(30,'Allow channel chat between factions'),
	(31,'Join channels without announce'),
	(32,'Change channel settings without being channel moderator'),
	(33,'Skip character creation checks'),
	(34,'Two side faction characters on the same account'),
	(35,'See two side who list'),
	(36,'Add friends of other faction'),
	(37,'See all security levels with who command'),
	(38,'Allows to add a gm to friend list'),
	(39,'Enable IP, Last Login and EMail output in pinfo'),
	(100,'View Accounts'),
	(101,'View Tickets'),
	(102,'View Characters');

/*!40000 ALTER TABLE `rbac_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_security_level_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_security_level_groups`;

CREATE TABLE `rbac_security_level_groups` (
  `rbac_security_level_groups_id` int(10) NOT NULL AUTO_INCREMENT,
  `secId` tinyint(3) unsigned NOT NULL COMMENT 'Security Level id',
  `groupId` int(10) unsigned NOT NULL COMMENT 'group id',
  PRIMARY KEY (`rbac_security_level_groups_id`),
  UNIQUE KEY `uq_rbac_security_level_groups__secId__groupId` (`secId`,`groupId`),
  KEY `fk__rbac_security_level_groups__rbac_groups` (`groupId`),
  CONSTRAINT `fk__rbac_security_level_groups__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Default groups to assign when an account is set gm level';

LOCK TABLES `rbac_security_level_groups` WRITE;
/*!40000 ALTER TABLE `rbac_security_level_groups` DISABLE KEYS */;

INSERT INTO `rbac_security_level_groups` (`rbac_security_level_groups_id`, `secId`, `groupId`)
VALUES
	(1,0,1),
	(2,1,1),
	(3,1,2),
	(4,2,1),
	(5,2,2),
	(6,2,3),
	(7,3,1),
	(8,3,2),
	(9,3,3),
	(10,3,4);

/*!40000 ALTER TABLE `rbac_security_level_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table realmcharacters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `realmcharacters`;

CREATE TABLE `realmcharacters` (
  `realmid` int(10) unsigned NOT NULL DEFAULT '0',
  `acctid` int(10) unsigned NOT NULL,
  `numchars` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`realmid`,`acctid`),
  KEY `acctid` (`acctid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Realm Character Tracker';



# Dump of table realmlist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `realmlist`;

CREATE TABLE `realmlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '127.0.0.1',
  `localAddress` varchar(255) NOT NULL DEFAULT '127.0.0.1',
  `localSubnetMask` varchar(255) NOT NULL DEFAULT '255.255.255.0',
  `port` smallint(5) unsigned NOT NULL DEFAULT '8085',
  `icon` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `flag` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `timezone` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `allowedSecurityLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `population` float unsigned NOT NULL DEFAULT '0',
  `gamebuild` int(10) unsigned NOT NULL DEFAULT '12340',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Realm System';



# Dump of table uptime
# ------------------------------------------------------------

DROP TABLE IF EXISTS `uptime`;

CREATE TABLE `uptime` (
  `realmid` int(10) unsigned NOT NULL,
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0',
  `maxplayers` smallint(5) unsigned NOT NULL DEFAULT '0',
  `revision` varchar(255) NOT NULL DEFAULT 'Trinitycore',
  PRIMARY KEY (`realmid`,`starttime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Uptime system';




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
