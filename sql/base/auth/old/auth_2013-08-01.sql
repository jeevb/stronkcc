# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: stronk.cc (MySQL 5.1.66-0+squeeze1)
# Database: auth
# Generation Time: 2013-08-02 05:14:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifier',
  `username` varchar(32) NOT NULL DEFAULT '',
  `sha_pass_hash` varchar(40) NOT NULL DEFAULT '',
  `sessionkey` varchar(80) NOT NULL DEFAULT '',
  `v` varchar(64) NOT NULL DEFAULT '',
  `s` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(254) NOT NULL DEFAULT '',
  `joindate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `failed_logins` int(10) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `lock_country` varchar(2) NOT NULL DEFAULT '00',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `online` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `expansion` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `mutetime` bigint(20) NOT NULL DEFAULT '0',
  `mutereason` varchar(255) NOT NULL DEFAULT '',
  `muteby` varchar(50) NOT NULL DEFAULT '',
  `locale` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `os` varchar(3) NOT NULL DEFAULT '',
  `recruiter` int(10) unsigned NOT NULL DEFAULT '0',
  `special` tinyint(4) NOT NULL DEFAULT '0',
  `active_realm_id` tinyint(3) unsigned NOT NULL,
  `vote_last` int(255) unsigned NOT NULL DEFAULT '0',
  `vote_message_sent` int(255) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account System';

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;

INSERT INTO `account` (`id`, `username`, `sha_pass_hash`, `sessionkey`, `v`, `s`, `email`, `joindate`, `last_ip`, `failed_logins`, `locked`, `lock_country`, `last_login`, `online`, `expansion`, `mutetime`, `mutereason`, `muteby`, `locale`, `os`, `recruiter`, `special`, `active_realm_id`, `vote_last`, `vote_message_sent`)
VALUES
	(1,'SJVINTAGE','4CD9EAC27D8F039C5C4DE3075CEEFBB5585199ED','3606BA9D9A442748B52C99F4D4780B596B798B5AC912A22B29EAD5F60CCA39C13316CBF45B955795','304315C933A8FB5A131DCECBF8EE7A7E3925EEDDD27CA2578D190B3EEDAC44B5','9FCF9E05B44C19FB440129E1F74069EC9C7B186D98FBB5149500CC76B39D1B8B','','2013-08-01 15:10:18','76.21.126.216',0,0,'00','2013-08-01 15:12:16',0,2,0,'','',0,'OSX',0,0,0,0,1375388252),
	(2,'RYAN','0CA51E3E8E853F2CDE88DDF3B5C352B404A6D288','','','','','2013-08-01 22:16:19','127.0.0.1',0,0,'00','0000-00-00 00:00:00',0,2,0,'','',0,'',0,0,0,0,0);

/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_access`;

CREATE TABLE `account_access` (
  `id` int(10) unsigned NOT NULL,
  `gmlevel` tinyint(3) unsigned NOT NULL,
  `RealmID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`,`RealmID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_access` WRITE;
/*!40000 ALTER TABLE `account_access` DISABLE KEYS */;

INSERT INTO `account_access` (`id`, `gmlevel`, `RealmID`)
VALUES
	(1,6,-1),
	(2,6,-1);

/*!40000 ALTER TABLE `account_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_banned
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_banned`;

CREATE TABLE `account_banned` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account id',
  `bandate` int(10) unsigned NOT NULL DEFAULT '0',
  `unbandate` int(10) unsigned NOT NULL DEFAULT '0',
  `bannedby` varchar(50) NOT NULL,
  `banreason` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ban List';



# Dump of table account_premium
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_premium`;

CREATE TABLE `account_premium` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT 'Account id',
  `setdate` bigint(40) NOT NULL DEFAULT '0',
  `unsetdate` bigint(40) NOT NULL DEFAULT '0',
  `premium_type` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`setdate`),
  KEY `setdate` (`setdate`,`unsetdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Premium Accounts';



# Dump of table autobroadcast
# ------------------------------------------------------------

DROP TABLE IF EXISTS `autobroadcast`;

CREATE TABLE `autobroadcast` (
  `realmid` int(11) NOT NULL DEFAULT '-1',
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `weight` tinyint(3) unsigned DEFAULT '1',
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`,`realmid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table gm_command_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gm_command_log`;

CREATE TABLE `gm_command_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `realm_id` int(11) unsigned NOT NULL,
  `command` text NOT NULL,
  `command_args` text NOT NULL,
  `gm_account_id` int(11) unsigned NOT NULL,
  `gm_char_guid` int(11) unsigned NOT NULL,
  `gm_pos_x` float NOT NULL,
  `gm_pos_y` float NOT NULL,
  `gm_pos_z` float NOT NULL,
  `gm_map_id` float unsigned NOT NULL,
  `target_type` varchar(12) DEFAULT '',
  `target_x` float DEFAULT NULL,
  `target_y` float DEFAULT NULL,
  `target_z` float DEFAULT NULL,
  `target_name` text,
  `target_guid` bigint(11) unsigned DEFAULT NULL,
  `target_guid_lopart` int(11) DEFAULT NULL,
  `target_account_id` int(11) DEFAULT NULL,
  `flagged_by` int(11) unsigned DEFAULT NULL COMMENT 'account id of the GM who flagged this command as suspicious / abusive',
  `flagged_by_timestamp` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `realm_id` (`realm_id`),
  KEY `gm_account_id` (`gm_account_id`),
  KEY `command_index` (`command`(5)),
  KEY `realm_id_2` (`realm_id`,`gm_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `gm_command_log` WRITE;
/*!40000 ALTER TABLE `gm_command_log` DISABLE KEYS */;

INSERT INTO `gm_command_log` (`id`, `realm_id`, `command`, `command_args`, `gm_account_id`, `gm_char_guid`, `gm_pos_x`, `gm_pos_y`, `gm_pos_z`, `gm_map_id`, `target_type`, `target_x`, `target_y`, `target_z`, `target_name`, `target_guid`, `target_guid_lopart`, `target_account_id`, `flagged_by`, `flagged_by_timestamp`, `timestamp`)
VALUES
	(1,2,'gm ','',1,1,-249.917,1029.19,54.3243,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:13:26'),
	(2,2,'gm chat','',1,1,-249.917,1029.19,54.3243,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:13:26'),
	(3,2,'gm visible','',1,1,-249.917,1029.19,54.3243,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:13:26'),
	(4,2,'dev','',1,1,-249.917,1029.19,54.3243,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:13:26'),
	(5,2,'whispers ','',1,1,-249.917,1029.19,54.3243,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:13:26'),
	(6,2,'guild create','Algorithm \"Staff\"',1,1,-258.948,1039.78,54.3193,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:14:40'),
	(7,2,'gm ','',1,1,-263.478,1038.59,54.3206,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:59:14'),
	(8,2,'gm chat','',1,1,-263.478,1038.59,54.3206,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:59:14'),
	(9,2,'gm visible','',1,1,-263.478,1038.59,54.3206,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:59:14'),
	(10,2,'dev','',1,1,-263.478,1038.59,54.3206,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:59:14'),
	(11,2,'whispers ','',1,1,-263.478,1038.59,54.3206,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 15:59:14'),
	(12,2,'npc info','',1,1,-268.912,1061.49,54.3123,530,'creature',-271.453,1066.93,56.3101,'Teleporter',17379392641429808494,7742830,NULL,NULL,NULL,'2013-08-01 16:00:16'),
	(13,2,'gm ','',1,1,-270.498,1063.79,54.3105,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 16:26:58'),
	(14,2,'gm chat','',1,1,-270.498,1063.79,54.3105,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 16:26:58'),
	(15,2,'gm visible','',1,1,-270.498,1063.79,54.3105,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 16:26:58'),
	(16,2,'dev','',1,1,-270.498,1063.79,54.3105,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 16:26:58'),
	(17,2,'whispers ','',1,1,-270.498,1063.79,54.3105,530,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-01 16:26:58');

/*!40000 ALTER TABLE `gm_command_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ip_banned
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ip_banned`;

CREATE TABLE `ip_banned` (
  `ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `bandate` int(10) unsigned NOT NULL,
  `unbandate` int(10) unsigned NOT NULL,
  `bannedby` varchar(50) NOT NULL DEFAULT '[Console]',
  `banreason` varchar(255) NOT NULL DEFAULT 'no reason',
  PRIMARY KEY (`ip`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Banned IPs';



# Dump of table ip2nation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ip2nation`;

CREATE TABLE `ip2nation` (
  `ip` int(11) unsigned NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '',
  KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ip2nationCountries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ip2nationCountries`;

CREATE TABLE `ip2nationCountries` (
  `code` varchar(4) NOT NULL DEFAULT '',
  `iso_code_2` varchar(2) NOT NULL DEFAULT '',
  `iso_code_3` varchar(3) DEFAULT '',
  `iso_country` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `lat` float NOT NULL DEFAULT '0',
  `lon` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `time` int(10) unsigned NOT NULL,
  `realm` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `string` text CHARACTER SET latin1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;

INSERT INTO `logs` (`time`, `realm`, `type`, `level`, `string`)
VALUES
	(1375384406,2,27,3,'Command: .gm [Player: Algorithm (Guid: 1) (Account: 1) X: -249.917007 Y: 1029.189941 Z: 54.324299 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375384406,2,27,3,'Command: .gm chat [Player: Algorithm (Guid: 1) (Account: 1) X: -249.917007 Y: 1029.189941 Z: 54.324299 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375384406,2,27,3,'Command: .gm visible [Player: Algorithm (Guid: 1) (Account: 1) X: -249.917007 Y: 1029.189941 Z: 54.324299 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375384406,2,27,3,'Command: .dev [Player: Algorithm (Guid: 1) (Account: 1) X: -249.917007 Y: 1029.189941 Z: 54.324299 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375384406,2,27,3,'Command: .whispers [Player: Algorithm (Guid: 1) (Account: 1) X: -249.917007 Y: 1029.189941 Z: 54.324299 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375384480,2,27,3,'Command: .guild create Algorithm \"Staff\" [Player: Algorithm (Guid: 1) (Account: 1) X: -258.948059 Y: 1039.782593 Z: 54.319256 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375387154,2,27,3,'Command: .gm [Player: Algorithm (Guid: 1) (Account: 1) X: -263.477905 Y: 1038.587646 Z: 54.320618 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375387154,2,27,3,'Command: .gm chat [Player: Algorithm (Guid: 1) (Account: 1) X: -263.477905 Y: 1038.587646 Z: 54.320618 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375387154,2,27,3,'Command: .gm visible [Player: Algorithm (Guid: 1) (Account: 1) X: -263.477905 Y: 1038.587646 Z: 54.320618 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375387154,2,27,3,'Command: .dev [Player: Algorithm (Guid: 1) (Account: 1) X: -263.477905 Y: 1038.587646 Z: 54.320618 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375387154,2,27,3,'Command: .whispers [Player: Algorithm (Guid: 1) (Account: 1) X: -263.477905 Y: 1038.587646 Z: 54.320618 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375387216,2,27,3,'Command: .npc info [Player: Algorithm (Guid: 1) (Account: 1) X: -268.911591 Y: 1061.487671 Z: 54.312347 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected creature: Teleporter (GUID: 7742830)]\n'),
	(1375388818,2,27,3,'Command: .gm [Player: Algorithm (Guid: 1) (Account: 1) X: -270.498444 Y: 1063.786133 Z: 54.310478 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375388818,2,27,3,'Command: .gm chat [Player: Algorithm (Guid: 1) (Account: 1) X: -270.498444 Y: 1063.786133 Z: 54.310478 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375388818,2,27,3,'Command: .gm visible [Player: Algorithm (Guid: 1) (Account: 1) X: -270.498444 Y: 1063.786133 Z: 54.310478 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375388818,2,27,3,'Command: .dev [Player: Algorithm (Guid: 1) (Account: 1) X: -270.498444 Y: 1063.786133 Z: 54.310478 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n'),
	(1375388818,2,27,3,'Command: .whispers [Player: Algorithm (Guid: 1) (Account: 1) X: -270.498444 Y: 1063.786133 Z: 54.310478 Map: 530 (Outland) Area: 3539 (The Stair of Destiny) Zone: Hellfire Peninsula Selected none:  (GUID: 0)]\n');

/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_account_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_account_groups`;

CREATE TABLE `rbac_account_groups` (
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `groupId` int(10) unsigned NOT NULL COMMENT 'Group id',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`accountId`,`groupId`,`realmId`),
  KEY `fk__rbac_account_groups__rbac_groups` (`groupId`),
  CONSTRAINT `fk__rbac_account_groups__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_groups__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Group relation';

LOCK TABLES `rbac_account_groups` WRITE;
/*!40000 ALTER TABLE `rbac_account_groups` DISABLE KEYS */;

INSERT INTO `rbac_account_groups` (`accountId`, `groupId`, `realmId`)
VALUES
	(1,1,-1),
	(2,1,-1),
	(1,2,-1),
	(2,2,-1),
	(1,3,-1),
	(2,3,-1),
	(1,4,-1),
	(2,4,-1),
	(1,5,-1),
	(2,5,-1),
	(1,6,-1),
	(2,6,-1),
	(1,7,-1),
	(2,7,-1);

/*!40000 ALTER TABLE `rbac_account_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_account_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_account_permissions`;

CREATE TABLE `rbac_account_permissions` (
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `permissionId` int(10) unsigned NOT NULL COMMENT 'Permission id',
  `granted` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Granted = 1, Denied = 0',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`accountId`,`permissionId`,`realmId`),
  KEY `fk__rbac_account_roles__rbac_permissions` (`permissionId`),
  CONSTRAINT `fk__rbac_account_permissions__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_roles__rbac_permissions` FOREIGN KEY (`permissionId`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Permission relation';



# Dump of table rbac_account_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_account_roles`;

CREATE TABLE `rbac_account_roles` (
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  `granted` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Granted = 1, Denied = 0',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`accountId`,`roleId`,`realmId`),
  KEY `fk__rbac_account_roles__rbac_roles` (`roleId`),
  CONSTRAINT `fk__rbac_account_roles__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_roles__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Role relation';



# Dump of table rbac_group_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_group_roles`;

CREATE TABLE `rbac_group_roles` (
  `groupId` int(10) unsigned NOT NULL COMMENT 'group id',
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  PRIMARY KEY (`groupId`,`roleId`),
  KEY `fk__rbac_group_roles__rbac_roles` (`roleId`),
  CONSTRAINT `fk__rbac_group_roles__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_group_roles__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Group Role relation';

LOCK TABLES `rbac_group_roles` WRITE;
/*!40000 ALTER TABLE `rbac_group_roles` DISABLE KEYS */;

INSERT INTO `rbac_group_roles` (`groupId`, `roleId`)
VALUES
	(1,1),
	(2,2),
	(3,3),
	(4,4),
	(2,5),
	(1,6),
	(1,7),
	(2,8),
	(3,8),
	(4,8),
	(2,9),
	(3,9),
	(4,9),
	(2,10),
	(3,10),
	(4,10),
	(2,11),
	(3,11),
	(4,11),
	(2,12),
	(3,12),
	(4,12),
	(2,13),
	(3,13),
	(4,13),
	(2,14),
	(3,14),
	(4,14),
	(2,15),
	(3,15),
	(4,15),
	(2,16),
	(3,16),
	(4,16),
	(2,17),
	(3,17),
	(4,17),
	(4,18),
	(1,19),
	(2,19),
	(3,19),
	(4,19),
	(2,20),
	(3,20),
	(4,20),
	(2,21),
	(3,21),
	(4,21),
	(2,22),
	(3,22),
	(4,22),
	(4,23),
	(2,24),
	(3,24),
	(4,24),
	(2,25),
	(3,25),
	(4,25),
	(2,26),
	(3,26),
	(4,26),
	(2,27),
	(3,27),
	(4,27),
	(1,28),
	(2,28),
	(3,28),
	(4,28),
	(2,29),
	(3,29),
	(4,29),
	(1,30),
	(2,30),
	(3,30),
	(4,30),
	(2,31),
	(2,32),
	(3,32),
	(4,32),
	(2,33),
	(3,33),
	(4,33),
	(1,34),
	(1,35),
	(2,35),
	(3,35),
	(4,35),
	(1,36),
	(2,36),
	(3,36),
	(4,36),
	(2,37),
	(3,37),
	(4,37),
	(2,38),
	(3,38),
	(4,38),
	(4,39),
	(7,50),
	(3,51),
	(6,52),
	(7,53),
	(5,54);

/*!40000 ALTER TABLE `rbac_group_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_groups`;

CREATE TABLE `rbac_groups` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Group id',
  `name` varchar(100) NOT NULL COMMENT 'Group name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Group List';

LOCK TABLES `rbac_groups` WRITE;
/*!40000 ALTER TABLE `rbac_groups` DISABLE KEYS */;

INSERT INTO `rbac_groups` (`id`, `name`)
VALUES
	(1,'Player'),
	(2,'Moderator'),
	(3,'GameMaster'),
	(4,'Administrator'),
	(5,'Developer'),
	(6,'Management'),
	(7,'Executive');

/*!40000 ALTER TABLE `rbac_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_permissions`;

CREATE TABLE `rbac_permissions` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Permission id',
  `name` varchar(100) NOT NULL COMMENT 'Permission name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Permission List';

LOCK TABLES `rbac_permissions` WRITE;
/*!40000 ALTER TABLE `rbac_permissions` DISABLE KEYS */;

INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES
	(1,'Instant logout'),
	(2,'Skip Queue'),
	(3,'Join Normal Battleground'),
	(4,'Join Random Battleground'),
	(5,'Join Arenas'),
	(6,'Join Dungeon Finder'),
	(7,'Player Commands (Temporal till commands moved to rbac)'),
	(8,'Moderator Commands (Temporal till commands moved to rbac)'),
	(9,'GameMaster Commands (Temporal till commands moved to rbac)'),
	(10,'Administrator Commands (Temporal till commands moved to rbac)'),
	(11,'Log GM trades'),
	(13,'Skip Instance required bosses check'),
	(14,'Skip character creation team mask check'),
	(15,'Skip character creation class mask check'),
	(16,'Skip character creation race mask check'),
	(17,'Skip character creation reserved name check'),
	(18,'Skip character creation heroic min level check'),
	(19,'Skip needed requirements to use channel check'),
	(20,'Skip disable map check'),
	(21,'Skip reset talents when used more than allowed check'),
	(22,'Skip spam chat check'),
	(23,'Skip over-speed ping check'),
	(24,'Two side faction characters on the same account'),
	(25,'Allow say chat between factions'),
	(26,'Allow channel chat between factions'),
	(27,'Two side mail interaction'),
	(28,'See two side who list'),
	(29,'Add friends of other faction'),
	(30,'Save character without delay with .save command'),
	(31,'Use params with .unstuck command'),
	(32,'Can be assigned tickets with .assign ticket command'),
	(33,'Notify if a command was not found'),
	(34,'Check if should appear in list using .gm ingame command'),
	(35,'See all security levels with who command'),
	(36,'Filter whispers'),
	(37,'Use staff badge in chat'),
	(38,'Resurrect with full Health Points'),
	(39,'Restore saved gm setting states'),
	(40,'Allows to add a gm to friend list'),
	(41,'Use Config option START_GM_LEVEL to assign new character level'),
	(42,'Allows to use CMSG_WORLD_TELEPORT opcode'),
	(43,'Allows to use CMSG_WHOIS opcode'),
	(44,'Receive global GM messages/texts'),
	(45,'Join channels without announce'),
	(46,'Change channel settings without being channel moderator'),
	(47,'Enables lower security than target check'),
	(48,'Enable IP, Last Login and EMail output in pinfo'),
	(49,'Can administer BuildCraft realm'),
	(50,'Can use GM Panel'),
	(51,'Can modify RBAC permissions'),
	(52,'Can view RBAC permissions'),
	(53,'Can view all GM logs'),
	(54,'Can view reward history'),
	(55,'Can resolve escalated tickets'),
	(56,'Can flag arena teams and games as wintraders'),
	(57,'Can view GM surveys'),
	(58,'Can view account information'),
	(59,'Can view character information'),
	(60,'Can view arena logs'),
	(61,'Can view guild information'),
	(62,'Can reset passwords'),
	(63,'Can process ban appeals'),
	(64,'Can schedule events'),
	(65,'Can edit RBAC roles and groups.'),
	(66,'Can modify player donation / vote points'),
	(67,'Management Commands'),
	(68,'Executive Commands'),
	(69,'Developer Commands');

/*!40000 ALTER TABLE `rbac_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_role_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_role_permissions`;

CREATE TABLE `rbac_role_permissions` (
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  `permissionId` int(10) unsigned NOT NULL COMMENT 'Permission id',
  PRIMARY KEY (`roleId`,`permissionId`),
  KEY `fk__role_permissions__rbac_permissions` (`permissionId`),
  CONSTRAINT `fk__role_permissions__rbac_permissions` FOREIGN KEY (`permissionId`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__role_permissions__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Role Permission relation';

LOCK TABLES `rbac_role_permissions` WRITE;
/*!40000 ALTER TABLE `rbac_role_permissions` DISABLE KEYS */;

INSERT INTO `rbac_role_permissions` (`roleId`, `permissionId`)
VALUES
	(5,1),
	(5,2),
	(6,3),
	(6,4),
	(6,5),
	(7,6),
	(1,7),
	(2,8),
	(3,9),
	(4,10),
	(8,11),
	(9,13),
	(33,14),
	(33,15),
	(33,16),
	(33,17),
	(33,18),
	(27,19),
	(22,20),
	(23,21),
	(24,22),
	(17,23),
	(34,24),
	(28,25),
	(30,26),
	(19,27),
	(35,28),
	(36,29),
	(11,30),
	(12,31),
	(10,32),
	(20,33),
	(14,34),
	(37,35),
	(29,36),
	(15,37),
	(13,38),
	(25,39),
	(38,40),
	(26,41),
	(18,42),
	(18,43),
	(16,44),
	(31,45),
	(32,46),
	(21,47),
	(39,48),
	(50,50),
	(51,50),
	(50,51),
	(50,52),
	(50,53),
	(50,54),
	(51,54),
	(50,55),
	(50,56),
	(51,56),
	(50,57),
	(51,57),
	(50,58),
	(51,58),
	(50,59),
	(51,59),
	(50,60),
	(51,60),
	(50,61),
	(51,61),
	(50,62),
	(50,63),
	(50,64),
	(50,65),
	(50,66),
	(52,67),
	(53,68),
	(54,69);

/*!40000 ALTER TABLE `rbac_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_roles`;

CREATE TABLE `rbac_roles` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Role id',
  `name` varchar(100) NOT NULL COMMENT 'Role name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Roles List';

LOCK TABLES `rbac_roles` WRITE;
/*!40000 ALTER TABLE `rbac_roles` DISABLE KEYS */;

INSERT INTO `rbac_roles` (`id`, `name`)
VALUES
	(1,'Player Commands'),
	(2,'Moderator Commands'),
	(3,'GameMaster Commands'),
	(4,'Administrator Commands'),
	(5,'Quick Login/Logout'),
	(6,'Use Battleground/Arenas'),
	(7,'Use Dungeon Finder'),
	(8,'Log GM trades'),
	(9,'Skip Instance required bosses check'),
	(10,'Ticket management'),
	(11,'Instant .save'),
	(12,'Allow params with .unstuck'),
	(13,'Full HP after resurrect'),
	(14,'Appear in GM ingame list'),
	(15,'Use staff badge in chat'),
	(16,'Receive global GM messages/texts'),
	(17,'Skip over-speed ping check'),
	(18,'Allows Admin Opcodes'),
	(19,'Two side mail interaction'),
	(20,'Notify if a command was not found'),
	(21,'Enables lower security than target check'),
	(22,'Skip disable map check'),
	(23,'Skip reset talents when used more than allowed check'),
	(24,'Skip spam chat check'),
	(25,'Restore saved gm setting states'),
	(26,'Use Config option START_GM_LEVEL to assign new character level'),
	(27,'Skip needed requirements to use channel check'),
	(28,'Allow say chat between factions'),
	(29,'Filter whispers'),
	(30,'Allow channel chat between factions'),
	(31,'Join channels without announce'),
	(32,'Change channel settings without being channel moderator'),
	(33,'Skip character creation checks'),
	(34,'Two side faction characters on the same account'),
	(35,'See two side who list'),
	(36,'Add friends of other faction'),
	(37,'See all security levels with who command'),
	(38,'Allows to add a gm to friend list'),
	(39,'Enable IP, Last Login and EMail output in pinfo'),
	(50,'Full GM Panel Access'),
	(51,'Game Master GM Panel Access'),
	(52,'Management Commands'),
	(53,'Executive Commands'),
	(54,'Developer Commands');

/*!40000 ALTER TABLE `rbac_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rbac_security_level_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rbac_security_level_groups`;

CREATE TABLE `rbac_security_level_groups` (
  `secId` tinyint(3) unsigned NOT NULL COMMENT 'Security Level id',
  `groupId` int(10) unsigned NOT NULL COMMENT 'group id',
  PRIMARY KEY (`secId`,`groupId`),
  KEY `fk__rbac_security_level_groups__rbac_groups` (`groupId`),
  CONSTRAINT `fk__rbac_security_level_groups__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Default groups to assign when an account is set gm level';

LOCK TABLES `rbac_security_level_groups` WRITE;
/*!40000 ALTER TABLE `rbac_security_level_groups` DISABLE KEYS */;

INSERT INTO `rbac_security_level_groups` (`secId`, `groupId`)
VALUES
	(0,1),
	(1,1),
	(2,1),
	(3,1),
	(1,2),
	(2,2),
	(3,2),
	(2,3),
	(3,3),
	(3,4);

/*!40000 ALTER TABLE `rbac_security_level_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table realmcharacters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `realmcharacters`;

CREATE TABLE `realmcharacters` (
  `realmid` int(10) unsigned NOT NULL DEFAULT '0',
  `acctid` int(10) unsigned NOT NULL,
  `numchars` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`realmid`,`acctid`),
  KEY `acctid` (`acctid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Realm Character Tracker';

LOCK TABLES `realmcharacters` WRITE;
/*!40000 ALTER TABLE `realmcharacters` DISABLE KEYS */;

INSERT INTO `realmcharacters` (`realmid`, `acctid`, `numchars`)
VALUES
	(1,1,0),
	(1,2,0),
	(2,1,1);

/*!40000 ALTER TABLE `realmcharacters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table realmlist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `realmlist`;

CREATE TABLE `realmlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '127.0.0.1',
  `localAddress` varchar(255) NOT NULL DEFAULT '127.0.0.1',
  `localSubnetMask` varchar(255) NOT NULL DEFAULT '255.255.255.0',
  `port` smallint(5) unsigned NOT NULL DEFAULT '8085',
  `icon` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `flag` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `timezone` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `allowedSecurityLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `population` float unsigned NOT NULL DEFAULT '0',
  `gamebuild` int(10) unsigned NOT NULL DEFAULT '12340',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Realm System';

LOCK TABLES `realmlist` WRITE;
/*!40000 ALTER TABLE `realmlist` DISABLE KEYS */;

INSERT INTO `realmlist` (`id`, `name`, `address`, `localAddress`, `localSubnetMask`, `port`, `icon`, `flag`, `timezone`, `allowedSecurityLevel`, `population`, `gamebuild`)
VALUES
	(1,'Clan Wars','stronk.cc','127.0.0.1','255.255.255.0',8086,1,0,2,0,0,12340);

/*!40000 ALTER TABLE `realmlist` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table uptime
# ------------------------------------------------------------

DROP TABLE IF EXISTS `uptime`;

CREATE TABLE `uptime` (
  `realmid` int(10) unsigned NOT NULL,
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0',
  `maxplayers` smallint(5) unsigned NOT NULL DEFAULT '0',
  `revision` varchar(255) NOT NULL DEFAULT 'Trinitycore',
  PRIMARY KEY (`realmid`,`starttime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Uptime system';

LOCK TABLES `uptime` WRITE;
/*!40000 ALTER TABLE `uptime` DISABLE KEYS */;

INSERT INTO `uptime` (`realmid`, `starttime`, `uptime`, `maxplayers`, `revision`)
VALUES
	(2,1375389740,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375389782,600,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375390730,600,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375391488,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375391891,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375392461,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375392799,600,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375393524,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375393601,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375393655,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375393692,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375393995,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375394064,600,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375394897,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375395104,600,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375395786,600,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375396643,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375396743,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375396832,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375396934,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375396970,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375397035,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375397175,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375397277,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375397538,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375397617,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375397983,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375398109,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375398383,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375398687,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375398835,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375398891,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375399166,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375399345,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375399498,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375400021,1201,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375401255,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375401366,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375401410,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375401499,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375401542,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375401759,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375401852,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375401948,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375402215,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375402292,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375402364,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375402508,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375402580,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375402627,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375403033,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375403591,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375404196,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375404280,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375404390,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375404569,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375404941,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375405108,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375405219,1200,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375406968,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375407164,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375407625,600,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375408280,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375408348,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375408469,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375408641,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375409014,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375409136,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375409632,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375409912,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375410404,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375410601,5400,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375416267,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375416347,0,0,'TrinityCore rev. c0b19f989b68 2013-07-31 00:40:45 -0700 (master branch) (Unix, Release)'),
	(2,1375416709,1200,0,'TrinityCore rev. 53df02f2c4d4 2013-08-01 21:06:22 -0700 (master branch) (Unix, Release)'),
	(2,1375418095,0,0,'TrinityCore rev. 53df02f2c4d4 2013-08-01 21:06:22 -0700 (master branch) (Unix, Release)'),
	(2,1375418264,0,0,'TrinityCore rev. 53df02f2c4d4 2013-08-01 21:06:22 -0700 (master branch) (Unix, Release)'),
	(2,1375418634,0,0,'TrinityCore rev. 53df02f2c4d4 2013-08-01 21:06:22 -0700 (master branch) (Unix, Release)'),
	(2,1375418744,600,0,'TrinityCore rev. 53df02f2c4d4 2013-08-01 21:06:22 -0700 (master branch) (Unix, Release)'),
	(2,1375419406,600,0,'TrinityCore rev. 53df02f2c4d4 2013-08-01 21:06:22 -0700 (master branch) (Unix, Release)');

/*!40000 ALTER TABLE `uptime` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
